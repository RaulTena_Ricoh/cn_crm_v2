package es.condenast.jsf;

import es.condenast.jpa.entities.CnetlFuente;
import es.condenast.jpa.entities.CnetlFuenteValor;
import es.condenast.jpa.entities.CnetlFuenteValorPK;
import es.condenast.jpa.entities.CnetlOrigen;
import es.condenast.jpa.session.CnetlFuenteFacade;
import es.condenast.jpa.session.CnetlFuenteValorFacade;
import es.condenast.jpa.session.CnetlOrigenFacade;
import es.condenast.jsf.util.JsfUtil;
import es.condenast.jsf.util.PaginationHelper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Named;

@Named("cnetlFuenteController")
@SessionScoped
public class CnetlFuenteController implements Serializable {

    private CnetlFuente current;
    private DataModel items = null;
    @EJB
    private es.condenast.jpa.session.CnetlFuenteFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    @EJB
    private es.condenast.jpa.session.CnetlOrigenFacade ejbOrigenFacade;

    @EJB
    private es.condenast.jpa.session.CnetlFuenteValorFacade ejbFuenteValorFacade;

    // Atributos formulario
    CnetlOrigen valorSeleccionadoBBDD;
    CnetlOrigen[] valoresBBDD;
    CnetlOrigen[] valoresBBDDFuenteTipo;
    List<CnetlFuenteValor> valoresOrigen;
    CnetlFuenteValor valorSeleccionadoOrigen;

    // Estructura interna de origenes
    private HashMap<Integer, CnetlOrigen> origenesHM = new HashMap<>();
    private HashMap<Integer, CnetlOrigen> origenesFuenteTipoHM = new HashMap<>();

    public CnetlFuenteController() {
    }

    public CnetlFuente getSelected() {
        if (current == null) {
            current = new CnetlFuente();
            selectedItemIndex = -1;
        }
        return current;
    }

    private CnetlFuenteFacade getFacade() {
        return ejbFacade;
    }

    public CnetlOrigenFacade getOrigenFacade() {
        return ejbOrigenFacade;
    }

    public CnetlFuenteValorFacade getFuenteValorFacade() {
        return ejbFuenteValorFacade;
    }

    public void setOrigenFacade(CnetlOrigenFacade ejbOrigenFacade) {
        this.ejbOrigenFacade = ejbOrigenFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize() - 1}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (CnetlFuente) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        prepareCreateListas();

        current = new CnetlFuente();
        selectedItemIndex = -1;
        return "Create";
    }

    private void prepareCreateListas() {
        // Inicializar lista
        valoresBBDD = null;
        // Inicializar datatable
        valoresOrigen = new ArrayList<>();
    }

    public String create() {
        try {
            if (!validateFields()) {
                return null;
            }

            getFacade().create(current);
            createFuenteValor();
            return prepareList();
        } catch (Throwable e) {
            String duplicateEntry = JsfUtil.isDuplicateEnty(e);
            if (duplicateEntry != null) {
                if (duplicateEntry.contains("UK_FUENTE_NOMBRE")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("FuenteDatosClaveDuplicada"),
                            new String[]{current.getNombre(), ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("FuenteDatosNombre")});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
            return null;
        }
    }

    private void createFuenteValor() {

        // Valores personalizados
        for (CnetlFuenteValor fvPersonalizados : valoresOrigen) {
            CnetlFuenteValorPK fvPersonalizadosPK = new CnetlFuenteValorPK(current.getIdFuente(), fvPersonalizados.getCnetlOrigen().getIdOrigen());
            fvPersonalizados.setCnetlFuenteValorPK(fvPersonalizadosPK);
            getFuenteValorFacade().create(fvPersonalizados);
        }
    }

    public String prepareEdit() {
        current = (CnetlFuente) getItems().getRowData();
        prepareEditListas();

        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    private void prepareEditListas() {
        // Inicializar datatable
        valoresOrigen = getFuenteValorFacade().findByIdFuente(current.getIdFuente());

        // Lista de Valores de BBDD que no estan en el tipo de fuente
        List<CnetlOrigen> listaOrigenes = getOrigenFacade().findValoresPosibles(false, current.getIdFuenteTipo().getIdFuenteTipo());
        origenesHM = CnetlOrigen.toHashMapId(listaOrigenes);
        for (CnetlFuenteValor valorOrigen : valoresOrigen) {
            listaOrigenes.remove(valorOrigen.getCnetlOrigen());

        }
        Collections.sort(listaOrigenes);

        // Lista de Valores de BBDD que estan en el tipo de fuente
        List<CnetlOrigen> listaOrigenesExistentes = getOrigenFacade().findValoresExistentes(false, current.getIdFuenteTipo().getIdFuenteTipo());
        Collections.sort(listaOrigenesExistentes);
        origenesFuenteTipoHM = CnetlOrigen.toHashMapId(listaOrigenesExistentes);

        listaOrigenesExistentes.toArray(new CnetlOrigen[listaOrigenesExistentes.size()]);
        listaOrigenesExistentes.addAll(listaOrigenes);

        valoresBBDD = listaOrigenesExistentes.toArray(new CnetlOrigen[listaOrigenesExistentes.size()]);

    }

    public String update() {
        try {
            if (!validateFields()) {
                return null;
            }

            getFacade().edit(current);
            performDestroyFuenteValor();
            createFuenteValor();

            return "List";
        } catch (Throwable e) {

            String duplicateEntry = JsfUtil.isDuplicateEnty(e);
            if (duplicateEntry != null) {
                if (duplicateEntry.contains("UK_FUENTE_NOMBRE")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("FuenteDatosClaveDuplicada"),
                            new String[]{current.getNombre(), ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("FuenteDatosNombre")});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
            return null;
        }
    }

    public String destroy() {
        current = (CnetlFuente) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            performDestroyFuenteValor();
            getFacade().remove(current);

        } catch (Exception e) {
            String foreignKey = JsfUtil.isForeignKey(e);

            if (foreignKey != null) {
                if (foreignKey.contains("FK_FUENTE_VALOR_FUENTE")
                        || foreignKey.contains("FK_POOL_FUENTE")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("FuenteDatosForeignKey"),
                            new String[]{current.getNombre()});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
        }
    }

    private void performDestroyFuenteValor() {
        // Valores personalizados
        List<CnetlFuenteValor> valores = getFuenteValorFacade().findByIdFuente(current.getIdFuente());
        for (CnetlFuenteValor fvPersonalizados : valores) {
            CnetlFuenteValorPK fvPersonalizadosPK = new CnetlFuenteValorPK(current.getIdFuente(), fvPersonalizados.getCnetlFuenteValorPK().getIdOrigen());
            fvPersonalizados.setCnetlFuenteValorPK(fvPersonalizadosPK);
            getFuenteValorFacade().remove(fvPersonalizados);
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public String toPage(Integer page) {
        getPagination().toPage(page);
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public CnetlFuente getCnetlFuente(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = CnetlFuente.class)
    public static class CnetlFuenteControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CnetlFuenteController controller = (CnetlFuenteController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "cnetlFuenteController");
            return controller.getCnetlFuente(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof CnetlFuente) {
                CnetlFuente o = (CnetlFuente) object;
                return getStringKey(o.getIdFuente());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + CnetlFuente.class.getName());
            }
        }

    }

    public String insertOrigen() {

        // Añadir en datatable
        CnetlOrigen origenFuente = getValorSeleccionadoBBDD();
        
        if (valoresOrigen == null) {
            valoresOrigen = new ArrayList<>();
        }

        Integer idFuente = Integer.MIN_VALUE;
        if (current.getIdFuente() != null) {
            idFuente = current.getIdFuente();
        }

        CnetlFuenteValorPK fuenteValorPK = new CnetlFuenteValorPK();
        fuenteValorPK.setIdFuente(idFuente);
        fuenteValorPK.setIdOrigen(origenFuente.getIdOrigen());
        CnetlFuenteValor fuenteValor = new CnetlFuenteValor(fuenteValorPK);
        fuenteValor.setCnetlOrigen(origenFuente);
        valoresOrigen.add(fuenteValor);

        // Eliminar de la lista
        ArrayList<CnetlOrigen> listaOrigenes = new ArrayList<>(Arrays.asList(valoresBBDD));
        listaOrigenes.remove(origenFuente);

        // Construir lista resultante
        listaOrigenes.removeAll(origenesFuenteTipoHM.values());
        Collections.sort(listaOrigenes);

        List<CnetlOrigen> listaOrigenesExistentes = new ArrayList<>();
        listaOrigenesExistentes.toArray(new CnetlOrigen[listaOrigenesExistentes.size()]);
        listaOrigenesExistentes.addAll(origenesFuenteTipoHM.values());
        Collections.sort(listaOrigenesExistentes);

        listaOrigenes.addAll(0, listaOrigenesExistentes);

        valoresBBDD = listaOrigenes.toArray(new CnetlOrigen[listaOrigenes.size()]);

        return null;
    }

    public String removeOrigen() {
        // Eliminar del datatable
        valoresOrigen.remove(valorSeleccionadoOrigen);

        // Añadir en lista
        ArrayList<CnetlOrigen> listaOrigenes = new ArrayList<>(Arrays.asList(valoresBBDD));
        listaOrigenes.add(valorSeleccionadoOrigen.getCnetlOrigen());

        // Construir lista resultante
        listaOrigenes.removeAll(origenesFuenteTipoHM.values());
        Collections.sort(listaOrigenes);

        List<CnetlOrigen> listaOrigenesExistentes = new ArrayList<>();
        listaOrigenesExistentes.toArray(new CnetlOrigen[listaOrigenesExistentes.size()]);
        listaOrigenesExistentes.addAll(origenesFuenteTipoHM.values());
        Collections.sort(listaOrigenesExistentes);

        listaOrigenes.addAll(0, listaOrigenesExistentes);

        valoresBBDD = listaOrigenes.toArray(new CnetlOrigen[listaOrigenes.size()]);

        return null;
    }

    public CnetlOrigen[] getValoresBBDD() {
        return valoresBBDD;
    }

    public void setValoresBBDD(CnetlOrigen[] valoresBBDD) {
        this.valoresBBDD = valoresBBDD;
    }

    public CnetlOrigen getValorSeleccionadoBBDD() {
        return valorSeleccionadoBBDD;
    }

    public void setValorSeleccionadoBBDD(CnetlOrigen valorSeleccionadoBBDD) {
        this.valorSeleccionadoBBDD = valorSeleccionadoBBDD;
    }

    public CnetlFuenteValor getValorSeleccionadoOrigen() {
        return valorSeleccionadoOrigen;
    }

    public void setValorSeleccionadoOrigen(CnetlFuenteValor valorSeleccionadoOrigen) {
        this.valorSeleccionadoOrigen = valorSeleccionadoOrigen;
    }

    public List<CnetlFuenteValor> getValoresOrigen() {
        return valoresOrigen;
    }

    public void setValoresOrigen(List<CnetlFuenteValor> valoresOrigen) {
        this.valoresOrigen = valoresOrigen;
    }

    public boolean validateFields() {
        boolean valido = true;
        if (current.getNombre() == null || current.getNombre().length() == 0) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("FuenteDatosNombreObligatorio"));
            valido = false;
        }
        if (current.getOrigen() == null || current.getOrigen().length() == 0) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("FuenteDatosOrigenObligatorio"));
            valido = false;
        }
        if (current.getIdFuenteTipo() == null) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("FuenteDatosFuenteTipoObligatorio"));
            valido = false;
        }

        return valido;
    }

    public void showDescription() {
        FacesContext context = FacesContext.getCurrentInstance();

        String id = context.getExternalContext().getRequestParameterMap().get("param");
        String descripcion = origenesHM.get(Integer.valueOf(id)).getDescripcion();
        context.addMessage(null, new FacesMessage("", descripcion));;
    }

    public boolean isDisabled(CnetlOrigen origen) {
        return origenesFuenteTipoHM.containsKey(origen.getIdOrigen());
    }

    public void changeFuenteTipo() {
        FacesContext context = FacesContext.getCurrentInstance();

        String idTipoFuente = context.getExternalContext().getRequestParameterMap().get("param");

        if (idTipoFuente.length() > 0) {
            // Lista de Valores de BBDD que no estan en el tipo de fuente
            List<CnetlOrigen> listaOrigenes = getOrigenFacade().findValoresPosibles(false, Short.parseShort(idTipoFuente));
            origenesHM = CnetlOrigen.toHashMapId(listaOrigenes);
            Collections.sort(listaOrigenes);

            // Lista de Valores de BBDD que estan en el tipo de fuente
            List<CnetlOrigen> listaOrigenesExistentes = getOrigenFacade().findValoresExistentes(false, Short.parseShort(idTipoFuente));
            Collections.sort(listaOrigenesExistentes);
            origenesFuenteTipoHM = CnetlOrigen.toHashMapId(listaOrigenesExistentes);

            listaOrigenesExistentes.toArray(new CnetlOrigen[listaOrigenesExistentes.size()]);
            listaOrigenesExistentes.addAll(listaOrigenes);

            valoresBBDD = listaOrigenesExistentes.toArray(new CnetlOrigen[listaOrigenesExistentes.size()]);
        } else {
            valoresBBDD = null;
        }

        // Inicializar datatable
        valoresOrigen = new ArrayList<>();
    }
}
