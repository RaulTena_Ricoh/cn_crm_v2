package es.condenast.jsf;

import es.condenast.jpa.entities.CnmasNivelIngreso;
import es.condenast.jpa.session.CnmasNivelIngresoFacade;
import es.condenast.jsf.util.JsfUtil;
import es.condenast.jsf.util.PaginationHelper;
import es.condenast.jsf.util.Range;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Named;

@Named("cnmasNivelIngresoController")
@SessionScoped
public class CnmasNivelIngresoController implements Serializable {

    private CnmasNivelIngreso current;
    private DataModel items = null;
    @EJB
    private es.condenast.jpa.session.CnmasNivelIngresoFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public CnmasNivelIngresoController() {
    }

    public CnmasNivelIngreso getSelected() {
        if (current == null) {
            current = new CnmasNivelIngreso();
            selectedItemIndex = -1;
        }
        return current;
    }

    private CnmasNivelIngresoFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize() - 1}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (CnmasNivelIngreso) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new CnmasNivelIngreso();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            if (!isValidNivelIngreso()) {
                return null;
            }
            getFacade().create(current);
            return prepareList();
        } catch (Exception e) {
            String duplicateEntry = JsfUtil.isDuplicateEnty(e);
            if (duplicateEntry != null) {
                if (duplicateEntry.contains("UK_NIVEL_INGRESO_NOMBRE")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("NivelesIngresoClaveDuplicada"),
                            new String[]{current.getNombre(), ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("NivelesIngresoNombre")});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
            return null;
        }
    }

    public String prepareEdit() {
        current = (CnmasNivelIngreso) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            if (!isValidNivelIngreso()) {
                return null;
            }
            getFacade().edit(current);
            return prepareList();
        } catch (Exception e) {
            String duplicateEntry = JsfUtil.isDuplicateEnty(e);
            if (duplicateEntry != null) {
                if (duplicateEntry.contains("UK_NIVEL_INGRESO_NOMBRE")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("NivelesIngresoClaveDuplicada"),
                            new String[]{current.getNombre(), ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("NivelesIngresoNombre")});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
            return null;
        }
    }

    public String destroy() {
        current = (CnmasNivelIngreso) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);

        } catch (Exception e) {
            String foreignKey = JsfUtil.isForeignKey(e);

            if (foreignKey != null) {
                if (foreignKey.contains("FK_PERSONA_NIVEL_INGRESO")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("NivelesIngresoForeignKey"),
                            new String[]{current.getNombre()});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public String toPage(Integer page) {
        getPagination().toPage(page);
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public CnmasNivelIngreso getCnmasNivelIngreso(java.lang.Short id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = CnmasNivelIngreso.class)
    public static class CnmasNivelIngresoControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CnmasNivelIngresoController controller = (CnmasNivelIngresoController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "cnmasNivelIngresoController");
            return controller.getCnmasNivelIngreso(getKey(value));
        }

        java.lang.Short getKey(String value) {
            java.lang.Short key;
            key = Short.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Short value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof CnmasNivelIngreso) {
                CnmasNivelIngreso o = (CnmasNivelIngreso) object;
                return getStringKey(o.getIdNivelIngreso());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + CnmasNivelIngreso.class.getName());
            }
        }

    }

    private boolean isValidNivelIngreso() {

        Double currentHasta = (current.getHasta() == null) ? Double.MAX_VALUE : current.getHasta().doubleValue();

        Range actual = new Range(current.getDesde().doubleValue(), currentHasta);

        if (!actual.isValidFirst()) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("NivelesIngresoErroneo"),
                    new String[]{ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("NivelesIngresoDesde")});
            return false;

        }

        if (!actual.isValidLast()) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("NivelesIngresoErroneo"),
                    new String[]{ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("NivelesIngresoHasta")});
            return false;

        }

        if (!actual.isValidRange()) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("NivelesIngresoHastaMenorDesde"),
                    new String[]{ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("NivelesIngresoHasta"), current.getHasta().toString(),
                        ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("NivelesIngresoDesde"), current.getDesde().toString()});
            return false;
        }

        // Comprobar solapamiento 
        List<CnmasNivelIngreso> nis = getFacade().findAllOrderByDesde();

        ArrayList<Range> niveles = new ArrayList<>();
        for (CnmasNivelIngreso ni : nis) {
            if (!ni.getIdNivelIngreso().equals(current.getIdNivelIngreso())) {
                Double nih = (ni.getHasta() == null) ? Double.MAX_VALUE : ni.getHasta().doubleValue();
                niveles.add(new Range(ni.getDesde().doubleValue(), nih));
            }
        }

        Range rangoSolapamiento = actual.isOverLappedByOpen(niveles);
        if (rangoSolapamiento != null) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("NivelesIngresoSolapamiento"),
                    new String[]{rangoSolapamiento.getFirst().toString(),
                        rangoSolapamiento.getLast() == Double.MAX_VALUE ? "" : rangoSolapamiento.getLast().toString()});
            return false;
        }

        // Comprobar huecos libres
        niveles.add(actual);
        if (Range.isGapOpen(niveles)) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("NivelesIngresoValoresCubrir"));
            return false;

        }

        return true;
    }

}
