/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jsf;

import es.condenast.ingesta.Proceso;
import es.condenast.ingesta.helper.FileUtils;
import es.condenast.jpa.entities.CnetlFuente;
import es.condenast.jpa.entities.CnetlMaster;
import es.condenast.jpa.session.CnetlFuenteFacade;
import es.condenast.jpa.session.CnetlMasterFacade;
import es.condenast.jpa.session.CnetlPoolFacade;
import es.condenast.jpa.session.CnetlPoolLogWarningFacade;
import es.condenast.jpa.session.CnetlPoolMasterFacade;
import es.condenast.jpa.session.CnetlPoolMasterLogWarningFacade;
import es.condenast.jpa.session.VietlLogFacade;
import es.condenast.jpa.session.VietlLogReglaFacade;
import es.condenast.jpa.session.VietlLogTransformacionFacade;
import es.condenast.jpa.session.VietlLogValidacionFacade;
import es.condenast.jpa.session.VietlLogValidacionLineaFacade;
import es.condenast.jsf.util.JsfUtil;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.servlet.http.Part;
import net.neoremind.sshxcute.core.ConnBean;
import net.neoremind.sshxcute.core.Result;
import net.neoremind.sshxcute.core.SSHExec;
import net.neoremind.sshxcute.exception.TaskExecFailException;
import net.neoremind.sshxcute.task.CustomTask;
import net.neoremind.sshxcute.task.impl.ExecCommand;
import net.neoremind.sshxcute.task.impl.ExecShellScript;
import org.primefaces.context.RequestContext;

@Named("cargaManualController")
@SessionScoped
public class CargaManualController implements Serializable {
    private static final String SSH_HOST = "process.ssh.host";
    private static final String SSH_USER = "process.ssh.user";
    private static final String SSH_PASSWORD = "process.ssh.password";
    private static final String SSH_SHELL_PATH = "process.ssh.shellPath";
    private static final String SSH_SHELL_SCRIPT = "process.ssh.shellScript";
    private Part file;
    private CnetlFuente fuente;
    private CnetlMaster master;

    @EJB
    private es.condenast.jpa.session.CnetlPoolFacade ejbPoolFacade;
    
    @EJB
    private es.condenast.jpa.session.CnetlFuenteFacade ejbFuenteFacade;
    
    @EJB
    private es.condenast.jpa.session.CnetlPoolMasterFacade ejbPoolMasterFacade;

    @EJB
    private es.condenast.jpa.session.CnetlMasterFacade ejbMasterFacade;

    @EJB
    private es.condenast.jpa.session.CnetlPoolLogWarningFacade ejbLogWarningFacade = new CnetlPoolLogWarningFacade();

    @EJB
    private es.condenast.jpa.session.CnetlPoolMasterLogWarningFacade ejbMasterLogWarningFacade = new CnetlPoolMasterLogWarningFacade();
    
    @EJB
    private es.condenast.jpa.session.VietlLogFacade ejbLogFacade = new VietlLogFacade();

    @EJB
    private es.condenast.jpa.session.VietlLogReglaFacade ejbLogReglaFacade = new VietlLogReglaFacade();

    @EJB
    private es.condenast.jpa.session.VietlLogTransformacionFacade ejbLogTransformacionFacade = new VietlLogTransformacionFacade();

    @EJB
    private es.condenast.jpa.session.VietlLogValidacionFacade ejbLogValidacionFacade = new VietlLogValidacionFacade();

    @EJB
    private es.condenast.jpa.session.VietlLogValidacionLineaFacade ejbLogValidacionLineaFacade = new VietlLogValidacionLineaFacade();

    public CargaManualController() {
    }

    public CnetlPoolFacade getPoolFacade() {
        return ejbPoolFacade;
    }

    public void setPoolFacade(CnetlPoolFacade ejbPoolFacade) {
        this.ejbPoolFacade = ejbPoolFacade;
    }

    public CnetlFuenteFacade getFuenteFacade() {
        return ejbFuenteFacade;
    }

    public void setFuenteFacade(CnetlFuenteFacade ejbFuenteFacade) {
        this.ejbFuenteFacade = ejbFuenteFacade;
    }

    public CnetlPoolMasterFacade getPoolMasterFacade() {
        return ejbPoolMasterFacade;
    }

    public void setPoolMasterFacade(CnetlPoolMasterFacade ejbPoolMasterFacade) {
        this.ejbPoolMasterFacade = ejbPoolMasterFacade;
    }

    public CnetlMasterFacade getMasterFacade() {
        return ejbMasterFacade;
    }

    public void setMasterFacade(CnetlMasterFacade ejbMasterFacade) {
        this.ejbMasterFacade = ejbMasterFacade;
    }
    
    public VietlLogFacade getLogFacade() {
        return ejbLogFacade;
    }

    public void setLogFacade(VietlLogFacade ejbLogFacade) {
        this.ejbLogFacade = ejbLogFacade;
    }

    public VietlLogReglaFacade getLogReglaFacade() {
        return ejbLogReglaFacade;
    }

    public void setLogReglaFacade(VietlLogReglaFacade ejbLogReglaFacade) {
        this.ejbLogReglaFacade = ejbLogReglaFacade;
    }

    public VietlLogTransformacionFacade getLogTransformacionFacade() {
        return ejbLogTransformacionFacade;
    }

    public void setLogTransformacionFacade(VietlLogTransformacionFacade ejbLogTransformacionFacade) {
        this.ejbLogTransformacionFacade = ejbLogTransformacionFacade;
    }

    public VietlLogValidacionFacade getLogValidacionFacade() {
        return ejbLogValidacionFacade;
    }

    public void setLogValidacionFacade(VietlLogValidacionFacade ejbLogValidacionFacade) {
        this.ejbLogValidacionFacade = ejbLogValidacionFacade;
    }

    public VietlLogValidacionLineaFacade getLogValidacionLineaFacade() {
        return ejbLogValidacionLineaFacade;
    }

    public void setLogValidacionLineaFacade(VietlLogValidacionLineaFacade ejbLogValidacionLineaFacade) {
        this.ejbLogValidacionLineaFacade = ejbLogValidacionLineaFacade;
    }

    public CnetlPoolLogWarningFacade getLogWarningFacade() {
        return ejbLogWarningFacade;
    }

    public void setLogWarningFacade(CnetlPoolLogWarningFacade ejbLogWarningFacade) {
        this.ejbLogWarningFacade = ejbLogWarningFacade;
    }
    
    public CnetlPoolMasterLogWarningFacade getMasterLogWarningFacade() {
        return ejbMasterLogWarningFacade;
    }

    public void setMasterLogWarningFacade(CnetlPoolMasterLogWarningFacade ejbMasterLogWarningFacade) {
        this.ejbMasterLogWarningFacade = ejbMasterLogWarningFacade;
    }

    public String prepareCargaManual() {
        return "CargaManual";
    }

    public String procesar() throws IOException {

        if(file == null){
            String msg = ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("CargaManualRutaObligatoria");          
            JsfUtil.addErrorMessage(msg);
            return "";
        }
        if(fuente == null){
            String msg = ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("CargaManualFuentesObligatoria");          
            JsfUtil.addErrorMessage(msg);
            return "";
        }
        InputStreamReader isr = new InputStreamReader(file.getInputStream(), "UTF-8");
        String nombreFichero = FileUtils.TMP_DIR + FileUtils.FILE_SEPARATOR + getFilename(file);
        nombreFichero = nombreFichero.replaceAll("/", "////");

        Writer out = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(nombreFichero), "UTF-8"));
        try {
            out.write(readFully(file.getInputStream(),"UTF-8"));
        } finally {
            out.close();
        }

        return procesarFichero(nombreFichero);

    }

    public String procesarMaster() throws IOException {

        if(file == null){
            String msg = ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("CargaManualRutaObligatoria");          
            JsfUtil.addErrorMessage(msg);
            return "";
        }
        if(master == null){
            String msg = ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("CargaManualFuentesObligatoria");          
            JsfUtil.addErrorMessage(msg);
            return "";
        }
        InputStreamReader isr = new InputStreamReader(file.getInputStream(), "UTF-8");
        String nombreFichero = FileUtils.TMP_DIR + FileUtils.FILE_SEPARATOR + getFilename(file);
        nombreFichero = nombreFichero.replace("\\", "\\\\");
        
        Writer out = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(nombreFichero), "UTF-8"));
        try {
            out.write(readFully(file.getInputStream(),"UTF-8"));
        } finally {
            out.close();
        }

        return procesarFicheroMaster(nombreFichero);

    }
    
    public String readFully(InputStream inputStream, String encoding)
            throws IOException {
        return new String(readFully(inputStream), encoding);
    }

    private byte[] readFully(InputStream inputStream)
            throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length = 0;
        while ((length = inputStream.read(buffer)) != -1) {
            baos.write(buffer, 0, length);
        }
        return baos.toByteArray();
    }

    private static String getFilename(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE fix.  
            }
        }
        return null;
    }

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
    }

    public String procesarFichero(String ruta) {

        Proceso.setPoolFacade(getPoolFacade());
        Proceso.setLogFacade(getLogFacade());
        Proceso.setLogWarningFacade(getLogWarningFacade());
        Proceso.setLogReglaFacade(getLogReglaFacade());
        Proceso.setLogTransformacionFacade(getLogTransformacionFacade());
        Proceso.setLogValidacionLineaFacade(getLogValidacionLineaFacade());
        Proceso.setLogValidacionFacade(getLogValidacionFacade());
        Proceso.ingestaManual(fuente, ruta);

        if (Proceso.getResultado() == Proceso.PROCESO_ERROR) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("CargaManualErrorEnCarga"));
            return null;
        } else if (Proceso.getResultado() == Proceso.PROCESO_ERROR_CORREO) {
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("resources/Messages").getString("CargaManualErrorEnCargaCorreo"));
            return null;
        } else {
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("resources/Messages").getString("CargaManualCargaRealizada"));
            return null;
        }
    }
    
    public String procesarFicheroMaster(String ruta) {

        Proceso.setPoolMasterFacade(getPoolMasterFacade());
        Proceso.setMasterFacade(getMasterFacade());
        Proceso.setMasterLogWarningFacade(getMasterLogWarningFacade());
        Proceso.cargaMaster(master, ruta);

        if (Proceso.getResultado() == Proceso.PROCESO_ERROR) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("CargaManualErrorEnCarga"));
            return null;
        } else if (Proceso.getResultado() == Proceso.PROCESO_ERROR_CORREO) {
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("resources/Messages").getString("CargaManualErrorEnCargaCorreo"));
            return null;
        } else {
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("resources/Messages").getString("CargaManualCargaRealizada"));
            return null;
        }
    }
    
    public void procesarTodo() {
        // Initialize a SSHExec instance without referring any object. 
        SSHExec ssh = null;
        // Wrap the whole execution jobs into try-catch block   
        try{
            String host = ResourceBundle.getBundle("resources/Configuration").getString(SSH_HOST);
            String user = ResourceBundle.getBundle("resources/Configuration").getString(SSH_USER);
            String password = ResourceBundle.getBundle("resources/Configuration").getString(SSH_PASSWORD);
            String shellPath = ResourceBundle.getBundle("resources/Configuration").getString(SSH_SHELL_PATH);
            String shellScript = ResourceBundle.getBundle("resources/Configuration").getString(SSH_SHELL_SCRIPT);
            // Initialize a ConnBean object, parameter list is ip, username, password
            ConnBean connBean = new ConnBean(host, user, password);
            // Put the ConnBean instance as parameter for SSHExec static method getInstance(ConnBean) to retrieve a real SSHExec instance
            ssh = SSHExec.getInstance(connBean);
            
            CustomTask ct0 = new ExecCommand("cd /home/ricoh/Proceso");
            // Create a ExecCommand, the reference class must be CustomTask
            CustomTask ct = new ExecShellScript(shellPath, shellScript, "");
            // Connect to server
            ssh.connect();
            // Upload sshxcute_test.sh to /home/tsadmin on remote system
            //ssh.uploadSingleDataToServer("data/sshxcute_test.sh", "/home/tsadmin");
            // Execute task and get the returned Result object
            ssh.exec(ct0);
            Result res = ssh.exec(ct);
            // Check result and print out messages.
            if (res.isSuccess)
            {
                Logger.getLogger(CargaManualController.class.getName()).log(Level.INFO, null, "Return code: " + res.rc);
                Logger.getLogger(CargaManualController.class.getName()).log(Level.INFO, null, "sysout: " + res.sysout);
            }
            else
            {
                Logger.getLogger(CargaManualController.class.getName()).log(Level.WARNING, null, "Return code: " + res.rc);
                Logger.getLogger(CargaManualController.class.getName()).log(Level.WARNING, null, "error message: " + res.error_msg);
            }
            ssh.disconnect();
        }catch(TaskExecFailException tef){
            Logger.getLogger(CargaManualController.class.getName()).log(Level.SEVERE, null, tef);
        }catch (Exception ex) {
            Logger.getLogger(CargaManualController.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('waitPanel').hide();");
        }
    
    }
    // Desactivar usuario por e-mail y marca
    public String informarProcesarTodo() {
        SSHExec ssh = null;
        try {
            String host = ResourceBundle.getBundle("resources/Configuration").getString(SSH_HOST);
            String user = ResourceBundle.getBundle("resources/Configuration").getString(SSH_USER);
            String password = ResourceBundle.getBundle("resources/Configuration").getString(SSH_PASSWORD);
            // Initialize a ConnBean object, parameter list is ip, username, password
            ConnBean connBean = new ConnBean(host, user, password);
            // Put the ConnBean instance as parameter for SSHExec static method getInstance(ConnBean) to retrieve a real SSHExec instance
            ssh = SSHExec.getInstance(connBean);
            RequestContext context = RequestContext.getCurrentInstance();
            if(!ssh.connect()){
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("ErrorConexionHost"));
            }
            else{
                context.execute("PF('waitPanel').hide();PF('dialogProcesarTodo').show()");
                ssh.disconnect();
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("ErrorConexionHost"));
        }
        return "";
    }


    public CnetlFuente getFuente() {
        return fuente;
    }

    public void setFuente(CnetlFuente fuente) {
        this.fuente = fuente;
    }

    public CnetlMaster getMaster() {
        return master;
    }

    public void setMaster(CnetlMaster master) {
        this.master = master;
    }

}
