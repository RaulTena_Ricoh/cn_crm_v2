package es.condenast.jsf;

import es.condenast.jpa.entities.CnmasMarcaAfin;
import es.condenast.jsf.util.JsfUtil;
import es.condenast.jsf.util.PaginationHelper;
import es.condenast.jpa.session.CnmasMarcaAfinFacade;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@Named("cnmasMarcaAfinController")
@SessionScoped
public class CnmasMarcaAfinController implements Serializable {

    private CnmasMarcaAfin current;
    private DataModel items = null;
    @EJB
    private es.condenast.jpa.session.CnmasMarcaAfinFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public CnmasMarcaAfinController() {
    }

    public CnmasMarcaAfin getSelected() {
        if (current == null) {
            current = new CnmasMarcaAfin();
            selectedItemIndex = -1;
        }
        return current;
    }

    private CnmasMarcaAfinFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize() - 1}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (CnmasMarcaAfin) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new CnmasMarcaAfin();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            return prepareList();
        } catch (Exception e) {
            String duplicateEntry = JsfUtil.isDuplicateEnty(e);
            if (duplicateEntry != null) {
                if (duplicateEntry.contains("UK_MARCA_AFIN_NOMBRE")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("MarcasAfinesClaveDuplicada"),
                            new String[]{current.getNombre(), ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("MarcasAfinesNombre")});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
            return null;
        }
    }

    public String prepareEdit() {
        current = (CnmasMarcaAfin) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            return prepareList();
        } catch (Exception e) {
            String duplicateEntry = JsfUtil.isDuplicateEnty(e);
            if (duplicateEntry != null) {
                if (duplicateEntry.contains("UK_MARCA_AFIN_NOMBRE")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("MarcasAfinesClaveDuplicada"),
                            new String[]{current.getNombre(), ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("MarcasAfinesNombre")});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
            return null;
        }
    }

    public String destroy() {
        current = (CnmasMarcaAfin) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);

        } catch (Exception e) {
            String foreignKey = JsfUtil.isForeignKey(e);

            if (foreignKey != null) {
                if (foreignKey.contains("FK_USUARIO_MARCA_AFIN_MARCA_AFIN")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("MarcasAfinesForeignKey"),
                            new String[]{current.getNombre()});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resoirces/Bundle5").getString("PersistenceErrorOccured"));
            }
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public String toPage(Integer page) {
        getPagination().toPage(page);
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public CnmasMarcaAfin getCnmasMarcaAfin(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = CnmasMarcaAfin.class)
    public static class CnmasMarcaAfinControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CnmasMarcaAfinController controller = (CnmasMarcaAfinController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "cnmasMarcaAfinController");
            return controller.getCnmasMarcaAfin(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof CnmasMarcaAfin) {
                CnmasMarcaAfin o = (CnmasMarcaAfin) object;
                return getStringKey(o.getIdMarcaAfin());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + CnmasMarcaAfin.class.getName());
            }
        }

    }

}
