package es.condenast.jsf;

import es.condenast.jpa.entities.CnetlFuenteTipoOrigen;
import es.condenast.jsf.util.JsfUtil;
import es.condenast.jsf.util.PaginationHelper;
import es.condenast.jpa.session.CnetlFuenteTipoOrigenFacade;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@Named("cnetlFuenteTipoOrigenController")
@SessionScoped
public class CnetlFuenteTipoOrigenController implements Serializable {

    private CnetlFuenteTipoOrigen current;
    private DataModel items = null;
    @EJB
    private es.condenast.jpa.session.CnetlFuenteTipoOrigenFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public CnetlFuenteTipoOrigenController() {
    }

    public CnetlFuenteTipoOrigen getSelected() {
        if (current == null) {
            current = new CnetlFuenteTipoOrigen();
            current.setCnetlFuenteTipoOrigenPK(new es.condenast.jpa.entities.CnetlFuenteTipoOrigenPK());
            selectedItemIndex = -1;
        }
        return current;
    }

    private CnetlFuenteTipoOrigenFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize() - 1}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (CnetlFuenteTipoOrigen) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new CnetlFuenteTipoOrigen();
        current.setCnetlFuenteTipoOrigenPK(new es.condenast.jpa.entities.CnetlFuenteTipoOrigenPK());
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            current.getCnetlFuenteTipoOrigenPK().setIdOrigen(current.getCnetlOrigen().getIdOrigen());
            current.getCnetlFuenteTipoOrigenPK().setIdFuenteTipo(current.getCnetlFuenteTipo().getIdFuenteTipo());
            getFacade().create(current);

            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (CnetlFuenteTipoOrigen) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            current.getCnetlFuenteTipoOrigenPK().setIdOrigen(current.getCnetlOrigen().getIdOrigen());
            current.getCnetlFuenteTipoOrigenPK().setIdFuenteTipo(current.getCnetlFuenteTipo().getIdFuenteTipo());
            getFacade().edit(current);
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (CnetlFuenteTipoOrigen) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public CnetlFuenteTipoOrigen getCnetlFuenteTipoOrigen(es.condenast.jpa.entities.CnetlFuenteTipoOrigenPK id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = CnetlFuenteTipoOrigen.class)
    public static class CnetlFuenteTipoOrigenControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CnetlFuenteTipoOrigenController controller = (CnetlFuenteTipoOrigenController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "cnetlFuenteTipoOrigenController");
            return controller.getCnetlFuenteTipoOrigen(getKey(value));
        }

        es.condenast.jpa.entities.CnetlFuenteTipoOrigenPK getKey(String value) {
            es.condenast.jpa.entities.CnetlFuenteTipoOrigenPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new es.condenast.jpa.entities.CnetlFuenteTipoOrigenPK();
            key.setIdFuenteTipo(Short.parseShort(values[0]));
            key.setIdOrigen(Integer.parseInt(values[1]));
            return key;
        }

        String getStringKey(es.condenast.jpa.entities.CnetlFuenteTipoOrigenPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getIdFuenteTipo());
            sb.append(SEPARATOR);
            sb.append(value.getIdOrigen());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof CnetlFuenteTipoOrigen) {
                CnetlFuenteTipoOrigen o = (CnetlFuenteTipoOrigen) object;
                return getStringKey(o.getCnetlFuenteTipoOrigenPK());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + CnetlFuenteTipoOrigen.class.getName());
            }
        }

    }

}
