/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jsf.util;

import es.condenast.jpa.entities.CnetlOrigen;
import java.util.List;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;

@FacesConverter("origenPickListConverter")
public class OrigenPickListConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return getObjectFromUIPickListComponent(component, value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        String string;
        if (value == null) {
            string = "";
        } else {
            try {
                string = String.valueOf(((CnetlOrigen) value).getNombre());
            } catch (ClassCastException cce) {
                throw new ConverterException("Error en conversion");
            }
        }
        return string;
    }

    @SuppressWarnings("unchecked")
    private CnetlOrigen getObjectFromUIPickListComponent(UIComponent component, String value) {
        final DualListModel<CnetlOrigen> dualList;
        try {
            dualList = (DualListModel<CnetlOrigen>) ((PickList) component).getValue();
            CnetlOrigen origen = getObjectFromList(dualList.getSource(), value);
            if (origen == null) {
                origen = getObjectFromList(dualList.getTarget(), value);
            }

            return origen;
        } catch (ClassCastException | NumberFormatException cce) {
            throw new ConverterException("Error en conversion");
        }
    }

    private CnetlOrigen getObjectFromList(final List<?> list, final String identifier) {
        for (final Object object : list) {
            final CnetlOrigen origen = (CnetlOrigen) object;
            if (origen.getNombre().equals(identifier)) {
                return origen;
            }
        }
        return null;
    }

}
