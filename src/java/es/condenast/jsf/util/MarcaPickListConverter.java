/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jsf.util;

import es.condenast.jpa.entities.CnmasMarca;
import java.util.List;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;

@FacesConverter("marcaPickListConverter")
public class MarcaPickListConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return getObjectFromUIPickListComponent(component, value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        String string;
        if (value == null) {
            string = "";
        } else {
            try {
                string = String.valueOf(((CnmasMarca) value).getNombre());
            } catch (ClassCastException cce) {
                throw new ConverterException("Error en conversion");
            }
        }
        return string;
    }

    @SuppressWarnings("unchecked")
    private CnmasMarca getObjectFromUIPickListComponent(UIComponent component, String value) {
        final DualListModel<CnmasMarca> dualList;
        try {
            dualList = (DualListModel<CnmasMarca>) ((PickList) component).getValue();
            CnmasMarca marca = getObjectFromList(dualList.getSource(), value);
            if (marca == null) {
                marca = getObjectFromList(dualList.getTarget(), value);
            }

            return marca;
        } catch (ClassCastException | NumberFormatException cce) {
            throw new ConverterException("Error en conversion");
        }
    }

    private CnmasMarca getObjectFromList(final List<?> list, final String identifier) {
        for (final Object object : list) {
            final CnmasMarca marca = (CnmasMarca) object;
            if (marca.getNombre().equals(identifier)) {
                return marca;
            }
        }
        return null;
    }

}
