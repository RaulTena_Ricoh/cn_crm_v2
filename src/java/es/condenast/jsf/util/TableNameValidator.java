/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jsf.util;

import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("tableNameValidator")
public class TableNameValidator implements Validator {

    private static final String PATTERN_NOMBRE_TABLA =  "[a-zA-Z_]+";
    
    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String nombreTabla = value.toString();
        Pattern pat = Pattern.compile(PATTERN_NOMBRE_TABLA);
        Matcher mat = pat.matcher(nombreTabla);
      
        if (!mat.matches()) {
            String msg = ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TipoFuenteDatosNombreTablaErroneo");
            FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
            throw new ValidatorException(facesMsg);
        }
            
    }
    
}
