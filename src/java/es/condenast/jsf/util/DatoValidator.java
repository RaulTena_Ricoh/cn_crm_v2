/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jsf.util;

import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author Javier.Castro
 */
@FacesValidator("datoValidator")
public class DatoValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String valor = value.toString();
        String regExp = (String) component.getAttributes().get("expReg");
        String campoBBDD = (String) component.getAttributes().get("campoBBDD");

        Pattern pat = Pattern.compile(regExp);
        Matcher mat = pat.matcher(valor);

        if (!mat.matches()) {
            String msg = ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("FuenteDatosErrorDato").replace("%1",campoBBDD);
            FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
            throw new ValidatorException(facesMsg);
        }

    }

}
