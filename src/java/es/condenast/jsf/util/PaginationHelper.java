package es.condenast.jsf.util;

import java.util.ArrayList;
import javax.faces.model.DataModel;

public abstract class PaginationHelper {

    private int pageSize;
    private int page;
    private ArrayList<Integer> pages;

    public PaginationHelper(int pageSize) {
        this.pageSize = pageSize;
    }

    public abstract int getItemsCount();

    public abstract DataModel createPageDataModel();

    public int getPageFirstItem() {
        return page * pageSize;
    }

    public int getPageLastItem() {
        int i = getPageFirstItem() + pageSize - 1;
        int count = getItemsCount() - 1;
        if (i > count) {
            i = count;
        }
        if (i < 0) {
            i = 0;
        }
        return i;
    }

    public boolean isHasNextPage() {
        return (page + 1) * pageSize + 1 <= getItemsCount();
    }

    public void nextPage() {
        if (isHasNextPage()) {
            page++;
        }
    }

    public boolean isHasPreviousPage() {
        return page > 0;
    }

    public void previousPage() {
        if (isHasPreviousPage()) {
            page--;
        }
    }

    public void toPage(Integer toPage) {
        page = toPage - 1;
    }

    public int getPageSize() {
        return pageSize;
    }

    public ArrayList<Integer> getPages() {
        double numPages = Math.ceil((double) getItemsCount() / (double) getPageSize());

        Integer firstPage = 1;
        Integer lastPage = (numPages > 10) ? 10 : (int) numPages;

        // Calcular primera pagina
        if (page + 1 > 6) {
            if (page + 5 < numPages) {
                firstPage = page + 1 - 5;
            } else {
                firstPage = (int) numPages - 10 + 1;
            }
        }

        // Calcular ultima pagina
        if (page + 1 > 6) {
            if (page + 5 < numPages) {
                lastPage = page + 5;
            } else {
                lastPage = (int) numPages;
            }
        }

        pages = new ArrayList<>();
        for (Integer i = firstPage; i <= lastPage; i++) {
            pages.add(i);
        }
        return pages;
    }

    public boolean isActivePage(Integer page) {
        return (page.equals(new Integer(getPageFirstItem() / getPageSize()) + 1));
    }

}
