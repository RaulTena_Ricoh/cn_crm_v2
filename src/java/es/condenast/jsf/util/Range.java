/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jsf.util;

import static java.lang.Double.compare;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

public class Range implements Comparable<Range> {

    private Double first;
    private Double last;

    public Range(Double first, Double last) {
        this.first = first;
        this.last = last;
        if (last == null) {
            this.last = Double.MAX_VALUE;
        }

    }

    public Double getFirst() {
        return first;
    }

    public void setFirst(Double first) {
        this.first = first;
    }

    public Double getLast() {
        return last;
    }

    public void setLast(Double last) {
        this.last = last;
    }

    // Comprobar si el rango es valido
    public boolean isValidRange() {
        if (this.last != null) {
            return (this.last >= this.getFirst());
        } else {
            return isValidFirst();
        }
    }

    public boolean isValidFirst() {
        return (this.getFirst() != null && this.getFirst() >= 0);
    }

    public boolean isValidLast() {
        return (this.last == null || this.last >= 0);
    }

    public boolean isOverlappedByClosed(Range r) {
        return r.containsClosed(this.getFirst())
                || r.containsClosed(this.getLast())
                || containsClosed(r.getFirst());

    }

    public Range isOverLappedByClosed(ArrayList<Range> rangos) {
        for (Range rango : rangos) {
            System.err.println("Rango (" + rango.getFirst() + "," + rango.getLast() + ")");
            if (isOverlappedByClosed(rango)) {
                return rango;
            }
        }
        return null;
    }

    public boolean isOverlappedByOpen(Range r) {
        return r.containsOpen(this.getFirst())
                || r.containsOpen(this.getLast())
                || containsOpen(r.getFirst());

    }

    public Range isOverLappedByOpen(ArrayList<Range> rangos) {
        for (Range rango : rangos) {
            if (isOverlappedByOpen(rango)) {
                return rango;
            }
        }
        return null;
    }

    // Compara rangos con el segundo valor cerrado
    public boolean containsClosed(Double element) {
        return compare(element, this.getFirst()) > -1
                && compare(element, this.getLast()) < 1;
    }

    // Compara rangos con el segundo valor abierto
    public boolean containsOpen(Double element) {
        return compare(element, this.getFirst()) > 0
                && compare(element, this.getLast()) < 0;
    }

    // Valores sin cubrir
    public static boolean isGapOpen(ArrayList<Range> ranges) {
        Collections.sort(ranges);
        int index = 0;
        while (index < ranges.size() - 1) {
            if (ranges.get(index).getFirst().equals(0.0) && ranges.get(index).getLast().equals(0.0)) {

            } else {
                if (!Objects.equals(ranges.get(index).getLast(), ranges.get(index + 1).getFirst())) {
                    return true;
                }
            }
            index++;
        }
        return false;
    }

    // Valores sin cubrir
    public static boolean isGapClosed(ArrayList<Range> ranges) {
        Collections.sort(ranges);
        int index = 0;
        while (index < ranges.size() - 1) {
            if (ranges.get(index).getFirst().equals(0.0) && ranges.get(index).getLast().equals(0.0)) {

            } else {
                if (!Objects.equals(ranges.get(index).getLast() + 1.0, ranges.get(index + 1).getFirst())) {
                    System.err.println("Entro en este " + ranges.get(index).getLast() + 1.0 + " --- " + ranges.get(index + 1).getFirst());
                    return true;
                }
            }
            index++;
        }
        return false;
    }

    @Override
    public int compareTo(Range o) {
        return this.getFirst().compareTo(o.getLast());
    }

}
