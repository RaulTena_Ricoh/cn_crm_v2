/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jsf;

import es.condenast.jpa.entities.CnmasParticipacionTipo;
import es.condenast.jpa.session.CnmasParticipacionTipoFacade;
import es.condenast.jsf.util.JsfUtil;
import es.condenast.jsf.util.PaginationHelper;
import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Named;

/**
 *
 * @author ivan.fernandez
 */
@Named("cnmasParticipacionTipoController")
@SessionScoped
public class CnmasParticipacionTipoController implements Serializable{
    private CnmasParticipacionTipo current;
    private DataModel items = null;
    @EJB
    private CnmasParticipacionTipoFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    
    public CnmasParticipacionTipoController(){
    }
    
    public CnmasParticipacionTipo getSelected(){
        if (current == null) {
            current = new CnmasParticipacionTipo();
            selectedItemIndex = -1;
        }
        return current;
    }
    
    private CnmasParticipacionTipoFacade getFacade(){
        return ejbFacade;
    }
    
    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize() - 1}));
                }
            };
        }
        return pagination;
    }
    
    public String prepareList() {
        recreateModel();
        return "List";
    }
    
    public String prepareView() {
        current = (CnmasParticipacionTipo) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new CnmasParticipacionTipo();
        selectedItemIndex = -1;
        return "Create";
    }
    
    public String create() {
        try {
            current.setIdParticipacionTipo(ejbFacade.getMaxId());
            getFacade().create(current);
            return prepareList();
        } catch (Exception e) {
            String duplicateEntry = JsfUtil.isDuplicateEnty(e);
            if (duplicateEntry != null) {
                if (duplicateEntry.contains("UK_PARTICIPACION_TIPO_NOMBRE")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("ParticipacionTipoClaveDuplicada"),
                            new String[]{current.getNombre(), ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("ParticipacionTipoNombre")});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
            return null;
        }
    }

    public String prepareEdit() {
        current = (CnmasParticipacionTipo) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            return prepareList();
        } catch (Exception e) {
            String duplicateEntry = JsfUtil.isDuplicateEnty(e);
            if (duplicateEntry != null) {
                if (duplicateEntry.contains("UK_PARTICIPACION_TIPO_NOMBRE")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("ParticipacionTipoClaveDuplicada"),
                            new String[]{current.getNombre(), ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("ParticipacionTipoNombre")});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
            return null;
        }
    }

    public String destroy() {
        current = (CnmasParticipacionTipo) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);

        } catch (Exception e) {
            String foreignKey = JsfUtil.isForeignKey(e);

            if (foreignKey != null) {
                if (foreignKey.contains("FK_PARTICIPACION_PARTICIPACION_TIPO")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("ParticipacionTipoForeignKey"),
                            new String[]{current.getNombre()});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    
    private void recreateModel() {
        items = null;
    }
    
    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public String toPage(Integer page) {
        getPagination().toPage(page);
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public CnmasParticipacionTipo getCnmasParticipacionTipo(java.lang.Short id) {
        return ejbFacade.find(id);
    }
    
    @FacesConverter(forClass = CnmasParticipacionTipo.class)
    public static class CnmasParticipacionTipoControllerConverter implements Converter {

        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CnmasParticipacionTipoController controller = (CnmasParticipacionTipoController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "cnmasParticipacionTipoController");
            return controller.getCnmasParticipacionTipo(getKey(value));
        }

        java.lang.Short getKey(String value) {
            java.lang.Short key;
            key = Short.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Short value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof CnmasParticipacionTipo) {
                CnmasParticipacionTipo o = (CnmasParticipacionTipo) object;
                return getStringKey(o.getIdParticipacionTipo());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + CnmasParticipacionTipo.class.getName());
            }
        }
    }

}
