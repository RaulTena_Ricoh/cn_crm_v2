package es.condenast.jsf;

import es.condenast.ingesta.entities.Log;
import es.condenast.ingesta.entities.LogRegla;
import es.condenast.ingesta.entities.LogTransformacion;
import es.condenast.ingesta.entities.LogValidacion;
import es.condenast.ingesta.entities.LogValidacionDetalle;
import es.condenast.ingesta.entities.LogWarning;
import es.condenast.ingesta.helper.CSVUtils;
import es.condenast.ingesta.helper.TimeUtils;
import es.condenast.jpa.entities.CnetlEstado;
import es.condenast.jpa.entities.CnetlFuente;
import es.condenast.jpa.entities.CnetlFuenteTipo;
import es.condenast.jpa.entities.CnetlPoolLog;
import es.condenast.jpa.entities.CnetlPoolLogWarning;
import es.condenast.jpa.entities.VietlLog;
import es.condenast.jpa.entities.VietlLogRegla;
import es.condenast.jpa.entities.VietlLogTransformacion;
import es.condenast.jpa.entities.VietlLogValidacion;
import es.condenast.jpa.entities.VietlLogValidacionLinea;
import es.condenast.jpa.session.CnetlEstadoFacade;
import es.condenast.jpa.session.CnetlFuenteFacade;
import es.condenast.jpa.session.CnetlFuenteTipoFacade;
import es.condenast.jpa.session.CnetlPoolLogFacade;
import es.condenast.jpa.session.CnetlPoolLogWarningFacade;
import es.condenast.jsf.util.JsfUtil;
import es.condenast.jsf.util.PaginationHelper;
import es.condenast.jpa.session.VietlLogFacade;
import es.condenast.jpa.session.VietlLogReglaFacade;
import es.condenast.jpa.session.VietlLogTransformacionFacade;
import es.condenast.jpa.session.VietlLogValidacionFacade;
import es.condenast.jpa.session.VietlLogValidacionLineaFacade;
import java.io.IOException;
import java.io.OutputStream;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;

@Named("vietlLogAdminController")
@SessionScoped
public class VietlLogAdminController implements Serializable {
    private static final String CSV_SEPARADOR = ";";
    private VietlLog current;
    private DataModel items = null;
    @EJB
    private VietlLogFacade ejbFacade;
    @EJB
    private CnetlFuenteFacade ejbCnetlFuente;
    @EJB
    private CnetlFuenteTipoFacade ejbCnetlFuenteTipo;
    @EJB
    private CnetlEstadoFacade ejbCnetlEstado;
    @EJB
    private CnetlPoolLogFacade ejbCnetlPoolLogFacade;
    @EJB
    private VietlLogReglaFacade ejbLogReglaFacade;
    @EJB
    private VietlLogTransformacionFacade ejbLogTransformacionFacade;
    @EJB
    private VietlLogValidacionFacade ejbLogValidacionFacade;
    @EJB
    private VietlLogValidacionLineaFacade ejbLogValidacionLineaFacade;
    @EJB
    private CnetlPoolLogWarningFacade ejbLogWarningFacade;
    
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private Date fechaDesde;
    private Date fechaHasta;
    private CnetlFuente fuente;
    private CnetlFuenteTipo fuenteTipo;
    private CnetlEstado estado;
    
    public VietlLogAdminController() {
    }

    public VietlLog getSelected() {
        if (current == null) {
            current = new VietlLog();
            selectedItemIndex = -1;
        }
        return current;
    }

    private VietlLogFacade getFacade() {
        return ejbFacade;
    }

    /**
     * @return the ejbCnetlFuente
     */
    public CnetlFuenteFacade getEjbCnetlFuente() {
        return ejbCnetlFuente;
    }

    /**
     * @return the ejbCnetlFuenteTipo
     */
    public CnetlFuenteTipoFacade getEjbCnetlFuenteTipo() {
        return ejbCnetlFuenteTipo;
    }

    /**
     * @return the ejbCnetlEstado
     */
    public CnetlEstadoFacade getEjbCnetlEstado() {
        return ejbCnetlEstado;
    }

    /**
     * @return the ejbCnetlPoolLogFacade
     */
    public CnetlPoolLogFacade getEjbCnetlPoolLogFacade() {
        return ejbCnetlPoolLogFacade;
    }

    public VietlLogReglaFacade getEjbLogReglaFacade() {
        return ejbLogReglaFacade;
    }

    public VietlLogTransformacionFacade getEjbLogTransformacionFacade() {
        return ejbLogTransformacionFacade;
    }

    public VietlLogValidacionFacade getEjbLogValidacionFacade() {
        return ejbLogValidacionFacade;
    }

    public VietlLogValidacionLineaFacade getEjbLogValidacionLineaFacade() {
        return ejbLogValidacionLineaFacade;
    }

    public CnetlPoolLogWarningFacade getEjbLogWarningFacade() {
        return ejbLogWarningFacade;
    }

    /**
     * @return the fechaDesde
     */
    public Date getFechaDesde() {
        return fechaDesde;
    }

    /**
     * @param fechaDesde the fechaDesde to set
     */
    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    /**
     * @return the fechaHasta
     */
    public Date getFechaHasta() {
        return fechaHasta;
    }

    /**
     * @param fechaHasta the fechaHasta to set
     */
    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    /**
     * @return the fuente
     */
    public CnetlFuente getFuente() {
        return fuente;
    }

    /**
     * @param fuente the fuente to set
     */
    public void setFuente(CnetlFuente fuente) {
        this.fuente = fuente;
    }

    /**
     * @return the fuenteTipo
     */
    public CnetlFuenteTipo getFuenteTipo() {
        return fuenteTipo;
    }

    /**
     * @param fuenteTipo the fuenteTipo to set
     */
    public void setFuenteTipo(CnetlFuenteTipo fuenteTipo) {
        this.fuenteTipo = fuenteTipo;
    }

    /**
     * @return the estado
     */
    public CnetlEstado getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(CnetlEstado estado) {
        this.estado = estado;
    }


    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(25) {

                @Override
                public int getItemsCount() {
                    return ejbFacade.countAdminLog(fechaDesde, fechaHasta, fuenteTipo.getNombre(), fuente == null?null:fuente.getNombre(), estado == null?null:estado.getNombre());
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(ejbFacade.findAdminLog(fechaDesde, fechaHasta, getFuenteTipo().getNombre(), fuente == null?null:fuente.getNombre(), 
                            estado == null?null:estado.getNombre(), new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String destroyAllItems() {
        try {
            Iterator log = items.iterator();
            while (log.hasNext()){
                VietlLog vietlLog = (VietlLog) log.next();
                CnetlPoolLog pooLog = ejbCnetlPoolLogFacade.findByIdPool(vietlLog.getIdPool()).get(0);
                ejbCnetlPoolLogFacade.remove(pooLog);
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("PersistenceErrorOccured"));
        }
        return buscarLogs();
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public String toPage(Integer page) {
        getPagination().toPage(page);
        recreateModel();
        return "List";
    }
    
    public String buscarLogs(){
        recreateModel();
        if (fechaDesde == null && fechaHasta == null && fuenteTipo == null){
            String msg = ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("AdminLogValidate");          
            JsfUtil.addErrorMessage(msg);
            return "";
        }
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public VietlLog getVietlLog(long id) {
        return ejbFacade.find(id);
    }

    public String exportToCSV() {
        downloadFile();  
        return "";
    }
    
    public String showDetailList() {
        current = (VietlLog) getItems().getRowData();
        downloadPoolFile(current);
        return "";
    }
    
    public String destroyItem() {
        current = (VietlLog) getItems().getRowData();
        try {
            CnetlPoolLog pooLog = ejbCnetlPoolLogFacade.findByIdPool(current.getIdPool()).get(0);
            ejbCnetlPoolLogFacade.remove(pooLog);
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("PersistenceErrorOccured"));
        }
        return buscarLogs();
    }
    
    @FacesConverter(forClass = VietlLog.class)
    public static class VietlLogAdminControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            VietlLogAdminController controller = (VietlLogAdminController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "VietlLogAdminController");
            return controller.getVietlLog(getKey(value));
        }

        long getKey(String value) {
            long key;
            key = Long.parseLong(value);
            return key;
        }

        String getStringKey(long value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof VietlLog) {
                VietlLog o = (VietlLog) object;
                return getStringKey(o.getIdPool());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + VietlLog.class.getName());
            }
        }

    }
    
    private void downloadPoolFile(VietlLog pool){
        try{
            DateFormat df = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
            String filename = "log-" + df.format(pool.getInicioPool()) + ".csv";
            FacesContext fc = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
            response.reset();
            response.setContentType("text/comma-separated-values");
            response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
            OutputStream output = response.getOutputStream();
            
            ArrayList<Log> logs = new ArrayList<>();
            ArrayList<VietlLog> poolLogs = (ArrayList<VietlLog>) getFacade().findByIdPool(pool.getIdPool());
            for (VietlLog poolLog : poolLogs) {
                Log log = convertVietlLog(poolLog);
                log.setReglas(convertVietlLogRegla(getEjbLogReglaFacade().findByIdPool(pool.getIdPool())));
                log.setTranformaciones(convertVietlLogTransformacion(getEjbLogTransformacionFacade().findByIdPool(pool.getIdPool())));
                log.setValidaciones(convertVietlValidacion(getEjbLogValidacionFacade().findByIdPool(pool.getIdPool())));
                log.setValidacionesDetalle(convertVietlValidacionLinea(getEjbLogValidacionLineaFacade().findByIdPool(pool.getIdPool())));
                log.setWarnings(convertLogWarning(getEjbLogWarningFacade().findByIdPool(pool.getIdPool())));
                logs.add(log);
            }
            
            // GENERACION CSV
            String csv = CSVUtils.convertCSV(logs);
            output.write(csv.getBytes());

            output.flush();
            output.close();
            fc.responseComplete();
        }
        catch(IOException e){
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("DownloadFileErrorOccured"));
        }
    }
    
    private void downloadFile(){
        try{
            Iterator log = items.iterator();
            String filename = "log.csv";
            DateFormat df = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
            FacesContext fc = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
            response.reset();
            response.setContentType("text/comma-separated-values");
            response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
            OutputStream output = response.getOutputStream();
            StringBuilder fila = new StringBuilder();
            fila.append(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("AdminLogInicio")).append(CSV_SEPARADOR).
                        append(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("AdminLogLoad")).append(CSV_SEPARADOR).
                        append(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("AdminLogFin")).append(CSV_SEPARADOR).
                        append(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("AdminLogFuenteTipo")).append(CSV_SEPARADOR).
                        append(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("AdminLogFuente")).append(CSV_SEPARADOR).
                        append(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("AdminLogEstado")).append(CSV_SEPARADOR).
                        append(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("AdminLogArchivo")).append(CSV_SEPARADOR).
                        append(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("AdminLogLineasTotal")).append(CSV_SEPARADOR).
                        append(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("AdminLogLineasOK")).append(CSV_SEPARADOR).
                        append(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("AdminLogLineasKO")).append(CSV_SEPARADOR).
                        append(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("AdminLogDuracion")).append("\n");
            output.write(fila.toString().getBytes());
            while (log.hasNext()){
                fila = new StringBuilder();
                VietlLog vietlLog = (VietlLog) log.next();
                fila.append(vietlLog.getInicioPool() == null?null:df.format(vietlLog.getInicioPool())).append(CSV_SEPARADOR).
                        append(vietlLog.getLoadPool() == null?null:df.format(vietlLog.getLoadPool())).append(CSV_SEPARADOR).
                        append(vietlLog.getFinPool() == null?null:df.format(vietlLog.getFinPool())).append(CSV_SEPARADOR).
                        append(vietlLog.getFuenteTipo()).append(CSV_SEPARADOR).
                        append(vietlLog.getFuente()).append(CSV_SEPARADOR).
                        append(vietlLog.getEstado()).append(CSV_SEPARADOR).
                        append(vietlLog.getArchivo()).append(CSV_SEPARADOR).
                        append(vietlLog.getLineas()).append(CSV_SEPARADOR).
                        append(vietlLog.getLineasOK()).append(CSV_SEPARADOR).
                        append(vietlLog.getLineasError()).append(CSV_SEPARADOR).
                        append(vietlLog.getDuracionPoolStr()).append("\n");
                output.write(fila.toString().getBytes());
            }
            output.flush();
            output.close();
            fc.responseComplete();
        }
        catch(IOException e){
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("DownloadFileErrorOccured"));
        }
    }

    private Log convertVietlLog(VietlLog vietlLog) {
        Log log = new Log();
        log.setIdPool(vietlLog.getIdPool());
        log.setFuente(vietlLog.getFuente());
        log.setFuenteTipo(vietlLog.getFuenteTipo());
        log.setFichero(vietlLog.getArchivo());
        log.setEstado(vietlLog.getEstado());
        log.setInicioPool(TimeUtils.convertUTC2(vietlLog.getInicioPool()));
        log.setLoadPool(TimeUtils.convertUTC2(vietlLog.getLoadPool()));
        log.setFinPool(TimeUtils.convertUTC2(vietlLog.getFinPool()));
        log.setLineas(vietlLog.getLineas());
        log.setLineasOk(vietlLog.getLineasOK());
        log.setLineasError(vietlLog.getLineasError());
        log.setDuracionPool(TimeUtils.convertDateDuration(vietlLog.getInicioPool(), vietlLog.getFinPool()));
        log.setDuracionLoad(TimeUtils.convertDateDuration(vietlLog.getInicioPool(), vietlLog.getLoadPool()));
        log.setError(vietlLog.getError());

        return log;
    }
    
    private List<LogRegla> convertVietlLogRegla(List<VietlLogRegla> logsRegla) {
        ArrayList<LogRegla> lista = new ArrayList<>();
        for (VietlLogRegla item : logsRegla) {
            LogRegla regla = new LogRegla();
            regla.setIdPool(item.getIdPool());
            regla.setIdRegla(item.getIdReglaImportacion());
            regla.setRegla(item.getRegla());
            regla.setInicio(TimeUtils.convertUTC2(item.getFechaInicio()));
            regla.setFin(TimeUtils.convertUTC2(item.getFechaFin()));
            regla.setDuracion(TimeUtils.convertDateDuration(item.getFechaInicio(), item.getFechaFin()));
            lista.add(regla);
        }
        return lista;
    }

    private List<LogTransformacion> convertVietlLogTransformacion(List<VietlLogTransformacion> transformaciones) {
        ArrayList<LogTransformacion> lista = new ArrayList<>();
        for (VietlLogTransformacion item : transformaciones) {
            LogTransformacion transformacion = new LogTransformacion();
            transformacion.setIdPool(item.getIdPool());
            transformacion.setIdTransformacion(item.getIdTransformacion());
            transformacion.setTransformacion(item.getTransformacion());
            transformacion.setInicio(TimeUtils.convertUTC2(item.getInicioTransformacion()));
            transformacion.setFin(TimeUtils.convertUTC2(item.getFinTransformacion()));
            transformacion.setDuracion(TimeUtils.convertDateDuration(item.getInicioTransformacion(), item.getFinTransformacion()));
            lista.add(transformacion);
        }
        return lista;
    }

    private List<LogValidacion> convertVietlValidacion(List<VietlLogValidacion> validaciones) {
        ArrayList<LogValidacion> lista = new ArrayList<>();
        for (VietlLogValidacion item : validaciones) {
            LogValidacion validacion = new LogValidacion();
            validacion.setIdPool(item.getIdPool());
            validacion.setIdValidacion(item.getIdValidacion());
            validacion.setValidacion(item.getValidacion());
            validacion.setFecha(TimeUtils.convertUTC2(item.getFechaValidacion()));
            validacion.setLineasOk(item.getLineasOkValidacion());
            validacion.setLineasError(item.getLineasErrorValidacion());
            lista.add(validacion);
        }
        return lista;
    }

    private List<LogValidacionDetalle> convertVietlValidacionLinea(List<VietlLogValidacionLinea> validacionesLinea) {
        ArrayList<LogValidacionDetalle> lista = new ArrayList<>();
        for (VietlLogValidacionLinea item : validacionesLinea) {
            LogValidacionDetalle detalle = new LogValidacionDetalle();
            detalle.setIdPool(item.getIdPool());
            detalle.setIdValidacion(item.getIdValidacion());
            detalle.setValidacion(item.getValidacion());
            detalle.setNumLinea(item.getLinea());
            detalle.setValor(item.getValor());
            lista.add(detalle);
        }
        return lista;
    }

    private List<LogWarning> convertLogWarning(List<CnetlPoolLogWarning> poolWarning) {
        ArrayList<LogWarning> lista = new ArrayList<>();
        for (CnetlPoolLogWarning item : poolWarning) {
            LogWarning log = new LogWarning();
            log.setCode(item.getCode());
            log.setLevel(item.getLevel());
            log.setMessage(item.getMessage());
            lista.add(log);
        }
        return lista;
    }
    
}
