/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jsf;

import es.condenast.jpa.entities.CncrmBorradoInactivos;
import es.condenast.jpa.session.CncrmBorradoInactivosFacade;
import es.condenast.jsf.util.JsfUtil;
import java.io.Serializable;
import java.util.Date;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Raul.Tena
 */
@Named("cncrmUsuariosInactivosController")
@SessionScoped
public class CncrmUsuariosInactivosController implements Serializable {
    
    private CncrmBorradoInactivos current;
    @EJB
    private CncrmBorradoInactivosFacade ejbFacade;

    @PostConstruct
    public void init() {
        if(current == null) {
            try {
                current = getFacade().findByFechaCambio();
            } catch (Exception e) {
                JsfUtil.addErrorMessage(e, ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("PersistenceErrorOccured"));
                current = new CncrmBorradoInactivos();
            }
        }
    }
    
    private CncrmBorradoInactivosFacade getFacade() {
        return ejbFacade;
    }
    
    public void save() {
        boolean create = false;
        if(current.isActivo()) {
            //Activar proceso
            if(!(current.getMesesInactividad() > 0 && current.getSemanasAviso() > 0 && !isNullOrEmpty(current.getDestinatarios()))) {
                String msg = ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("BorradoInactivosValidate");          
                JsfUtil.addErrorMessage(msg);
            } else {
                current.setDestinatarios(current.getDestinatarios().replaceAll("\\s",""));
                create = true;
            }
        } else {
            //Desactivar proceso
            create = true;
        }
        
        if(create) {
            current.setFechaCambio(new Date());
            getFacade().create(current);
        }
    }

    private boolean isNullOrEmpty(String cadena){
        return cadena == null || "".equals(cadena);
    }
    
    public CncrmBorradoInactivos getCurrent() {
        return current;
    }

    public void setCurrent(CncrmBorradoInactivos current) {
        this.current = current;
    }
    
}
