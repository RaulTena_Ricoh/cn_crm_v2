package es.condenast.jsf;

import es.condenast.jpa.entities.CnmasMarca;
import es.condenast.jpa.entities.CnsegUsuario;
import es.condenast.jpa.session.CnsegPerfilFacade;
import es.condenast.jpa.session.CnsegUsuarioFacade;
import es.condenast.jsf.util.JsfUtil;
import es.condenast.jsf.util.PaginationHelper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Named;

@Named("cnsegUsuarioController")
@SessionScoped
public class CnsegUsuarioController implements Serializable {

    private CnsegUsuario current;
    private DataModel items = null;

    private static final Short PERFIL_NUMERO_FILAS_DETALLE = 2;

    public Short getPERFIL_NUMERO_FILAS_DETALLE() {
        return PERFIL_NUMERO_FILAS_DETALLE;
    }

    // Join con tabla CNSEG_USUARIO_MARCAS
    private CnmasMarca[] marcas;

    public CnmasMarca[] getMarcas() {
        return marcas;
    }

    public void setMarcas(CnmasMarca[] marcas) {
        this.marcas = marcas;
    }

    // Activo/Inactivo
    private boolean activo;

    public boolean getActivo() {
        return current.getFechaBaja() == null;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    // Numero filas
    private Integer numFilas;

    public Integer getNumFilas() {
        return numFilas;
    }

    public void setNumFilas(Integer numFilas) {
        this.numFilas = numFilas;
    }

    @EJB
    private es.condenast.jpa.session.CnsegUsuarioFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    @EJB
    private es.condenast.jpa.session.CnsegPerfilFacade ejbPerfilFacade;

    public CnsegUsuarioController() {
    }

    public CnsegUsuario getSelected() {
        if (current == null) {
            current = new CnsegUsuario();
            selectedItemIndex = -1;
        }
        return current;
    }

    private CnsegUsuarioFacade getFacade() {
        return ejbFacade;
    }

    public CnsegPerfilFacade getPerfilFacade() {
        return ejbPerfilFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize() - 1}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (CnsegUsuario) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new CnsegUsuario();
        marcas = null;
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            // Añadir los elementos de CNSEG_USUARIO_MARCA
            Collection<CnmasMarca> marcasEnt = new ArrayList<>();
            marcasEnt.addAll(Arrays.asList(marcas));
            current.setCnmasMarcaCollection(marcasEnt);

            // Fecha de Alta
            current.setFechaAlta(new Date());

            getFacade().create(current);

            // Actualizar datos perfil
            updatePerfil();

            return prepareList();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle(JsfUtil.BUNDLE).getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (CnsegUsuario) getItems().getRowData();

        // Añadir los elementos de CNSEG_USUARIO_MARCA
        int i = 0;
        marcas = new CnmasMarca[current.getCnmasMarcaCollection().size()];
        for (CnmasMarca marca : current.getCnmasMarcaCollection()) {
            marcas[i++] = marca;
        }

        // Obtener num filas
        refreshNumFilas(current.getIdPerfil().getIdPerfil());

        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    private void refreshNumFilas(Short idPerfil) {
        getSelected().setIdPerfil(getPerfilFacade().find(idPerfil));
    }

    public String update() {
        try {

            // Añadir los elementos de CNSEG_USUARIO_MARCA
            Collection<CnmasMarca> marcasEnt = new ArrayList<>();
            marcasEnt.addAll(Arrays.asList(marcas));
            current.setCnmasMarcaCollection(marcasEnt);

            // Fecha de Baja
            if (!activo) {
                current.setFechaBaja(new Date());
            } else {
                current.setFechaBaja(null);
            }

            getFacade().edit(current);

            // Actualizar datos perfil
            updatePerfil();

            return "List";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle(JsfUtil.BUNDLE).getString("PersistenceErrorOccured"));
            return null;
        }
    }

    private void updatePerfil() {
        if (Objects.equals(current.getIdPerfil().getIdPerfil(), PERFIL_NUMERO_FILAS_DETALLE)) {
            getPerfilFacade().edit(current.getIdPerfil());
        }

    }

    public String destroy() {
        current = (CnsegUsuario) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            //JsfUtil.addSuccessMessage(ResourceBundle.getBundle(JsfUtil.BUNDLE).getString("CnsegUsuarioDeleted"));
        } catch (Exception e) {
            String foreignKey = JsfUtil.isForeignKey(e);

            if (foreignKey != null) {
                if (foreignKey.contains("FK_SEG_USUARIO_MARCA_USUARIO")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("UsuarioCNForeignKey"),
                            new String[]{current.getNombre()});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }
    
    public String toPage(Integer page) {
        getPagination().toPage(page);
        recreateModel();
        return "List";
    }


    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public CnsegUsuario getCnsegUsuario(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = CnsegUsuario.class)
    public static class CnsegUsuarioControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CnsegUsuarioController controller = (CnsegUsuarioController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "cnsegUsuarioController");
            return controller.getCnsegUsuario(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof CnsegUsuario) {
                CnsegUsuario o = (CnsegUsuario) object;
                return getStringKey(o.getIdUsuario());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + CnsegUsuario.class.getName());
            }
        }

    }

    public void changePerfil() throws Exception {
        FacesContext context = FacesContext.getCurrentInstance();;
        String idPerfil = context.getExternalContext().getRequestParameterMap().get("param");
        refreshNumFilas(Short.parseShort(idPerfil));

        System.err.println("Valor -> " + getSelected().getIdPerfil().getNumeroFilasDetalle());
    }

}
