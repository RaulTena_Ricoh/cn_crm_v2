package es.condenast.jsf;

import es.condenast.jpa.entities.CnapiClientes;
import es.condenast.jpa.entities.CnapiMarcasCliente;
import es.condenast.jpa.entities.CnmasMarca;
import es.condenast.jpa.session.CnapiClientesFacade;
import es.condenast.jpa.session.CnapiMarcasClienteFacade;
import es.condenast.jpa.session.CnmasMarcaFacade;
import es.condenast.jsf.util.JsfUtil;
import es.condenast.jsf.util.PaginationHelper;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.UUID;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;

@Named("cnapiClientesController")
@SessionScoped
public class CnapiClientesController implements Serializable {

    private final SecureRandom random = new SecureRandom();
    
    private CnapiClientes current;
    private DataModel items = null;
    @EJB
    private es.condenast.jpa.session.CnapiClientesFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    
    @EJB
    private es.condenast.jpa.session.CnmasMarcaFacade ejbFacadeMarca;
    private DualListModel<CnmasMarca> marcas;

    @EJB
    private es.condenast.jpa.session.CnapiMarcasClienteFacade ejbFacadeMarcaAPI;
    
    public CnapiClientesController() {
    }

    public CnapiClientes getSelected() {
        if (current == null) {
            current = new CnapiClientes();
            selectedItemIndex = -1;
        }
        return current;
    }

    private CnapiClientesFacade getFacade() {
        return ejbFacade;
    }
    
    private CnmasMarcaFacade getFacadeMarcas() {
        return ejbFacadeMarca;
    }
    
    private CnapiMarcasClienteFacade getFacadeMarcasAPI() {
        return ejbFacadeMarcaAPI;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize() - 1}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (CnapiClientes) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new CnapiClientes();
        
        UUID guid = UUID.randomUUID();
        current.setIdCliente(guid.toString().substring(0, 8));
        
        selectedItemIndex = -1;
        prepareCreateMarcas();  //Recupera las marcas para asociarlas a un cliente;
        return "Create";
    }
    
    private void prepareCreateMarcas() {
        //Listas de Origenes
        ArrayList<CnmasMarca> marcasSource = new ArrayList<>(getFacadeMarcas().findAll());
        Collections.sort(marcasSource);
        //origenesHM = CnetlOrigen.toHashMap(origenesSource);

        List<CnmasMarca> marcasTarget = new ArrayList<>();
        marcas = new DualListModel<>(marcasSource, marcasTarget);
    }

    public String create() {
        try {
            if (!validateFields()) {
                return null;
            }
            
            current.setToken(new BigInteger(130, random).toString(32));        
            current.setCodigoAuth(new BigInteger(130, random).toString(32));
            
            getFacade().create(current);
            current.setCnapiMarcasClienteCollection(getMarcasCliente(getMarcas().getTarget()));
            getFacade().edit(current);
            
            //JsfUtil.addSuccessMessage(ResourceBundle.getBundle(JsfUtil.BUNDLE).getString("CnapiClientesCreated"));
            //return prepareCreate();
            return prepareList();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle(JsfUtil.BUNDLE).getString("PersistenceErrorOccured"));
            return null;
        }
    }
    
    private  ArrayList<CnapiMarcasCliente>  getMarcasCliente(List<CnmasMarca> marcas){
        ArrayList<CnapiMarcasCliente> listaMarcas = new ArrayList<>();
            
        for(CnmasMarca marca : marcas)
        {
            listaMarcas.add(new CnapiMarcasCliente(current.getId(), marca.getIdMarca()));
        }
        return  listaMarcas;
    }

    public String prepareEdit() {
        current = (CnapiClientes) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        
        prepareEditMarcas();
        
        return "Edit";
    }
    
    private void prepareEditMarcas() {
        //Listas de Origenes Fuente 
        ArrayList<CnmasMarca> marcasSource = new ArrayList<>(getFacadeMarcas().findAll());
        Collections.sort(marcasSource);
        
        // Lista de Origenes Destino
        List<CnmasMarca> marcasTarget = new ArrayList<>();
             
        List<CnapiMarcasCliente> marcasAPI = getFacadeMarcasAPI().findByIdCliente(current.getId());

        for (CnapiMarcasCliente marcaAPI : marcasAPI) {
            marcasTarget.add(getFacadeMarcas().findByIdMarca(marcaAPI.getCnapiMarcasClientePK().getIdMarca()));
        }

        marcasSource.removeAll(marcasTarget);

        marcas = new DualListModel<>(marcasSource, marcasTarget);
    }

    public String update() {
        try {
            if (!validateFields()) {
                return null;
            }
            
            getFacade().edit(current);
            
            getFacadeMarcasAPI().removeByIdCliente(current.getId());
            for(CnmasMarca marca : getMarcas().getTarget())
            {
                getFacadeMarcasAPI().create(new CnapiMarcasCliente(current.getId(), marca.getIdMarca()));
            }
            
            //JsfUtil.addSuccessMessage(ResourceBundle.getBundle(JsfUtil.BUNDLE).getString("CnapiClientesUpdated"));
            return "List";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle(JsfUtil.BUNDLE).getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (CnapiClientes) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            //JsfUtil.addSuccessMessage(ResourceBundle.getBundle(JsfUtil.BUNDLE).getString("CnapiClientesDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle(JsfUtil.BUNDLE).getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public CnapiClientes getCnapiClientes(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    private boolean validateFields() {
        boolean valido = true;
        if (current.getNombreCliente()== null || current.getNombreCliente().length() == 0) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("ClienteApiNombreObligatorio"));
            valido = false;
        }
        
        if (current.getFechaCaducidad()== null) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("ClienteApiFechaCaducidadObligatorio"));
            valido = false;
        }
        
        if (current.getNumeroRenovaciones() == null) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("ClienteApiNumeroRenovacionesObligatorio"));
            valido = false;
        }
        
        return valido;
    }

    @FacesConverter(forClass = CnapiClientes.class)
    public static class CnapiClientesControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CnapiClientesController controller = (CnapiClientesController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "cnapiClientesController");
            return controller.getCnapiClientes(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof CnapiClientes) {
                CnapiClientes o = (CnapiClientes) object;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + CnapiClientes.class.getName());
            }
        }

    }

    public DualListModel<CnmasMarca> getMarcas() {
        return marcas;
    }

    public void setMarcas(DualListModel<CnmasMarca> marcas) {
        this.marcas = marcas;
    }

    public void onElementDualListTransfer(TransferEvent event) {
        if (event.isRemove()) {
            ArrayList<CnmasMarca> marcasSource = new ArrayList<>(getMarcas().getSource());
            Collections.sort(marcasSource);
            marcas = new DualListModel<>(marcasSource, marcas.getTarget());
        }
    }
    
    public String toPage(Integer page) {
        getPagination().toPage(page);
        recreateModel();
        return "List";
    }
  
}
