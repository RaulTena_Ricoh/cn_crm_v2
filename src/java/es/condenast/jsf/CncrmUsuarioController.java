package es.condenast.jsf;

import es.condenast.jpa.entities.CncrmUsuario;
import es.condenast.jpa.entities.CnmasMarca;
import es.condenast.jpa.session.CncrmUsuarioFacade;
import es.condenast.jpa.session.CncrmUsuarioMarcaCondicionLegalFacade;
import es.condenast.jsf.util.JsfUtil;
import es.condenast.jsf.util.PaginationHelper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import org.primefaces.context.RequestContext;

@Named("cncrmUsuarioController")
@SessionScoped
public class CncrmUsuarioController implements Serializable {

    private CncrmUsuario current;
    private DataModel items = null;
    @EJB
    private es.condenast.jpa.session.CncrmUsuarioFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    @EJB
    private es.condenast.jpa.session.CncrmUsuarioMarcaCondicionLegalFacade ejbUsuarioMarcaCondicionLegalFacade;

    // Lista de marcas
    CnmasMarca[] marcas;

    public CnmasMarca[] getMarcas() {
        return marcas;
    }

    public void setMarcas(CnmasMarca[] marcas) {
        this.marcas = marcas;
    }

    // Emails
    private String emails;
    private String[] emailUsuario;
    private String emailDesactivar;

    //Numero Registros
    private int numeroRegistros;

    List<Long> idsEliminar;
    private CnmasMarca[] marcasEliminar;

    public String getEmails() {
        return emails;
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }

    public String[] getEmailUsuario() {
        return emailUsuario;
    }

    public void setEmailUsuario(String[] emailUsuario) {
        this.emailUsuario = emailUsuario;
    }

    public String getEmailDesactivar() {
        return emailDesactivar;
    }

    public void setEmailDesactivar(String emailDesactivar) {
        this.emailDesactivar = emailDesactivar;
    }

    // Fecha
    private Date fecha;

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public List<Long> getIdsEliminar() {
        return idsEliminar;
    }

    public void setIdsEliminar(List<Long> idsEliminar) {
        this.idsEliminar = idsEliminar;
    }

    public CncrmUsuarioController() {
    }

    public CncrmUsuario getSelected() {
        if (current == null) {
            current = new CncrmUsuario();
            selectedItemIndex = -1;
        }
        return current;
    }

    private CncrmUsuarioFacade getFacade() {
        return ejbFacade;
    }

    private CncrmUsuarioMarcaCondicionLegalFacade getUsuarioMarcaCondicionLegalFacade() {
        return ejbUsuarioMarcaCondicionLegalFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize() - 1}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (CncrmUsuario) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new CncrmUsuario();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (CncrmUsuario) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (CncrmUsuario) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public CncrmUsuario getCncrmUsuario(java.lang.Long id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = CncrmUsuario.class)
    public static class CncrmUsuarioControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CncrmUsuarioController controller = (CncrmUsuarioController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "cncrmUsuarioController");
            return controller.getCncrmUsuario(getKey(value));
        }

        java.lang.Long getKey(String value) {
            java.lang.Long key;
            key = Long.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Long value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof CncrmUsuario) {
                CncrmUsuario o = (CncrmUsuario) object;
                return getStringKey(o.getIdUsuario());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + CncrmUsuario.class.getName());
            }
        }

    }

    private String prepareDelete() {
        emails = null;
        marcas = null;
        current = null;
        fecha = null;
        return "Delete";
    }

    // Desactivar usuario por e-mail y marca
    public String desactivar() {

        ArrayList<Short> idMarcas = new ArrayList<>();
        for (CnmasMarca marca : marcasEliminar) {
            idMarcas.add(marca.getIdMarca());
        }

        try {
            getUsuarioMarcaCondicionLegalFacade().desactivarByIdUsuarioMarcas(emailDesactivar, idMarcas);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("resources/Messages").getString("UsuarioDesactivadoCorrectamente"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("resources/Messages").getString("ErrorDesactivarUsuario"));
        }
        return prepareDelete();

    }

    // Desactivar usuario por e-mail y marca
    public String preguntaDesactivar() {

        emailDesactivar = current.getEmail();
        ArrayList<Short> idMarcas = new ArrayList<>();
        marcasEliminar = marcas;
        for (CnmasMarca marca : marcas) {
            idMarcas.add(marca.getIdMarca());
        }

        try {
            int res = getUsuarioMarcaCondicionLegalFacade().countDesactivarByIdUsuarioMarcas(current.getEmail(), idMarcas);
            RequestContext context = RequestContext.getCurrentInstance();
            if (res > 0) {
                context.execute("PF('confDesactivar').show()");
            } else {
                context.execute("PF('confDesactivar0').show()");
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("resources/Messages").getString("ErrorDesactivarUsuario"));
        }
        return prepareDelete();

    }

    // Borrado fisico de usuarios por email
    public String eliminarPorEmails() {

        String[] emails = emailUsuario;
        try {
            for (String email : emails) {
                getFacade().deleteByEmail(email);
            }
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("resources/Messages").getString("UsuariosEliminadosCorrectamente"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("resources/Messages").getString("ErrorEliminarUsuarios"));
        }
        return prepareDelete();
    }

    public String countPorEmails() {

        emailUsuario = emails.replaceAll(" ", "").split(";");
        numeroRegistros = 0;
        try {
            for (String email : emailUsuario) {
                List<CncrmUsuario> usu = getFacade().findByEmail(email);
                if (usu.size() > 0) {
                    numeroRegistros++;
                }
            }
            RequestContext context = RequestContext.getCurrentInstance();
            if (numeroRegistros > 0) {
                context.execute("PF('confirmacionPorEmail').show()");
            } else {
                context.execute("PF('confirmacionPorEmail0').show()");
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("resources/Messages").getString("ErrorEliminarUsuarios"));
        }
        return prepareDelete();
    }

    // Borrado fisico de usuarios por fecha
    public String countPorFecha() {
        try {
            idsEliminar = getFacade().selectByFecha(fecha);
            numeroRegistros = idsEliminar.size();
            RequestContext context = RequestContext.getCurrentInstance();
            if (numeroRegistros > 0) {
                context.execute("PF('confirmacionPorFecha').show()");
            } else {
                context.execute("PF('confirmacionPorFecha0').show()");
            }

        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("resources/Messages").getString("ErrorEliminarUsuarios"));
        }
        return prepareDelete();
    }

    public String eliminarPorFecha() {
        try {
            getFacade().deleteByIds(idsEliminar);
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('confirmacionPorFecha').hide()");
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("resources/Messages").getString("UsuariosEliminadosCorrectamente"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("resources/Messages").getString("ErrorEliminarUsuarios"));
        }
        return prepareDelete();
    }

    public int getNumeroRegistros() {
        return numeroRegistros;
    }

    public void setNumeroRegistros(int numeroRegistros) {
        this.numeroRegistros = numeroRegistros;
    }

}
