/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jsf;

import es.condenast.jpa.entities.CncrmSearchUserResult;
import es.condenast.jpa.entities.CncrmSearchUserResultFK;
import es.condenast.jpa.entities.CncrmUsuario;
import es.condenast.jpa.entities.CncrmUsuarioDireccionFacade;
import es.condenast.jpa.entities.CnmasMarca;
import es.condenast.jpa.entities.CnmasProducto;
import es.condenast.jpa.session.CncrmSearchUserResultFacade;
import es.condenast.jpa.session.CncrmUsuarioFacade;
import es.condenast.jpa.session.CnmasMarcaFacade;
import es.condenast.jpa.session.CnmasParticipacionFacade;
import es.condenast.jpa.session.CnmasProductoFacade;
import es.condenast.jsf.util.JsfUtil;
import es.condenast.jsf.util.PaginationHelper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;

/**
 *
 * @author ivan.fernandez
 */
@Named("cncrmBuscarUsuarioController")
@SessionScoped
public class CncrmBuscarUsuarioController implements Serializable {
    private static final String AND = "AND";
    private static final String IGNORE = "";
    private static final String TABLE_NAME_PARAM = "tableName";
    private static final String UNIDAD_NEGOCIO_NAME_PARAM = "unidadNegocioName";
    private static final String CONDITION_FIELD_PARAM = "conditionField";
    private static final String CONDITION_VALUE_PARAM = "conditionValue";
    private static final String COLUMN_SEPARATOR = ",";
    private static final String SEPARATOR = "#";
    private static final String SEPARATOR_ESCAPED = "\\#";
    
    private CncrmSearchUserResult current;
    private CncrmUsuario currentUsuario;
    private DataModel items = null;
    @EJB
    private CncrmSearchUserResultFacade ejbFacade = new CncrmSearchUserResultFacade();
    @EJB
    private CnmasMarcaFacade ejbMarcaFacade;
    @EJB
    private CnmasProductoFacade ejbProductoFacade;
    @EJB
    private CnmasParticipacionFacade ejbParticipacionFacade;
    @EJB
    private CncrmUsuarioFacade ejbUsuario;
    @EJB
    private CncrmUsuarioDireccionFacade ejbDireccionUsuario;
    
    private String email;
    private Date fechaAlta;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private String participacion;
    private Boolean aceptaCL1;
    private Boolean aceptaCL2;
    private Boolean aceptaCL3;
    private String marcaCondition;
    private String marcaSubCondition = AND;
    private String fechaAltaCondition;
    private String productoCondition;
    private String productoSubCondition = AND;
    private String enviosCondition;
    private String participacionCondition;
    private CnmasMarca[] marcas;
    private CnmasProducto[] productos;
    private List<CncrmSearchUserResult> searchResult;
    private String queryString;
    private CncrmUsuario usuario;
    private List details;
    private List detailHeader;
    private String unidadNegocioName;
    
    public CncrmBuscarUsuarioController(){
        recreateModel();
    }

    /**
     * @return the ejbFacade
     */
    public CncrmSearchUserResultFacade getEjbFacade() {
        return ejbFacade;
    }

    /**
     * @param ejbFacade the ejbFacade to set
     */
    public void setEjbFacade(CncrmSearchUserResultFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }
    
    public CncrmSearchUserResult getSelected() {
        if (current == null) {
            current = new CncrmSearchUserResult();
            selectedItemIndex = -1;
        }
        return current;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(25) {

                @Override
                public int getItemsCount() {
                    return ejbFacade.countSearch(email, marcas, marcaCondition, marcaSubCondition, 
                            productos, productoCondition, productoSubCondition, fechaAlta, fechaAltaCondition, 
                            enviosCondition, aceptaCL1, aceptaCL2, aceptaCL3, participacionCondition, participacion);
                    //return ejbFacade.countSearch(queryString);
                }

                @Override
                public DataModel createPageDataModel() {
                    //return new ListDataModel(ejbFacade.findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize() - 1}, queryString));
                    return new ListDataModel(ejbFacade.findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize() - 1}, 
                            email, marcas, marcaCondition, marcaSubCondition, productos, productoCondition, productoSubCondition, 
                            fechaAlta, fechaAltaCondition, enviosCondition, aceptaCL1, aceptaCL2, aceptaCL3, participacionCondition, participacion));
                    
                }
            };
        }
        return pagination;
    }

    /**
     * @return the current
     */
    public CncrmSearchUserResult getCurrent() {
        return current;
    }

    /**
     * @param current the current to set
     */
    public void setCurrent(CncrmSearchUserResult current) {
        this.current = current;
    }

    /**
     * @return the currentUsuario
     */
    public CncrmUsuario getCurrentUsuario() {
        return currentUsuario;
    }

    /**
     * @param currentUsuario the currentUsuario to set
     */
    public void setCurrentUsuario(CncrmUsuario currentUsuario) {
        this.currentUsuario = currentUsuario;
    }
    
    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }
    
    private void recreateModel() {
        items = null;
    }
    
    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public String toPage(Integer page) {
        getPagination().toPage(page);
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (CncrmSearchUserResult) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public boolean disableMarcaConditions() {
        if (marcas != null && marcas.length > 1) {
            return false;
        }
        return true;
    }
    
    public boolean disableProductoConditions() {
        if (productos != null && productos.length > 1) {
            return false;
        }
        return true;
    }
    
    @PostConstruct
    public void reset() {
           setEmail(null);
           setMarcaCondition(IGNORE);
           setMarcaSubCondition(null);
           setMarcas(null);
           setFechaAltaCondition(IGNORE);
           setFechaAlta(null);
           setProductoCondition(IGNORE);
           setProductoSubCondition(null);
           setProductos(null);
           setEnviosCondition(IGNORE);
           setAceptaCL1(null);
           setAceptaCL2(null);
           setAceptaCL3(null);
           setParticipacionCondition(IGNORE);
           setParticipacion(null);
    }
    
    /**
     * @return the ejbMarcaFacade
     */
    public CnmasMarcaFacade getEjbMarcaFacade() {
        return ejbMarcaFacade;
    }

    /**
     * @param ejbMarcaFacade the ejbMarcaFacade to set
     */
    public void setEjbMarcaFacade(CnmasMarcaFacade ejbMarcaFacade) {
        this.ejbMarcaFacade = ejbMarcaFacade;
    }

    /**
     * @return the ejbProductoFacade
     */
    public CnmasProductoFacade getEjbProductoFacade() {
        return ejbProductoFacade;
    }

    /**
     * @param ejbProductoFacade the ejbProductoFacade to set
     */
    public void setEjbProductoFacade(CnmasProductoFacade ejbProductoFacade) {
        this.ejbProductoFacade = ejbProductoFacade;
    }

    /**
     * @return the selectedItemIndex
     */
    public int getSelectedItemIndex() {
        return selectedItemIndex;
    }

    /**
     * @param selectedItemIndex the selectedItemIndex to set
     */
    public void setSelectedItemIndex(int selectedItemIndex) {
        this.selectedItemIndex = selectedItemIndex;
    }

    /**
     * @return the fechaAlta
     */
    public Date getFechaAlta() {
        return fechaAlta;
    }

    /**
     * @param fechaAlta the fechaAlta to set
     */
    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    /**
     * @return the participacion
     */
    public String getParticipacion() {
        return participacion;
    }

    /**
     * @param participacion the participacion to set
     */
    public void setParticipacion(String participacion) {
        this.participacion = participacion;
    }
    
    private boolean isNullOrEmpty(String cadena){
        return cadena == null || "".equals(cadena);
    }
    
    private boolean isNullOrEmpty(Date fecha){
        return fecha == null;
    }
    /**
     * @return the aceptaCL1
     */
    public Boolean getAceptaCL1() {
        return aceptaCL1;
    }

    /**
     * @param aceptaCL1 the aceptaCL1 to set
     */
    public void setAceptaCL1(Boolean aceptaCL1) {
        this.aceptaCL1 = aceptaCL1;
    }

    /**
     * @return the aceptaCL2
     */
    public Boolean getAceptaCL2() {
        return aceptaCL2;
    }

    /**
     * @param aceptaCL2 the aceptaCL2 to set
     */
    public void setAceptaCL2(Boolean aceptaCL2) {
        this.aceptaCL2 = aceptaCL2;
    }

    /**
     * @return the aceptaCL3
     */
    public Boolean getAceptaCL3() {
        return aceptaCL3;
    }

    /**
     * @param aceptaCL3 the aceptaCL3 to set
     */
    public void setAceptaCL3(Boolean aceptaCL3) {
        this.aceptaCL3 = aceptaCL3;
    }

    /**
     * @return the marcaCondition
     */
    public String getMarcaCondition() {
        return marcaCondition;
    }

    /**
     * @param marcaCondition the marcaCondition to set
     */
    public void setMarcaCondition(String marcaCondition) {
        this.marcaCondition = marcaCondition;
    }

    /**
     * @return the marcaSubCondition
     */
    public String getMarcaSubCondition() {
        return marcaSubCondition;
    }

    /**
     * @param marcaSubCondition the marcaSubCondition to set
     */
    public void setMarcaSubCondition(String marcaSubCondition) {
        this.marcaSubCondition = marcaSubCondition;
    }

    /**
     * @return the fechaAltaCondition
     */
    public String getFechaAltaCondition() {
        return fechaAltaCondition;
    }

    /**
     * @param fechaAltaCondition the fechaAltaCondition to set
     */
    public void setFechaAltaCondition(String fechaAltaCondition) {
        this.fechaAltaCondition = fechaAltaCondition;
    }

    /**
     * @return the productoCondition
     */
    public String getProductoCondition() {
        return productoCondition;
    }

    public void setProductoCondition(String productoCondition) {
        this.productoCondition = productoCondition;
    }

    /**
     * @return the productoSubCondition
     */
    public String getProductoSubCondition() {
        return productoSubCondition;
    }

    /**
     * @param productoSubCondition the productoSubCondition to set
     */
    public void setProductoSubCondition(String productoSubCondition) {
        this.productoSubCondition = productoSubCondition;
    }


    public String getEnviosCondition() {
        return enviosCondition;
    }

    public void setEnviosCondition(String enviosCondition) {
        this.enviosCondition = enviosCondition;
    }

    public String getParticipacionCondition() {
        return participacionCondition;
    }

    /**
     * @param participacionCondition the participacionCondition to set
     */
    public void setParticipacionCondition(String participacionCondition) {
        this.participacionCondition = participacionCondition;
    }

    /**
     * @return the marcas
     */
    public CnmasMarca[] getMarcas() {
        return marcas;
    }

    /**
     * @param marcas the marcas to set
     */
    public void setMarcas(CnmasMarca[] marcas) {
        this.marcas = marcas;
    }

    /**
     * @return the productos
     */
    public CnmasProducto[] getProductos() {
        return productos;
    }

    /**
     * @param productos the productos to set
     */
    public void setProductos(CnmasProducto[] productos) {
        this.productos = productos;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    public String buscarUsuarios(){
        recreateModel();
        if (isNullOrEmpty(email) && (marcas == null ||marcas.length == 0) && (productos == null || productos.length == 0) && isNullOrEmpty(fechaAlta) && isNullOrEmpty(participacion) && aceptaCL1 == null && aceptaCL2 == null && aceptaCL3 == null){
            String msg = ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("BuscarUsuarioCRMValidate");          
            JsfUtil.addErrorMessage(msg);
            return "";
        }
        return "List";
    }
    
    public String showDetailList() {
        current = (CncrmSearchUserResult) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        currentUsuario = ejbUsuario.find(current.getCncrmSearchUserResultFK().getIdUsuario());
        return "DetailList";
    }
    /**
     * @return the searchResult
     */
    public List<CncrmSearchUserResult> getSearchResult() {
        return searchResult;
    }

    /**
     * @param searchResult the searchResult to set
     */
    public void setSearchResult(List<CncrmSearchUserResult> searchResult) {
        this.searchResult = searchResult;
    }
    
    /**
     * @return the queryString
     */
    public String getQueryString() {
        return queryString;
    }

    /**
     * @param queryString the queryString to set
     */
    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    /**
     * @return the ejbParticipacionFacade
     */
    public CnmasParticipacionFacade getEjbParticipacionFacade() {
        return ejbParticipacionFacade;
    }

    /**
     * @param ejbParticipacionFacade the ejbParticipacionFacade to set
     */
    public void setEjbParticipacionFacade(CnmasParticipacionFacade ejbParticipacionFacade) {
        this.ejbParticipacionFacade = ejbParticipacionFacade;
    }
    
    /**
     * @return the ejbUsuario
     */
    public CncrmUsuarioFacade getEjbUsuario() {
        return ejbUsuario;
    }

    /**
     * @param ejbUsuario the ejbUsuario to set
     */
    public void setEjbUsuario(CncrmUsuarioFacade ejbUsuario) {
        this.ejbUsuario = ejbUsuario;
    }
    
    public String goUsuarioDetail(){
        usuario = ejbUsuario.findById(current.getCncrmSearchUserResultFK().getIdUsuario());
        return "Detail";
    }

    /**
     * @return the usuario
     */
    public CncrmUsuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(CncrmUsuario usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the ejbDireccionUsuario
     */
    public CncrmUsuarioDireccionFacade getEjbDireccionUsuario() {
        return ejbDireccionUsuario;
    }

    /**
     * @param ejbDireccionUsuario the ejbDireccionUsuario to set
     */
    public void setEjbDireccionUsuario(CncrmUsuarioDireccionFacade ejbDireccionUsuario) {
        this.ejbDireccionUsuario = ejbDireccionUsuario;
    }

    public String showUnidadNegocio() {
        details = new ArrayList();
        detailHeader = new ArrayList();
	String tableName  =  FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(TABLE_NAME_PARAM);
        unidadNegocioName = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(UNIDAD_NEGOCIO_NAME_PARAM);
        String conditionField = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(CONDITION_FIELD_PARAM);
        String conditionValue = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(CONDITION_VALUE_PARAM);
        
        String columns = ejbFacade.getSelectColumns(tableName);
        List records = new ArrayList();
        if(conditionField != null) {
            records = ejbFacade.getDetailWithCondition(current.getCncrmSearchUserResultFK().getIdUsuario(), conditionField, conditionValue, columns, tableName);
        } else {
            records = ejbFacade.getDetail(current.getCncrmSearchUserResultFK().getIdUsuario(), columns, tableName);
        }
        
        if(records.isEmpty()){
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("BuscarUsuarioCRMUnidadNegocio"),
                            new String[]{unidadNegocioName});
            return null;
        }
        List<String> columnList = Arrays.asList(columns.split(COLUMN_SEPARATOR));
        for (int n = 0; n < records.size() + 1; n++){
            if (n == 0){
                detailHeader.add("Campo");
            }else{
                if (records.size() == 1){
                    detailHeader.add("Valor");
                }else{
                    detailHeader.add("Valor " + n);
                }
            }
        }
        for (int j = 0; j < columnList.size(); j++){
            List<Object> parcial = new ArrayList();
            parcial.add(columnList.get(j));
            for (Object record : records) {
                Object[] objects = (Object[]) record;
                parcial.add(objects[j]);
            }
            details.add(parcial);
        }
        return "Detail";
    }

    /**
     * @return the details
     */
    public List<List> getDetails() {
        return details;
    }

    /**
     * @param details the details to set
     */
    public void setDetails(List<List> details) {
        this.details = details;
    }

    /**
     * @return the detailHeader
     */
    public List getDetailHeader() {
        return detailHeader;
    }

    /**
     * @param detailHeader the detailHeader to set
     */
    public void setDetailHeader(List detailHeader) {
        this.detailHeader = detailHeader;
    }

    /**
     * @return the unidadNegocioName
     */
    public String getUnidadNegocioName() {
        return unidadNegocioName;
    }

    /**
     * @param unidadNegocioName the unidadNegocioName to set
     */
    public void setUnidadNegocioName(String unidadNegocioName) {
        this.unidadNegocioName = unidadNegocioName;
    }
    
    public CncrmSearchUserResult getCncrmSearchUserResult(CncrmSearchUserResultFK cncrmSearchUserResultFK) {
        return ejbFacade.find(cncrmSearchUserResultFK);
    }
    
    @FacesConverter(forClass = CncrmSearchUserResult.class)
    public static class CncrmBuscarUsuarioControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CncrmBuscarUsuarioController controller = (CncrmBuscarUsuarioController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "cncrmBuscarUsuarioController");
            return controller.getCncrmSearchUserResult(getKey(value));
        }

        CncrmSearchUserResultFK getKey(String value) {
            CncrmSearchUserResultFK key;
            key = new CncrmSearchUserResultFK();
            String values[] = value.split(SEPARATOR_ESCAPED);
            key.setIdUsuario(Long.parseLong(values[0]));
            key.setIdMarca(Short.parseShort(values[1]));
            key.setIdProducto(Short.parseShort(values[2]));
            return key;
        }

        String getStringKey(CncrmSearchUserResultFK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getIdUsuario());
            sb.append(SEPARATOR);
            sb.append(value.getIdMarca());
            sb.append(SEPARATOR);
            sb.append(value.getIdProducto());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof CncrmSearchUserResult) {
                CncrmSearchUserResult o = (CncrmSearchUserResult) object;
                return getStringKey(o.getCncrmSearchUserResultFK());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + CncrmSearchUserResult.class.getName());
            }
        }

    }
}
