package es.condenast.jsf;

import es.condenast.jpa.entities.CnetlMaster;
import es.condenast.jpa.entities.CnetlMasterValor;
import es.condenast.jpa.entities.CnetlMasterValorPK;
import es.condenast.jpa.entities.CnetlOrigen;
import es.condenast.jpa.session.CnetlMasterFacade;
import es.condenast.jpa.session.CnetlMasterValorFacade;
import es.condenast.jpa.session.CnetlOrigenFacade;
import es.condenast.jsf.util.JsfUtil;
import es.condenast.jsf.util.PaginationHelper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Named;

@Named("cnetlMasterController")
@SessionScoped
public class CnetlMasterController implements Serializable {

    private CnetlMaster current;
    private DataModel items = null;
    @EJB
    private es.condenast.jpa.session.CnetlMasterFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    @EJB
    private es.condenast.jpa.session.CnetlOrigenFacade ejbOrigenFacade;

    @EJB
    private es.condenast.jpa.session.CnetlMasterValorFacade ejbMasterValorFacade;

    // Atributos formulario
    CnetlOrigen valorSeleccionadoBBDD;
    CnetlOrigen[] valoresBBDD;
    CnetlOrigen[] valoresBBDDMasterTipo;
    List<CnetlMasterValor> valoresOrigen;
    CnetlMasterValor valorSeleccionadoOrigen;

    // Estructura interna de origenes
    private HashMap<Integer, CnetlOrigen> origenesHM = new HashMap<>();
    private HashMap<Integer, CnetlOrigen> origenesMasterTipoHM = new HashMap<>();

    public CnetlMasterController() {
    }

    public CnetlMaster getSelected() {
        if (current == null) {
            current = new CnetlMaster();
            selectedItemIndex = -1;
        }
        return current;
    }

    private CnetlMasterFacade getFacade() {
        return ejbFacade;
    }

    public CnetlOrigenFacade getOrigenFacade() {
        return ejbOrigenFacade;
    }

    public CnetlMasterValorFacade getMasterValorFacade() {
        return ejbMasterValorFacade;
    }

    public void setOrigenFacade(CnetlOrigenFacade ejbOrigenFacade) {
        this.ejbOrigenFacade = ejbOrigenFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize() - 1}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (CnetlMaster) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        prepareCreateListas();

        current = new CnetlMaster();
        selectedItemIndex = -1;
        return "Create";
    }

    private void prepareCreateListas() {
        // Inicializar lista
        valoresBBDD = null;
        // Inicializar datatable
        valoresOrigen = new ArrayList<>();
    }

    public String create() {
        try {
            if (!validateFields()) {
                return null;
            }

            getFacade().create(current);
            createMasterValor();
            return prepareList();
        } catch (Throwable e) {
            String duplicateEntry = JsfUtil.isDuplicateEnty(e);
            if (duplicateEntry != null) {
                if (duplicateEntry.contains("UK_FUENTE_NOMBRE")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("MasterDatosClaveDuplicada"),
                            new String[]{current.getNombre(), ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("MasterDatosNombre")});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
            return null;
        }
    }

    private void createMasterValor() {

        // Valores personalizados
        for (CnetlMasterValor fvPersonalizados : valoresOrigen) {
            CnetlMasterValorPK fvPersonalizadosPK = new CnetlMasterValorPK(current.getIdMaster(), fvPersonalizados.getCnetlOrigen().getIdOrigen());
            fvPersonalizados.setCnetlMasterValorPK(fvPersonalizadosPK);
            getMasterValorFacade().create(fvPersonalizados);
        }
    }

    public String prepareEdit() {
        current = (CnetlMaster) getItems().getRowData();
        prepareEditListas();

        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    private void prepareEditListas() {
        // Inicializar datatable
        valoresOrigen = getMasterValorFacade().findByIdMaster(current.getIdMaster());

        // Lista de Valores de BBDD que no estan en el tipo de fuente
        List<CnetlOrigen> listaOrigenes = getOrigenFacade().findValoresPosiblesMaster(false, current.getIdMasterTipo().getIdMasterTipo());
        origenesHM = CnetlOrigen.toHashMapId(listaOrigenes);
        for (CnetlMasterValor valorOrigen : valoresOrigen) {
            listaOrigenes.remove(valorOrigen.getCnetlOrigen());

        }
        Collections.sort(listaOrigenes);

        // Lista de Valores de BBDD que estan en el tipo de fuente
        List<CnetlOrigen> listaOrigenesExistentes = getOrigenFacade().findValoresExistentesMaster(false, current.getIdMasterTipo().getIdMasterTipo());
        Collections.sort(listaOrigenesExistentes);
        origenesMasterTipoHM = CnetlOrigen.toHashMapId(listaOrigenesExistentes);

        listaOrigenesExistentes.toArray(new CnetlOrigen[listaOrigenesExistentes.size()]);
        listaOrigenesExistentes.addAll(listaOrigenes);

        valoresBBDD = listaOrigenesExistentes.toArray(new CnetlOrigen[listaOrigenesExistentes.size()]);

    }

    public String update() {
        try {
            if (!validateFields()) {
                return null;
            }

            getFacade().edit(current);
            performDestroyMasterValor();
            createMasterValor();

            return "List";
        } catch (Throwable e) {

            String duplicateEntry = JsfUtil.isDuplicateEnty(e);
            if (duplicateEntry != null) {
                if (duplicateEntry.contains("UK_FUENTE_NOMBRE")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("MasterDatosClaveDuplicada"),
                            new String[]{current.getNombre(), ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("MasterDatosNombre")});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
            return null;
        }
    }

    public String destroy() {
        current = (CnetlMaster) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            performDestroyMasterValor();
            getFacade().remove(current);

        } catch (Exception e) {
            String foreignKey = JsfUtil.isForeignKey(e);

            if (foreignKey != null) {
                if (foreignKey.contains("FK_FUENTE_VALOR_FUENTE")
                        || foreignKey.contains("FK_POOL_FUENTE")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("MasterDatosForeignKey"),
                            new String[]{current.getNombre()});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
        }
    }

    private void performDestroyMasterValor() {
        // Valores personalizados
        List<CnetlMasterValor> valores = getMasterValorFacade().findByIdMaster(current.getIdMaster());
        for (CnetlMasterValor fvPersonalizados : valores) {
            CnetlMasterValorPK fvPersonalizadosPK = new CnetlMasterValorPK(current.getIdMaster(), fvPersonalizados.getCnetlMasterValorPK().getIdOrigen());
            fvPersonalizados.setCnetlMasterValorPK(fvPersonalizadosPK);
            getMasterValorFacade().remove(fvPersonalizados);
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public String toPage(Integer page) {
        getPagination().toPage(page);
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public CnetlMaster getCnetlMaster(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = CnetlMaster.class)
    public static class CnetlMasterControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CnetlMasterController controller = (CnetlMasterController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "cnetlMasterController");
            return controller.getCnetlMaster(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof CnetlMaster) {
                CnetlMaster o = (CnetlMaster) object;
                return getStringKey(o.getIdMaster());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + CnetlMaster.class.getName());
            }
        }

    }

    public String insertOrigen() {

        // Añadir en datatable
        CnetlOrigen origenMaster = getValorSeleccionadoBBDD();
        
        if (valoresOrigen == null) {
            valoresOrigen = new ArrayList<>();
        }

        Integer idMaster = Integer.MIN_VALUE;
        if (current.getIdMaster() != null) {
            idMaster = current.getIdMaster();
        }

        CnetlMasterValorPK masterValorPK = new CnetlMasterValorPK();
        masterValorPK.setIdMaster(idMaster);
        masterValorPK.setIdOrigen(origenMaster.getIdOrigen());
        CnetlMasterValor fuenteValor = new CnetlMasterValor(masterValorPK);
        fuenteValor.setCnetlOrigen(origenMaster);
        valoresOrigen.add(fuenteValor);

        // Eliminar de la lista
        ArrayList<CnetlOrigen> listaOrigenes = new ArrayList<>(Arrays.asList(valoresBBDD));
        listaOrigenes.remove(origenMaster);

        // Construir lista resultante
        listaOrigenes.removeAll(origenesMasterTipoHM.values());
        Collections.sort(listaOrigenes);

        List<CnetlOrigen> listaOrigenesExistentes = new ArrayList<>();
        listaOrigenesExistentes.toArray(new CnetlOrigen[listaOrigenesExistentes.size()]);
        listaOrigenesExistentes.addAll(origenesMasterTipoHM.values());
        Collections.sort(listaOrigenesExistentes);

        listaOrigenes.addAll(0, listaOrigenesExistentes);

        valoresBBDD = listaOrigenes.toArray(new CnetlOrigen[listaOrigenes.size()]);

        return null;
    }

    public String removeOrigen() {
        // Eliminar del datatable
        valoresOrigen.remove(valorSeleccionadoOrigen);

        // Añadir en lista
        ArrayList<CnetlOrigen> listaOrigenes = new ArrayList<>(Arrays.asList(valoresBBDD));
        listaOrigenes.add(valorSeleccionadoOrigen.getCnetlOrigen());

        // Construir lista resultante
        listaOrigenes.removeAll(origenesMasterTipoHM.values());
        Collections.sort(listaOrigenes);

        List<CnetlOrigen> listaOrigenesExistentes = new ArrayList<>();
        listaOrigenesExistentes.toArray(new CnetlOrigen[listaOrigenesExistentes.size()]);
        listaOrigenesExistentes.addAll(origenesMasterTipoHM.values());
        Collections.sort(listaOrigenesExistentes);

        listaOrigenes.addAll(0, listaOrigenesExistentes);

        valoresBBDD = listaOrigenes.toArray(new CnetlOrigen[listaOrigenes.size()]);

        return null;
    }

    public CnetlOrigen[] getValoresBBDD() {
        return valoresBBDD;
    }

    public void setValoresBBDD(CnetlOrigen[] valoresBBDD) {
        this.valoresBBDD = valoresBBDD;
    }

    public CnetlOrigen getValorSeleccionadoBBDD() {
        return valorSeleccionadoBBDD;
    }

    public void setValorSeleccionadoBBDD(CnetlOrigen valorSeleccionadoBBDD) {
        this.valorSeleccionadoBBDD = valorSeleccionadoBBDD;
    }

    public CnetlMasterValor getValorSeleccionadoOrigen() {
        return valorSeleccionadoOrigen;
    }

    public void setValorSeleccionadoOrigen(CnetlMasterValor valorSeleccionadoOrigen) {
        this.valorSeleccionadoOrigen = valorSeleccionadoOrigen;
    }

    public List<CnetlMasterValor> getValoresOrigen() {
        return valoresOrigen;
    }

    public void setValoresOrigen(List<CnetlMasterValor> valoresOrigen) {
        this.valoresOrigen = valoresOrigen;
    }

    public boolean validateFields() {
        boolean valido = true;
        if (current.getNombre() == null || current.getNombre().length() == 0) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("MasterDatosNombreObligatorio"));
            valido = false;
        }
        if (current.getOrigen() == null || current.getOrigen().length() == 0) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("MasterDatosOrigenObligatorio"));
            valido = false;
        }
        if (current.getIdMasterTipo() == null) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("MasterDatosMasterTipoObligatorio"));
            valido = false;
        }

        return valido;
    }

    public void showDescription() {
        FacesContext context = FacesContext.getCurrentInstance();

        String id = context.getExternalContext().getRequestParameterMap().get("param");
        String descripcion = origenesHM.get(Integer.valueOf(id)).getDescripcion();
        context.addMessage(null, new FacesMessage("", descripcion));;
    }

    public boolean isDisabled(CnetlOrigen origen) {
        return origenesMasterTipoHM.containsKey(origen.getIdOrigen());
    }

    public void changeMasterTipo() {
        FacesContext context = FacesContext.getCurrentInstance();

        String idTipoMaster = context.getExternalContext().getRequestParameterMap().get("param");

        if (idTipoMaster.length() > 0) {
            // Lista de Valores de BBDD que no estan en el tipo de fuente
            List<CnetlOrigen> listaOrigenes = getOrigenFacade().findValoresPosiblesMaster(false, Short.parseShort(idTipoMaster));
            origenesHM = CnetlOrigen.toHashMapId(listaOrigenes);
            Collections.sort(listaOrigenes);

            // Lista de Valores de BBDD que estan en el tipo de fuente
            List<CnetlOrigen> listaOrigenesExistentes = getOrigenFacade().findValoresExistentesMaster(false, Short.parseShort(idTipoMaster));
            Collections.sort(listaOrigenesExistentes);
            origenesMasterTipoHM = CnetlOrigen.toHashMapId(listaOrigenesExistentes);

            listaOrigenesExistentes.toArray(new CnetlOrigen[listaOrigenesExistentes.size()]);
            listaOrigenesExistentes.addAll(listaOrigenes);

            valoresBBDD = listaOrigenesExistentes.toArray(new CnetlOrigen[listaOrigenesExistentes.size()]);
        } else {
            valoresBBDD = null;
        }

        // Inicializar datatable
        valoresOrigen = new ArrayList<>();
    }
}
