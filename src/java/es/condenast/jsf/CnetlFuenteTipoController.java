package es.condenast.jsf;

import es.condenast.jpa.entities.CnetlFuenteTipo;
import es.condenast.jpa.entities.CnetlFuenteTipoOrigen;
import es.condenast.jpa.entities.CnetlFuenteTipoOrigenPK;
import es.condenast.jpa.entities.CnetlOrigen;
import es.condenast.jpa.session.CnetlFuenteTipoFacade;
import es.condenast.jpa.session.CnetlFuenteTipoOrigenFacade;
import es.condenast.jpa.session.CnetlOrigenFacade;
import es.condenast.jsf.util.JsfUtil;
import es.condenast.jsf.util.PaginationHelper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;

@Named("cnetlFuenteTipoController")
@SessionScoped
public class CnetlFuenteTipoController implements Serializable {

    private CnetlFuenteTipo current;
    private DataModel items = null;

    // Atributo para picker
    private DualListModel<CnetlOrigen> origenes;

    // Estructura interna de origenes
    private HashMap<String, CnetlOrigen> origenesHM = new HashMap<>();

    @EJB
    private es.condenast.jpa.session.CnetlFuenteTipoFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    @EJB
    private es.condenast.jpa.session.CnetlOrigenFacade ejbOrigenFacade;

    @EJB
    private es.condenast.jpa.session.CnetlFuenteTipoOrigenFacade ejbFuenteTipoOrigenFacade;

    public CnetlFuenteTipoController() {
    }

    public CnetlFuenteTipo getSelected() {
        if (current == null) {
            current = new CnetlFuenteTipo();
            selectedItemIndex = -1;
        }
        return current;
    }

    private CnetlFuenteTipoFacade getFacade() {
        return ejbFacade;
    }

    private CnetlOrigenFacade getOrigenFacade() {
        return ejbOrigenFacade;
    }

    private CnetlFuenteTipoOrigenFacade getFuenteTipoOrigenFacade() {
        return ejbFuenteTipoOrigenFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize() - 1}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (CnetlFuenteTipo) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new CnetlFuenteTipo();
        selectedItemIndex = -1;
        prepareCreateOrigenes();
        return "Create";
    }

    private void prepareCreateOrigenes() {
        //Listas de Origenes
        ArrayList<CnetlOrigen> origenesSource = new ArrayList<>(getOrigenFacade().findAll());
        Collections.sort(origenesSource);
        origenesHM = CnetlOrigen.toHashMap(origenesSource);

        List<CnetlOrigen> origenesTarget = new ArrayList<>();
        origenes = new DualListModel<>(origenesSource, origenesTarget);
    }

    public String create() {
        try {
            getFacade().create(current);
            current.setCnetlFuenteTipoOrigenCollection(getOrigenesTarget());
            getFacade().edit(current);

            return prepareList();

        } catch (Throwable e) {

            String duplicateEntry = JsfUtil.isDuplicateEnty(e);
            if (duplicateEntry != null) {
                if (duplicateEntry.contains("UK_FUENTE_TIPO_NOMBRE")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TipoFuenteDatosClaveDuplicada"),
                            new String[]{current.getNombre(), ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TipoFuenteDatosNombre")});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
            return null;
        }
    }

    public Collection<CnetlFuenteTipoOrigen> getOrigenesTarget() {
        List<CnetlOrigen> origenesTarget = getOrigenes().getTarget();
        Collection<CnetlFuenteTipoOrigen> fuenteTipoOrigenes = new ArrayList<>();
        short orden = 1;
        for (CnetlOrigen origenTarget : origenesTarget) {
            CnetlFuenteTipoOrigenPK fuenteTipoOrigenPK = new CnetlFuenteTipoOrigenPK(current.getIdFuenteTipo(), origenTarget.getIdOrigen());
            CnetlFuenteTipoOrigen fuenteTipoOrigen = new CnetlFuenteTipoOrigen(fuenteTipoOrigenPK, orden++);
            fuenteTipoOrigen.setCnetlOrigen(origenTarget);
            fuenteTipoOrigen.setCnetlFuenteTipo(current);
            fuenteTipoOrigenes.add(fuenteTipoOrigen);
        }

        return fuenteTipoOrigenes;

    }

    public String prepareEdit() {
        CnetlFuenteTipo fuenteTipo = (CnetlFuenteTipo) getItems().getRowData();
        current = ejbFacade.find(fuenteTipo.getIdFuenteTipo());
        current.setCnetlFuenteTipoOrigenCollection(ejbFuenteTipoOrigenFacade.findByIdFuenteTipo(current.getIdFuenteTipo()));

        prepareEditOrigenes();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    private void prepareEditOrigenes() {
        //Listas de Origenes Fuente 
        ArrayList<CnetlOrigen> origenesSource = new ArrayList<>(getOrigenFacade().findAll());
        Collections.sort(origenesSource);
        origenesHM = CnetlOrigen.toHashMap(origenesSource);

        // Lista de Origenes Destino
        List<CnetlOrigen> origenesTarget = new ArrayList<>();
        List<CnetlFuenteTipoOrigen> fuentes = getFuenteTipoOrigenFacade().findByIdFuenteTipo(current.getIdFuenteTipo());

        for (CnetlFuenteTipoOrigen fuenteTipoOrigen : fuentes) {
            origenesTarget.add(fuenteTipoOrigen.getCnetlOrigen());
        }

        origenesSource.removeAll(origenesTarget);

        origenes = new DualListModel<>(origenesSource, origenesTarget);
    }

    public String update() {
        try {
            getFacade().edit(current);
            performDestroyOrigenes();
            createFuenteTipoOrigen();

            recreateModel();
            return "List";

        } catch (Throwable e) {

            String duplicateEntry = JsfUtil.isDuplicateEnty(e);
            if (duplicateEntry != null) {
                if (duplicateEntry.contains("UK_FUENTE_TIPO_NOMBRE")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TipoFuenteDatosClaveDuplicada"),
                            new String[]{current.getNombre(), ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TipoFuenteDatosNombre")});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
            return null;
        }
    }

    private void createFuenteTipoOrigen() {
        for (CnetlFuenteTipoOrigen origen : getOrigenesTarget()) {
            getFuenteTipoOrigenFacade().create(origen);
        }
    }

    public String destroy() {
        current = (CnetlFuenteTipo) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            performDestroyOrigenes();
            getFacade().remove(current);
        } catch (Exception e) {
            String foreignKey = JsfUtil.isForeignKey(e);

            if (foreignKey != null) {
                if (foreignKey.contains("FK_FUENTE_FUENTE_TIPO")
                        || foreignKey.contains("FK_FUENTE_TIPO_ORIGEN_FUENTE_TIPO")
                        || foreignKey.contains("FK_POOL_FUENTE_TIPO")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TipoFuenteDatosForeignKey"),
                            new String[]{current.getNombre()});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
        }
    }

    private void performDestroyOrigenes() {
        getFuenteTipoOrigenFacade().removeByIdFuenteTipo(current.getIdFuenteTipo());
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public String toPage(Integer page) {
        getPagination().toPage(page);
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public CnetlFuenteTipo getCnetlFuenteTipo(java.lang.Short id) {
        return ejbFacade.find(id);
    }

    public void onElementDualListTransfer(TransferEvent event) {
        if (event.isRemove()) {
            ArrayList<CnetlOrigen> origenesSource = new ArrayList<>(getOrigenes().getSource());
            Collections.sort(origenesSource);
            origenes = new DualListModel<>(origenesSource, origenes.getTarget());
        }
    }

    public void showDescription() {
        FacesContext context = FacesContext.getCurrentInstance();

        String nombre = context.getExternalContext().getRequestParameterMap().get("param");
        String descripcion = origenesHM.get(nombre).getDescripcion();
        context.addMessage(null, new FacesMessage("", descripcion));;
    }

    @FacesConverter(forClass = CnetlFuenteTipo.class)
    public static class CnetlFuenteTipoControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CnetlFuenteTipoController controller = (CnetlFuenteTipoController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "cnetlFuenteTipoController");
            return controller.getCnetlFuenteTipo(getKey(value));
        }

        java.lang.Short getKey(String value) {
            java.lang.Short key;
            key = Short.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Short value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof CnetlFuenteTipo) {
                CnetlFuenteTipo o = (CnetlFuenteTipo) object;
                return getStringKey(o.getIdFuenteTipo());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + CnetlFuenteTipo.class.getName());
            }
        }

    }

    public DualListModel<CnetlOrigen> getOrigenes() {
        return origenes;
    }

    public void setOrigenes(DualListModel<CnetlOrigen> origenes) {
        this.origenes = origenes;
    }

}
