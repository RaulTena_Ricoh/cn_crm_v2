/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jsf;

import es.condenast.jpa.entities.CnmasRangoEdad;
import es.condenast.jpa.session.CnmasRangoEdadFacade;
import es.condenast.jsf.util.JsfUtil;
import es.condenast.jsf.util.PaginationHelper;
import es.condenast.jsf.util.Range;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Named;

/**
 *
 * @author ivan.fernandez
 */
@Named("cnmasRangoEdadController")
@SessionScoped
public class CnmasRangoEdadController implements Serializable{
    private CnmasRangoEdad current;
    private DataModel items = null;
    @EJB
    private es.condenast.jpa.session.CnmasRangoEdadFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public CnmasRangoEdadController() {
    }

    public CnmasRangoEdad getSelected() {
        if (current == null) {
            current = new CnmasRangoEdad();
            selectedItemIndex = -1;
        }
        return current;
    }

    private CnmasRangoEdadFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize() - 1}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (CnmasRangoEdad) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new CnmasRangoEdad();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            if (!isValidRango()) {
                return null;
            }
            getFacade().create(current);
            return prepareList();
        } catch (Exception e) {
            String duplicateEntry = JsfUtil.isDuplicateEnty(e);
            if (duplicateEntry != null) {
                if (duplicateEntry.contains("UK_RANGO_EDAD_NOMBRE")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("RangoEdadClaveDuplicada"),
                            new String[]{current.getNombre(), ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("RangoEdadNombre")});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
            return null;
        }
    }

    public String prepareEdit() {
        current = (CnmasRangoEdad) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            if (!isValidRango()) {
                return null;
            }
            getFacade().edit(current);
            return prepareList();
        } catch (Exception e) {
            String duplicateEntry = JsfUtil.isDuplicateEnty(e);
            if (duplicateEntry != null) {
                if (duplicateEntry.contains("UK_RANGO_EDAD_NOMBRE")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("RangoEdadClaveDuplicada"),
                            new String[]{current.getNombre(), ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("RangoEdadNombre")});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
            return null;
        }
    }

    public String destroy() {
        current = (CnmasRangoEdad) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);

        } catch (Exception e) {
            String foreignKey = JsfUtil.isForeignKey(e);
            JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            /*if (foreignKey != null) {
                if (foreignKey.contains("FK_PERSONA_NIVEL_INGRESO")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("RangoEdadForeignKey"),
                            new String[]{current.getNombre()});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }*/
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public String toPage(Integer page) {
        getPagination().toPage(page);
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public CnmasRangoEdad getCnmasRangoEdad(java.lang.Short id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = CnmasRangoEdad.class)
    public static class CnmasRangoEdadControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CnmasRangoEdadController controller = (CnmasRangoEdadController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "cnmasRangoEdadController");
            return controller.getCnmasRangoEdad(getKey(value));
        }

        java.lang.Short getKey(String value) {
            java.lang.Short key;
            key = Short.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Short value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof CnmasRangoEdad) {
                CnmasRangoEdad o = (CnmasRangoEdad) object;
                return getStringKey(o.getIdRango());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + CnmasRangoEdad.class.getName());
            }
        }
    }
    
    private boolean isValidRango() {

        Double currentHasta = (current.getHasta() == null) ? Short.MAX_VALUE : current.getHasta().doubleValue();
        
        Range actual = new Range(current.getDesde().doubleValue(), currentHasta);

        if (!actual.isValidFirst()) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("RangoEdadErroneo"),
                    new String[]{ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("RangoEdadDesde")});
            return false;

        }

        if (!actual.isValidLast()) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("RangoEdadErroneo"),
                    new String[]{ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("RangoEdadHasta")});
            return false;

        }

        if (!actual.isValidRange()) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("RangoEdadHastaMenorDesde"),
                    new String[]{ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("RangoEdadHasta"), current.getHasta().toString(),
                        ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("RangoEdadDesde"), current.getDesde().toString()});
            return false;
        }

        // Comprobar solapamiento 
        List<CnmasRangoEdad> nis = getFacade().findAllOrderByDesde();

        ArrayList<Range> niveles = new ArrayList<>();
        for (CnmasRangoEdad ni : nis) {
            if (!ni.getIdRango().equals(current.getIdRango())) {
                Double nih = (ni.getHasta() == null) ? Double.MAX_VALUE : ni.getHasta().doubleValue();
                niveles.add(new Range(ni.getDesde().doubleValue(), nih));
            }
        }

        Range rangoSolapamiento = actual.isOverLappedByOpen(niveles);
        if (rangoSolapamiento != null) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("RangoEdadSolapamiento"),
                    new String[]{rangoSolapamiento.getFirst().toString(),
                        rangoSolapamiento.getLast() == Double.MAX_VALUE ? "" : rangoSolapamiento.getLast().toString()});
            return false;
        }

        // Comprobar huecos libres
        niveles.add(actual);
        if (Range.isGapClosed(niveles)) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("RangoEdadValoresCubrir"));
            return false;
        }

        return true;
    }
}
