package es.condenast.jsf;

import es.condenast.jpa.entities.CnmasUsuarioTipo;
import es.condenast.jpa.session.CnmasUsuarioTipoFacade;
import es.condenast.jsf.util.JsfUtil;
import es.condenast.jsf.util.PaginationHelper;
import es.condenast.jsf.util.Range;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Named;

@Named("cnmasUsuarioTipoController")
@SessionScoped
public class CnmasUsuarioTipoController implements Serializable {

    private CnmasUsuarioTipo current;
    private DataModel items = null;
    @EJB
    private es.condenast.jpa.session.CnmasUsuarioTipoFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public CnmasUsuarioTipoController() {
    }

    public CnmasUsuarioTipo getSelected() {
        if (current == null) {
            current = new CnmasUsuarioTipo();
            selectedItemIndex = -1;
        }
        return current;
    }

    private CnmasUsuarioTipoFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize() - 1}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (CnmasUsuarioTipo) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new CnmasUsuarioTipo();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            if (!isValidMarca()) {
                return null;
            }

            if (!isValidCN()) {
                return null;
            }

            getFacade().create(current);
            return prepareList();
        } catch (Exception e) {
            String duplicateEntry = JsfUtil.isDuplicateEnty(e);
            if (duplicateEntry != null) {
                if (duplicateEntry.contains("UK_USUARIO_TIPO_NOMBRE")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioClaveDuplicada"),
                            new String[]{current.getNombre(), ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioNombre")});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
            return null;
        }
    }

    public String prepareEdit() {
        current = (CnmasUsuarioTipo) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            if (!isValidMarca()) {
                return null;
            }

            if (!isValidCN()) {
                return null;
            }

            getFacade().edit(current);
            return prepareList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
//            String duplicateEntry = JsfUtil.isDuplicateEnty(e);
//            if (duplicateEntry != null) {
//                if (duplicateEntry.contains("UK_USUARIO_TIPO_NOMBRE")) {
//                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioClaveDuplicada"),
//                            new String[]{current.getNombre(), ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioNombre")});
//                }
//            } else {
//                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
//            }
//            return null;
        }
    }

    public String destroy() {
        current = (CnmasUsuarioTipo) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
        } catch (Exception e) {
            String foreignKey = JsfUtil.isForeignKey(e);

            if (foreignKey != null) {
                if (foreignKey.contains("FK_USUARIO_USUARIO_TIPO")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioForeignKey"),
                            new String[]{current.getNombre()});
                } else if (foreignKey.contains("FK_USUARIO_MARCA_USUARIO_TIPO")) {
                    JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioForeignKey"),
                            new String[]{current.getNombre()});
                }
            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("resources/Messages").getString("PersistenceErrorOccured"));
            }
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public String toPage(Integer page) {
        getPagination().toPage(page);
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public CnmasUsuarioTipo getCnmasUsuarioTipo(java.lang.Short id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = CnmasUsuarioTipo.class)
    public static class CnmasUsuarioTipoControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CnmasUsuarioTipoController controller = (CnmasUsuarioTipoController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "cnmasUsuarioTipoController");
            return controller.getCnmasUsuarioTipo(getKey(value));
        }

        java.lang.Short getKey(String value) {
            java.lang.Short key;
            key = Short.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Short value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof CnmasUsuarioTipo) {
                CnmasUsuarioTipo o = (CnmasUsuarioTipo) object;
                return getStringKey(o.getIdUsuarioTipo());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + CnmasUsuarioTipo.class.getName());
            }
        }

    }

    private boolean isValidMarca() {

        Double currentMarcaHasta = (current.getMarcaHasta() == null) ? Double.MAX_VALUE : current.getMarcaHasta().doubleValue();

        Range actual = new Range(current.getMarcaDesde().doubleValue(), currentMarcaHasta);

        if (!actual.isValidFirst()) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioErroneo"),
                    new String[]{ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioDesde")});
            return false;

        }

        if (!actual.isValidLast()) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioErroneo"),
                    new String[]{ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioHasta")});
            return false;

        }

        if (!actual.isValidRange()) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioMenorDesde"),
                    new String[]{ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioHasta"), current.getMarcaHasta().toString(),
                        ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioDesde"), current.getMarcaDesde().toString()});
            return false;
        }

        // Comprobar solapamiento 
        List<CnmasUsuarioTipo> uts = getFacade().findAllOrderByMarcaDesde();

        ArrayList<Range> marcas = new ArrayList<>();
        for (CnmasUsuarioTipo ut : uts) {
            if (!ut.getIdUsuarioTipo().equals(current.getIdUsuarioTipo())) {
                Double utmh = (ut.getMarcaHasta() == null) ? Double.MAX_VALUE : ut.getMarcaHasta().doubleValue();
System.out.println("------------> Rango --> (" + ut.getMarcaDesde()+"," + utmh+")");
                marcas.add(new Range(ut.getMarcaDesde().doubleValue(), utmh));
            }
        }

        Range rangoSolapamiento = actual.isOverLappedByClosed(marcas);
        if (rangoSolapamiento != null) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioSolapamiento"),
                    new String[]{ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioMarcas"),
                        rangoSolapamiento.getFirst().toString(),
                        rangoSolapamiento.getLast() == Double.MAX_VALUE ? "" : rangoSolapamiento.getLast().toString()});
            return false;
        }

        // Comprobar huecos libres
        marcas.add(actual);
        if (Range.isGapClosed(marcas)) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioValoresCubrir"),
                    new String[] {ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioMarcas")});
            return false;

        }
        return true;
    }

    private boolean isValidCN() {

        Double currentCnHasta = (current.getCnHasta() == null) ? Double.MAX_VALUE : current.getCnHasta().doubleValue();

        Range actual = new Range(current.getCnDesde().doubleValue(), currentCnHasta);

        if (!actual.isValidFirst()) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioErroneo"),
                    new String[]{ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioDesde")});
            return false;

        }

        if (!actual.isValidLast()) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioErroneo"),
                    new String[]{ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioHasta")});
            return false;

        }

        if (!actual.isValidRange()) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioMenorDesde"),
                    new String[]{ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioHasta"), current.getCnHasta().toString(),
                        ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioDesde"), current.getCnDesde().toString()});
            return false;
        }

        // Comprobar solapamiento 
        List<CnmasUsuarioTipo> uts = getFacade().findAllOrderByCNDesde();

        ArrayList<Range> cns = new ArrayList<>();
        for (CnmasUsuarioTipo ut : uts) {
            if (!ut.getIdUsuarioTipo().equals(current.getIdUsuarioTipo())) {
                Double utcnh = (ut.getCnHasta() == null) ? Double.MAX_VALUE : ut.getCnHasta().doubleValue();
                cns.add(new Range(ut.getCnDesde().doubleValue(), utcnh));
            }
        }

        Range rangoSolapamiento = actual.isOverLappedByOpen(cns);
        if (rangoSolapamiento != null) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioSolapamiento"),
                    new String[]{ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioCNs"),
                        rangoSolapamiento.getFirst().toString(),
                        rangoSolapamiento.getLast() == Double.MAX_VALUE ? "" : rangoSolapamiento.getLast().toString()});
            return false;
        }

        // Comprobar huecos libres
        cns.add(actual);
        if (Range.isGapOpen(cns)) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioValoresCubrir"),
                    new String[] {ResourceBundle.getBundle(JsfUtil.MESSAGES).getString("TiposUsuarioCNs")});
            return false;

        }
        return true;
    }

}
