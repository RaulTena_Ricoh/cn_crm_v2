/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.borrado;

import es.condenast.jpa.session.CncrmBorradoInactivosFacade;

/**
 *
 * @author Raul.Tena
 */
public class ProcesoBorrado {
    
    private static CncrmBorradoInactivosFacade ejbBorradoInactivosFacade;

    public static final int PROCESO_OK = 0;
    public static final int PROCESO_ERROR = 1;
    public static final int PROCESO_ERROR_CORREO = 2;
    
    public static final int AVISO_BORRADO = 1;
    public static final int BORRADO = 2;
    
    private static int resultado;
    private static int tipoBorrado;

    public static void enviarAviso() {
        setTipoBorrado(AVISO_BORRADO);
        HandlerBorrado handler = new HandlerBorrado(ejbBorradoInactivosFacade);
        handler.sendEmailAviso();
    }
    
    public static void ejecutarBorradoInactivos() {
        setTipoBorrado(BORRADO);
        HandlerBorrado handler = new HandlerBorrado(ejbBorradoInactivosFacade);
        handler.borrarInactivos();
    }
    
    public static CncrmBorradoInactivosFacade getEjbBorradoInactivosFacade() {
        return ejbBorradoInactivosFacade;
    }

    public static void setEjbBorradoInactivosFacade(CncrmBorradoInactivosFacade ejbBorradoInactivosFacade) {
        ProcesoBorrado.ejbBorradoInactivosFacade = ejbBorradoInactivosFacade;
    }

    public static int getResultado() {
        return resultado;
    }

    public static void setResultado(int resultado) {
        ProcesoBorrado.resultado = resultado;
    }

    public static int getTipoBorrado() {
        return tipoBorrado;
    }

    public static void setTipoBorrado(int tipoBorrado) {
        ProcesoBorrado.tipoBorrado = tipoBorrado;
    }

}
