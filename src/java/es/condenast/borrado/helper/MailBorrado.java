/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.borrado.helper;

import es.condenast.borrado.ProcesoBorrado;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class MailBorrado {

//    @Resource(name = "mailSession") 
//    private Session mailSession;
    private static final String MAIL_SMTP_HOST = "mail.smtp.host";
    private static final String MAIL_SMTP_PORT = "mail.smtp.port";
    private static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
    private static final String MAIL_SMTP_STARTLS_ENABLE = "mail.smtp.starttls.enable";
    private static final String MAIL_SMTP_USER = "mail.user";
    private static final String MAIL_SMTP_PASSWORD = "mail.password";
    private static final String MAIL_TO_ADDRESS = "mail.toAddress";
    private static final String MAIL_SUBJECT_AVISO = "mail.subject.borrado.aviso";
    private static final String MAIL_SUBJECT_BORRADO = "mail.subject.borrado.ejecucion";
    private static final String MAIL_MESSAGE_AVISO = "mail.message.borrado.aviso";
    private static final String MAIL_MESSAGE_BORRADO = "mail.message.borrado.ejecucion";

    private static final DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

    static final Properties properties = new Properties();
    static final String userName;
    static final String password;
    static final String toAddress;
    static final String subject;
    static final String message;

    static {

        userName = ResourceBundle.getBundle("resources/Configuration").getString(MAIL_SMTP_USER);
        password = ResourceBundle.getBundle("resources/Configuration").getString(MAIL_SMTP_PASSWORD);

        toAddress = ResourceBundle.getBundle("resources/Configuration").getString(MAIL_TO_ADDRESS);

        if (ProcesoBorrado.getTipoBorrado() == ProcesoBorrado.AVISO_BORRADO) {
            subject = ResourceBundle.getBundle("resources/Configuration").getString(MAIL_SUBJECT_AVISO);
            message = ResourceBundle.getBundle("resources/Configuration").getString(MAIL_MESSAGE_AVISO);
        } else {
            subject = ResourceBundle.getBundle("resources/Configuration").getString(MAIL_SUBJECT_BORRADO);
            message = ResourceBundle.getBundle("resources/Configuration").getString(MAIL_MESSAGE_BORRADO);
        }

        properties.put(MAIL_SMTP_HOST, ResourceBundle.getBundle("resources/Configuration").getString(MAIL_SMTP_HOST));
        properties.put(MAIL_SMTP_PORT, ResourceBundle.getBundle("resources/Configuration").getString(MAIL_SMTP_PORT));
        properties.put(MAIL_SMTP_AUTH, ResourceBundle.getBundle("resources/Configuration").getString(MAIL_SMTP_AUTH));
        properties.put(MAIL_SMTP_STARTLS_ENABLE, ResourceBundle.getBundle("resources/Configuration").getString(MAIL_SMTP_STARTLS_ENABLE));
        properties.put(MAIL_SMTP_USER, userName);
        properties.put(MAIL_SMTP_PASSWORD, password);
    }

    public static void sendInactiveUserEmail(List<String> inactiveUsers, String fechaBorrado, List<String> sToAddresses) throws MessagingException {
    
        Session session = Session.getInstance(properties);

        Message msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = new InternetAddress[sToAddresses.size()];
        for(int i = 0; i < sToAddresses.size(); i++) {
            toAddresses[i] = new InternetAddress(sToAddresses.get(i));
        }
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject(subject.replace("%1%", String.valueOf(inactiveUsers.size()))
                              .replace("%2%", fechaBorrado));
        msg.setSentDate(new Date());
        
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        StringBuilder mensaje = new StringBuilder();
        mensaje.append(message);
        mensaje.append("\n\n");
        for(String registro : inactiveUsers) {
            mensaje.append(registro);
            mensaje.append("\n");
        }
        messageBodyPart.setContent(mensaje.toString(), "text/plain");
        
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        msg.setContent(multipart);
        
        System.out.println(mensaje);

        Transport.send(msg);
    }

}
