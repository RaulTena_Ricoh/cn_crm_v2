/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.borrado;

import es.condenast.borrado.helper.MailBorrado;
import es.condenast.jpa.entities.CncrmBorradoInactivos;
import es.condenast.jpa.session.CncrmBorradoInactivosFacade;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;

/**
 *
 * @author Raul.Tena
 */
public class HandlerBorrado {
    
    private CncrmBorradoInactivosFacade ejbBorradoInactivosFacade;

    public HandlerBorrado(CncrmBorradoInactivosFacade ejbBorradoInactivosFacade) {
        this.ejbBorradoInactivosFacade = ejbBorradoInactivosFacade;
    }
    
    public void sendEmailAviso() {
        try {
            System.out.println("INICIO - SendEmailAviso");
            List resultados = getEjbBorradoInactivosFacade().findEmailAviso();
            if(resultados.size() > 0) {
                CncrmBorradoInactivos proceso = getEjbBorradoInactivosFacade().findByFechaCambio();
                String[] sAddresses = proceso.getDestinatarios().split(";");
                List<String> toAddresses = Arrays.asList(sAddresses);
                                
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.add(Calendar.DAY_OF_YEAR, (proceso.getSemanasAviso() * 7));
                Date fechaBorrado = calendar.getTime();
                
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                String sFechaBorrado = df.format(fechaBorrado);
            
                List<String> inactiveUsers = new ArrayList<>();
                for(int i = 0; i < resultados.size(); i++) {
                    Object[] obj = (Object[]) resultados.get(i);
                    String sObj = obj[0].toString();
                    // FORMATO: email[fecha
                    inactiveUsers.add(sObj + "[" + sFechaBorrado);
                }
                
                MailBorrado.sendInactiveUserEmail(inactiveUsers, sFechaBorrado, toAddresses);
            } else {
                System.out.println("No hay usuarios inactivos.");
                Logger.getLogger(HandlerBorrado.class.getName()).log(Level.INFO, "No hay usuarios inactivos.");
            }
            ProcesoBorrado.setResultado(ProcesoBorrado.PROCESO_OK);
            System.out.println("FIN - SendEmailAviso");
        } catch(IOException | MessagingException ex) {
            System.out.println("FIN - SendEmailAviso EMAIL ERRONEO");
            ProcesoBorrado.setResultado(ProcesoBorrado.PROCESO_ERROR_CORREO);
            Logger.getLogger(HandlerBorrado.class.getName()).log(Level.SEVERE, null, ex);
        } catch(Exception ex) {
            System.out.println("FIN - SendEmailAviso ERRONEO");
            ProcesoBorrado.setResultado(ProcesoBorrado.PROCESO_ERROR);
            Logger.getLogger(HandlerBorrado.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void borrarInactivos() {
        try {
            System.out.println("INICIO - BorrarInactivos");
            List resultados = getEjbBorradoInactivosFacade().deleteInactiveUsers();
            if(resultados.size() > 0) {
                CncrmBorradoInactivos proceso = getEjbBorradoInactivosFacade().findByFechaCambio();
                String[] sAddresses = proceso.getDestinatarios().split(";");
                List<String> toAddresses = Arrays.asList(sAddresses);
                
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                String sFechaBorrado = df.format(new Date());
            
                List<String> inactiveUsers = new ArrayList<>();
                for(int i = 0; i < resultados.size(); i++) {
                    Object[] obj = (Object[]) resultados.get(i);
                    String sObj = obj[0].toString();
                    // FORMATO: email[fecha
                    inactiveUsers.add(sObj + "[" + sFechaBorrado);
                }
                
                MailBorrado.sendInactiveUserEmail(inactiveUsers, sFechaBorrado, toAddresses);
            } else {
                System.out.println("No hay usuarios inactivos.");
                Logger.getLogger(HandlerBorrado.class.getName()).log(Level.INFO, "No hay usuarios inactivos.");
            }
            ProcesoBorrado.setResultado(ProcesoBorrado.PROCESO_OK);
            System.out.println("FIN - BorrarInactivos");
        } catch(IOException | MessagingException ex) {
            System.out.println("FIN - BorrarInactivos EMAIL ERRONEO");
            ProcesoBorrado.setResultado(ProcesoBorrado.PROCESO_ERROR_CORREO);
            Logger.getLogger(HandlerBorrado.class.getName()).log(Level.SEVERE, null, ex);
        } catch(Exception ex) {
            System.out.println("FIN - BorrarInactivos ERRONEO");
            ProcesoBorrado.setResultado(ProcesoBorrado.PROCESO_ERROR);
            Logger.getLogger(HandlerBorrado.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public CncrmBorradoInactivosFacade getEjbBorradoInactivosFacade() {
        return ejbBorradoInactivosFacade;
    }

    public void setEjbBorradoInactivosFacade(CncrmBorradoInactivosFacade ejbBorradoInactivosFacade) {
        this.ejbBorradoInactivosFacade = ejbBorradoInactivosFacade;
    }

}
