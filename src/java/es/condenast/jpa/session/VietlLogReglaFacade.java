/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.VietlLogRegla;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class VietlLogReglaFacade extends AbstractFacade<VietlLogRegla> {

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    public VietlLogReglaFacade() {
        super(VietlLogRegla.class);
    }

    public List<VietlLogRegla> findByIdPool(Long idPool) {
        Query query = getEntityManager().createNamedQuery("VietlLogRegla.findByIdPool");
        query.setParameter("idPool", idPool);
        return query.getResultList();
    }

}
