/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnmasMarca;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class CnmasMarcaFacade extends AbstractFacade<CnmasMarca> {
    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CnmasMarcaFacade() {
        super(CnmasMarca.class);
    }
    
    public CnmasMarca findByIdMarca(Short idMarca) {
        Query query = getEntityManager().createNamedQuery("CnmasMarca.findByIdMarca");
        query.setParameter("idMarca", idMarca);
        return (CnmasMarca) query.getSingleResult();
    }

    @Override
    public List<CnmasMarca> findRange(int[] range) {
        javax.persistence.Query q =getEntityManager().createNamedQuery("CnmasMarca.findAll");
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
    
    @Override
    public List<CnmasMarca> findAll() {
        Query query = getEntityManager().createNamedQuery("CnmasMarca.findAll");
        return query.getResultList();
    }    
    
}
