/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.borrado.ProcesoBorrado;
import es.condenast.jpa.entities.CncrmBorradoInactivos;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author Raul.Tena
 */
@Stateless
public class CncrmBorradoInactivosFacade extends AbstractFacade<CncrmBorradoInactivos> {
    
    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }
    
    public CncrmBorradoInactivosFacade() {
        super(CncrmBorradoInactivos.class);
    }
    
    public CncrmBorradoInactivos findByFechaCambio() throws Exception {
        CncrmBorradoInactivos proceso = new CncrmBorradoInactivos();
        try {
            if(ProcesoBorrado.getTipoBorrado() != 0) {
                getEntityManager().getTransaction().begin();
            }
            Query query = getEntityManager().createNamedQuery("CncrmBorradoInactivos.findByFechaCambio");
            query.setMaxResults(1);
            proceso = (CncrmBorradoInactivos) query.getResultList().get(0);
            if(ProcesoBorrado.getTipoBorrado()  != 0) {
                getEntityManager().getTransaction().commit();
            }
        } catch (Exception e) {
            if(ProcesoBorrado.getTipoBorrado()  != 0) {
                getEntityManager().getTransaction().rollback();
            }
            System.out.println("Excepcion findByFechaCambio " + e.toString());
            throw new Exception();
        }
        
        return proceso;
    }
    
    public List findEmailAviso() throws Exception {
        List resultados = new ArrayList();
        try {
            if(ProcesoBorrado.getTipoBorrado() == ProcesoBorrado.AVISO_BORRADO) {
                getEntityManager().getTransaction().begin();
            }
            StoredProcedureQuery query = getEntityManager().createNamedStoredProcedureQuery("CncrmBorradoInactivos.SPCRM_INACTIVE_USERS_WARNING");
            resultados = query.getResultList();
            if(ProcesoBorrado.getTipoBorrado() == ProcesoBorrado.AVISO_BORRADO) {
                getEntityManager().getTransaction().commit();
            }
        } catch(Exception e) {
            if(ProcesoBorrado.getTipoBorrado() == ProcesoBorrado.AVISO_BORRADO) {
                getEntityManager().getTransaction().rollback();
            }
            System.out.println("Excepcion findEmailAviso " + e.toString());
            throw new Exception();
        }
        return resultados;
    }
    
    public List deleteInactiveUsers() throws Exception {
        List resultados = new ArrayList<>();
        try {
            if(ProcesoBorrado.getTipoBorrado() == ProcesoBorrado.BORRADO) {
                getEntityManager().getTransaction().begin();
            }
            StoredProcedureQuery query = getEntityManager().createNamedStoredProcedureQuery("CncrmBorradoInactivos.SPCRM_INACTIVE_USERS_DELETE");
            query.execute();
            resultados = query.getResultList();
            if(ProcesoBorrado.getTipoBorrado() == ProcesoBorrado.BORRADO) {
                getEntityManager().getTransaction().commit();
            }
        } catch(Exception e) {
            if(ProcesoBorrado.getTipoBorrado() == ProcesoBorrado.BORRADO) {
                getEntityManager().getTransaction().rollback();
            }
            System.out.println("Excepcion deleteInactiveUsers " + e.toString());
            throw new Exception();
        }
        return resultados;
    }
    
}
