/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnetlPoolLogValidacionDetalle;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class CnetlPoolLogValidacionDetalleFacade extends AbstractFacade<CnetlPoolLogValidacionDetalle> {

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    public CnetlPoolLogValidacionDetalleFacade() {
        super(CnetlPoolLogValidacionDetalle.class);
    }

    public List<CnetlPoolLogValidacionDetalle> findByIdPool(Long idPool) {
        Query query = getEntityManager().createNamedQuery("CnetlPoolLogValidacionDetalle.findByIdPool");
        query.setParameter("idPool", idPool);
        return query.getResultList();
    }
}
