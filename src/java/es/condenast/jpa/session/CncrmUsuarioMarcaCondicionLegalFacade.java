/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CncrmUsuarioMarcaCondicionLegal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class CncrmUsuarioMarcaCondicionLegalFacade extends AbstractFacade<CncrmUsuarioMarcaCondicionLegal> {

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CncrmUsuarioMarcaCondicionLegalFacade() {
        super(CncrmUsuarioMarcaCondicionLegal.class);
    }

    public void desactivarByIdUsuarioMarcas(String email, List<Short> idMarcas) {
        System.out.println("email --" + email);
        Query query = getEntityManager().createNamedQuery("CncrmUsuarioMarcaCondicionLegal.desactivarByIdUsuarioMarcas");
        query.setParameter("email", email);
        query.setParameter("idMarcas", idMarcas);
        query.executeUpdate();
    }

    public int countDesactivarByIdUsuarioMarcas(String email, List<Short> idMarcas) {
        Query query = getEntityManager().createNamedQuery("CncrmUsuarioMarcaCondicionLegal.countDesactivarByIdUsuarioMarcas");
        query.setParameter("email", email);
        query.setParameter("idMarcas", idMarcas);
        return query.getResultList().size();
    }

}
