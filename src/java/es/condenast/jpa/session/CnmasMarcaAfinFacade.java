/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnmasMarcaAfin;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class CnmasMarcaAfinFacade extends AbstractFacade<CnmasMarcaAfin> {
    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CnmasMarcaAfinFacade() {
        super(CnmasMarcaAfin.class);
    }
 
    @Override
    public List<CnmasMarcaAfin> findRange(int[] range) {
        javax.persistence.Query q =getEntityManager().createNamedQuery("CnmasMarcaAfin.findAll");
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
    
    @Override
    public List<CnmasMarcaAfin> findAll() {
        Query query = getEntityManager().createNamedQuery("CnmasMarcaAfin.findAll");
        return query.getResultList();
    }    
    
}
