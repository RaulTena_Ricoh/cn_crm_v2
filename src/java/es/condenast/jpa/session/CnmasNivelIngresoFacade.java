/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnmasNivelIngreso;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class CnmasNivelIngresoFacade extends AbstractFacade<CnmasNivelIngreso> {

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CnmasNivelIngresoFacade() {
        super(CnmasNivelIngreso.class);
    }

    public List<CnmasNivelIngreso> findAllOrderByDesde() {
        Query query = getEntityManager().createNamedQuery("CnmasNivelIngreso.findAllOrderByDesde");
        return query.getResultList();
    }

}
