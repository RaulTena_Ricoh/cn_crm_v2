/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnmasCategoria;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class CnmasCategoriaFacade extends AbstractFacade<CnmasCategoria> {

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CnmasCategoriaFacade() {
        super(CnmasCategoria.class);
    }

    @Override
    public List<CnmasCategoria> findRange(int[] range) {
        javax.persistence.Query q =getEntityManager().createNamedQuery("CnmasCategoria.findAll");
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
    
    @Override
    public List<CnmasCategoria> findAll() {
        Query query = getEntityManager().createNamedQuery("CnmasCategoria.findAll");
        return query.getResultList();
    } 

}
