/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnetlMasterTipoOrigen;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Raul.Tena
 */
@Stateless
public class CnetlMasterTipoOrigenFacade extends AbstractFacade<CnetlMasterTipoOrigen> {

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CnetlMasterTipoOrigenFacade() {
        super(CnetlMasterTipoOrigen.class);
    }

    public List<CnetlMasterTipoOrigen> findByIdMasterTipo(Short idMasterTipo) {
        Query query = getEntityManager().createNamedQuery("CnetlMasterTipoOrigen.findByIdMasterTipo");
        query.setParameter("idMasterTipo", idMasterTipo);
        return query.getResultList();
    }

    public void removeByIdMasterTipo(Short idMasterTipo) {
        Query query = getEntityManager().createNamedQuery("CnetlMasterTipoOrigen.removeByIdMasterTipo");
        query.setParameter("idMasterTipo", idMasterTipo);
        query.executeUpdate();
    }
}
