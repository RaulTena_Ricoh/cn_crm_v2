/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnmasEstadoCivil;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author ivan.fernandez
 */
@Stateless
public class CnmasEstadoCivilFacade extends AbstractFacade<CnmasEstadoCivil>{
    
    @PersistenceContext(unitName = "CondeNastPU")
    
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public CnmasEstadoCivilFacade(){
        super(CnmasEstadoCivil.class);
    }
    
    public Short getMaxId(){
        Query query = getEntityManager().createNamedQuery("CnmasEstadoCivil.getMaxId");
        return ((Short) query.getSingleResult());
    }
    
    @Override
    public List<CnmasEstadoCivil> findRange(int[] range) {
        javax.persistence.Query q =getEntityManager().createNamedQuery("CnmasEstadoCivil.findAll");
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
    
    @Override
    public List<CnmasEstadoCivil> findAll() {
        Query query = getEntityManager().createNamedQuery("CnmasEstadoCivil.findAll");
        return query.getResultList();
    }
    
}
