/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnapiMarcasCliente;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author david.yunta
 */
@Stateless
public class CnapiMarcasClienteFacade extends AbstractFacade<CnapiMarcasCliente> {
    //@PersistenceContext(unitName = "CondeNastPUAPI")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CnapiMarcasClienteFacade() {
        super(CnapiMarcasCliente.class);
    }
    
    public List<CnapiMarcasCliente> findByIdCliente(Integer idCliente) {
        Query query = getEntityManager().createNamedQuery("CnapiMarcasCliente.findByIdCliente");
        query.setParameter("idCliente", idCliente);
        return query.getResultList();
    }
    
    public void removeByIdCliente(Integer idCliente) {
        Query query = getEntityManager().createNamedQuery("CnapiMarcasCliente.removeByIdCliente");
        query.setParameter("idCliente", idCliente);
        query.executeUpdate();
    }
    
}
