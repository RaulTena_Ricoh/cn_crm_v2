/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.ingesta.Proceso;
import es.condenast.ingesta.entities.LogWarning;
import es.condenast.ingesta.helper.FileUtils;
import es.condenast.jpa.entities.CnetlPool;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class CnetlPoolFacade extends AbstractFacade<CnetlPool> {

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    public static final Short POOL_OK = 2;
    public static final Short POOL_ERROR = 3;
    public static final Short POOL_EXCEPCION = 4;
    public static final Short POOL_FILE_NO_EXISTE = 5;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    public CnetlPoolFacade() {
        super(CnetlPool.class);
    }

    public CnetlPool findByIdPool(Long idPool) {
        Query query = getEntityManager().createNamedQuery("CnetlPool.findByIdPool");
        query.setParameter("idPool", idPool);
        return (CnetlPool) query.getSingleResult();
    }

    public List<CnetlPool> findAllActivos() {
        Query query = getEntityManager().createNamedQuery("CnetlPool.findByIdEstado");
        query.setParameter("idEstado", 1);
        return query.getResultList();
    }

    public Short getEstado(Long idPool) {
        Query query = getEntityManager().createNamedQuery("CnetlPool.getEstado");
        query.setParameter("idPool", idPool);
        return (Short) query.getSingleResult();
    }

    public void generarTablasTemporales() throws Exception {
        try {
            if (Proceso.getTipoIngesta() == Proceso.AUTOMATICA) {
                getEntityManager().getTransaction().begin();
            }
            StoredProcedureQuery query = getEntityManager().createNamedStoredProcedureQuery("CnetlPool.SPETL_POOL_BUILD");
            query.execute();
            if (Proceso.getTipoIngesta() == Proceso.AUTOMATICA) {
                getEntityManager().getTransaction().commit();
            }
        } catch (Exception e) {
            if (Proceso.getTipoIngesta() == Proceso.AUTOMATICA) {
                getEntityManager().getTransaction().rollback();
            }
            System.out.println("Excepcion generarTablasTemporales " + e.toString());
            throw new Exception();
        }
    }

    public void optimize() throws Exception {
        try {
            if (Proceso.getTipoIngesta() == Proceso.AUTOMATICA) {
                getEntityManager().getTransaction().begin();
            }
            StoredProcedureQuery query = getEntityManager().createNamedStoredProcedureQuery("CnetlPool.SPETL_POOL_EXECUTE_OPTIMIZE");
            query.execute();
            if (Proceso.getTipoIngesta() == Proceso.AUTOMATICA) {
                getEntityManager().getTransaction().commit();
            }
        } catch (Exception e) {
            if (Proceso.getTipoIngesta() == Proceso.AUTOMATICA) {
                getEntityManager().getTransaction().rollback();
            }
            System.out.println("Excepcion optimize " + e.toString());
            throw new Exception();
        }

    }

    public int registrar(Integer idFuente, String fichero, String ficheroOk, String ficheroError, boolean automatico) throws Exception {
        try {

            if (Proceso.getTipoIngesta() == Proceso.AUTOMATICA) {
                getEntityManager().getTransaction().begin();
            }
            StoredProcedureQuery query = getEntityManager().createNamedStoredProcedureQuery("CnetlPool.SPETL_POOL_QUERY");
            query.setParameter("P_IdFuente", idFuente);
            query.setParameter("P_File", fichero);
            query.setParameter("P_File_Ok", ficheroOk);
            query.setParameter("P_File_Error", ficheroError);
            query.setParameter("P_EsAutomatico", automatico);
            query.execute();
            if (Proceso.getTipoIngesta() == Proceso.AUTOMATICA) {
                getEntityManager().getTransaction().commit();
            }

            if (Proceso.getTipoIngesta() == Proceso.AUTOMATICA) {
                getEntityManager().getTransaction().begin();
            }
            System.out.println("Obtener ultimo id");
            Query queryId = getEntityManager().createNativeQuery("SELECT LAST_INSERT_ID()");
            int id = ((BigInteger) queryId.getSingleResult()).intValue();
            if (Proceso.getTipoIngesta() == Proceso.AUTOMATICA) {
                getEntityManager().getTransaction().commit();
            }
            return id;
        } catch (Exception e) {
            if (Proceso.getTipoIngesta() == Proceso.AUTOMATICA) {
                getEntityManager().getTransaction().rollback();
            }
            System.out.println("Excepcion registrar " + e.toString());
            throw new Exception();
        }

    }

    public List<LogWarning> executeQuery(String strQuery) throws Exception {
        try {
            SQLWarning sqlWarnings = null;
            String[] statements = strQuery.split(";");
            for (String st : statements) {

                if (Proceso.getTipoIngesta() == Proceso.AUTOMATICA) {
                    getEntityManager().getTransaction().begin();
                }
                Connection conn = getEntityManager().unwrap(java.sql.Connection.class);
                PreparedStatement statement = conn.prepareStatement(st + ";");
                if (st.contains("SHOW WARNINGS")) {
                    sqlWarnings = statement.getWarnings();
                } else {
                    statement.execute();
                }
                if (Proceso.getTipoIngesta() == Proceso.AUTOMATICA) {
                    getEntityManager().getTransaction().commit();
                }

            }
            return transformarSQLWarning(sqlWarnings);
        } catch (SQLException e) {
            if (Proceso.getTipoIngesta() == Proceso.AUTOMATICA) {
                getEntityManager().getTransaction().rollback();
            }
            System.out.println("Excepcion executeQuery " + e.toString());
            throw new Exception();
        }

    }

    private List<LogWarning> transformarSQLWarning(SQLWarning sqlWarnings) {
        List<LogWarning> warnings = new ArrayList<>();
        if (sqlWarnings != null) {
            while (sqlWarnings != null) {
                LogWarning warning = new LogWarning();
                warning.setCode(Integer.toString(sqlWarnings.getErrorCode()));
                warning.setLevel(sqlWarnings.getSQLState());
                warning.setMessage(sqlWarnings.getMessage());
                warnings.add(warning);
                sqlWarnings = sqlWarnings.getNextWarning();
            }
        }
        return warnings;
    }

    public void procesar(Long idPool) throws Exception {
        try {
            if (Proceso.getTipoIngesta() == Proceso.AUTOMATICA) {
                getEntityManager().getTransaction().begin();
            }
            StoredProcedureQuery query = getEntityManager().createNamedStoredProcedureQuery("CnetlPool.SPETL_POOL_EXECUTE");
            query.setParameter("P_IdPool", idPool);
            query.execute();
            if (Proceso.getTipoIngesta() == Proceso.AUTOMATICA) {
                getEntityManager().getTransaction().commit();
            }
        } catch (Exception e) {
            if (Proceso.getTipoIngesta() == Proceso.AUTOMATICA) {
                getEntityManager().getTransaction().rollback();
            }
            System.out.println("Excepcion procesar " + e.toString());
            throw new Exception();
        }
    }

    public void reglasImportacion(List<CnetlPool> poolesActivos) throws Exception {
        try {
            if (Proceso.getTipoIngesta() == Proceso.AUTOMATICA) {
                getEntityManager().getTransaction().begin();
            }
            String idsPool = "";
            for (CnetlPool pool : poolesActivos) {
                idsPool += pool.getIdPool() + ",";
            }
            idsPool = idsPool.substring(0, idsPool.length() - 1);
            StoredProcedureQuery query = getEntityManager().createNamedStoredProcedureQuery("CnetlPool.SPETL_POOL_REGLA");
            query.setParameter("P_IdPools", idsPool);
            query.execute();
            if (Proceso.getTipoIngesta() == Proceso.AUTOMATICA) {
                getEntityManager().getTransaction().commit();
            }
        } catch (Exception e) {
            if (Proceso.getTipoIngesta() == Proceso.AUTOMATICA) {
                getEntityManager().getTransaction().rollback();
            }
            System.out.println("Excepcion reglasImportacion " + e.toString());
            throw new Exception();
        }
    }

    public void moverFichero(CnetlPool pool) throws Exception {
        Short estado = getEstado(pool.getIdPool());
        System.out.println("         Pool -- moverFichero. Estado -->" + estado);
        if (estado.equals(POOL_OK)) {
            FileUtils.move(pool.getArchivo(), pool.getArchivoOk());
        } else if (estado.equals(POOL_EXCEPCION) || (estado.equals(POOL_ERROR))) {
            FileUtils.move(pool.getArchivo(), pool.getArchivoError());
        }
        System.out.println("         Pool -- moverFichero. Movido");

    }

    public void eliminarFichero(CnetlPool pool) throws Exception {
        FileUtils.delete(pool.getArchivo());
    }

}
