/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.VietlLog;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class VietlLogFacade extends AbstractFacade<VietlLog> {

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    public VietlLogFacade() {
        super(VietlLog.class);
    }

    public List<VietlLog> findByIdPool(Long idPool) {
        Query query = getEntityManager().createNamedQuery("VietlLog.findByIdPool");
        query.setParameter("idPool", idPool);
        return query.getResultList();
    }

    public List<VietlLog> findAdminLog(Date fechaDesde, Date fechaHasta, String fuenteTipo, String fuente, String estado, int[] range){
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<VietlLog> criteriaQuery = criteriaBuilder.createQuery(VietlLog.class);
        Root<VietlLog> root = criteriaQuery.from(VietlLog.class);

        if(fechaDesde.equals(fechaHasta)) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fechaHasta);
            calendar.add(Calendar.HOUR, 23);
            calendar.add(Calendar.MINUTE, 59);
            calendar.add(Calendar.SECOND, 59);
            calendar.add(Calendar.MILLISECOND, 999);
            
            fechaHasta = calendar.getTime();
        }
        
        List<Predicate> predicates = getPredicates(fechaDesde, fechaHasta, fuenteTipo, fuente, estado, root);
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
        TypedQuery<VietlLog> query = em.createQuery(criteriaQuery);
        query.setParameter("fechaDesde", fechaDesde);
        query.setParameter("fechaHasta", fechaHasta);
        query.setParameter("fuenteTipo", fuenteTipo);
        query.setParameter("fuente", fuente);
        query.setParameter("estado", estado);
        query.setMaxResults(range[1] - range[0] + 1);
        query.setFirstResult(range[0]);
        System.out.println(query.toString());
        return query.getResultList();
    }
    private List<Predicate> getPredicates(Date fechaDesde, Date fechaHasta, String fuenteTipo, String fuente, String estado, 
            Root<VietlLog> root){
        List<Predicate> predicates = new ArrayList<Predicate>();
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        Path<Date> inicioPoolPath = root.get("inicioPool");
        Path<String> fuenteTipoPath = root.get("fuenteTipo");
        ParameterExpression<Date> fechaDesdeParam = criteriaBuilder.parameter(Date.class, "fechaDesde");
        ParameterExpression<Date> fechaHastaParam = criteriaBuilder.parameter(Date.class, "fechaHasta");
        ParameterExpression<String> fuenteTipoParam = criteriaBuilder.parameter(String.class, "fuenteTipo");
        predicates.add(criteriaBuilder.greaterThanOrEqualTo(inicioPoolPath, fechaDesdeParam));
        predicates.add(criteriaBuilder.lessThanOrEqualTo(inicioPoolPath, fechaHastaParam));
        predicates.add(criteriaBuilder.equal(fuenteTipoPath, fuenteTipoParam));
        if(!isNullOrEmpty(fuente)){
            ParameterExpression<String> fuenteParam = criteriaBuilder.parameter(String.class, "fuente");
            Path<String> fuentePath = root.get("fuente");
            predicates.add(criteriaBuilder.equal(fuentePath, fuenteParam));
        }
        if(!isNullOrEmpty(estado)){
            ParameterExpression<String> estadoParam = criteriaBuilder.parameter(String.class, "estado");
            Path<String> estadoPath = root.get("estado");
            predicates.add(criteriaBuilder.equal(estadoPath, estadoParam));
        }
        return predicates;
        
    }
    public int countAdminLog(Date fechaDesde, Date fechaHasta, String fuenteTipo, String fuente, String estado) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root<VietlLog> root = criteriaQuery.from(VietlLog.class);
        List<Predicate> predicates = getPredicates(fechaDesde, fechaHasta, fuenteTipo, fuente, estado, root);
        criteriaQuery.select(criteriaBuilder.count(root));
        criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[0])));
        TypedQuery query = em.createQuery(criteriaQuery);
        query.setParameter("fechaDesde", fechaDesde);
        query.setParameter("fechaHasta", fechaHasta);
        query.setParameter("fuenteTipo", fuenteTipo);
        query.setParameter("fuente", fuente);
        query.setParameter("estado", estado);
        System.out.println(query.toString());
        return ((Long) query.getSingleResult()).intValue();
    }
    
    private boolean isNullOrEmpty(String cadena){
        return cadena == null || "".equals(cadena);
    }
}
