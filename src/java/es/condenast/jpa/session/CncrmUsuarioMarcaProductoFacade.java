/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import es.condenast.jpa.entities.CncrmUsuarioMarcaProducto;

/**
 *
 * @author ivan.fernandez
 */
@Stateless
public class CncrmUsuarioMarcaProductoFacade extends AbstractFacade<CncrmUsuarioMarcaProducto>{
    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public CncrmUsuarioMarcaProductoFacade(){
        super(CncrmUsuarioMarcaProducto.class);
    }

}
