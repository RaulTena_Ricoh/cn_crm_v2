/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnetlFuenteTipoOrigen;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class CnetlFuenteTipoOrigenFacade extends AbstractFacade<CnetlFuenteTipoOrigen> {

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CnetlFuenteTipoOrigenFacade() {
        super(CnetlFuenteTipoOrigen.class);
    }

    public List<CnetlFuenteTipoOrigen> findByIdFuenteTipo(Short idFuenteTipo) {
        Query query = getEntityManager().createNamedQuery("CnetlFuenteTipoOrigen.findByIdFuenteTipo");
        query.setParameter("idFuenteTipo", idFuenteTipo);
        return query.getResultList();
    }

    public void removeByIdFuenteTipo(Short idFuenteTipo) {
        Query query = getEntityManager().createNamedQuery("CnetlFuenteTipoOrigen.removeByIdFuenteTipo");
        query.setParameter("idFuenteTipo", idFuenteTipo);
        query.executeUpdate();
    }
}
