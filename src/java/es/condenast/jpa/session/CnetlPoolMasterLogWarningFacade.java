/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.ingesta.entities.LogWarning;
import es.condenast.jpa.entities.CnetlPoolMasterLogWarning;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Raul.Tena
 */
@Stateless
public class CnetlPoolMasterLogWarningFacade extends AbstractFacade<CnetlPoolMasterLogWarning> {

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    public CnetlPoolMasterLogWarningFacade() {
        super(CnetlPoolMasterLogWarning.class);
    }

    public void saveWarnings(Long idPoolMaster, List<LogWarning> warnings) {
        for (LogWarning w : warnings) {
            CnetlPoolMasterLogWarning poolLog = new CnetlPoolMasterLogWarning();
            poolLog.setIdPoolMaster(idPoolMaster);
            poolLog.setCode(w.getCode());
            poolLog.setLevel(w.getLevel());
            poolLog.setMessage(w.getMessage());
            create(poolLog);
        }
    }

    public List<CnetlPoolMasterLogWarning> findByIdPoolMaster(Long idPoolMaster) {
        Query query = getEntityManager().createNamedQuery("CnetlPoolMasterLogWarning.findByIdPoolMaster");
        query.setParameter("idPoolMaster", idPoolMaster);
        return query.getResultList();
    }

}
