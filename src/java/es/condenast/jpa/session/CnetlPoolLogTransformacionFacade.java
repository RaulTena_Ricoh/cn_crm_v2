/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnetlPoolLogTransformacion;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class CnetlPoolLogTransformacionFacade extends AbstractFacade<CnetlPoolLogTransformacion> {

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    public CnetlPoolLogTransformacionFacade() {
        super(CnetlPoolLogTransformacion.class);
    }

    public List<CnetlPoolLogTransformacion> findByIdPool(Long idPool) {
        Query query = getEntityManager().createNamedQuery("CnetlPoolLogTransformacion.findByIdPool");
        query.setParameter("idPool", idPool);
        return query.getResultList();
    }

}
