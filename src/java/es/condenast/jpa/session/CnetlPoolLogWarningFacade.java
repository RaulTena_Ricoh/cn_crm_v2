/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.ingesta.entities.LogWarning;
import es.condenast.jpa.entities.CnetlPoolLogWarning;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class CnetlPoolLogWarningFacade extends AbstractFacade<CnetlPoolLogWarning> {

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    public CnetlPoolLogWarningFacade() {
        super(CnetlPoolLogWarning.class);
    }

    public void saveWarnings(Long idPool, List<LogWarning> warnings) {
        for (LogWarning w : warnings) {
            CnetlPoolLogWarning poolLog = new CnetlPoolLogWarning();
            poolLog.setIdPool(idPool);
            poolLog.setCode(w.getCode());
            poolLog.setLevel(w.getLevel());
            poolLog.setMessage(w.getMessage());
            create(poolLog);
        }
    }

    public List<CnetlPoolLogWarning> findByIdPool(Long idPool) {
        Query query = getEntityManager().createNamedQuery("CnetlPoolLogWarning.findByIdPool");
        query.setParameter("idPool", idPool);
        return query.getResultList();
    }

}
