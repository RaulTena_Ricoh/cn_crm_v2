/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnmasUsuarioTipo;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class CnmasUsuarioTipoFacade extends AbstractFacade<CnmasUsuarioTipo> {

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CnmasUsuarioTipoFacade() {
        super(CnmasUsuarioTipo.class);
    }

    public List<CnmasUsuarioTipo> findAllOrderByMarcaDesde() {
        Query query = getEntityManager().createNamedQuery("CnmasUsuarioTipo.findAllOrderByMarcaDesde");
        return query.getResultList();
    }

    public List<CnmasUsuarioTipo> findAllOrderByCNDesde() {
        Query query = getEntityManager().createNamedQuery("CnmasUsuarioTipo.findAllOrderByCNDesde");
        return query.getResultList();
    }
}
