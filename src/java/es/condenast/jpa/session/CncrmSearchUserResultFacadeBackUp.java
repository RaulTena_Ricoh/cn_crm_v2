/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CncrmSearchUserResult;
import es.condenast.jpa.entities.CncrmUsuario;
import es.condenast.jpa.entities.CncrmUsuarioMarca;
import es.condenast.jpa.entities.CncrmUsuarioMarcaCondicionLegal;
import es.condenast.jpa.entities.CncrmUsuarioMarcaProducto;
import es.condenast.jpa.entities.CncrmUsuarioParticipacion;
import es.condenast.jpa.entities.CnmasMarca;
import es.condenast.jpa.entities.CnmasParticipacion;
import es.condenast.jpa.entities.CnmasProducto;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author ivan.fernandez
 */
@Stateless
public class CncrmSearchUserResultFacadeBackUp extends AbstractFacade<CncrmSearchUserResult>{
    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public CncrmSearchUserResultFacadeBackUp(){
        super(CncrmSearchUserResult.class);
    }
    
    public int countSearch(String queryString) {
        Query subQuery = getEntityManager().createNativeQuery(queryString);
        List ids = subQuery.getResultList();
        if (ids.isEmpty()){
            return 0;
        }
        return ids.size();
    }
    
    public int countSearch2(String email,
            CnmasMarca[] marcas, String marcaCondition,
            CnmasProducto[] productos, String productoCondition) {
        TypedQuery<Tuple> query = searhUserQuery(email, marcas, marcaCondition,
            productos, productoCondition);
        return query.getResultList().size();
    }
    

    public List<CncrmSearchUserResult> findRange(int[] range, String queryString) {
        Query query = getEntityManager().createNativeQuery(queryString, CncrmSearchUserResult.class);
        query.setMaxResults(range[1] - range[0] + 1);
        query.setFirstResult(range[0]);
        return query.getResultList();
    }
    
    public String getSelectColumns(String tableName) {
        String sql = "SELECT GROUP_CONCAT(COLUMN_NAME) FROM  INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = ?";
        Query query = getEntityManager().createNativeQuery(sql);
        query.setParameter(1, tableName);
        return (String)query.getResultList().get(0);
    }
    
    public List<List> getDetail(Long idUsuario, String columns, String tableName){
        String sql = "SELECT " + columns + " FROM " + tableName + " WHERE idUsuario = ?" ;
        Query query = getEntityManager().createNativeQuery(sql);
        query.setParameter(1, idUsuario);
        return query.getResultList();
    }
    
    
    private TypedQuery searhUserQuery1(String email, CnmasMarca[] marcas, String marcaCondition,
            CnmasProducto[] productos, String productoCondition){
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
        Root<CncrmUsuario> root = criteriaQuery.from(CncrmUsuario.class);
        Join<CncrmUsuario, CncrmUsuarioMarca> usuarioMarca;
        Join<CncrmUsuarioMarca, CncrmUsuarioMarcaProducto> usuarioMarcaProducto;
        Join<CncrmUsuarioMarca, CncrmUsuarioMarcaCondicionLegal> usuarioCondicionLegal;
        Join<CncrmUsuarioMarca, CncrmUsuarioParticipacion> usuarioParticipacion;
        Join<CncrmUsuarioMarca, CnmasParticipacion> participacionMarca;
        
        List<Predicate> conditions = new ArrayList();
        
        if (marcas == null || marcas.length == 0 || "IGNORE".equals(marcaCondition)){
            usuarioMarca = root.join("cncrmUsuarioMarcaCollection", JoinType.INNER);
        }else{
            if ("OR".equals(marcaCondition)){
                usuarioMarca = root.join("cncrmUsuarioMarcaCollection", JoinType.LEFT);
                //conditions.add(criteriaBuilder.equal(usuarioMarca.get("idMarca"), idMarca));
                for(int i = 0; i < marcas.length; i++){
                    usuarioMarca.on(criteriaBuilder.equal(usuarioMarca.get("cncrmUsuarioMarcaPK").get("idMarca"), marcas[i].getIdMarca()));
                }
                //usuarioMarca.on(criteriaBuilder.equal(usuarioMarca.get("cncrmUsuarioMarcaPK").get("idMarca"), idMarca));
            }else if ("AND".equals(marcaCondition)){
                usuarioMarca = root.join("cncrmUsuarioMarcaCollection", JoinType.INNER);
                /*for(int i = 0; i < marcas.length; i++){
                    usuarioMarca.in(criteriaBuilder.equal(usuarioMarca.get("cncrmUsuarioMarcaPK").get("idMarca"), marcas[i].getIdMarca()));
                }*/
                usuarioMarca.in(criteriaBuilder.equal(usuarioMarca.get("cncrmUsuarioMarca").get("cnmasMarca"), marcas)); 
            }else{
                usuarioMarca = root.join("cncrmUsuarioMarcaCollection", JoinType.INNER);
                for(int i = 0; i < marcas.length; i++){
                    usuarioMarca.on(criteriaBuilder.notEqual(usuarioMarca.get("cncrmUsuarioMarcaPK").get("idMarca"), marcas[i].getIdMarca()));
                }
            }
        }
        if (productos == null || productos.length == 0 || "IGNORE".equals(productoCondition)){
            usuarioMarcaProducto = root.join("cncrmUsuarioMarcaProductoCollection", JoinType.INNER);
        }else{
            if ("OR".equals(productoCondition)){
                usuarioMarcaProducto = root.join("cncrmUsuarioMarcaProductoCollection", JoinType.LEFT);
                for (CnmasProducto prod : productos) {
                    usuarioMarca.on(criteriaBuilder.equal(usuarioMarcaProducto.get("cncrmUsuarioMarcaProductoPK").get("idProducto"), prod.getIdProducto()));
                }
            }else if ("AND".equals(productoCondition)){
                usuarioMarcaProducto = root.join("cncrmUsuarioMarcaProductoCollection", JoinType.INNER);
                for (CnmasProducto prod : productos) {
                    usuarioMarca.on(criteriaBuilder.equal(usuarioMarcaProducto.get("cncrmUsuarioMarcaProductoPK").get("idProducto"), prod.getIdProducto()));
                }
            }else{
                usuarioMarcaProducto = root.join("cncrmUsuarioMarcaProductoCollection", JoinType.INNER);
                for (CnmasProducto prod : productos) {
                    usuarioMarca.on(criteriaBuilder.notEqual(usuarioMarcaProducto.get("cncrmUsuarioMarcaProductoPK").get("idProducto"), prod.getIdProducto()));
                }
            }
            usuarioMarcaProducto = root.join("cncrmUsuarioMarcaProductoCollection", JoinType.LEFT);
        }
        usuarioCondicionLegal = root.join("cncrmUsuarioMarcaCondicionLegalCollection", JoinType.INNER);
        
        usuarioParticipacion = root.join("cncrmUsuarioParticipacionCollection", JoinType.INNER);

        participacionMarca = usuarioMarca.join("cncrmUsuarioMarcaPK", JoinType.INNER);

        List<Predicate> predicatesWhere = new ArrayList<Predicate>();
        if(!email.isEmpty()){
            
            Path<String> emailPath = root.get("email");
            ParameterExpression<String> emailParam = criteriaBuilder.parameter(String.class, "email");
            predicatesWhere.add(criteriaBuilder.like(emailPath, emailParam));
            
        }
        criteriaQuery.multiselect(root.get("idUsuario").alias("idUsuario"), root.get("email").alias("email"), 
                usuarioMarca.get("cncrmUsuarioMarcaPK").get("idMarca").alias("idMarca"), 
                usuarioMarca.get("cnmasMarca").get("nombre").alias("nombreMarca"),
                usuarioMarcaProducto.get("cncrmUsuarioMarcaProductoPK").get("idProducto").alias("idProducto"), 
                usuarioMarcaProducto.get("cnmasProducto").get("nombre").alias("nombreProducto"),
                usuarioMarcaProducto.get("fechaAlta").alias("fechaAlta"),
                usuarioMarcaProducto.get("fechaBaja").alias("fechaBaja"),
                usuarioParticipacion.get("cncrmUsuarioParticipacionPK").get("idParticipacion").alias("idParticipacion"),
                usuarioParticipacion.get("cnmasParticipacion").get("nombre").alias("nombreParticipacion"),
                usuarioCondicionLegal.get("c1FechaBaja").alias("c1_FechaAlta"),
                usuarioCondicionLegal.get("c2FechaBaja").alias("c2_FechaAlta"),
                usuarioCondicionLegal.get("c3FechaBaja").alias("c3_FechaAlta"));
        criteriaQuery.where(criteriaBuilder.and(predicatesWhere.toArray(new Predicate[0])));
        TypedQuery<Tuple> query = em.createQuery(criteriaQuery);
        query.setParameter("email", email);
        
        return query;
    }
    
    
    private TypedQuery searhUserQuery(String email, CnmasMarca[] marcas, String marcaCondition,
            CnmasProducto[] productos, String productoCondition){
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
        Root<CncrmUsuario> root = criteriaQuery.from(CncrmUsuario.class);
        
        List<Predicate> conditions = new ArrayList();
        
       /* if (marcas == null || marcas.length == 0 || "IGNORE".equals(marcaCondition)){
            usuarioMarca = root.join("cncrmUsuarioMarcaCollection", JoinType.INNER);
        }else{
            if ("OR".equals(marcaCondition)){
                usuarioMarca = root.join("cncrmUsuarioMarcaCollection", JoinType.LEFT);
                for(int i = 0; i < marcas.length; i++){
                    usuarioMarca.on(criteriaBuilder.equal(usuarioMarca.get("cncrmUsuarioMarcaPK").get("idMarca"), marcas[i].getIdMarca()));
                }
            }else if ("AND".equals(marcaCondition)){
                usuarioMarca = root.join("cncrmUsuarioMarcaCollection", JoinType.INNER);
                usuarioMarca.in(criteriaBuilder.equal(usuarioMarca.get("cncrmUsuarioMarca").get("cnmasMarca"), marcas)); 
            }else{
                usuarioMarca = root.join("cncrmUsuarioMarcaCollection", JoinType.INNER);
                for(int i = 0; i < marcas.length; i++){
                    usuarioMarca.on(criteriaBuilder.notEqual(usuarioMarca.get("cncrmUsuarioMarcaPK").get("idMarca"), marcas[i].getIdMarca()));
                }
            }
        }
        if (productos == null || productos.length == 0 || "IGNORE".equals(productoCondition)){
            usuarioMarcaProducto = root.join("cncrmUsuarioMarcaProductoCollection", JoinType.INNER);
        }else{
            if ("OR".equals(productoCondition)){
                usuarioMarcaProducto = root.join("cncrmUsuarioMarcaProductoCollection", JoinType.LEFT);
                for (CnmasProducto prod : productos) {
                    usuarioMarca.on(criteriaBuilder.equal(usuarioMarcaProducto.get("cncrmUsuarioMarcaProductoPK").get("idProducto"), prod.getIdProducto()));
                }
            }else if ("AND".equals(productoCondition)){
                usuarioMarcaProducto = root.join("cncrmUsuarioMarcaProductoCollection", JoinType.INNER);
                for (CnmasProducto prod : productos) {
                    usuarioMarca.on(criteriaBuilder.equal(usuarioMarcaProducto.get("cncrmUsuarioMarcaProductoPK").get("idProducto"), prod.getIdProducto()));
                }
            }else{
                usuarioMarcaProducto = root.join("cncrmUsuarioMarcaProductoCollection", JoinType.INNER);
                for (CnmasProducto prod : productos) {
                    usuarioMarca.on(criteriaBuilder.notEqual(usuarioMarcaProducto.get("cncrmUsuarioMarcaProductoPK").get("idProducto"), prod.getIdProducto()));
                }
            }
            usuarioMarcaProducto = root.join("cncrmUsuarioMarcaProductoCollection", JoinType.LEFT);
        }
        usuarioCondicionLegal = root.join("cncrmUsuarioMarcaCondicionLegalCollection", JoinType.INNER);
        
        usuarioParticipacion = root.join("cncrmUsuarioParticipacionCollection", JoinType.INNER);

        participacionMarca = usuarioMarca.join("cncrmUsuarioMarcaPK", JoinType.INNER);*/

        List<Predicate> predicatesWhere = new ArrayList<Predicate>();
        if(!email.isEmpty()){
            
            Path<String> emailPath = root.get("email");
            ParameterExpression<String> emailParam = criteriaBuilder.parameter(String.class, "email");
            predicatesWhere.add(criteriaBuilder.like(emailPath, emailParam));
            
        }
        criteriaQuery.multiselect(root.get("idUsuario").alias("idUsuario"), root.get("email").alias("email"), 
                root.get("cncrmUsuarioMarcaCollection").get("cncrmUsuarioMarcaPK").get("idMarca").alias("idMarca"), 
                root.get("cncrmUsuarioMarcaCollection").get("cnmasMarca").get("nombre").alias("nombreMarca"),
                root.get("cncrmUsuarioMarcaProductoCollection").get("cncrmUsuarioMarcaProductoPK").get("idProducto").alias("idProducto"), 
                root.get("cncrmUsuarioMarcaProductoCollection").get("cnmasProducto").get("nombre").alias("nombreProducto"),
                root.get("cncrmUsuarioMarcaProductoCollection").get("fechaAlta").alias("fechaAlta"),
                root.get("cncrmUsuarioMarcaProductoCollection").get("fechaBaja").alias("fechaBaja"),
                root.get("cncrmUsuarioParticipacionCollection").get("cncrmUsuarioParticipacionPK").get("idParticipacion").alias("idParticipacion"),
                root.get("cncrmUsuarioParticipacionCollection").get("cnmasParticipacion").get("nombre").alias("nombreParticipacion"),
                root.get("cncrmUsuarioMarcaCondicionLegalCollection").get("c1FechaBaja").alias("c1_FechaAlta"),
                root.get("cncrmUsuarioMarcaCondicionLegalCollection").get("c2FechaBaja").alias("c2_FechaAlta"),
                root.get("cncrmUsuarioMarcaCondicionLegalCollection").get("c3FechaBaja").alias("c3_FechaAlta"));
        criteriaQuery.where(criteriaBuilder.and(predicatesWhere.toArray(new Predicate[0])));
        TypedQuery<Tuple> query = em.createQuery(criteriaQuery);
        query.setParameter("email", email);
        
        return query;
    }
    
    public List<CncrmSearchUserResult> findRange2(int[] range, String email,
            CnmasMarca[] marcas, String marcaCondition,
            CnmasProducto[] productos, String productoCondition) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        TypedQuery<Tuple> query = searhUserQuery(email, marcas, marcaCondition,
            productos, productoCondition);
        List<CncrmSearchUserResult> searchUserResult = new ArrayList();
        query.setMaxResults(range[1] - range[0] + 1);
        query.setFirstResult(range[0]);
        
        for(Tuple tuple: query.getResultList()){
            CncrmSearchUserResult csur = new CncrmSearchUserResult();
            csur.getCncrmSearchUserResultFK().setIdUsuario(new Long((long) tuple.get("idUsuario")));
            csur.setEmail((String) tuple.get("email"));
            csur.getCncrmSearchUserResultFK().setIdMarca((Short) tuple.get("idMarca"));
            csur.getCncrmSearchUserResultFK().setIdProducto((Short) tuple.get("idProducto"));
            csur.setNombreProducto((String) tuple.get("nombreProducto"));
            csur.setNombreMarca((String) tuple.get("nombreMarca"));
            csur.setIdParticipacion((Short) tuple.get("idParticipacion"));
            csur.setNombreParticipacion((String) tuple.get("nombreParticipacion"));
            csur.setFechaAlta((Date) tuple.get("fechaAlta"));
            csur.setFechaBaja((Date) tuple.get("fechaBaja"));
            Date c1_FechaAlta = (Date) tuple.get("c1_FechaAlta");
            Date c2_FechaAlta = (Date) tuple.get("c2_FechaAlta");
            Date c3_FechaAlta = (Date) tuple.get("c3_FechaAlta");
            csur.setC1_FechaAlta(c1_FechaAlta);
            if(c1_FechaAlta == null){
                csur.setCl1(new Long("1"));
            }else{
                csur.setCl1(new Long("0"));
            }
            
            csur.setC2_FechaAlta(c2_FechaAlta);
            if(c2_FechaAlta == null){
                csur.setCl2(new Long("1"));
            }else{
                csur.setCl2(new Long("0"));
            }
            csur.setC3_FechaAlta(c3_FechaAlta);
            if(c3_FechaAlta == null){
                csur.setCl3(new Long("1"));
            }else{
                csur.setCl3(new Long("0"));
            }
            searchUserResult.add(csur);
        }
        return searchUserResult;
    }
}
