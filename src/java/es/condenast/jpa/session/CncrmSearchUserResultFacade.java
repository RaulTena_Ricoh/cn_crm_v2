/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CncrmSearchUserResult;
import es.condenast.jpa.entities.CncrmSearchUserResultFK;
import es.condenast.jpa.entities.CncrmSearchUserResult_;
import es.condenast.jpa.entities.CnmasMarca;
import es.condenast.jpa.entities.CnmasProducto;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 *
 * @author ivan.fernandez
 */
@Stateless
public class CncrmSearchUserResultFacade extends AbstractFacade<CncrmSearchUserResult>{
    
    private static final String IGNORE = "IGNORE";
    private static final String AND = "AND";
    private static final String OR = "OR";

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public CncrmSearchUserResultFacade(){
        super(CncrmSearchUserResult.class);
    }
    
    public CncrmSearchUserResult find(Long idUsuario, Short idMarca, Short idProducto){
        Query query = getEntityManager().createNamedQuery("CncrmSearchUserResult.findById");
        query.setParameter("idUsuario", idUsuario);
        query.setParameter("idMarca", idMarca);
        query.setParameter("idProducto", idProducto);
        List users = query.getResultList();
        if (users.size() > 0){
            return (CncrmSearchUserResult) users.get(0);
        }else{
            return null;
        }
    }
    public CncrmSearchUserResult find(CncrmSearchUserResultFK cncrmSearchUserResultFK){
        Query query = getEntityManager().createNamedQuery("CncrmSearchUserResult.findById");
        query.setParameter("idUsuario", cncrmSearchUserResultFK.getIdUsuario());
        query.setParameter("idMarca", cncrmSearchUserResultFK.getIdMarca());
        query.setParameter("idProducto", cncrmSearchUserResultFK.getIdProducto());
        List users = query.getResultList();
        if (users.size() > 0){
            return (CncrmSearchUserResult) users.get(0);
        }else{
            return null;
        }
    }    
       
    public int countSearch(String email, CnmasMarca[] marcas, String marcaCondition, String marcaSubCondition,
            CnmasProducto[] productos, String productoCondition, String productoSubCondition, 
            Date fechaAlta, String fechaAltaCondition, String enviosCondition, Boolean aceptaCL1, Boolean aceptaCL2, Boolean aceptaCL3, String participacionCondition, String participacion) {

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root<CncrmSearchUserResult> root = criteriaQuery.from(CncrmSearchUserResult.class);
        HashMap <String, List<Predicate>> predicates = getPredicates(email, marcas, marcaCondition, marcaSubCondition,
            productos, productoCondition, productoSubCondition, 
            fechaAlta, fechaAltaCondition, enviosCondition, aceptaCL1, aceptaCL2, aceptaCL3, 
            participacionCondition, participacion, root, criteriaQuery);
        List<Predicate> predicatesListAnd = predicates.get(AND);
        List<Predicate> predicatesListOr = predicates.get(OR);
        
        if(predicatesListAnd.size() > 0 && predicatesListOr.size() > 0){
            criteriaQuery.where(criteriaBuilder.and(predicatesListAnd.toArray(new Predicate[predicatesListAnd.size()])), criteriaBuilder.or(predicatesListOr.toArray(new Predicate[predicatesListOr.size()])));
        }else if(predicatesListAnd.size() > 0 && predicatesListOr.isEmpty()){
            criteriaQuery.where(criteriaBuilder.and(predicatesListAnd.toArray(new Predicate[predicatesListAnd.size()])));
        }else if(predicatesListAnd.isEmpty() && predicatesListOr.size() > 0){
            criteriaQuery.where(criteriaBuilder.or(predicatesListOr.toArray(new Predicate[predicatesListAnd.size()])));
        }
        criteriaQuery.select(criteriaBuilder.count(root));
        TypedQuery query = em.createQuery(criteriaQuery);
        query.setParameter("email", email);
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        if(fechaAlta == null){
            query.setParameter("fechaAlta", null);
        }else{
            query.setParameter("fechaAlta", new Date(df.format(fechaAlta)), TemporalType.DATE);
        }
        query.setParameter("cl1", aceptaCL1);
        query.setParameter("cl2", aceptaCL2);
        query.setParameter("cl3", aceptaCL3);
        query.setParameter("nombreParticipacion", participacion);

        return ((Long) query.getSingleResult()).intValue();
    }
    
    public List<CncrmSearchUserResult> findRange(int[] range, String email, CnmasMarca[] marcas, String marcaCondition, String marcaSubCondition,
            CnmasProducto[] productos, String productoCondition, String productoSubCondition, 
            Date fechaAlta, String fechaAltaCondition, String enviosCondition, Boolean aceptaCL1, Boolean aceptaCL2, Boolean aceptaCL3, String participacionCondition, String participacion) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<CncrmSearchUserResult> criteriaQuery = criteriaBuilder.createQuery(CncrmSearchUserResult.class);
        Root<CncrmSearchUserResult> root = criteriaQuery.from(CncrmSearchUserResult.class);
        criteriaQuery.select(root);

        HashMap <String, List<Predicate>> predicates = getPredicates(email, marcas, marcaCondition, marcaSubCondition,
            productos, productoCondition, productoSubCondition, 
            fechaAlta, fechaAltaCondition, enviosCondition, aceptaCL1, aceptaCL2, aceptaCL3, 
            participacionCondition, participacion, root, criteriaQuery);
        List<Predicate> predicatesListAnd = predicates.get(AND);
        List<Predicate> predicatesListOr = predicates.get(OR);
        if(predicatesListAnd.size() > 0 && predicatesListOr.size() > 0){
            criteriaQuery.where(criteriaBuilder.and(predicatesListAnd.toArray(new Predicate[predicatesListAnd.size()])), criteriaBuilder.or(predicatesListOr.toArray(new Predicate[predicatesListOr.size()])));
        }else if(predicatesListAnd.size() > 0 && predicatesListOr.isEmpty()){
            criteriaQuery.where(criteriaBuilder.and(predicatesListAnd.toArray(new Predicate[predicatesListAnd.size()])));
        }else if(predicatesListAnd.isEmpty() && predicatesListOr.size() > 0){
            criteriaQuery.where(criteriaBuilder.or(predicatesListOr.toArray(new Predicate[predicatesListAnd.size()])));
        }
        criteriaQuery.orderBy(criteriaBuilder.asc(root.get("email")), criteriaBuilder.asc(root.get("nombreMarca")), criteriaBuilder.asc(root.get("nombreProducto")), criteriaBuilder.asc(root.get("fechaAlta")));
        TypedQuery<CncrmSearchUserResult> query = em.createQuery(criteriaQuery);
        query.setParameter("email", email);
        //DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
        if(fechaAlta == null){
            query.setParameter("fechaAlta", null);
        }else{
            //query.setParameter("fechaAlta", new Date(df.format(fechaAlta)), TemporalType.DATE);
            query.setParameter("fechaAlta", fechaAlta);
        }
        query.setParameter("cl1", aceptaCL1);
        query.setParameter("cl2", aceptaCL2);
        query.setParameter("cl3", aceptaCL3);
        query.setParameter("nombreParticipacion", participacion);
        
        query.setMaxResults(range[1] - range[0] + 1);
        query.setFirstResult(range[0]);
        System.out.println(query.toString());
        return query.getResultList();

    }
    
    private HashMap <String, List<Predicate>> getPredicates(String email, CnmasMarca[] marcas, String marcaCondition, String marcaSubCondition,
            CnmasProducto[] productos, String productoCondition, String productoSubCondition, 
            Date fechaAlta, String fechaAltaCondition, String enviosCondition, Boolean aceptaCL1, Boolean aceptaCL2, Boolean aceptaCL3, 
            String participacionCondition, String participacion, Root<CncrmSearchUserResult> root, CriteriaQuery criteriaQuery){
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        HashMap <String, List<Predicate>> predicastesHash = new HashMap();
        List<Predicate> predicatesAnd = new ArrayList<>();
        List<Predicate> predicatesOr = new ArrayList<>();
        if (((!isNullOrEmpty(marcaCondition) && !IGNORE.equals(marcaCondition)) && marcas.length > 0)){
            if(AND.equals(marcaSubCondition)){
                Subquery subquery = criteriaQuery.subquery(Object.class);
                Root<CncrmSearchUserResult> rootSub = subquery.from(CncrmSearchUserResult.class);
                Path<Object> path = rootSub.get("cncrmSearchUserResultFK").get("idMarca");
                In<Object> in = criteriaBuilder.in(path);
                for (CnmasMarca marca : marcas) {
                   in.value(marca.getIdMarca());
                }
                criteriaBuilder.countDistinct(rootSub.get("cncrmSearchUserResultFK").get("idUsuario"));
                subquery.select(rootSub.get(CncrmSearchUserResult_.cncrmSearchUserResultFK).get("idUsuario"));
                subquery.where(criteriaBuilder.and(criteriaBuilder.and(in)));
                subquery.groupBy(rootSub.get("cncrmSearchUserResultFK").get("idUsuario"));
                subquery.having(criteriaBuilder.gt(criteriaBuilder.countDistinct(rootSub.get("cncrmSearchUserResultFK").get("idMarca")), marcas.length-1));
                if(AND.equals(marcaCondition)){
                    predicatesAnd.add(criteriaBuilder.and(criteriaBuilder.in(root.get("cncrmSearchUserResultFK").get("idUsuario")).value(subquery)));
                }else if(OR.equals(marcaCondition)){
                    predicatesOr.add(criteriaBuilder.or(criteriaBuilder.in(root.get("cncrmSearchUserResultFK").get("idUsuario")).value(subquery)));
                }else{
                    predicatesAnd.add(criteriaBuilder.and(criteriaBuilder.not(criteriaBuilder.in(root.get("cncrmSearchUserResultFK").get("idUsuario")).value(subquery))));
                }
            }
            Path<Object> path = root.get("cncrmSearchUserResultFK").get("idMarca");
            In<Object> in = criteriaBuilder.in(path);
            for (CnmasMarca marca : marcas) {
               in.value(marca.getIdMarca());
            }
            if(AND.equals(marcaCondition)){
                predicatesAnd.add(criteriaBuilder.and(criteriaBuilder.and(in)));
            }else if(OR.equals(marcaCondition)){
                predicatesOr.add(criteriaBuilder.or(criteriaBuilder.or(in)));
            }else{
                predicatesAnd.add(criteriaBuilder.and(criteriaBuilder.not(criteriaBuilder.and(in))));
            }
         }
         
         if (((!isNullOrEmpty(productoCondition) && !IGNORE.equals(productoCondition)) && productos.length > 0)){
            if(AND.equals(productoSubCondition)){
                Subquery subquery = criteriaQuery.subquery(Object.class);
                Root<CncrmSearchUserResult> rootSub = subquery.from(CncrmSearchUserResult.class);
                Path<Object> path = rootSub.get("cncrmSearchUserResultFK").get("idProducto");
                In<Object> in = criteriaBuilder.in(path);
                for (CnmasProducto producto : productos) {
                   in.value(producto.getIdProducto());
                }
                subquery.select(rootSub.get(CncrmSearchUserResult_.cncrmSearchUserResultFK).get("idUsuario"));
                subquery.where(criteriaBuilder.and(criteriaBuilder.and(in)));
                subquery.groupBy(rootSub.get("cncrmSearchUserResultFK").get("idUsuario"));
                subquery.having(criteriaBuilder.gt(criteriaBuilder.countDistinct(rootSub.get("cncrmSearchUserResultFK").get("idProducto")), productos.length-1));
                if(AND.equals(productoCondition)){
                    predicatesAnd.add(criteriaBuilder.and(criteriaBuilder.in(root.get("cncrmSearchUserResultFK").get("idUsuario")).value(subquery)));
                }else if(OR.equals(productoCondition)){
                    predicatesOr.add(criteriaBuilder.or(criteriaBuilder.in(root.get("cncrmSearchUserResultFK").get("idUsuario")).value(subquery)));
                }else{
                    predicatesAnd.add(criteriaBuilder.and(criteriaBuilder.not(criteriaBuilder.in(root.get("cncrmSearchUserResultFK").get("idUsuario")).value(subquery))));
                }
            }
            Path<Object> path = root.get("cncrmSearchUserResultFK").get("idProducto");
            In<Object> in = criteriaBuilder.in(path);
            for (CnmasProducto producto : productos) {
                in.value(producto.getIdProducto());
            }

            if(AND.equals(productoCondition)){
                predicatesAnd.add(criteriaBuilder.and(criteriaBuilder.and(in)));
            }else if(OR.equals(productoCondition)){
                predicatesOr.add(criteriaBuilder.or(criteriaBuilder.or(in)));
            }else{
                predicatesAnd.add(criteriaBuilder.and(criteriaBuilder.not(criteriaBuilder.and(in))));
            }
         }

        if (!isNullOrEmpty(fechaAltaCondition) && !IGNORE.equals(fechaAltaCondition) && fechaAlta != null){
            Path<Date> fechaAltaPath = root.get("fechaAlta");
            ParameterExpression<Date> fechaAltaParam = criteriaBuilder.parameter(Date.class, "fechaAlta");
            
            if(AND.equals(fechaAltaCondition)){
                predicatesAnd.add(criteriaBuilder.and(criteriaBuilder.equal(fechaAltaPath, fechaAltaParam)));
            }else if(OR.equals(fechaAltaCondition)){
                predicatesOr.add(criteriaBuilder.or(criteriaBuilder.equal(fechaAltaPath, fechaAltaParam)));
            }else{
                predicatesAnd.add(criteriaBuilder.not(criteriaBuilder.and(criteriaBuilder.equal(fechaAltaPath, fechaAltaParam))));
            }
        }
        
        if (!isNullOrEmpty(enviosCondition) && !IGNORE.equals(enviosCondition) && (aceptaCL1 != null || aceptaCL2 != null || aceptaCL3 != null)){
            Path <Long> cl1Path = root.get("cl1");
            ParameterExpression<Long> cl1Param = criteriaBuilder.parameter(Long.class, "cl1");
            Path <Long> cl2Path = root.get("cl2");
            ParameterExpression<Long> cl2Param = criteriaBuilder.parameter(Long.class, "cl2");
            Path <Long> cl3Path = root.get("cl3");
            ParameterExpression<Long> cl3Param = criteriaBuilder.parameter(Long.class, "cl3");
            
            if(AND.equals(enviosCondition)){
                if (aceptaCL1 != null){
                    predicatesAnd.add(criteriaBuilder.and(criteriaBuilder.equal(cl1Path, cl1Param)));
                }
                if (aceptaCL2 != null){
                    predicatesAnd.add(criteriaBuilder.and(criteriaBuilder.equal(cl2Path, cl2Param)));
                }
                if (aceptaCL3 != null){
                    predicatesAnd.add(criteriaBuilder.and(criteriaBuilder.equal(cl3Path, cl3Param)));
                }
            }
            else if(OR.equals(enviosCondition)){
                if (aceptaCL1 != null){
                    predicatesOr.add(criteriaBuilder.or(criteriaBuilder.equal(cl1Path, cl1Param)));
                }
                if (aceptaCL2 != null){
                    predicatesOr.add(criteriaBuilder.or(criteriaBuilder.equal(cl2Path, cl2Param)));
                }
                if (aceptaCL3 != null){
                    predicatesOr.add(criteriaBuilder.or(criteriaBuilder.equal(cl3Path, cl3Param)));
                }
            }
            else{
                if (aceptaCL1 != null){
                    predicatesAnd.add(criteriaBuilder.not(criteriaBuilder.and(criteriaBuilder.equal(cl1Path, cl1Param))));
                }
                if (aceptaCL2 != null){
                    predicatesAnd.add(criteriaBuilder.not(criteriaBuilder.and(criteriaBuilder.equal(cl2Path, cl2Param))));
                }
                if (aceptaCL3 != null){
                    predicatesAnd.add(criteriaBuilder.not(criteriaBuilder.and(criteriaBuilder.equal(cl3Path, cl3Param))));
                }
            }
        }
        if (!isNullOrEmpty(participacionCondition) && !IGNORE.equals(participacionCondition) && !isNullOrEmpty(participacion)){
            Path<String> participacionPath = root.get("nombreParticipacion");
            ParameterExpression<String> participacionParam = criteriaBuilder.parameter(String.class, "nombreParticipacion");
            if(AND.equals(participacionCondition)){
                predicatesAnd.add(criteriaBuilder.and(criteriaBuilder.like(participacionPath, participacionParam)));
            }else if(OR.equals(participacionCondition)){
                predicatesOr.add(criteriaBuilder.or(criteriaBuilder.like(participacionPath, participacionParam)));
            }else{
                predicatesAnd.add(criteriaBuilder.not(criteriaBuilder.and(criteriaBuilder.like(participacionPath, participacionParam))));
            }
        }
        if(!email.isEmpty()){
            Path<String> emailPath = root.get("email");
            ParameterExpression<String> emailParam = criteriaBuilder.parameter(String.class, "email");
            predicatesAnd.add(criteriaBuilder.and(criteriaBuilder.like(emailPath, emailParam)));
        }
        predicastesHash.put(OR, predicatesOr);
        predicastesHash.put(AND, predicatesAnd);
        return predicastesHash;
    }
    
    private boolean isNullOrEmpty(String cadena){
        return cadena == null || "".equals(cadena);
    }
    
    public String getSelectColumns(String tableName) {
        String sql = "SELECT GROUP_CONCAT(COLUMN_NAME) FROM  INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = ?";
        Query query = getEntityManager().createNativeQuery(sql);
        query.setParameter(1, tableName);
        return (String)query.getResultList().get(0);
    }
    
    public List<List> getDetail(Long idUsuario, String columns, String tableName){
        String sql = "SELECT " + columns + " FROM " + tableName + " WHERE idUsuario = ?" ;
        Query query = getEntityManager().createNativeQuery(sql);
        query.setParameter(1, idUsuario);
        return query.getResultList();
    }
    
    public List<List> getDetailWithCondition(Long idUsuario, String condition, String value, String columns, String tableName){
        String sql = "SELECT " + columns + " FROM " + tableName + " WHERE idUsuario = ? AND " + condition + " = ?";
        Query query = getEntityManager().createNativeQuery(sql);
        query.setParameter(1, idUsuario);
        query.setParameter(2, value);
        return query.getResultList();
    }

}
