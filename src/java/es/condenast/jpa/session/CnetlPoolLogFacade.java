/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnetlPool;
import es.condenast.jpa.entities.CnetlPoolLog;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class CnetlPoolLogFacade extends AbstractFacade<CnetlPoolLog> {

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    public CnetlPoolLogFacade() {
        super(CnetlPoolLog.class);
    }

    public List<CnetlPoolLog> findByIdPool(Long idPool) {
        Query query = getEntityManager().createNamedQuery("CnetlPoolLog.findByIdPool");
        query.setParameter("idPool", idPool);
        return query.getResultList();
    }

    public List<CnetlPoolLog> find(List<CnetlPool> pooles) {
        ArrayList<CnetlPoolLog> logs = new ArrayList<>();
        for (CnetlPool pool : pooles) {
            CnetlPoolLog log = find(pool.getIdPool());
            logs.add(log);
        }
        return logs;
    }
}
