/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnetlFuenteValor;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class CnetlFuenteValorFacade extends AbstractFacade<CnetlFuenteValor> {

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CnetlFuenteValorFacade() {
        super(CnetlFuenteValor.class);
    }

    public List<CnetlFuenteValor> findByIdFuente(Integer idFuente) {
        Query query = getEntityManager().createNamedQuery("CnetlFuenteValor.findByIdFuente");
        query.setParameter("idFuente", idFuente);
        return query.getResultList();
    }
}
