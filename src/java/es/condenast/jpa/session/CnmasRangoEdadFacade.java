/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnmasRangoEdad;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author ivan.fernandez
 */
@Stateless
public class CnmasRangoEdadFacade extends AbstractFacade<CnmasRangoEdad> {
    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CnmasRangoEdadFacade() {
        super(CnmasRangoEdad.class);
    }

    public List<CnmasRangoEdad> findAllOrderByDesde() {
        Query query = getEntityManager().createNamedQuery("CnmasRangoEdad.findAllOrderByDesde");
        return query.getResultList();
    }
}
