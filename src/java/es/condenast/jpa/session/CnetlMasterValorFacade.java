/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnetlMasterValor;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Raul.Tena
 */
@Stateless
public class CnetlMasterValorFacade extends AbstractFacade<CnetlMasterValor> {

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CnetlMasterValorFacade() {
        super(CnetlMasterValor.class);
    }

    public List<CnetlMasterValor> findByIdMaster(Integer idMaster) {
        Query query = getEntityManager().createNamedQuery("CnetlMasterValor.findByIdMaster");
        query.setParameter("idMaster", idMaster);
        return query.getResultList();
    }
}
