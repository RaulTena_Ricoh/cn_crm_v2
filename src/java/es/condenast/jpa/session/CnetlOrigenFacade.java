/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnetlOrigen;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class CnetlOrigenFacade extends AbstractFacade<CnetlOrigen> {

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CnetlOrigenFacade() {
        super(CnetlOrigen.class);
    }

    public List<CnetlOrigen> findbyIgnorar(boolean ignorar) {
        Query query = getEntityManager().createNamedQuery("CnetlOrigen.findByIgnorar");
        query.setParameter("ignorar", ignorar);
        return query.getResultList();
    }

    public List<CnetlOrigen> findValoresPosibles(boolean ignorar, short idFuenteTipo) {
        Query query = getEntityManager().createNamedQuery("CnetlOrigen.findValoresPosibles");
        query.setParameter("ignorar", ignorar);
        query.setParameter("idFuenteTipo", idFuenteTipo);
        return query.getResultList();
    }

    public List<CnetlOrigen> findValoresExistentes(boolean ignorar, short idFuenteTipo) {
        Query query = getEntityManager().createNamedQuery("CnetlOrigen.findValoresExistentes");
        query.setParameter("ignorar", ignorar);
        query.setParameter("idFuenteTipo", idFuenteTipo);
        return query.getResultList();
    }

    public List<CnetlOrigen> findValoresPosiblesMaster(boolean ignorar, short idMasterTipo) {
        Query query = getEntityManager().createNamedQuery("CnetlOrigen.findValoresPosiblesMaster");
        query.setParameter("ignorar", ignorar);
        query.setParameter("idMasterTipo", idMasterTipo);
        return query.getResultList();
    }

    public List<CnetlOrigen> findValoresExistentesMaster(boolean ignorar, short idMasterTipo) {
        Query query = getEntityManager().createNamedQuery("CnetlOrigen.findValoresExistentesMaster");
        query.setParameter("ignorar", ignorar);
        query.setParameter("idMasterTipo", idMasterTipo);
        return query.getResultList();
    }
    
}
