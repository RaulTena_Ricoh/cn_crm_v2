/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnetlMaster;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class CnetlMasterFacade extends AbstractFacade<CnetlMaster> {

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    public CnetlMasterFacade() {
        super(CnetlMaster.class);
    }

    public List<CnetlMaster> findAllActivas() {
        Query query = getEntityManager().createNamedQuery("CnetlMaster.findByActivo");
        query.setParameter("activo", true);
        return query.getResultList();
    }

    public boolean exists(String nombre) {
        Query query = getEntityManager().createNamedQuery("CnetlMaster.findByNombre");
        query.setParameter("nombre", nombre);
        return query.getResultList().size() > 0;
    }

}
