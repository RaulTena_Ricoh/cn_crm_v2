/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnmasSituacionLaboral;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class CnmasSituacionLaboralFacade extends AbstractFacade<CnmasSituacionLaboral> {

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CnmasSituacionLaboralFacade() {
        super(CnmasSituacionLaboral.class);
    }

    public List<CnmasSituacionLaboral> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<CnmasSituacionLaboral> c = cq.from(CnmasSituacionLaboral.class);
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        cq.select(c);
        cq.orderBy(cb.asc(c.get("idSituacionLaboral")));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    @Override
    public List<CnmasSituacionLaboral> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<CnmasSituacionLaboral> c = cq.from(CnmasSituacionLaboral.class);
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        cq.select(c);
        cq.orderBy(cb.asc(c.get("idSituacionLaboral")));
        return getEntityManager().createQuery(cq).getResultList();
    }

}
