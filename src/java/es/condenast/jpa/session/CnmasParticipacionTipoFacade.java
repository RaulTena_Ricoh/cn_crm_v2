/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnmasParticipacionTipo;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author ivan.fernandez
 */
@Stateless
public class CnmasParticipacionTipoFacade extends AbstractFacade<CnmasParticipacionTipo>{
    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public CnmasParticipacionTipoFacade(){
        super(CnmasParticipacionTipo.class);
    }
    
    public Short getMaxId(){
        Query query = getEntityManager().createNamedQuery("CnmasParticipacionTipo.getMaxId");
        return ((Short) query.getSingleResult());
    }
    
    @Override
    public List<CnmasParticipacionTipo> findRange(int[] range) {
        javax.persistence.Query q =getEntityManager().createNamedQuery("CnmasParticipacionTipo.findAll");
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
    
    @Override
    public List<CnmasParticipacionTipo> findAll() {
        Query query = getEntityManager().createNamedQuery("CnmasParticipacionTipo.findAll");
        return query.getResultList();
    }     
    
}
