/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnmasOcupacionTipo;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

/**
 *
 * @author ivan.fernandez
 */
@Stateless
public class CnmasOcupacionTipoFacade extends AbstractFacade<CnmasOcupacionTipo> {
    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CnmasOcupacionTipoFacade() {
        super(CnmasOcupacionTipo.class);
    }

    @Override
    public List<CnmasOcupacionTipo> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<CnmasOcupacionTipo> c = cq.from(CnmasOcupacionTipo.class);
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        cq.select(c);
        cq.orderBy(cb.asc(c.get("idOcupacionTipo")));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    @Override
    public List<CnmasOcupacionTipo> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<CnmasOcupacionTipo> c = cq.from(CnmasOcupacionTipo.class);
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        cq.select(c);
        cq.orderBy(cb.asc(c.get("idOcupacionTipo")));
        return getEntityManager().createQuery(cq).getResultList();
    }
}
