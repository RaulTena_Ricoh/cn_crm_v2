/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnmasUsuarioOrigen;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class CnmasUsuarioOrigenFacade extends AbstractFacade<CnmasUsuarioOrigen> {

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CnmasUsuarioOrigenFacade() {
        super(CnmasUsuarioOrigen.class);
    }

    @Override
    public List<CnmasUsuarioOrigen> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<CnmasUsuarioOrigen> c = cq.from(CnmasUsuarioOrigen.class);
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        cq.select(c);
        cq.orderBy(cb.asc(c.get("idUsuarioOrigen")));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    @Override
    public List<CnmasUsuarioOrigen> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<CnmasUsuarioOrigen> c = cq.from(CnmasUsuarioOrigen.class);
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        cq.select(c);
        cq.orderBy(cb.asc(c.get("idUsuarioOrigen")));
        return getEntityManager().createQuery(cq).getResultList();
    }

}
