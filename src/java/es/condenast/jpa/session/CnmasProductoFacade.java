/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnmasProducto;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class CnmasProductoFacade extends AbstractFacade<CnmasProducto> {
    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CnmasProductoFacade() {
        super(CnmasProducto.class);
    }
    
}
