/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnmasInteres;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class CnmasInteresFacade extends AbstractFacade<CnmasInteres> {
    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CnmasInteresFacade() {
        super(CnmasInteres.class);
    }
    
}
