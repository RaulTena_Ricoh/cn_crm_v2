/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnmasDocumento;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Raul.Tena
 */
@Stateless
public class CnmasDocumentoFacade extends AbstractFacade<CnmasDocumento> {
    
    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public CnmasDocumentoFacade() {
        super(CnmasDocumento.class);
    }

    public Short getMaxId(){
        Query query = getEntityManager().createNamedQuery("CnmasDocumento.getMaxId");
        return ((Short) query.getSingleResult());
    }
    
    @Override
    public List<CnmasDocumento> findRange(int[] range) {
        javax.persistence.Query q =getEntityManager().createNamedQuery("CnmasDocumento.findAll");
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
    
    @Override
    public List<CnmasDocumento> findAll() {
        Query query = getEntityManager().createNamedQuery("CnmasDocumento.findAll");
        return query.getResultList();
    } 
    
}
