/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnmasProductoCategoria;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class CnmasProductoCategoriaFacade extends AbstractFacade<CnmasProductoCategoria> {

    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CnmasProductoCategoriaFacade() {
        super(CnmasProductoCategoria.class);
    }

    @Override
    public List<CnmasProductoCategoria> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<CnmasProductoCategoria> c = cq.from(CnmasProductoCategoria.class);
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        cq.select(c);
        cq.orderBy(cb.asc(c.get("idProductoCategoria")));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    @Override
    public List<CnmasProductoCategoria> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<CnmasProductoCategoria> c = cq.from(CnmasProductoCategoria.class);
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        cq.select(c);
        cq.orderBy(cb.asc(c.get("idProductoCategoria")));
        return getEntityManager().createQuery(cq).getResultList();
    }

}
