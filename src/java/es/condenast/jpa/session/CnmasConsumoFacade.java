/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnmasConsumo;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Raul.Tena
 */
@Stateless
public class CnmasConsumoFacade extends AbstractFacade<CnmasConsumo>{
 
    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public CnmasConsumoFacade() {
        super(CnmasConsumo.class);
    }
    
    @Override
    public List<CnmasConsumo> findRange(int[] range) {
        javax.persistence.Query q =getEntityManager().createNamedQuery("CnmasConsumo.findAll");
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
    
    @Override
    public List<CnmasConsumo> findAll() {
        Query query = getEntityManager().createNamedQuery("CnmasConsumo.findAll");
        return query.getResultList();
    }
    
}
