/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CncrmUsuario;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author Javier.Castro
 */
@Stateless
public class CncrmUsuarioFacade extends AbstractFacade<CncrmUsuario> {
    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CncrmUsuarioFacade() {
        super(CncrmUsuario.class);
    }

    public List<CncrmUsuario> findByEmail(String email) {
        Query query = getEntityManager().createNamedQuery("CncrmUsuario.findByEmail");
        query.setParameter("email", email);
        return query.getResultList();
    }

    public void deleteByEmail(String email) {
        Query query = getEntityManager().createNamedQuery("CncrmUsuario.deleteByEmail");
        query.setParameter("email", email);
        query.executeUpdate();
    }

    public List<Long> selectByFecha(Date fecha) {
        String sql
                = "select cncrm_usuario.IdUsuario "
                + "from cncrm_usuario, "
                + "(select idUsuario, count(*) as count_todos from cncrm_usuario_marca_producto group by IdUsuario) todos, "
                + "(select idUsuario, count(*) as count_no_activos from cncrm_usuario_marca_producto where FechaBaja is not null and FechaBaja <= ? group by IdUsuario) no_activo "
                + "where todos.IdUsuario = cncrm_usuario.IdUsuario and no_activo.IdUsuario = todos.IdUsuario "
                + "and count_todos = count_no_activos";
        Query query = getEntityManager().createNativeQuery(sql);
        query.setParameter(1, fecha, TemporalType.DATE);
        return query.getResultList();
    }

    public void deleteByIds(List<Long> ids) {
        for (Long id : ids) {
            Query query = getEntityManager().createNamedQuery("CncrmUsuario.deleteByIdUsuario");
            query.setParameter("idUsuario", id);
            query.executeUpdate();
        }

    }
    public List<CncrmUsuario> searchUser(String queryString) {
        Query subQuery = getEntityManager().createNativeQuery(queryString);
        List<Long> ids = subQuery.getResultList();

        Query query = getEntityManager().createNamedQuery("CncrmUsuario.searchUser");
        query.setParameter("Ids", ids);
        return query.getResultList();
    }
    
    
    public List<CncrmUsuario> findRange(int[] range, String queryString) {
        Query subQuery = getEntityManager().createNativeQuery(queryString);
        List<Long> ids = subQuery.getResultList();

        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(CncrmUsuario.class));
        Query query = getEntityManager().createNamedQuery("CncrmUsuario.searchUser");
        query.setMaxResults(range[1] - range[0] + 1);
        query.setFirstResult(range[0]);
        query.setParameter("Ids", ids);
        return query.getResultList();
    }
    
    public int countSearch(String queryString) {
        Query subQuery = getEntityManager().createNativeQuery(queryString);
        List<Long> ids = subQuery.getResultList();
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(CncrmUsuario.class));
        Query query = getEntityManager().createNamedQuery("CncrmUsuario.countSearchUser");
        query.setParameter("Ids", ids);
        return ((Long) query.getSingleResult()).intValue();
    }
    
    public CncrmUsuario findById(Long idUsuario) {
        Query query = getEntityManager().createNamedQuery("CncrmUsuario.findByIdUsuario");
        query.setParameter("idUsuario", idUsuario);
        return (CncrmUsuario)query.getResultList().get(0);
    }
}
