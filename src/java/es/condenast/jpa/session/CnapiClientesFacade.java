/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnapiClientes;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author david.yunta
 */
@Stateless
public class CnapiClientesFacade extends AbstractFacade<CnapiClientes> {
    //@PersistenceContext(unitName = "CondeNastPUAPI")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CnapiClientesFacade() {
        super(CnapiClientes.class);
    }
    
}
