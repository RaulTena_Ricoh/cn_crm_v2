/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.session;

import es.condenast.jpa.entities.CnmasCompraOnline;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author ivan.fernandez
 */
@Stateless
public class CnmasCompraOnlineFacade extends AbstractFacade<CnmasCompraOnline>{
    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public CnmasCompraOnlineFacade(){
        super(CnmasCompraOnline.class);
    }

    @Override
    public List<CnmasCompraOnline> findRange(int[] range) {
        javax.persistence.Query q =getEntityManager().createNamedQuery("CnmasCompraOnline.findAll");
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
    
    @Override
    public List<CnmasCompraOnline> findAll() {
        Query query = getEntityManager().createNamedQuery("CnmasCompraOnline.findAll");
        return query.getResultList();
    }
}
