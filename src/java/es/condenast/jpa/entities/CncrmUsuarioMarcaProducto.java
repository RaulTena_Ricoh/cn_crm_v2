/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ivan.fernandez
 */
@Entity
@Table(name = "cncrm_usuario_marca_producto")
@NamedQueries({
    @NamedQuery(name = "CncrmUsuarioMarcaProducto.findAll", query = "SELECT c FROM CncrmUsuarioMarcaProducto c"),
    @NamedQuery(name = "CncrmUsuarioMarcaProducto.findByIdUsuario", query = "SELECT c FROM CncrmUsuarioMarcaProducto c WHERE c.cncrmUsuarioMarcaProductoPK.idUsuario = :idUsuario"),
    @NamedQuery(name = "CncrmUsuarioMarcaProducto.findByIdProducto", query = "SELECT c FROM CncrmUsuarioMarcaProducto c WHERE c.cncrmUsuarioMarcaProductoPK.idProducto = :idProducto"),
    @NamedQuery(name = "CncrmUsuarioMarcaProducto.findByIdMarca", query = "SELECT c FROM CncrmUsuarioMarcaProducto c WHERE c.cncrmUsuarioMarcaProductoPK.idMarca = :idMarca"),
    @NamedQuery(name = "CncrmUsuarioMarcaProducto.findByFechaAltaPrimera", query = "SELECT c FROM CncrmUsuarioMarcaProducto c WHERE c.fechaAltaPrimera = :fechaAltaPrimera"),
    @NamedQuery(name = "CncrmUsuarioMarcaProducto.findByFechaAlta", query = "SELECT c FROM CncrmUsuarioMarcaProducto c WHERE c.fechaAlta = :fechaAlta"),
    @NamedQuery(name = "CncrmUsuarioMarcaProducto.findByFechaBaja", query = "SELECT c FROM CncrmUsuarioMarcaProducto c WHERE c.fechaBaja = :fechaBaja"),
    @NamedQuery(name = "CncrmUsuarioMarcaProducto.findByFechaCambio", query = "SELECT c FROM CncrmUsuarioMarcaProducto c WHERE c.fechaCambio = :fechaCambio")})
public class CncrmUsuarioMarcaProducto implements Serializable{
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CncrmUsuarioMarcaProductoPK cncrmUsuarioMarcaProductoPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaAltaPrimera")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAltaPrimera;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaAlta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAlta;
    @Column(name = "FechaBaja")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaBaja;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaCambio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCambio;
    @Column(name = "IntervaloAlta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date intervaloAlta;
    @Column(name = "IntervaloBaja")
    @Temporal(TemporalType.TIMESTAMP)
    private Date intervaloBaja;
    @Column(name = "EsRenovacion")
    private boolean esRenovacion;
    @JoinColumn(name = "IdMarca", referencedColumnName = "IdMarca", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasMarca cnmasMarca;
    @JoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CncrmUsuario cncrmUsuario;
    @JoinColumn(name = "IdProducto", referencedColumnName = "IdProducto", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasProducto cnmasProducto;
    
    public CncrmUsuarioMarcaProducto(){
    }
    public CncrmUsuarioMarcaProducto(CncrmUsuarioMarcaProductoPK cncrmUsuarioMarcaProductoPK) {
        this.cncrmUsuarioMarcaProductoPK = cncrmUsuarioMarcaProductoPK;
    }
    
    public CncrmUsuarioMarcaProducto(CncrmUsuarioMarcaProductoPK cncrmUsuarioMarcaProductoPK, Date fechaAltaPrimera, Date fechaAlta,
            Date fechaBaja, Date fechaCambio){
        this.cncrmUsuarioMarcaProductoPK = cncrmUsuarioMarcaProductoPK;
        this.fechaAltaPrimera = fechaAltaPrimera;
        this.fechaAlta = fechaAlta;
        this.fechaBaja = fechaBaja;
        this.fechaCambio = fechaCambio;
    }

    /**
     * @return the cncrmUsuarioMarcaProductoPK
     */
    public CncrmUsuarioMarcaProductoPK getCncrmUsuarioMarcaProductoPK() {
        return cncrmUsuarioMarcaProductoPK;
    }

    /**
     * @param cncrmUsuarioMarcaProductoPK the cncrmUsuarioMarcaProductoPK to set
     */
    public void setCncrmUsuarioMarcaProductoPK(CncrmUsuarioMarcaProductoPK cncrmUsuarioMarcaProductoPK) {
        this.cncrmUsuarioMarcaProductoPK = cncrmUsuarioMarcaProductoPK;
    }

    /**
     * @return the fechaAltaPrimera
     */
    public Date getFechaAltaPrimera() {
        return fechaAltaPrimera;
    }

    /**
     * @param fechaAltaPrimera the fechaAltaPrimera to set
     */
    public void setFechaAltaPrimera(Date fechaAltaPrimera) {
        this.fechaAltaPrimera = fechaAltaPrimera;
    }

    /**
     * @return the fechaAlta
     */
    public Date getFechaAlta() {
        return fechaAlta;
    }

    /**
     * @param fechaAlta the fechaAlta to set
     */
    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    /**
     * @return the fechaBaja
     */
    public Date getFechaBaja() {
        return fechaBaja;
    }

    /**
     * @param fechaBaja the fechaBaja to set
     */
    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    /**
     * @return the fechaCambio
     */
    public Date getFechaCambio() {
        return fechaCambio;
    }

    /**
     * @param fechaCambio the fechaCambio to set
     */
    public void setFechaCambio(Date fechaCambio) {
        this.fechaCambio = fechaCambio;
    }

    public Date getIntervaloAlta() {
        return intervaloAlta;
    }

    public void setIntervaloAlta(Date intervaloAlta) {
        this.intervaloAlta = intervaloAlta;
    }

    public Date getIntervaloBaja() {
        return intervaloBaja;
    }

    public void setIntervaloBaja(Date intervaloBaja) {
        this.intervaloBaja = intervaloBaja;
    }

    /**
     * @return the esRenovacion
     */
    public boolean isEsRenovacion() {
        return esRenovacion;
    }

    /**
     * @param esRenovacion the fechaCambio to set
     */
    public void setEsRenovacion(boolean esRenovacion) {
        this.esRenovacion = esRenovacion;
    }
    
    /**
     * @return the cnmasMarca
     */
    public CnmasMarca getCnmasMarca() {
        return cnmasMarca;
    }

    /**
     * @param cnmasMarca the cnmasMarca to set
     */
    public void setCnmasMarca(CnmasMarca cnmasMarca) {
        this.cnmasMarca = cnmasMarca;
    }

    /**
     * @return the cncrmUsuario
     */
    public CncrmUsuario getCncrmUsuario() {
        return cncrmUsuario;
    }

    /**
     * @param cncrmUsuario the cncrmUsuario to set
     */
    public void setCncrmUsuario(CncrmUsuario cncrmUsuario) {
        this.cncrmUsuario = cncrmUsuario;
    }

    /**
     * @return the cnmasProducto
     */
    public CnmasProducto getCnmasProducto() {
        return cnmasProducto;
    }

    /**
     * @param cnmasProducto the cnmasProducto to set
     */
    public void setCnmasProducto(CnmasProducto cnmasProducto) {
        this.cnmasProducto = cnmasProducto;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cncrmUsuarioMarcaProductoPK != null ? cncrmUsuarioMarcaProductoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CncrmUsuarioMarcaProducto)) {
            return false;
        }
        CncrmUsuarioMarcaProducto other = (CncrmUsuarioMarcaProducto) object;
        if ((this.cncrmUsuarioMarcaProductoPK == null && other.cncrmUsuarioMarcaProductoPK != null) || (this.cncrmUsuarioMarcaProductoPK != null && !this.cncrmUsuarioMarcaProductoPK.equals(other.cncrmUsuarioMarcaProductoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioMarcaProducto[ cncrmUsuarioMarcaProductoPK=" + cncrmUsuarioMarcaProductoPK + " ]";
    }
}
