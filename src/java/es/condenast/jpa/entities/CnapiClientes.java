/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author david.yunta
 */
@Entity
@Table(name = "cnapi_clientes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CnapiClientes.findAll", query = "SELECT c FROM CnapiClientes c"),
    @NamedQuery(name = "CnapiClientes.findById", query = "SELECT c FROM CnapiClientes c WHERE c.id = :id"),
    @NamedQuery(name = "CnapiClientes.findByNombreCliente", query = "SELECT c FROM CnapiClientes c WHERE c.nombreCliente = :nombreCliente"),
    @NamedQuery(name = "CnapiClientes.findByToken", query = "SELECT c FROM CnapiClientes c WHERE c.token = :token"),
    @NamedQuery(name = "CnapiClientes.findByNumeroRenovaciones", query = "SELECT c FROM CnapiClientes c WHERE c.numeroRenovaciones = :numeroRenovaciones"),
    @NamedQuery(name = "CnapiClientes.findByFechaCaducidad", query = "SELECT c FROM CnapiClientes c WHERE c.fechaCaducidad = :fechaCaducidad"),
    @NamedQuery(name = "CnapiClientes.findByIdCliente", query = "SELECT c FROM CnapiClientes c WHERE c.idCliente = :idCliente")})
public class CnapiClientes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 0, max = 200)
    @Column(name = "NombreCliente")
    private String nombreCliente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "Token")
    private String token;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "CodigoAuth")
    private String codigoAuth;
    @Column(name = "NumeroRenovaciones")
    private Short numeroRenovaciones;
    @Column(name = "FechaCaducidad")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCaducidad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "IdCliente")
    private String idCliente;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnapiClientes")
    private Collection<CnapiMarcasCliente> cnapiMarcasClienteCollection;

    @Transient  //Atributos no persistentes en BBDD
    private String stringFechaCaducidad;
    
    public CnapiClientes() {
    }

    public CnapiClientes(Integer id) {
        this.id = id;
    }

    public CnapiClientes(Integer id, String nombreCliente, String token, String idCliente) {
        this.id = id;
        this.nombreCliente = nombreCliente;
        this.token = token;
        this.idCliente = idCliente;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCodigoAuth() {
        return codigoAuth;
    }

    public void setCodigoAuth(String codigoAuth) {
        this.codigoAuth = codigoAuth;
    }

    public Short getNumeroRenovaciones() {
        return numeroRenovaciones;
    }

    public void setNumeroRenovaciones(Short numeroRenovaciones) {
        this.numeroRenovaciones = numeroRenovaciones;
    }

    public Date getFechaCaducidad() {
        return fechaCaducidad;
    }

    public void setFechaCaducidad(Date fechaCaducidad) {
        this.fechaCaducidad = fechaCaducidad;
        if(fechaCaducidad == null)
            this.stringFechaCaducidad = "";
        else
            this.stringFechaCaducidad = new SimpleDateFormat("dd/MM/yyyy").format(fechaCaducidad);
    }

    public String getStringFechaCaducidad() {
        if(fechaCaducidad != null)
            return new SimpleDateFormat("dd/MM/yyyy").format(fechaCaducidad);
        else 
            return "";
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    @XmlTransient
    public Collection<CnapiMarcasCliente> getCnapiMarcasClienteCollection() {
        return cnapiMarcasClienteCollection;
    }

    public void setCnapiMarcasClienteCollection(Collection<CnapiMarcasCliente> cnapiMarcasClienteCollection) {
        this.cnapiMarcasClienteCollection = cnapiMarcasClienteCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnapiClientes)) {
            return false;
        }
        CnapiClientes other = (CnapiClientes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnapiClientes[ id=" + id + " ]";
    }
    
}
