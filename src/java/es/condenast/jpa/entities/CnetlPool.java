/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import static javax.persistence.ParameterMode.IN;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnetl_pool")
@NamedQueries({
    @NamedQuery(name = "CnetlPool.findAll", query = "SELECT c FROM CnetlPool c"),
    @NamedQuery(name = "CnetlPool.findByIdPool", query = "SELECT c FROM CnetlPool c WHERE c.idPool = :idPool"),
    @NamedQuery(name = "CnetlPool.findByArchivo", query = "SELECT c FROM CnetlPool c WHERE c.archivo = :archivo"),
    @NamedQuery(name = "CnetlPool.findByError", query = "SELECT c FROM CnetlPool c WHERE c.error = :error"),
    @NamedQuery(name = "CnetlPool.findByEsAutomatico", query = "SELECT c FROM CnetlPool c WHERE c.esAutomatico = :esAutomatico"),
    @NamedQuery(name = "CnetlPool.getEstado", query = "SELECT c.idEstado FROM CnetlPool c WHERE c.idPool = :idPool"),
    @NamedQuery(name = "CnetlPool.findByIdEstado", query = "SELECT c FROM CnetlPool c WHERE c.idEstado = :idEstado")})

@NamedStoredProcedureQueries({
    @NamedStoredProcedureQuery(name = "CnetlPool.SPETL_POOL_BUILD", procedureName = "SPETL_POOL_BUILD"),
    @NamedStoredProcedureQuery(name = "CnetlPool.SPETL_POOL_QUERY", procedureName = "SPETL_POOL_QUERY",
            parameters = {
                @StoredProcedureParameter(mode = IN, name = "P_IdFuente", type = Integer.class),
                @StoredProcedureParameter(mode = IN, name = "P_File", type = String.class),
                @StoredProcedureParameter(mode = IN, name = "P_File_Ok", type = String.class),
                @StoredProcedureParameter(mode = IN, name = "P_File_Error", type = String.class),
                @StoredProcedureParameter(mode = IN, name = "P_EsAutomatico", type = Boolean.class)
            }),
    @NamedStoredProcedureQuery(name = "CnetlPool.SPETL_POOL_EXECUTE", procedureName = "SPETL_POOL_EXECUTE",
            parameters = {
                @StoredProcedureParameter(mode = IN, name = "P_IdPool", type = Long.class)
            }),
    @NamedStoredProcedureQuery(name = "CnetlPool.SPETL_POOL_REGLA", procedureName = "SPETL_POOL_REGLA",
            parameters = {
                @StoredProcedureParameter(mode = IN, name = "P_IdPools", type = String.class)
            }),
    @NamedStoredProcedureQuery(name = "CnetlPool.SPETL_POOL_EXECUTE_OPTIMIZE", procedureName = "SPETL_POOL_EXECUTE_OPTIMIZE")
})
public class CnetlPool implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdPool")
    private Long idPool;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "Query")
    private String query;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Archivo")
    private String archivo;
    @Size(max = 200)
    @Column(name = "Error")
    private String error;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EsAutomatico")
    private boolean esAutomatico;
//    @JoinColumn(name = "IdEstado", referencedColumnName = "IdEstado")
////    @ManyToOne(optional = false)
//    private CnetlEstado idEstado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdEstado")
    private Short idEstado;

    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "ArchivoOk")
    private String archivoOk;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "ArchivoError")
    private String archivoError;

    public CnetlPool() {
    }

    public CnetlPool(Long idPool) {
        this.idPool = idPool;
    }

    public CnetlPool(Long idPool, String query, String archivo, boolean esAutomatico) {
        this.idPool = idPool;
        this.query = query;
        this.archivo = archivo;
        this.esAutomatico = esAutomatico;
    }

    public Long getIdPool() {
        return idPool;
    }

    public void setIdPool(Long idPool) {
        this.idPool = idPool;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public boolean getEsAutomatico() {
        return esAutomatico;
    }

    public void setEsAutomatico(boolean esAutomatico) {
        this.esAutomatico = esAutomatico;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPool != null ? idPool.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlPool)) {
            return false;
        }
        CnetlPool other = (CnetlPool) object;
        if ((this.idPool == null && other.idPool != null) || (this.idPool != null && !this.idPool.equals(other.idPool))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnetlPool[ idPool=" + idPool + " ]";
    }

//    public CnetlEstado getIdEstado() {
//        return idEstado;
//    }
//
//    public void setIdEstado(CnetlEstado idEstado) {
//        this.idEstado = idEstado;
//    }
    public Short getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Short idEstado) {
        this.idEstado = idEstado;
    }

    
    
    
    public String getArchivoOk() {
        return archivoOk;
    }

    public void setArchivoOk(String archivoOk) {
        this.archivoOk = archivoOk;
    }

    public String getArchivoError() {
        return archivoError;
    }

    public void setArchivoError(String archivoError) {
        this.archivoError = archivoError;
    }

}
