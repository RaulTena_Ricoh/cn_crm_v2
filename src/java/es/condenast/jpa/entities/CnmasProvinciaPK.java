/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ivan.fernandez
 */
@Embeddable
public class CnmasProvinciaPK implements Serializable{
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdProvincia")
    private long idProvincia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdPais")
    private Short idPais;
    
    public CnmasProvinciaPK(){
        
    }
    public CnmasProvinciaPK(Short idPais, Short idProvincia){
        this.idPais = idPais;
        this.idProvincia = idProvincia;
    }

    /**
     * @return the idProvincia
     */
    public long getIdProvincia() {
        return idProvincia;
    }

    /**
     * @param idProvincia the idProvincia to set
     */
    public void setIdProvincia(long idProvincia) {
        this.idProvincia = idProvincia;
    }

    /**
     * @return the idPais
     */
    public Short getIdPais() {
        return idPais;
    }

    /**
     * @param idPais the idPais to set
     */
    public void setIdPais(Short idPais) {
        this.idPais = idPais;
    }
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasProvinciaPK)) {
            return false;
        }
        CnmasProvinciaPK other = (CnmasProvinciaPK) object;
        if (!Objects.equals(this.idPais, other.idPais)) {
            return false;
        }
        return this.idProvincia == other.idProvincia;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasProvinciaPK[ idPais=" + idPais + ", idProvincia=" + idProvincia + " ]";
    }
}
