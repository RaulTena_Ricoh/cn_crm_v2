/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cnetl_master_valor")
@NamedQueries({
    @NamedQuery(name = "CnetlMasterValor.findAll", query = "SELECT c FROM CnetlMasterValor c"),
    @NamedQuery(name = "CnetlMasterValor.findByIdMaster", query = "SELECT c FROM CnetlMasterValor c WHERE c.cnetlMasterValorPK.idMaster = :idMaster"),
    @NamedQuery(name = "CnetlMasterValor.findByIdOrigen", query = "SELECT c FROM CnetlMasterValor c WHERE c.cnetlMasterValorPK.idOrigen = :idOrigen"),
    @NamedQuery(name = "CnetlMasterValor.findByValor", query = "SELECT c FROM CnetlMasterValor c WHERE c.valor = :valor")})
public class CnetlMasterValor implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CnetlMasterValorPK cnetlMasterValorPK;
    @Basic(optional = false)
    @NotNull
    // TODO: JDC
    @Size(min = 0, max = 200)
    @Column(name = "Valor")
    private String valor;
    @JoinColumn(name = "IdMaster", referencedColumnName = "IdMaster", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnetlMaster cnetlMaster;
    @JoinColumn(name = "IdOrigen", referencedColumnName = "IdOrigen", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnetlOrigen cnetlOrigen;

    public CnetlMasterValor() {
    }

    public CnetlMasterValor(CnetlMasterValorPK cnetlMasterValorPK) {
        this.cnetlMasterValorPK = cnetlMasterValorPK;
    }

    public CnetlMasterValor(CnetlMasterValorPK cnetlMasterValorPK, String valor) {
        this.cnetlMasterValorPK = cnetlMasterValorPK;
        this.valor = valor;
    }

    public CnetlMasterValor(int idMaster, int idOrigen) {
        this.cnetlMasterValorPK = new CnetlMasterValorPK(idMaster, idOrigen);
    }

    public CnetlMasterValorPK getCnetlMasterValorPK() {
        return cnetlMasterValorPK;
    }

    public void setCnetlMasterValorPK(CnetlMasterValorPK cnetlMasterValorPK) {
        this.cnetlMasterValorPK = cnetlMasterValorPK;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public CnetlMaster getCnetlMaster() {
        return cnetlMaster;
    }

    public void setCnetlMaster(CnetlMaster cnetlMaster) {
        this.cnetlMaster = cnetlMaster;
    }

    public CnetlOrigen getCnetlOrigen() {
        return cnetlOrigen;
    }

    public void setCnetlOrigen(CnetlOrigen cnetlOrigen) {
        this.cnetlOrigen = cnetlOrigen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cnetlMasterValorPK != null ? cnetlMasterValorPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlMasterValor)) {
            return false;
        }
        CnetlMasterValor other = (CnetlMasterValor) object;
        if ((this.cnetlMasterValorPK == null && other.cnetlMasterValorPK != null) || (this.cnetlMasterValorPK != null && !this.cnetlMasterValorPK.equals(other.cnetlMasterValorPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnetlMasterValor[ cnetlMasterValorPK=" + cnetlMasterValorPK + " ]";
    }
    
}
