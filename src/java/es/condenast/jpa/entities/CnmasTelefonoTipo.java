/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name="cnmas_telefono_tipo")
@NamedQueries({
    @NamedQuery(name = "CnmasTelefonoTipo.findAll", query = "SELECT c FROM CnmasTelefonoTipo c"),
    @NamedQuery(name = "CnmasTelefonoTipo.findByIdTelefonoTipo", query = "SELECT c FROM CnmasTelefonoTipo c WHERE c.idTelefonoTipo = :idTelefonoTipo")})
public class CnmasTelefonoTipo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "IdTelefonoTipo")
    private Short idTelefonoTipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;

    public CnmasTelefonoTipo() {
    }

    public Short getIdTelefonoTipo() {
        return idTelefonoTipo;
    }

    public void setIdTelefonoTipo(Short idTelefonoTipo) {
        this.idTelefonoTipo = idTelefonoTipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTelefonoTipo != null ? idTelefonoTipo.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasTelefonoTipo)) {
            return false;
        }
        CnmasTelefonoTipo other = (CnmasTelefonoTipo) object;
        if ((this.idTelefonoTipo == null && other.idTelefonoTipo != null) || (this.idTelefonoTipo != null && !this.idTelefonoTipo.equals(other.idTelefonoTipo))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasTelefonoTipo[ idTelefonoTipo=" + idTelefonoTipo + " ]";
    }
    
}
