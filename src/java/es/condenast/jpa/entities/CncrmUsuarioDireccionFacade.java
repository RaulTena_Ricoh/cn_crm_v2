/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author ivan.fernandez
 */
@Stateless
public class CncrmUsuarioDireccionFacade  extends AbstractFacade<CncrmUsuarioDireccion>{
    @PersistenceContext(unitName = "CondeNastPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public CncrmUsuarioDireccionFacade(){
        super(CncrmUsuarioDireccion.class);
    }

    public List<CncrmUsuarioDireccion> getDetail(Long idUsuario, String select, String tableName){
        Query query = getEntityManager().createNamedQuery("CncrmUsuarioDireccion.getDetail");
        String sql = select + " FROM " + tableName + " WHERE idUsuario = ?" ;
        query.setParameter("idUsuario", idUsuario);
        return query.getResultList();
    }
}
