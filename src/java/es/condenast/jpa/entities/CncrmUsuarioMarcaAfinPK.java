/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Embeddable
public class CncrmUsuarioMarcaAfinPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdUsuario")
    private long idUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdMarcaAfin")
    private Short idMarcaAfin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdMarca")
    private Short idMarca;

    /**
     *
     */
    public CncrmUsuarioMarcaAfinPK() {
    }

    /**
     *
     * @param idUsuario
     * @param idMarcaAfin
     * @param idMarca
     */
    public CncrmUsuarioMarcaAfinPK(long idUsuario, Short idMarcaAfin, Short idMarca) {
        this.idUsuario = idUsuario;
        this.idMarcaAfin = idMarcaAfin;
        this.idMarca = idMarca;
    }

    /**
     *
     * @return
     */
    public long getIdUsuario() {
        return idUsuario;
    }

    /**
     *
     * @param idUsuario
     */
    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     *
     * @return
     */
    public Short getIdMarcaAfin() {
        return idMarcaAfin;
    }

    /**
     *
     * @param idMarcaAfin
     */
    public void setIdMarcaAfin(Short idMarcaAfin) {
        this.idMarcaAfin = idMarcaAfin;
    }

    /**
     *
     * @return
     */
    public Short getIdMarca() {
        return idMarca;
    }

    /**
     *
     * @param idMarca
     */
    public void setIdMarca(Short idMarca) {
        this.idMarca = idMarca;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) this.idUsuario;
        hash += this.idMarcaAfin;
        hash += this.idMarca;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CncrmUsuarioMarcaAfinPK)) {
            return false;
        }
        CncrmUsuarioMarcaAfinPK other = (CncrmUsuarioMarcaAfinPK) object;
        if (this.idUsuario != other.idUsuario) {
            return false;
        }
        if (!Objects.equals(this.idMarcaAfin, other.idMarcaAfin)) {
            return false;
        }
        if (!Objects.equals(this.idMarca, other.idMarca)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioMarcaAfinPK[ idUsuario=" + idUsuario
                + "idMarcaAfin=" + idMarcaAfin
                + "idMarca=" + idMarca + " ]";
    }
    
}
