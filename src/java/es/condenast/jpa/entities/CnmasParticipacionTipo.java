/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ivan.fernandez
 */
@Entity
@Table(name="cnmas_participacion_tipo")
@NamedQueries({
    @NamedQuery(name = "CnmasParticipacionTipo.findAll", query = "SELECT c FROM CnmasParticipacionTipo c ORDER BY c.idParticipacionTipo"),
    @NamedQuery(name = "CnmasParticipacionTipo.getMaxId", query = "SELECT max(c.idParticipacionTipo) + 1 FROM CnmasParticipacionTipo c"),
    @NamedQuery(name = "CnmasParticipacionTipo.findByIdParticipacionTipo", query = "SELECT c FROM CnmasParticipacionTipo c WHERE c.idParticipacionTipo = :idParticipacionTipo")})
public class CnmasParticipacionTipo implements Serializable{
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdParticipacionTipo")
    private Short idParticipacionTipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;

    /**
     * @return the idParticipacionTipo
     */
    public Short getIdParticipacionTipo() {
        return idParticipacionTipo;
    }

    /**
     * @param idParticipacionTipo the idParticipacionTipo to set
     */
    public void setIdParticipacionTipo(Short idParticipacionTipo) {
        this.idParticipacionTipo = idParticipacionTipo;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idParticipacionTipo != null ? idParticipacionTipo.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasParticipacionTipo)) {
            return false;
        }
        CnmasParticipacionTipo other = (CnmasParticipacionTipo) object;
        if ((this.idParticipacionTipo == null && other.idParticipacionTipo != null) || (this.idParticipacionTipo != null && !this.idParticipacionTipo.equals(other.idParticipacionTipo))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasParticipacionTipo[ idParticipacionTipo=" + idParticipacionTipo + " ]";
    }  
}
