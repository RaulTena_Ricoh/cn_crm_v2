/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Ivan.Fernandez
 */
@Entity
@Table(name="cnmas_participacion")
@NamedQueries({
    @NamedQuery(name = "CnmasParticipacion.findAll", query = "SELECT c FROM CnmasParticipacion C"),
    @NamedQuery(name = "CnmasParticipacion.findByNombre", query = "SELECT c FROM CnmasParticipacion C WHERE c.nombre = :nombre"),
    @NamedQuery(name = "CnmasParticipacion.findByIdParticipacion", query = "SELECT c FROM CnmasParticipacion C WHERE c.idParticipacionTipo = :idParticipacionTipo")})
public class CnmasParticipacion implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdParticipacion")
    private Short idParticipacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @Column(name = "Fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @JoinColumn(name = "IdParticipacionTipo", referencedColumnName = "IdParticipacionTipo")
    @ManyToOne(optional = false)
    private CnmasParticipacionTipo idParticipacionTipo;
    @JoinColumn(name = "IdMarca", referencedColumnName = "IdMarca")
    @ManyToOne(optional = false)
    private CnmasMarca IdMarca;
    @JoinColumn(name = "IdProducto", referencedColumnName = "IdProducto")
    @ManyToOne(optional = false)
    private CnmasProducto idProducto;
    @JoinColumn(name = "IdCategoria", referencedColumnName = "IdCategoria")
    @ManyToOne(optional = false)
    private CnmasCategoria idCategoria;

    /**
     * @return the idParticipacion
     */
    public Short getIdParticipacion() {
        return idParticipacion;
    }

    /**
     * @param idParticipacion the idParticipacion to set
     */
    public void setIdParticipacion(Short idParticipacion) {
        this.idParticipacion = idParticipacion;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the fecha
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the IdMarca
     */
    public CnmasMarca getIdMarca() {
        return IdMarca;
    }

    /**
     * @param IdMarca the IdMarca to set
     */
    public void setIdMarca(CnmasMarca IdMarca) {
        this.IdMarca = IdMarca;
    }

    /**
     * @return the idProducto
     */
    public CnmasProducto getIdProducto() {
        return idProducto;
    }

    /**
     * @param idProducto the idProducto to set
     */
    public void setIdProducto(CnmasProducto idProducto) {
        this.idProducto = idProducto;
    }

    /**
     * @return the idCategoria
     */
    public CnmasCategoria getIdCategoria() {
        return idCategoria;
    }

    /**
     * @param idCategoria the idCategoria to set
     */
    public void setIdCategoria(CnmasCategoria idCategoria) {
        this.idCategoria = idCategoria;
    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getIdParticipacion() != null ? getIdParticipacion().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasParticipacion)) {
            return false;
        }
        CnmasParticipacion other = (CnmasParticipacion) object;
        if ((this.getIdParticipacion() == null && other.getIdParticipacion() != null) || (this.getIdParticipacion() != null && !this.idParticipacion.equals(other.idParticipacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre;
    }

    /**
     * @return the idParticipacionTipo
     */
    public CnmasParticipacionTipo getIdParticipacionTipo() {
        return idParticipacionTipo;
    }

    /**
     * @param idParticipacionTipo the idParticipacionTipo to set
     */
    public void setIdParticipacionTipo(CnmasParticipacionTipo idParticipacionTipo) {
        this.idParticipacionTipo = idParticipacionTipo;
    }
}
