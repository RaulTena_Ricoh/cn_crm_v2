/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cncrm_usuario_compra_online")
@NamedQueries({
    @NamedQuery(name = "CncrmUsuarioCompraOnline.findAll", query = "SELECT c FROM CncrmUsuarioCompraOnline c"),
    @NamedQuery(name = "CncrmUsuarioCompraOnline.findByIdUsuario", query = "SELECT c FROM CncrmUsuarioCompraOnline c WHERE c.cncrmUsuarioCompraOnlinePK.idUsuario = :idUsuario"),
    @NamedQuery(name = "CncrmUsuarioCompraOnline.findByIdCompraOnline", query = "SELECT c FROM CncrmUsuarioCompraOnline c WHERE c.cncrmUsuarioCompraOnlinePK.idCompraOnline = :idCompraOnline")})
public class CncrmUsuarioCompraOnline implements Serializable {
    
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @EmbeddedId
    protected CncrmUsuarioCompraOnlinePK cncrmUsuarioCompraOnlinePK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    
    @JoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CncrmUsuario cncrmUsuario;
    @JoinColumn(name = "IdCompraOnline", referencedColumnName = "IdCompraOnline", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasCompraOnline cnmasCompraOnline;

    /**
     *
     */
    public CncrmUsuarioCompraOnline() {
    }

    /**
     *
     * @param cncrmUsuarioCompraOnlinePK
     */
    public CncrmUsuarioCompraOnline(CncrmUsuarioCompraOnlinePK cncrmUsuarioCompraOnlinePK) {
        this.cncrmUsuarioCompraOnlinePK = cncrmUsuarioCompraOnlinePK;
    }

    /**
     *
     * @param cncrmUsuarioCompraOnlinePK
     * @param fecha
     */
    public CncrmUsuarioCompraOnline(CncrmUsuarioCompraOnlinePK cncrmUsuarioCompraOnlinePK, Date fecha) {
        this.cncrmUsuarioCompraOnlinePK = cncrmUsuarioCompraOnlinePK;
        this.fecha = fecha;
    }

    /**
     *
     * @param idUsuario
     * @param idCompraOnline
     */
    public CncrmUsuarioCompraOnline(Long idUsuario, Short idCompraOnline) {
        this.cncrmUsuarioCompraOnlinePK = new CncrmUsuarioCompraOnlinePK(idUsuario, idCompraOnline);
    }

    /**
     *
     * @return
     */
    public CncrmUsuarioCompraOnlinePK getCncrmUsuarioCompraOnlinePK() {
        return cncrmUsuarioCompraOnlinePK;
    }

    /**
     *
     * @param cncrmUsuarioCompraOnlinePK
     */
    public void setCncrmUsuarioCompraOnlinePK(CncrmUsuarioCompraOnlinePK cncrmUsuarioCompraOnlinePK) {
        this.cncrmUsuarioCompraOnlinePK = cncrmUsuarioCompraOnlinePK;
    }

    /**
     *
     * @return
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     *
     * @param fecha
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     *
     * @return
     */
    public CncrmUsuario getCncrmUsuario() {
        return cncrmUsuario;
    }

    /**
     *
     * @param cncrmUsuario
     */
    public void setCncrmUsuario(CncrmUsuario cncrmUsuario) {
        this.cncrmUsuario = cncrmUsuario;
    }

    /**
     *
     * @return
     */
    public CnmasCompraOnline getCnmasCompraOnline() {
        return cnmasCompraOnline;
    }

    /**
     *
     * @param cnmasCompraOnline
     */
    public void setCnmasCompraOnline(CnmasCompraOnline cnmasCompraOnline) {
        this.cnmasCompraOnline = cnmasCompraOnline;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cncrmUsuarioCompraOnlinePK != null ? cncrmUsuarioCompraOnlinePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CncrmUsuarioCompraOnline)) {
            return false;
        }
        CncrmUsuarioCompraOnline other = (CncrmUsuarioCompraOnline) object;
        if ((this.cncrmUsuarioCompraOnlinePK == null && other.cncrmUsuarioCompraOnlinePK != null) || (this.cncrmUsuarioCompraOnlinePK != null && !this.cncrmUsuarioCompraOnlinePK.equals(other.cncrmUsuarioCompraOnlinePK))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioCompraOnline[ cncrmUsuarioCompraOnlinePK=" + cncrmUsuarioCompraOnlinePK + " ]";
    }
    
}
