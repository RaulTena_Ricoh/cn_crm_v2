/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "vietl_log_regla")
@NamedQueries({
    @NamedQuery(name = "VietlLogRegla.findAll", query = "SELECT v FROM VietlLogRegla v"),
    @NamedQuery(name = "VietlLogRegla.findByIdPool", query = "SELECT v FROM VietlLogRegla v WHERE v.idPool = :idPool"),
    @NamedQuery(name = "VietlLogRegla.findByFuente", query = "SELECT v FROM VietlLogRegla v WHERE v.fuente = :fuente"),
    @NamedQuery(name = "VietlLogRegla.findByFuenteTipo", query = "SELECT v FROM VietlLogRegla v WHERE v.fuenteTipo = :fuenteTipo"),
    @NamedQuery(name = "VietlLogRegla.findByInicioPool", query = "SELECT v FROM VietlLogRegla v WHERE v.inicioPool = :inicioPool"),
    @NamedQuery(name = "VietlLogRegla.findByLoadPool", query = "SELECT v FROM VietlLogRegla v WHERE v.loadPool = :loadPool"),
    @NamedQuery(name = "VietlLogRegla.findByFinPool", query = "SELECT v FROM VietlLogRegla v WHERE v.finPool = :finPool"),
    @NamedQuery(name = "VietlLogRegla.findByLineas", query = "SELECT v FROM VietlLogRegla v WHERE v.lineas = :lineas"),
    @NamedQuery(name = "VietlLogRegla.findByLineasOK", query = "SELECT v FROM VietlLogRegla v WHERE v.lineasOK = :lineasOK"),
    @NamedQuery(name = "VietlLogRegla.findByLineasError", query = "SELECT v FROM VietlLogRegla v WHERE v.lineasError = :lineasError"),
    @NamedQuery(name = "VietlLogRegla.findByIdReglaImportacion", query = "SELECT v FROM VietlLogRegla v WHERE v.idReglaImportacion = :idReglaImportacion"),
    @NamedQuery(name = "VietlLogRegla.findByRegla", query = "SELECT v FROM VietlLogRegla v WHERE v.regla = :regla"),
    @NamedQuery(name = "VietlLogRegla.findByFechaInicio", query = "SELECT v FROM VietlLogRegla v WHERE v.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "VietlLogRegla.findByFechaFin", query = "SELECT v FROM VietlLogRegla v WHERE v.fechaFin = :fechaFin"),
    @NamedQuery(name = "VietlLogRegla.findByDuracionPool", query = "SELECT v FROM VietlLogRegla v WHERE v.duracionPool = :duracionPool"),
    @NamedQuery(name = "VietlLogRegla.findByDuracionLoad", query = "SELECT v FROM VietlLogRegla v WHERE v.duracionLoad = :duracionLoad"),
    @NamedQuery(name = "VietlLogRegla.findByDuracionRegla", query = "SELECT v FROM VietlLogRegla v WHERE v.duracionRegla = :duracionRegla"),
    @NamedQuery(name = "VietlLogRegla.findByFecha", query = "SELECT v FROM VietlLogRegla v WHERE v.fecha = :fecha")})
public class VietlLogRegla implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdPool")
    @Id
    private long idPool;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Fuente")
    @Id
    private String fuente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "FuenteTipo")
    @Id
    private String fuenteTipo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "InicioPool")
    @Temporal(TemporalType.TIMESTAMP)
    private Date inicioPool;
    @Column(name = "LoadPool")
    @Temporal(TemporalType.TIMESTAMP)
    private Date loadPool;
    @Column(name = "FinPool")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finPool;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Lineas")
    private int lineas;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LineasOK")
    private int lineasOK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LineasError")
    private int lineasError;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdReglaImportacion")
    @Id
    private short idReglaImportacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Regla")
    @Id
    private String regla;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaInicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    @Column(name = "FechaFin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;
    @Column(name = "DuracionPool")
    @Temporal(TemporalType.TIME)
    private Date duracionPool;
    @Column(name = "DuracionLoad")
    @Temporal(TemporalType.TIME)
    private Date duracionLoad;
    @Column(name = "DuracionRegla")
    @Temporal(TemporalType.TIME)
    private Date duracionRegla;
    @Column(name = "Fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    public VietlLogRegla() {
    }

    public long getIdPool() {
        return idPool;
    }

    public void setIdPool(long idPool) {
        this.idPool = idPool;
    }

    public String getFuente() {
        return fuente;
    }

    public void setFuente(String fuente) {
        this.fuente = fuente;
    }

    public String getFuenteTipo() {
        return fuenteTipo;
    }

    public void setFuenteTipo(String fuenteTipo) {
        this.fuenteTipo = fuenteTipo;
    }

    public Date getInicioPool() {
        return inicioPool;
    }

    public void setInicioPool(Date inicioPool) {
        this.inicioPool = inicioPool;
    }

    public Date getLoadPool() {
        return loadPool;
    }

    public void setLoadPool(Date loadPool) {
        this.loadPool = loadPool;
    }

    public Date getFinPool() {
        return finPool;
    }

    public void setFinPool(Date finPool) {
        this.finPool = finPool;
    }

    public int getLineas() {
        return lineas;
    }

    public void setLineas(int lineas) {
        this.lineas = lineas;
    }

    public int getLineasOK() {
        return lineasOK;
    }

    public void setLineasOK(int lineasOK) {
        this.lineasOK = lineasOK;
    }

    public int getLineasError() {
        return lineasError;
    }

    public void setLineasError(int lineasError) {
        this.lineasError = lineasError;
    }

    public short getIdReglaImportacion() {
        return idReglaImportacion;
    }

    public void setIdReglaImportacion(short idReglaImportacion) {
        this.idReglaImportacion = idReglaImportacion;
    }

    public String getRegla() {
        return regla;
    }

    public void setRegla(String regla) {
        this.regla = regla;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getDuracionPool() {
        return duracionPool;
    }

    public void setDuracionPool(Date duracionPool) {
        this.duracionPool = duracionPool;
    }

    public Date getDuracionLoad() {
        return duracionLoad;
    }

    public void setDuracionLoad(Date duracionLoad) {
        this.duracionLoad = duracionLoad;
    }

    public Date getDuracionRegla() {
        return duracionRegla;
    }

    public void setDuracionRegla(Date duracionRegla) {
        this.duracionRegla = duracionRegla;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

}
