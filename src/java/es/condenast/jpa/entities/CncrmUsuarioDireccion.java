/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ivan.fernandez
 */
@Entity
@Table(name = "cncrm_direccion")
@NamedQueries({
    @NamedQuery(name = "CncrmUsuarioDireccion.findAll", query = "SELECT c FROM CncrmUsuarioDireccion c"),
    @NamedQuery(name = "CncrmUsuarioDireccion.getDetail", query = "SELECT c FROM CncrmUsuarioDireccion c WHERE c.idUsuario = :idUsuario"),
    @NamedQuery(name = "CncrmUsuarioDireccion.findByIdDireccion", query = "SELECT c FROM CncrmUsuarioDireccion c WHERE c.idDireccion = :idDireccion")})
public class CncrmUsuarioDireccion implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdDireccion")
    private Long idDireccion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdUsuario")
    private Long idUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdDireccionTipo")
    private Short idDireccionTipo;
    @JoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CncrmUsuario cncrmUsuario;
    @JoinColumn(name = "IdMarca", referencedColumnName = "IdMarca", insertable = false, updatable = false)
    @ManyToOne(optional = true)
    private CnmasMarca cnmasMarca;
    @JoinColumn(name = "IdDireccionTipo", referencedColumnName = "IdDireccionTipo", insertable = false, nullable = false,  updatable = false)
    @ManyToOne(optional = false)
    private CnmasDireccionTipo cnmasDreccionTipo;
    @JoinColumn(name = "IdPais", referencedColumnName = "IdPais", insertable = false, updatable = false)
    @ManyToOne(optional = true)
    private CnmasPais cnmasPais;
   /* @JoinColumns({
         @JoinColumn(name = "IdPais", referencedColumnName = "IdPais", insertable = false, updatable = false),
         @JoinColumn(name = "IdProvincia", referencedColumnName = "IdProvincia", insertable = false, updatable = false)
     })
    @ManyToOne(optional = true)
    private CnmasProvincia cnmasProvincia;
    @JoinColumns({
        @JoinColumn(name = "IdPoblacion", referencedColumnName = "IdPoblacion", insertable = false, updatable = false),
        @JoinColumn(name = "IdProvincia", referencedColumnName = "IdProvincia", insertable = false, updatable = false)
    })
    @ManyToOne(optional = true)
    private CnmasPoblacion cnmasPoblacion;*/
    @Basic(optional = true)
    @Column(name = "IdMarca")
    private Short idMarca;
    @Basic(optional = true)
    @Column(name = "IdPais")
    private Short idPais;
    @Basic(optional = true)
    @Column(name = "IdProvincia")
    private long idProvincia;
    @Basic(optional = true)
    @Column(name = "IdPoblacion")
    private long idPoblacion;
    @Basic(optional = true)
    @Size(min = 1, max = 10)
    @Column(name = "CodigoPostal")
    private String codigoPostal;
    @Basic(optional = true)
    @Size(min = 1, max = 200)
    @Column(name = "Direccion")
    private String direccion;
    @Basic(optional = true)
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = true)
    @Size(min = 1, max = 200)
    @Column(name = "Apellido1")
    private String apellido1;
    @Size(min = 1, max = 200)
    @Column(name = "Apellido2")
    private String apellido2;
    @Size(min = 1, max = 200)
    @Column(name = "PaisLiteral")
    private String paisLiteral;
    @Size(min = 1, max = 200)
    @Column(name = "ProvinciaLiteral")
    private String provinciaLiteral;
    @Size(min = 1, max = 200)
    @Column(name = "PoblacionLiteral")
    private String poblacionLiteral;
    @Size(min = 1, max = 200)
    @Column(name = "CodigoPostalLiteral")
    private String codigoPostalLiteral;
    @Basic(optional = true)
    @Column(name = "FechaAlta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAlta;

    public CncrmUsuarioDireccion(){
        
    }
    
    public CncrmUsuarioDireccion(Long idDireccion, Long idUsuario, Short idDireccionTipo, Short idMarca,
            String codigoPostal, Short idPais, Long idPoblacion, Long idProvincia,
            String direccion, String nombre, String apellido1, String apellido2, 
            String paisLiteral, String provinciaLiteral, String poblacionLiteral, String codigoPostalLiteral, 
            Date fechaAlta){
        this.idDireccion = idDireccion;
        this.idUsuario = idUsuario;
        this.idDireccionTipo = idDireccionTipo;
        this.idMarca = idMarca;
        this.codigoPostal = codigoPostal;
        this.idPais = idPais;
        this.idProvincia = idProvincia;
        this.idPoblacion = idPoblacion;
        this.direccion = direccion;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.paisLiteral = paisLiteral;
        this.provinciaLiteral = provinciaLiteral;
        this.poblacionLiteral = poblacionLiteral;
        this.codigoPostalLiteral = codigoPostalLiteral;
        
    }
    /**
     * @return the idDireccion
     */
    public Long getIdDireccion() {
        return idDireccion;
    }

    /**
     * @param idDireccion the idDireccion to set
     */
    public void setIdDireccion(Long idDireccion) {
        this.idDireccion = idDireccion;
    }

    /**
     * @return the cncrmUsuario
     */
    public CncrmUsuario getCncrmUsuario() {
        return cncrmUsuario;
    }

    /**
     * @param cncrmUsuario the cncrmUsuario to set
     */
    public void setCncrmUsuario(CncrmUsuario cncrmUsuario) {
        this.cncrmUsuario = cncrmUsuario;
    }

    /**
     * @return the cnmasMarca
     */
    public CnmasMarca getCnmasMarca() {
        return cnmasMarca;
    }

    /**
     * @param cnmasMarca the cnmasMarca to set
     */
    public void setCnmasMarca(CnmasMarca cnmasMarca) {
        this.cnmasMarca = cnmasMarca;
    }

    /**
     * @return the cnmasDreccionTipo
     */
    public CnmasDireccionTipo getCnmasDreccionTipo() {
        return cnmasDreccionTipo;
    }

    /**
     * @param cnmasDreccionTipo the cnmasDreccionTipo to set
     */
    public void setCnmasDreccionTipo(CnmasDireccionTipo cnmasDreccionTipo) {
        this.cnmasDreccionTipo = cnmasDreccionTipo;
    }

    /**
     * @return the cnmasPais
     */
    public CnmasPais getCnmasPais() {
        return cnmasPais;
    }

    /**
     * @param cnmasPais the cnmasPais to set
     */
    public void setCnmasPais(CnmasPais cnmasPais) {
        this.cnmasPais = cnmasPais;
    }
/*
    public CnmasProvincia getCnmasProvincia() {
        return cnmasProvincia;
    }

    public void setCnmasProvincia(CnmasProvincia cnmasProvincia) {
        this.cnmasProvincia = cnmasProvincia;
    }

    public CnmasPoblacion getCnmasPoblacion() {
        return cnmasPoblacion;
    }

    public void setCnmasPoblacion(CnmasPoblacion cnmasPoblacion) {
        this.cnmasPoblacion = cnmasPoblacion;
    }*/

    /**
     * @return the codigoPostal
     */
    public String getCodigoPostal() {
        return codigoPostal;
    }

    /**
     * @param codigoPostal the codigoPostal to set
     */
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the apellido1
     */
    public String getApellido1() {
        return apellido1;
    }

    /**
     * @param apellido1 the apellido1 to set
     */
    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    /**
     * @return the apellido2
     */
    public String getApellido2() {
        return apellido2;
    }

    /**
     * @param apellido2 the apellido2 to set
     */
    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    /**
     * @return the paisLiteral
     */
    public String getPaisLiteral() {
        return paisLiteral;
    }

    /**
     * @param paisLiteral the paisLiteral to set
     */
    public void setPaisLiteral(String paisLiteral) {
        this.paisLiteral = paisLiteral;
    }

    /**
     * @return the provinciaLiteral
     */
    public String getProvinciaLiteral() {
        return provinciaLiteral;
    }

    /**
     * @param provinciaLiteral the provinciaLiteral to set
     */
    public void setProvinciaLiteral(String provinciaLiteral) {
        this.provinciaLiteral = provinciaLiteral;
    }

    /**
     * @return the poblacionLiteral
     */
    public String getPoblacionLiteral() {
        return poblacionLiteral;
    }

    /**
     * @param poblacionLiteral the poblacionLiteral to set
     */
    public void setPoblacionLiteral(String poblacionLiteral) {
        this.poblacionLiteral = poblacionLiteral;
    }

    /**
     * @return the codigoPostalLiteral
     */
    public String getCodigoPostalLiteral() {
        return codigoPostalLiteral;
    }

    /**
     * @param codigoPostalLiteral the codigoPostalLiteral to set
     */
    public void setCodigoPostalLiteral(String codigoPostalLiteral) {
        this.codigoPostalLiteral = codigoPostalLiteral;
    }

    /**
     * @return the fechaAlta
     */
    public Date getFechaAlta() {
        return fechaAlta;
    }

    /**
     * @param fechaAlta the fechaAlta to set
     */
    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    /**
     * @return the idUsuario
     */
    public Long getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @return the idDireccionTipo
     */
    public Short getIdDireccionTipo() {
        return idDireccionTipo;
    }

    /**
     * @param idDireccionTipo the idDireccionTipo to set
     */
    public void setIdDireccionTipo(Short idDireccionTipo) {
        this.idDireccionTipo = idDireccionTipo;
    }

    /**
     * @return the idPais
     */
    public Short getIdPais() {
        return idPais;
    }

    /**
     * @param idPais the idPais to set
     */
    public void setIdPais(Short idPais) {
        this.idPais = idPais;
    }

    /**
     * @return the idProvincia
     */
    public long getIdProvincia() {
        return idProvincia;
    }

    /**
     * @param idProvincia the idProvincia to set
     */
    public void setIdProvincia(long idProvincia) {
        this.idProvincia = idProvincia;
    }

    /**
     * @return the idPoblacion
     */
    public long getIdPoblacion() {
        return idPoblacion;
    }

    /**
     * @param idPoblacion the idPoblacion to set
     */
    public void setIdPoblacion(long idPoblacion) {
        this.idPoblacion = idPoblacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDireccion != null ? idDireccion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CncrmUsuarioDireccion)) {
            return false;
        }
        CncrmUsuarioDireccion other = (CncrmUsuarioDireccion) object;
        if ((this.idDireccion == null && other.idDireccion != null)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioDireccion[ idDireccion=" + idDireccion + " ]";
    }

    /**
     * @return the idMarca
     */
    public Short getIdMarca() {
        return idMarca;
    }

    /**
     * @param idMarca the idMarca to set
     */
    public void setIdMarca(Short idMarca) {
        this.idMarca = idMarca;
    }
}
