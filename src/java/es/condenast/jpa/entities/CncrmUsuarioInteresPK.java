/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Embeddable
public class CncrmUsuarioInteresPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdUsuario")
    private long idUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdInteres")
    private int idInteres;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdInteresTipo")
    private Short idInteresTipo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdMarca")
    private Short idMarca;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdProducto")
    private Short idProducto;

    /**
     *
     */
    public CncrmUsuarioInteresPK() {
    }

    /**
     *
     * @param idUsuario
     * @param idInteres
     * @param idInteresTipo
     * @param idMarca
     * @param idProducto
     */
    public CncrmUsuarioInteresPK(long idUsuario, int idInteres, Short idInteresTipo, Short idMarca, Short idProducto) {
        this.idUsuario = idUsuario;
        this.idInteres = idInteres;
        this.idInteresTipo = idInteresTipo;
        this.idMarca = idMarca;
        this.idProducto = idProducto;
    }

    /**
     *
     * @return
     */
    public long getIdUsuario() {
        return idUsuario;
    }

    /**
     *
     * @param idUsuario
     */
    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     *
     * @return
     */
    public int getIdInteres() {
        return idInteres;
    }

    /**
     *
     * @param idInteres
     */
    public void setIdInteres(int idInteres) {
        this.idInteres = idInteres;
    }

    /**
     *
     * @return
     */
    public Short getIdInteresTipo() {
        return idInteresTipo;
    }

    /**
     *
     * @param idInteresTipo
     */
    public void setIdInteresTipo(Short idInteresTipo) {
        this.idInteresTipo = idInteresTipo;
    }

    /**
     *
     * @return
     */
    public Short getIdMarca() {
        return idMarca;
    }

    /**
     *
     * @param idMarca
     */
    public void setIdMarca(Short idMarca) {
        this.idMarca = idMarca;
    }

    /**
     *
     * @return
     */
    public Short getIdProducto() {
        return idProducto;
    }

    /**
     *
     * @param idProducto
     */
    public void setIdProducto(Short idProducto) {
        this.idProducto = idProducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) this.idUsuario;
        hash += this.idInteres;
        hash += this.idInteresTipo;
        hash += this.idMarca;
        hash += this.idProducto;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CncrmUsuarioInteresPK)) {
            return false;
        }
        CncrmUsuarioInteresPK other = (CncrmUsuarioInteresPK) object;
        if (this.idUsuario != other.idUsuario) {
            return false;
        }
        if (!Objects.equals(this.idInteres, other.idInteres)) {
            return false;
        }
        if (!Objects.equals(this.idInteresTipo, other.idInteresTipo)) {
            return false;
        }
        if (!Objects.equals(this.idMarca, other.idMarca)) {
            return false;
        }
        if (!Objects.equals(this.idProducto, other.idProducto)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioInteresPK[ idUsuario=" + idUsuario
                + "idInteres=" + idInteres
                + "idInteresTipo=" + idInteresTipo
                + "idMarca=" + idMarca
                + "idProducto=" + idProducto + " ]";
    }
    
}
