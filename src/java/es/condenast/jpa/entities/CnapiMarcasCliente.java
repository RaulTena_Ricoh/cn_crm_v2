/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author david.yunta
 */
@Entity
@Table(name = "cnapi_marcas_cliente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CnapiMarcasCliente.findAll", query = "SELECT c FROM CnapiMarcasCliente c"),
    @NamedQuery(name = "CnapiMarcasCliente.findByIdCliente", query = "SELECT c FROM CnapiMarcasCliente c WHERE c.cnapiMarcasClientePK.idCliente = :idCliente"),
    @NamedQuery(name = "CnapiMarcasCliente.findByIdMarca", query = "SELECT c FROM CnapiMarcasCliente c WHERE c.cnapiMarcasClientePK.idMarca = :idMarca"),
    @NamedQuery(name = "CnapiMarcasCliente.removeByIdCliente", query = "DELETE FROM CnapiMarcasCliente c WHERE c.cnapiMarcasClientePK.idCliente = :idCliente")})
public class CnapiMarcasCliente implements Serializable {
    @JoinColumn(name = "idCliente", referencedColumnName = "Id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnapiClientes cnapiClientes;
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CnapiMarcasClientePK cnapiMarcasClientePK;

    public CnapiMarcasCliente() {
    }

    public CnapiMarcasCliente(CnapiMarcasClientePK cnapiMarcasClientePK) {
        this.cnapiMarcasClientePK = cnapiMarcasClientePK;
    }

    public CnapiMarcasCliente(int idCliente, Short idMarca) {
        this.cnapiMarcasClientePK = new CnapiMarcasClientePK(idCliente, idMarca);
    }

    public CnapiMarcasClientePK getCnapiMarcasClientePK() {
        return cnapiMarcasClientePK;
    }

    public void setCnapiMarcasClientePK(CnapiMarcasClientePK cnapiMarcasClientePK) {
        this.cnapiMarcasClientePK = cnapiMarcasClientePK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cnapiMarcasClientePK != null ? cnapiMarcasClientePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnapiMarcasCliente)) {
            return false;
        }
        CnapiMarcasCliente other = (CnapiMarcasCliente) object;
        if ((this.cnapiMarcasClientePK == null && other.cnapiMarcasClientePK != null) || (this.cnapiMarcasClientePK != null && !this.cnapiMarcasClientePK.equals(other.cnapiMarcasClientePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnapiMarcasCliente[ cnapiMarcasClientePK=" + cnapiMarcasClientePK + " ]";
    }

    public CnapiClientes getCnapiClientes() {
        return cnapiClientes;
    }

    public void setCnapiClientes(CnapiClientes cnapiClientes) {
        this.cnapiClientes = cnapiClientes;
    }
    
}
