/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Embeddable
public class CncrmTelefonoPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdUsuario")
    private long idUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdTelefonoTipo")
    private Short idTelefonoTipo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Numero")
    private String numero;

    /**
     *
     */
    public CncrmTelefonoPK() {
    }

    /**
     *
     * @param idUsuario
     * @param idTelefonoTipo
     * @param numero
     */
    public CncrmTelefonoPK(long idUsuario, Short idTelefonoTipo, String numero) {
        this.idUsuario = idUsuario;
        this.idTelefonoTipo = idTelefonoTipo;
        this.numero = numero;
    }

    /**
     *
     * @return
     */
    public long getIdUsuario() {
        return idUsuario;
    }

    /**
     *
     * @param idUsuario
     */
    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     *
     * @return
     */
    public Short getIdTelefonoTipo() {
        return idTelefonoTipo;
    }

    /**
     *
     * @param idTelefonoTipo
     */
    public void setIdTelefonoTipo(Short idTelefonoTipo) {
        this.idTelefonoTipo = idTelefonoTipo;
    }

    /**
     *
     * @return
     */
    public String getNumero() {
        return numero;
    }

    /**
     *
     * @param numero
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }
    
        @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) this.idUsuario;
        hash += (int) this.idTelefonoTipo;
        hash += this.numero.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CncrmTelefonoPK)) {
            return false;
        }
        CncrmTelefonoPK other = (CncrmTelefonoPK) object;
        if (this.idUsuario != other.idUsuario) {
            return false;
        }
        if (!Objects.equals(this.idTelefonoTipo, other.idTelefonoTipo)) {
            return false;
        }
        if (!Objects.equals(this.numero, other.numero)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmTelefonoPK[ idUsuario=" + idUsuario
                + "idTelefonoTipo=" + idTelefonoTipo
                + "numero=" + numero + " ]";
    }
    
}
