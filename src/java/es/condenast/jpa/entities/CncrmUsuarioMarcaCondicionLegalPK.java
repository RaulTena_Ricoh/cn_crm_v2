/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Javier.Castro
 */
@Embeddable
public class CncrmUsuarioMarcaCondicionLegalPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdUsuario")
    private long idUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdMarca")
    private short idMarca;


    public CncrmUsuarioMarcaCondicionLegalPK() {
    }



    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public short getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(short idMarca) {
        this.idMarca = idMarca;
    }



    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idUsuario;
        hash += (int) idMarca;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CncrmUsuarioMarcaCondicionLegalPK)) {
            return false;
        }
        CncrmUsuarioMarcaCondicionLegalPK other = (CncrmUsuarioMarcaCondicionLegalPK) object;
        if (this.idUsuario != other.idUsuario) {
            return false;
        }
        if (this.idMarca != other.idMarca) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioMarcaCondicionLegalPK[ idUsuario=" + idUsuario + ", idMarca=" + idMarca +  " ]";
    }
    
}
