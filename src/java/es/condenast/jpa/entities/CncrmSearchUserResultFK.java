/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ivan.fernandez
 */
@Embeddable
public class CncrmSearchUserResultFK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdUsuario")
    private Long idUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdMarca")
    private Short idMarca;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdProducto")
    private Short idProducto;


    public CncrmSearchUserResultFK (){
    }
    
    public CncrmSearchUserResultFK(Long idUsuario, Short idMarca, Short idProducto){
        this.idUsuario = idUsuario;
        this.idMarca = idMarca;
        this.idProducto = idProducto;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (long) idUsuario;
        hash += (int) idMarca;
        hash += (int) idProducto;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CncrmSearchUserResultFK)) {
            return false;
        }
        CncrmSearchUserResultFK other = (CncrmSearchUserResultFK) object;
        if (this.idUsuario != other.idUsuario) {
            return false;
        }
        if (this.idMarca != other.idMarca) {
            return false;
        }
        if (this.idProducto != other.idProducto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmSearchUserResultFK[ idUsuario=" + idUsuario + ", idMarca=" + idMarca +  ", idProducto=" + idProducto + " ]";
    }
    /**
     * @return the idUsuario
     */
    public Long getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @return the idMarca
     */
    public Short getIdMarca() {
        return idMarca;
    }

    /**
     * @param idMarca the idMarca to set
     */
    public void setIdMarca(Short idMarca) {
        this.idMarca = idMarca;
    }

    /**
     * @return the idProducto
     */
    public Short getIdProducto() {
        return idProducto;
    }

    /**
     * @param idProducto the idProducto to set
     */
    public void setIdProducto(Short idProducto) {
        this.idProducto = idProducto;
    }

}
