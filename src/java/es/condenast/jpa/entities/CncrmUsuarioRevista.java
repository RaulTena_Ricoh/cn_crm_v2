/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cncrm_usuario_revista")
@NamedQueries({
    @NamedQuery(name = "CncrmUsuarioRevista.findAll", query = "SELECT c FROM CncrmUsuarioRevista c"),
    @NamedQuery(name = "CncrmUsuarioRevista.findByIdUsuario", query = "SELECT c FROM CncrmUsuarioRevista c WHERE c.cncrmUsuarioRevistaPK.idUsuario = :idUsuario"),
    @NamedQuery(name = "CncrmUsuarioRevista.findByIdRevista", query = "SELECT c FROM CncrmUsuarioRevista c WHERE c.cncrmUsuarioRevistaPK.idRevista = :idRevista"),
    @NamedQuery(name = "CncrmUsuarioRevista.findByIdMarca", query = "SELECT c FROM CncrmUsuarioRevista c WHERE c.cncrmUsuarioRevistaPK.idMarca = :idMarca"),
    @NamedQuery(name = "CncrmUsuarioRevista.findByIdFrecuenciaAno", query = "SELECT c FROM CncrmUsuarioRevista c WHERE c.cncrmUsuarioRevistaPK.idFrecuenciaAno = :idFrecuenciaAno")})
public class CncrmUsuarioRevista implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CncrmUsuarioRevistaPK cncrmUsuarioRevistaPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    
    @JoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CncrmUsuario cncrmUsuario;
    @JoinColumn(name = "IdRevista", referencedColumnName = "IdRevista", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasRevista cnmasRevista;
    @JoinColumn(name = "IdMarca", referencedColumnName = "IdMarca", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasMarca cnmasMarca;
    @JoinColumn(name = "IdFrecuenciaAno", referencedColumnName = "IdFrecuenciaAno", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasFrecuenciaAno cnmasFrecuenciaAno;

    public CncrmUsuarioRevista() {
    }

    public CncrmUsuarioRevista(CncrmUsuarioRevistaPK cncrmUsuarioRevistaPK) {
        this.cncrmUsuarioRevistaPK = cncrmUsuarioRevistaPK;
    }

    public CncrmUsuarioRevista(CncrmUsuarioRevistaPK cncrmUsuarioRevistaPK, Date fecha) {
        this.cncrmUsuarioRevistaPK = cncrmUsuarioRevistaPK;
        this.fecha = fecha;
    }

    public CncrmUsuarioRevistaPK getCncrmUsuarioRevistaPK() {
        return cncrmUsuarioRevistaPK;
    }

    public void setCncrmUsuarioRevistaPK(CncrmUsuarioRevistaPK cncrmUsuarioRevistaPK) {
        this.cncrmUsuarioRevistaPK = cncrmUsuarioRevistaPK;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public CncrmUsuario getCncrmUsuario() {
        return cncrmUsuario;
    }

    public void setCncrmUsuario(CncrmUsuario cncrmUsuario) {
        this.cncrmUsuario = cncrmUsuario;
    }

    public CnmasRevista getCnmasRevista() {
        return cnmasRevista;
    }

    public void setCnmasRevista(CnmasRevista cnmasRevista) {
        this.cnmasRevista = cnmasRevista;
    }

    public CnmasMarca getCnmasMarca() {
        return cnmasMarca;
    }

    public void setCnmasMarca(CnmasMarca cnmasMarca) {
        this.cnmasMarca = cnmasMarca;
    }

    public CnmasFrecuenciaAno getCnmasFrecuenciaAno() {
        return cnmasFrecuenciaAno;
    }

    public void setCnmasFrecuenciaAno(CnmasFrecuenciaAno cnmasFrecuenciaAno) {
        this.cnmasFrecuenciaAno = cnmasFrecuenciaAno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += this.cncrmUsuarioRevistaPK.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CncrmUsuarioRevista)) {
            return false;
        }
        CncrmUsuarioRevista other = (CncrmUsuarioRevista) object;
        if ((this.cncrmUsuarioRevistaPK == null && other.cncrmUsuarioRevistaPK != null) ||
                (this.cncrmUsuarioRevistaPK != null && !this.cncrmUsuarioRevistaPK.equals(other.cncrmUsuarioRevistaPK))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioRevista[ cncrmUsuarioRevistaPK=" + cncrmUsuarioRevistaPK + " ]";
    }
    
}
