/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cnetl_master")
@NamedQueries({
    @NamedQuery(name = "CnetlMaster.findAll", query = "SELECT c FROM CnetlMaster c"),
    @NamedQuery(name = "CnetlMaster.findByIdMaster", query = "SELECT c FROM CnetlMaster c WHERE c.idMaster = :idMaster"),
    @NamedQuery(name = "CnetlMaster.findByNombre", query = "SELECT c FROM CnetlMaster c WHERE UPPER(c.nombre) = UPPER(:nombre)"),
    @NamedQuery(name = "CnetlMaster.findByOrigen", query = "SELECT c FROM CnetlMaster c WHERE c.origen = :origen"),
    @NamedQuery(name = "CnetlMaster.findByEsFichero", query = "SELECT c FROM CnetlMaster c WHERE c.esFichero = :esFichero"),
    @NamedQuery(name = "CnetlMaster.findByActivo", query = "SELECT c FROM CnetlMaster c WHERE c.activo = :activo")})
public class CnetlMaster implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = true)
    @Column(name = "IdMaster")
    private Integer idMaster;
    @Basic(optional = false)
    @NotNull
    @Size(min = 0, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 0, max = 300)
    @Column(name = "Origen")
    private String origen;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EsFichero")
    private boolean esFichero;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Activo")
    private boolean activo;
    @JoinColumn(name = "IdMasterTipo", referencedColumnName = "IdMasterTipo")
    @ManyToOne(optional = false)
    private CnetlMasterTipo idMasterTipo;

    public CnetlMaster() {
    }

    public CnetlMaster(Integer idMaster) {
        this.idMaster = idMaster;
    }

    public CnetlMaster(Integer idMaster, String nombre, String origen, boolean esFichero, boolean activo) {
        this.idMaster = idMaster;
        this.nombre = nombre;
        this.origen = origen;
        this.esFichero = esFichero;
        this.activo = activo;
    }

    public Integer getIdMaster() {
        return idMaster;
    }

    public void setIdMaster(Integer idMaster) {
        this.idMaster = idMaster;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public boolean getEsFichero() {
        return esFichero;
    }

    public void setEsFichero(boolean esFichero) {
        this.esFichero = esFichero;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public CnetlMasterTipo getIdMasterTipo() {
        return idMasterTipo;
    }

    public void setIdMasterTipo(CnetlMasterTipo idMasterTipo) {
        this.idMasterTipo = idMasterTipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMaster != null ? idMaster.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlMaster)) {
            return false;
        }
        CnetlMaster other = (CnetlMaster) object;
        if ((this.idMaster == null && other.idMaster != null) || (this.idMaster != null && !this.idMaster.equals(other.idMaster))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre;
    }
    
}
