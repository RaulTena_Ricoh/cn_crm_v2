/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ivan.fernandez
 */
@Entity
@Table(name="cnmas_sistema_operativo")
@NamedQueries({
    @NamedQuery(name = "CnmasSistemaOperativo.findAll", query = "SELECT c FROM CnmasSistemaOperativo c ORDER BY c.idSistemaOperativo"),
    @NamedQuery(name = "CnmasSistemaOperativo.findByIdCompraOnline", query = "SELECT c FROM CnmasSistemaOperativo c WHERE c.idSistemaOperativo = :idSistemaOperativo")})
public class CnmasSistemaOperativo implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdSistemaOperativo")
    private Short idSistemaOperativo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasSistemaOperativo")
    private Collection<CncrmUsuarioTipoDispositivoDigital> cncrmUsuarioTipoDispositivoDigital;
    
    /**
     * @return the idSistemaOperativo
     */
    public Short getIdSistemaOperativo() {
        return idSistemaOperativo;
    }

    /**
     * @param idSistemaOperativo the idSistemaOperativo to set
     */
    public void setIdSistemaOperativo(Short idSistemaOperativo) {
        this.idSistemaOperativo = idSistemaOperativo;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSistemaOperativo != null ? idSistemaOperativo.hashCode() : 0);
        return hash;
    }

    public Collection<CncrmUsuarioTipoDispositivoDigital> getCncrmUsuarioTipoDispositivoDigital() {
        return cncrmUsuarioTipoDispositivoDigital;
    }

    public void setCncrmUsuarioTipoDispositivoDigital(Collection<CncrmUsuarioTipoDispositivoDigital> cncrmUsuarioTipoDispositivoDigital) {
        this.cncrmUsuarioTipoDispositivoDigital = cncrmUsuarioTipoDispositivoDigital;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasSistemaOperativo)) {
            return false;
        }
        CnmasSistemaOperativo other = (CnmasSistemaOperativo) object;
        if ((this.idSistemaOperativo == null && other.idSistemaOperativo != null) || (this.idSistemaOperativo != null && !this.idSistemaOperativo.equals(other.idSistemaOperativo))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasSistemaOperativo[ idSistemaOperativo=" + idSistemaOperativo + " ]";
    }    
}
