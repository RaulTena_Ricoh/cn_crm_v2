/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cnmas_documento")
@NamedQueries({
    @NamedQuery(name = "CnmasDocumento.findAll", query = "SELECT c FROM CnmasDocumento c ORDER BY c.idDocumento"),
    @NamedQuery(name = "CnmasDocumento.findByIdCnmasDocumento", query = "SELECT c FROM CnmasDocumento c WHERE c.idDocumento = :idDocumento"),
    @NamedQuery(name = "CnmasDocumento.getMaxId", query = "SELECT max(c.idDocumento) + 1 FROM CnmasDocumento c"),
    @NamedQuery(name = "CnmasDocumento.findByNombre", query = "SELECT c FROM CnmasDocumento c WHERE c.nombre = :nombre")})
public class CnmasDocumento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdDocumento")
    private Short idDocumento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasDocumento")
    private Collection<CncrmPersona> cncrmPersonaCollection;

    /**
     *
     */
    public CnmasDocumento() {
    }

    /**
     *
     * @param idDocumento
     */
    public CnmasDocumento(Short idDocumento) {
        this.idDocumento = idDocumento;
    }

    /**
     *
     * @param idDocumento
     * @param nombre
     */
    public CnmasDocumento(Short idDocumento, String nombre) {
        this.idDocumento = idDocumento;
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public Short getIdDocumento() {
        return idDocumento;
    }

    /**
     *
     * @param idDocumento
     */
    public void setIdDocumento(Short idDocumento) {
        this.idDocumento = idDocumento;
    }

    /**
     *
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public Collection<CncrmPersona> getCncrmPersonaCollection() {
        return cncrmPersonaCollection;
    }

    /**
     *
     * @param cncrmPersonaCollection
     */
    public void setCncrmPersonaCollection(Collection<CncrmPersona> cncrmPersonaCollection) {
        this.cncrmPersonaCollection = cncrmPersonaCollection;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDocumento != null ? idDocumento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if(!(object instanceof CnmasDocumento)) {
            return false;
        }
        CnmasDocumento other = (CnmasDocumento) object;
        if((this.idDocumento == null && other.idDocumento != null) || (this.idDocumento != null && !this.idDocumento.equals(other.idDocumento))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasDocumento[ idDocumento=" + idDocumento + " ]";
    }
    
}
