/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cncrm_usuario_marca_condicion_legal")
@NamedQueries({
    @NamedQuery(name = "CncrmUsuarioMarcaCondicionLegal.findAll", query = "SELECT c FROM CncrmUsuarioMarcaCondicionLegal c"),
    @NamedQuery(name = "CncrmUsuarioMarcaCondicionLegal.findByIdUsuario", query = "SELECT c FROM CncrmUsuarioMarcaCondicionLegal c WHERE c.cncrmUsuarioMarcaCondicionLegalPK.idUsuario = :idUsuario"),
    @NamedQuery(name = "CncrmUsuarioMarcaCondicionLegal.findByIdMarca", query = "SELECT c FROM CncrmUsuarioMarcaCondicionLegal c WHERE c.cncrmUsuarioMarcaCondicionLegalPK.idMarca = :idMarca"),
    @NamedQuery(name = "CncrmUsuarioMarcaCondicionLegal.desactivarByIdUsuarioMarcas", query = "UPDATE CncrmUsuarioMarcaCondicionLegal c SET c.c1FechaBaja = CURRENT_DATE, c.c2FechaBaja = CURRENT_DATE, c.c3FechaBaja = CURRENT_DATE, c.c4FechaBaja = CURRENT_DATE, c.c5FechaBaja = CURRENT_DATE "
            + "WHERE c.cncrmUsuarioMarcaCondicionLegalPK.idUsuario = (SELECT cu.idUsuario FROM CncrmUsuario cu where cu.email = :email) AND c.cncrmUsuarioMarcaCondicionLegalPK.idMarca IN :idMarcas"),
    @NamedQuery(name = "CncrmUsuarioMarcaCondicionLegal.countDesactivarByIdUsuarioMarcas", query = "SELECT c.cncrmUsuario.idUsuario FROM CncrmUsuarioMarcaCondicionLegal c "
            + "WHERE c.cncrmUsuarioMarcaCondicionLegalPK.idUsuario = (SELECT cu.idUsuario FROM CncrmUsuario cu where cu.email = :email) AND c.cncrmUsuarioMarcaCondicionLegalPK.idMarca IN :idMarcas AND (c.c1FechaBaja IS NULL OR c.c2FechaBaja IS NULL OR c.c3FechaBaja IS NULL OR c.c4FechaBaja IS NULL OR c.c5FechaBaja IS NULL)"),
    
})

public class CncrmUsuarioMarcaCondicionLegal implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CncrmUsuarioMarcaCondicionLegalPK cncrmUsuarioMarcaCondicionLegalPK;

    @Basic(optional = false)
    @NotNull
    @Column(name = "C1_FechaAlta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date c1FechaAlta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "C1_FechaBaja")
    @Temporal(TemporalType.TIMESTAMP)
    private Date c1FechaBaja;

    @Basic(optional = false)
    @NotNull
    @Column(name = "C2_FechaAlta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date c2FechaAlta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "C2_FechaBaja")
    @Temporal(TemporalType.TIMESTAMP)
    private Date c2FechaBaja;

    @Basic(optional = false)
    @NotNull
    @Column(name = "C3_FechaAlta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date c3FechaAlta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "C3_FechaBaja")
    @Temporal(TemporalType.TIMESTAMP)
    private Date c3FechaBaja;

    @Basic(optional = false)
    @NotNull
    @Column(name = "C4_FechaAlta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date c4FechaAlta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "C4_FechaBaja")
    @Temporal(TemporalType.TIMESTAMP)
    private Date c4FechaBaja;

    @Basic(optional = false)
    @NotNull
    @Column(name = "C5_FechaAlta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date c5FechaAlta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "C5_FechaBaja")
    @Temporal(TemporalType.TIMESTAMP)
    private Date c5FechaBaja;

    @JoinColumn(name = "IdMarca", referencedColumnName = "IdMarca", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasMarca cnmasMarca;
    @JoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CncrmUsuario cncrmUsuario;
    @JoinColumns({
        @JoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario", insertable = false, updatable = false),
        @JoinColumn(name = "IdMarca", referencedColumnName = "IdMarca", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private CncrmUsuarioMarca cncrmUsuarioMarca;

    public CncrmUsuarioMarcaCondicionLegal() {
    }

    public CncrmUsuarioMarcaCondicionLegal(CncrmUsuarioMarcaCondicionLegalPK cncrmUsuarioMarcaCondicionLegalPK) {
        this.cncrmUsuarioMarcaCondicionLegalPK = cncrmUsuarioMarcaCondicionLegalPK;
    }



    public CncrmUsuarioMarcaCondicionLegalPK getCncrmUsuarioMarcaCondicionLegalPK() {
        return cncrmUsuarioMarcaCondicionLegalPK;
    }

    public void setCncrmUsuarioMarcaCondicionLegalPK(CncrmUsuarioMarcaCondicionLegalPK cncrmUsuarioMarcaCondicionLegalPK) {
        this.cncrmUsuarioMarcaCondicionLegalPK = cncrmUsuarioMarcaCondicionLegalPK;
    }


    public CnmasMarca getCnmasMarca() {
        return cnmasMarca;
    }

    public void setCnmasMarca(CnmasMarca cnmasMarca) {
        this.cnmasMarca = cnmasMarca;
    }

    public CncrmUsuario getCncrmUsuario() {
        return cncrmUsuario;
    }

    public void setCncrmUsuario(CncrmUsuario cncrmUsuario) {
        this.cncrmUsuario = cncrmUsuario;
    }

    public CncrmUsuarioMarca getCncrmUsuarioMarca() {
        return cncrmUsuarioMarca;
    }

    public void setCncrmUsuarioMarca(CncrmUsuarioMarca cncrmUsuarioMarca) {
        this.cncrmUsuarioMarca = cncrmUsuarioMarca;
    }

    public Date getC1FechaAlta() {
        return c1FechaAlta;
    }

    public void setC1FechaAlta(Date c1FechaAlta) {
        this.c1FechaAlta = c1FechaAlta;
    }

    public Date getC1FechaBaja() {
        return c1FechaBaja;
    }

    public void setC1FechaBaja(Date c1FechaBaja) {
        this.c1FechaBaja = c1FechaBaja;
    }

    public Date getC2FechaAlta() {
        return c2FechaAlta;
    }

    public void setC2FechaAlta(Date c2FechaAlta) {
        this.c2FechaAlta = c2FechaAlta;
    }

    public Date getC2FechaBaja() {
        return c2FechaBaja;
    }

    public void setC2FechaBaja(Date c2FechaBaja) {
        this.c2FechaBaja = c2FechaBaja;
    }

    public Date getC3FechaAlta() {
        return c3FechaAlta;
    }

    public void setC3FechaAlta(Date c3FechaAlta) {
        this.c3FechaAlta = c3FechaAlta;
    }

    public Date getC3FechaBaja() {
        return c3FechaBaja;
    }

    public void setC3FechaBaja(Date c3FechaBaja) {
        this.c3FechaBaja = c3FechaBaja;
    }

    public Date getC4FechaAlta() {
        return c4FechaAlta;
    }

    public void setC4FechaAlta(Date c4FechaAlta) {
        this.c4FechaAlta = c4FechaAlta;
    }

    public Date getC4FechaBaja() {
        return c4FechaBaja;
    }

    public void setC4FechaBaja(Date c4FechaBaja) {
        this.c4FechaBaja = c4FechaBaja;
    }

    public Date getC5FechaAlta() {
        return c5FechaAlta;
    }

    public void setC5FechaAlta(Date c5FechaAlta) {
        this.c5FechaAlta = c5FechaAlta;
    }

    public Date getC5FechaBaja() {
        return c5FechaBaja;
    }

    public void setC5FechaBaja(Date c5FechaBaja) {
        this.c5FechaBaja = c5FechaBaja;
    }

    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cncrmUsuarioMarcaCondicionLegalPK != null ? cncrmUsuarioMarcaCondicionLegalPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CncrmUsuarioMarcaCondicionLegal)) {
            return false;
        }
        CncrmUsuarioMarcaCondicionLegal other = (CncrmUsuarioMarcaCondicionLegal) object;
        if ((this.cncrmUsuarioMarcaCondicionLegalPK == null && other.cncrmUsuarioMarcaCondicionLegalPK != null) || (this.cncrmUsuarioMarcaCondicionLegalPK != null && !this.cncrmUsuarioMarcaCondicionLegalPK.equals(other.cncrmUsuarioMarcaCondicionLegalPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioMarcaCondicionLegal[ cncrmUsuarioMarcaCondicionLegalPK=" + cncrmUsuarioMarcaCondicionLegalPK + " ]";
    }

}
