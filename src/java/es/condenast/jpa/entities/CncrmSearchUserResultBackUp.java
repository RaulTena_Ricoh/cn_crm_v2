/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.SecondaryTables;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ivan.fernandez
 */
@Entity
@Table(name = "cncrm_usuario_marca_producto")
@SecondaryTables({
    @SecondaryTable(name="cncrm_usuario", pkJoinColumns = {@PrimaryKeyJoinColumn(name = "idUsuario", referencedColumnName = "IdUsuario")}),
    @SecondaryTable(name="cncrm_usuario_participacion", pkJoinColumns = {@PrimaryKeyJoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario")}),
    @SecondaryTable(name="cncrm_usuario_marca_condicion_legal", pkJoinColumns = {@PrimaryKeyJoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario"),
                                                                                 @PrimaryKeyJoinColumn(name = "IdMarca", referencedColumnName = "IdMarca")})
})
@NamedQueries({
    @NamedQuery(name = "CncrmSearchUserResult.countSearchUser", query = "SELECT count(c.idUsuario) FROM CncrmSearchUserResult c WHERE c.idUsuario IN :Ids"),
    @NamedQuery(name = "CncrmSearchUserResult.searchUser", query = "SELECT c FROM CncrmSearchUserResult c WHERE c.idUsuario IN :Ids")})

public class CncrmSearchUserResultBackUp implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "IdUsuario")
    private Long idUsuario;
    @Id
    @Column(name = "IdMarca")
    private Short idMarca;
    @Id
    @Column(name = "IdProducto")
    private Short idProducto;
    @Column(table="cncrm_usuario", name = "Email")
    private String email;
    @Column(name = "FechaAlta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAlta;
    @Column(name = "FechaBaja")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaBaja;
    @JoinColumn(name = "IdMarca", referencedColumnName = "IdMarca", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasMarca cnmasMarca;
    @JoinColumn(name = "IdProducto", referencedColumnName = "IdProducto", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasProducto cnmasProducto;
    @Column(table="cncrm_usuario_participacion", name = "IdParticipacion",nullable = true)
    @Basic(optional = true)
    private Short idParticipacion;
    
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "cncrmUsuario")
    private CncrmUsuarioParticipacion cncrmUsuarioParticipacionCollection;

    
    public CncrmSearchUserResultBackUp(){
        
    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getIdUsuario() != null ? getIdUsuario().hashCode() : 0);
        hash += (getIdMarca() != null ? getIdMarca().hashCode() : 0);
        hash += (getIdProducto() != null ? getIdProducto().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CncrmSearchUserResultBackUp)) {
            return false;
        }
        CncrmSearchUserResultBackUp other = (CncrmSearchUserResultBackUp) object;
        if ((this.getIdUsuario() == null && other.getIdUsuario() != null) || (this.getIdUsuario() != null && !this.idUsuario.equals(other.idUsuario))) {
            return false;
        }
        if ((this.getIdMarca() == null && other.getIdMarca() != null) || (this.getIdMarca() != null && !this.idMarca.equals(other.idMarca))) {
            return false;
        }
        if ((this.getIdProducto() == null && other.getIdProducto() != null) || (this.getIdProducto() != null && !this.idProducto.equals(other.idProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioParticipacion[ idUsuario=" + getIdUsuario() + ", idMarca=" + getIdMarca() + " + \", idProducto=\" + idProducto + \" ]";
    }

    /**
     * @return the idUsuario
     */
    public Long getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the idMarca
     */
    public Short getIdMarca() {
        return idMarca;
    }

    /**
     * @param idMarca the idMarca to set
     */
    public void setIdMarca(Short idMarca) {
        this.idMarca = idMarca;
    }

    /**
     * @return the idProducto
     */
    public Short getIdProducto() {
        return idProducto;
    }

    /**
     * @param idProducto the idProducto to set
     */
    public void setIdProducto(Short idProducto) {
        this.idProducto = idProducto;
    }

    /**
     * @return the fechaAlta
     */
    public Date getFechaAlta() {
        return fechaAlta;
    }

    /**
     * @param fechaAlta the fechaAlta to set
     */
    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    /**
     * @return the cnmasMarca
     */
    public CnmasMarca getCnmasMarca() {
        return cnmasMarca;
    }

    /**
     * @param cnmasMarca the cnmasMarca to set
     */
    public void setCnmasMarca(CnmasMarca cnmasMarca) {
        this.cnmasMarca = cnmasMarca;
    }

    /**
     * @return the cnmasProducto
     */
    public CnmasProducto getCnmasProducto() {
        return cnmasProducto;
    }

    /**
     * @param cnmasProducto the cnmasProducto to set
     */
    public void setCnmasProducto(CnmasProducto cnmasProducto) {
        this.cnmasProducto = cnmasProducto;
    }

    /**
     * @return the idParticipacion
     */
    public Short getIdParticipacion() {
        return idParticipacion;
    }

    /**
     * @param idParticipacion the idParticipacion to set
     */
    public void setIdParticipacion(Short idParticipacion) {
        this.idParticipacion = idParticipacion;
    }

    /**
     * @return the fechaBaja
     */
    public Date getFechaBaja() {
        return fechaBaja;
    }

    /**
     * @param fechaBaja the fechaBaja to set
     */
    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    /**
     * @return the cncrmUsuarioParticipacionCollection
     */
    public CncrmUsuarioParticipacion getCncrmUsuarioParticipacionCollection() {
        return cncrmUsuarioParticipacionCollection;
    }

    /**
     * @param cncrmUsuarioParticipacionCollection the cncrmUsuarioParticipacionCollection to set
     */
    public void setCncrmUsuarioParticipacionCollection(CncrmUsuarioParticipacion cncrmUsuarioParticipacionCollection) {
        this.cncrmUsuarioParticipacionCollection = cncrmUsuarioParticipacionCollection;
    }

}
