/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnetl_pool_log")
@NamedQueries({
    @NamedQuery(name = "CnetlPoolLog.findAll", query = "SELECT c FROM CnetlPoolLog c"),
    @NamedQuery(name = "CnetlPoolLog.findByIdPool", query = "SELECT c FROM CnetlPoolLog c WHERE c.idPool = :idPool"),
    @NamedQuery(name = "CnetlPoolLog.findByFechaInicio", query = "SELECT c FROM CnetlPoolLog c WHERE c.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "CnetlPoolLog.findByCargaCompletada", query = "SELECT c FROM CnetlPoolLog c WHERE c.cargaCompletada = :cargaCompletada"),
    @NamedQuery(name = "CnetlPoolLog.findByFechaFin", query = "SELECT c FROM CnetlPoolLog c WHERE c.fechaFin = :fechaFin"),
    @NamedQuery(name = "CnetlPoolLog.findByLineas", query = "SELECT c FROM CnetlPoolLog c WHERE c.lineas = :lineas"),
    @NamedQuery(name = "CnetlPoolLog.findByLineasOK", query = "SELECT c FROM CnetlPoolLog c WHERE c.lineasOK = :lineasOK"),
    @NamedQuery(name = "CnetlPoolLog.findByLineasError", query = "SELECT c FROM CnetlPoolLog c WHERE c.lineasError = :lineasError")})
public class CnetlPoolLog implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdPool")
    private Long idPool;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaInicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    @Column(name = "CargaCompletada")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cargaCompletada;
    @Column(name = "FechaFin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Lineas")
    private int lineas;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LineasOK")
    private int lineasOK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LineasError")
    private int lineasError;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnetlPoolLog")
    private Collection<CnetlPoolLogTransformacion> cnetlPoolLogTransformacion;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnetlPoolLog")
    private Collection<CnetlPoolLogValidacion> cnetlPoolLogValidacion;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnetlPoolLog")
    private Collection<CnetlPoolLogValidacionDetalle> cnetlPoolLogValidacionDetalle;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnetlPoolLog")
    private Collection<CnetlPoolLogRegla> cnetlPoolLogRegla;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnetlPoolLog")
    private Collection<CnetlPoolLogWarning> cnetlPoolLogWarning;
    
    public CnetlPoolLog() {
    }

    public CnetlPoolLog(Long idPool) {
        this.idPool = idPool;
    }

    public CnetlPoolLog(Long idPool, Date fechaInicio, int lineas, int lineasOK, int lineasError) {
        this.idPool = idPool;
        this.fechaInicio = fechaInicio;
        this.lineas = lineas;
        this.lineasOK = lineasOK;
        this.lineasError = lineasError;
    }

    public Long getIdPool() {
        return idPool;
    }

    public void setIdPool(Long idPool) {
        this.idPool = idPool;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getCargaCompletada() {
        return cargaCompletada;
    }

    public void setCargaCompletada(Date cargaCompletada) {
        this.cargaCompletada = cargaCompletada;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public int getLineas() {
        return lineas;
    }

    public void setLineas(int lineas) {
        this.lineas = lineas;
    }

    public int getLineasOK() {
        return lineasOK;
    }

    public void setLineasOK(int lineasOK) {
        this.lineasOK = lineasOK;
    }

    public int getLineasError() {
        return lineasError;
    }

    public void setLineasError(int lineasError) {
        this.lineasError = lineasError;
    }

    public Collection<CnetlPoolLogValidacion> getCnetlPoolLogValidacion() {
        return cnetlPoolLogValidacion;
    }

    public void setCnetlPoolLogValidacion(Collection<CnetlPoolLogValidacion> cnetlPoolLogValidacion) {
        this.cnetlPoolLogValidacion = cnetlPoolLogValidacion;
    }

    public Collection<CnetlPoolLogValidacionDetalle> getCnetlPoolLogValidacionDetalle() {
        return cnetlPoolLogValidacionDetalle;
    }

    public void setCnetlPoolLogValidacionDetalle(Collection<CnetlPoolLogValidacionDetalle> cnetlPoolLogValidacionDetalle) {
        this.cnetlPoolLogValidacionDetalle = cnetlPoolLogValidacionDetalle;
    }

    public Collection<CnetlPoolLogRegla> getCnetlPoolLogRegla() {
        return cnetlPoolLogRegla;
    }

    public void setCnetlPoolLogRegla(Collection<CnetlPoolLogRegla> cnetlPoolLogRegla) {
        this.cnetlPoolLogRegla = cnetlPoolLogRegla;
    }

    public Collection<CnetlPoolLogTransformacion> getCnetlPoolLogTransformacion() {
        return cnetlPoolLogTransformacion;
    }

    public void setCnetlPoolLogTransformacion(Collection<CnetlPoolLogTransformacion> cnetlPoolLogTransformacion) {
        this.cnetlPoolLogTransformacion = cnetlPoolLogTransformacion;
    }

    public Collection<CnetlPoolLogWarning> getCnetlPoolLogWarning() {
        return cnetlPoolLogWarning;
    }

    public void setCnetlPoolLogWarning(Collection<CnetlPoolLogWarning> cnetlPoolLogWarning) {
        this.cnetlPoolLogWarning = cnetlPoolLogWarning;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPool != null ? idPool.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlPoolLog)) {
            return false;
        }
        CnetlPoolLog other = (CnetlPoolLog) object;
        if ((this.idPool == null && other.idPool != null) || (this.idPool != null && !this.idPool.equals(other.idPool))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnetlPoolLog[ idPool=" + idPool + " ]";
    }
    
}
