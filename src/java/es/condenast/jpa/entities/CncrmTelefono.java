/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cncrm_telefono")
@NamedQueries({
    @NamedQuery(name = "CncrmTelefono.findAll", query = "SELECT c FROM CncrmTelefono c"),
    @NamedQuery(name = "CncrmTelefono.findByIdUsuario", query = "SELECT c FROM CncrmTelefono c WHERE c.cncrmTelefonoPK.idUsuario = :idUsuario"),
    @NamedQuery(name = "CncrmTelefono.findByIdTelefonoTipo", query = "SELECT c FROM CncrmTelefono c WHERE c.cncrmTelefonoPK.idTelefonoTipo = :idTelefonoTipo"),
    @NamedQuery(name = "CncrmTelefono.findByNumero", query = "SELECT c FROM CncrmTelefono c WHERE c.cncrmTelefonoPK.numero = :numero")})
public class CncrmTelefono implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CncrmTelefonoPK cncrmTelefonoPK;

    @JoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CncrmUsuario cncrmUsuario;
    @JoinColumn(name = "IdTelefonoTipo", referencedColumnName = "IdTelefonoTipo", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasTelefonoTipo cnmasTelefonoTipo;
    
    /**
     *
     * @param cncrmTelefonoPK
     */
    public CncrmTelefono(CncrmTelefonoPK cncrmTelefonoPK) {
        this.cncrmTelefonoPK = cncrmTelefonoPK;
    }

    /**
     *
     */
    public CncrmTelefono() {
    }

    /**
     *
     * @return
     */
    public CncrmTelefonoPK getCncrmTelefonoPK() {
        return cncrmTelefonoPK;
    }

    /**
     *
     * @param cncrmTelefonoPK
     */
    public void setCncrmTelefonoPK(CncrmTelefonoPK cncrmTelefonoPK) {
        this.cncrmTelefonoPK = cncrmTelefonoPK;
    }

    /**
     *
     * @return
     */
    public CncrmUsuario getCncrmUsuario() {
        return cncrmUsuario;
    }

    /**
     *
     * @param cncrmUsuario
     */
    public void setCncrmUsuario(CncrmUsuario cncrmUsuario) {
        this.cncrmUsuario = cncrmUsuario;
    }

    /**
     *
     * @return
     */
    public CnmasTelefonoTipo getCnmasTelefonoTipo() {
        return cnmasTelefonoTipo;
    }

    /**
     *
     * @param cnmasTelefonoTipo
     */
    public void setCnmasTelefonoTipo(CnmasTelefonoTipo cnmasTelefonoTipo) {
        this.cnmasTelefonoTipo = cnmasTelefonoTipo;
    }
    
     @Override
    public int hashCode() {
        int hash = 0;
        hash += this.cncrmTelefonoPK.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CncrmTelefono)) {
            return false;
        }
        CncrmTelefono other = (CncrmTelefono) object;
        if ((this.cncrmTelefonoPK == null && other.cncrmTelefonoPK != null) ||
                (this.cncrmTelefonoPK != null && !this.cncrmTelefonoPK.equals(other.cncrmTelefonoPK))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmTelefono[ cncrmTelefonoPK=" + cncrmTelefonoPK + " ]";
    }
    
}
