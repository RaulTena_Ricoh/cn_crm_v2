/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ivan.fernandez
 */
@Entity
@Table(name = "cnmas_ocupacion")
@NamedQueries({
    @NamedQuery(name = "CnmasOcupacion.findAll", query = "SELECT c FROM CnmasOcupacion c"),
    @NamedQuery(name = "CnmasOcupacion.findByIdProducto", query = "SELECT c FROM CnmasOcupacion c WHERE c.idOcupacion = :idOcupacion"),
    @NamedQuery(name = "CnmasOcupacion.findByNombre", query = "SELECT c FROM CnmasOcupacion c WHERE c.nombre = :nombre")})
public class CnmasOcupacion implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdOcupacion")
    private Short idOcupacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @JoinColumn(name = "IdOcupacionTipo", referencedColumnName = "IdOcupacionTipo")
    @ManyToOne(optional = false)
    private CnmasOcupacionTipo idOcupacionTipo;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasOcupacion")
    private Collection<CncrmPersona> cncrmPersonaCollection;

    /**
     * @return the idOcupacion
     */
    public Short getIdOcupacion() {
        return idOcupacion;
    }

    /**
     * @param idOcupacion the idOcupacion to set
     */
    public void setIdOcupacion(Short idOcupacion) {
        this.idOcupacion = idOcupacion;
    }
    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the idOcupacionTipo
     */
    public CnmasOcupacionTipo getIdOcupacionTipo() {
        return idOcupacionTipo;
    }

    /**
     * @param idOcupacionTipo the idOcupacionTipo to set
     */
    public void setIdOcupacionTipo(CnmasOcupacionTipo idOcupacionTipo) {
        this.idOcupacionTipo = idOcupacionTipo;
    }

    /**
     *
     * @return
     */
    public Collection<CncrmPersona> getCncrmPersonaCollection() {
        return cncrmPersonaCollection;
    }

    /**
     *
     * @param cncrmPersonaCollection
     */
    public void setCncrmPersonaCollection(Collection<CncrmPersona> cncrmPersonaCollection) {
        this.cncrmPersonaCollection = cncrmPersonaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getIdOcupacion() != null ? getIdOcupacion().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasOcupacion)) {
            return false;
        }
        CnmasOcupacion other = (CnmasOcupacion) object;
        if ((this.getIdOcupacion() == null && other.getIdOcupacion() != null) || (this.getIdOcupacion() != null && !this.idOcupacion.equals(other.idOcupacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre;
    }


}
