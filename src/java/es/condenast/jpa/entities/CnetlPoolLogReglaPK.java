/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Javier.Castro
 */
@Embeddable
public class CnetlPoolLogReglaPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdPool")
    private long idPool;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdReglaImportacion")
    private short idReglaImportacion;

    public CnetlPoolLogReglaPK() {
    }

    public CnetlPoolLogReglaPK(long idPool, short idReglaImportacion) {
        this.idPool = idPool;
        this.idReglaImportacion = idReglaImportacion;
    }

    public long getIdPool() {
        return idPool;
    }

    public void setIdPool(long idPool) {
        this.idPool = idPool;
    }

    public short getIdReglaImportacion() {
        return idReglaImportacion;
    }

    public void setIdReglaImportacion(short idReglaImportacion) {
        this.idReglaImportacion = idReglaImportacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idPool;
        hash += (int) idReglaImportacion;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlPoolLogReglaPK)) {
            return false;
        }
        CnetlPoolLogReglaPK other = (CnetlPoolLogReglaPK) object;
        if (this.idPool != other.idPool) {
            return false;
        }
        if (this.idReglaImportacion != other.idReglaImportacion) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnetlPoolLogReglaPK[ idPool=" + idPool + ", idReglaImportacion=" + idReglaImportacion + " ]";
    }
    
}
