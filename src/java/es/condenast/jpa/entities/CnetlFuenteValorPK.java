/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Javier.Castro
 */
@Embeddable
public class CnetlFuenteValorPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdFuente")
    private int idFuente;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdOrigen")
    private int idOrigen;

    public CnetlFuenteValorPK() {
    }

    public CnetlFuenteValorPK(int idFuente, int idOrigen) {
        this.idFuente = idFuente;
        this.idOrigen = idOrigen;
    }

    public int getIdFuente() {
        return idFuente;
    }

    public void setIdFuente(int idFuente) {
        this.idFuente = idFuente;
    }

    public int getIdOrigen() {
        return idOrigen;
    }

    public void setIdOrigen(int idOrigen) {
        this.idOrigen = idOrigen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idFuente;
        hash += (int) idOrigen;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlFuenteValorPK)) {
            return false;
        }
        CnetlFuenteValorPK other = (CnetlFuenteValorPK) object;
        if (this.idFuente != other.idFuente) {
            return false;
        }
        if (this.idOrigen != other.idOrigen) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnetlFuenteValorPK[ idFuente=" + idFuente + ", idOrigen=" + idOrigen + " ]";
    }
    
}
