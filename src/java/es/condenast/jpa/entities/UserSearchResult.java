/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

/**
 *
 * @author Ivan.Fernandez
 */
public class UserSearchResult {
    private Long idUsuario;
    private String emal;
    private Short idProducto;
    private String nombreProducto;
    private Short idMarca;
    private String nombreMarca;
    private Boolean cl1;
    private Boolean cl2;
    private Boolean cl3;
    private Short idParticipacion;
    private String nombreParticipacion;

    /**
     * @return the idUsuario
     */
    public Long getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @return the emal
     */
    public String getEmal() {
        return emal;
    }

    /**
     * @param emal the emal to set
     */
    public void setEmal(String emal) {
        this.emal = emal;
    }

    /**
     * @return the idProducto
     */
    public Short getIdProducto() {
        return idProducto;
    }

    /**
     * @param idProducto the idProducto to set
     */
    public void setIdProducto(Short idProducto) {
        this.idProducto = idProducto;
    }

    /**
     * @return the nombreProducto
     */
    public String getNombreProducto() {
        return nombreProducto;
    }

    /**
     * @param nombreProducto the nombreProducto to set
     */
    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    /**
     * @return the idMarca
     */
    public Short getIdMarca() {
        return idMarca;
    }

    /**
     * @param idMarca the idMarca to set
     */
    public void setIdMarca(Short idMarca) {
        this.idMarca = idMarca;
    }

    /**
     * @return the nombreMarca
     */
    public String getNombreMarca() {
        return nombreMarca;
    }

    /**
     * @param nombreMarca the nombreMarca to set
     */
    public void setNombreMarca(String nombreMarca) {
        this.nombreMarca = nombreMarca;
    }

    /**
     * @return the cl1
     */
    public Boolean getCl1() {
        return cl1;
    }

    /**
     * @param cl1 the cl1 to set
     */
    public void setCl1(Boolean cl1) {
        this.cl1 = cl1;
    }

    /**
     * @return the cl2
     */
    public Boolean getCl2() {
        return cl2;
    }

    /**
     * @param cl2 the cl2 to set
     */
    public void setCl2(Boolean cl2) {
        this.cl2 = cl2;
    }

    /**
     * @return the cl3
     */
    public Boolean getCl3() {
        return cl3;
    }

    /**
     * @param cl3 the cl3 to set
     */
    public void setCl3(Boolean cl3) {
        this.cl3 = cl3;
    }

    /**
     * @return the idParticipacion
     */
    public Short getIdParticipacion() {
        return idParticipacion;
    }

    /**
     * @param idParticipacion the idParticipacion to set
     */
    public void setIdParticipacion(Short idParticipacion) {
        this.idParticipacion = idParticipacion;
    }

    /**
     * @return the nombreParticipacion
     */
    public String getNombreParticipacion() {
        return nombreParticipacion;
    }

    /**
     * @param nombreParticipacion the nombreParticipacion to set
     */
    public void setNombreParticipacion(String nombreParticipacion) {
        this.nombreParticipacion = nombreParticipacion;
    }
    
    
}
