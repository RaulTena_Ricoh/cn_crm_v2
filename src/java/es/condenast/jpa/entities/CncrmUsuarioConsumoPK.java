/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Embeddable
public class CncrmUsuarioConsumoPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdUsuario")
    private int idUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdMarca")
    private Short idMarca;

    public CncrmUsuarioConsumoPK() {
    }

    public CncrmUsuarioConsumoPK(int idUsuario, Short IdMarca) {
        this.idUsuario = idUsuario;
        this.idMarca = IdMarca;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Short getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(Short IdMarca) {
        this.idMarca = IdMarca;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idUsuario;
        hash += (int) idMarca;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CncrmUsuarioConsumoPK)) {
            return false;
        }
        CncrmUsuarioConsumoPK other = (CncrmUsuarioConsumoPK) object;
        if (this.idUsuario != other.idUsuario) {
            return false;
        }
        if (this.idMarca != other.idMarca) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioConsumoPK[ idUsuario=" + idUsuario + ", idMarca=" + idMarca + " ]";
    }
}
