/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ivan.fernandez
 */
@Entity
@Table(name = "cnmas_provincia")
@NamedQueries({
    @NamedQuery(name = "CnmasProvincia.findAll", query = "SELECT c FROM CnmasProvincia c"),
    @NamedQuery(name = "CnmasProvincia.findByIdPais", query = "SELECT c FROM CnmasProvincia c WHERE c.cnmasProvinciaPK.idPais = :idPais"),
    @NamedQuery(name = "CnmasProvincia.findByIdProvincia", query = "SELECT c FROM CnmasProvincia c WHERE c.cnmasProvinciaPK.idProvincia = :idProvincia")})
public class CnmasProvincia implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    private CnmasProvinciaPK cnmasProvinciaPK;
    @JoinColumn(name = "IdPais", referencedColumnName = "IdPais", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasPais cnmasPais;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = true)
    @Size(min = 1, max = 10)
    @Column(name = "ISO")
    private String iso;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasProvincia")
    private Collection<CncrmDireccion> cncrmDireccionCollection;
    
    public CnmasProvincia(){
    }
    
    public CnmasProvincia(CnmasProvinciaPK cnmasProvinciaPK, String nombre, String iso){
        this.cnmasProvinciaPK = cnmasProvinciaPK;
        this.nombre = nombre;
        this.iso = iso;
        
    }
    /**
     * @return the cnmasProvinciaPK
     */
    public CnmasProvinciaPK getCnmasProvinciaPK() {
        return cnmasProvinciaPK;
    }

    /**
     * @param cnmasProvinciaPK the cnmasProvinciaPK to set
     */
    public void setCnmasProvinciaPK(CnmasProvinciaPK cnmasProvinciaPK) {
        this.cnmasProvinciaPK = cnmasProvinciaPK;
    }

    /**
     * @return the cnmasPais
     */
    public CnmasPais getCnmasPais() {
        return cnmasPais;
    }

    /**
     * @param cnmasPais the cnmasPais to set
     */
    public void setCnmasPais(CnmasPais cnmasPais) {
        this.cnmasPais = cnmasPais;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the iso
     */
    public String getIso() {
        return iso;
    }

    /**
     * @param iso the iso to set
     */
    public void setIso(String iso) {
        this.iso = iso;
    }

    /**
     *
     * @return
     */
    public Collection<CncrmDireccion> getCncrmDireccionCollection() {
        return cncrmDireccionCollection;
    }

    /**
     *
     * @param cncrmDireccionCollection
     */
    public void setCncrmDireccionCollection(Collection<CncrmDireccion> cncrmDireccionCollection) {
        this.cncrmDireccionCollection = cncrmDireccionCollection;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cnmasProvinciaPK != null ? cnmasProvinciaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasProvincia)) {
            return false;
        }
        CnmasProvincia other = (CnmasProvincia) object;
        if ((this.cnmasProvinciaPK == null && other.cnmasProvinciaPK != null) || (this.cnmasProvinciaPK != null && !this.cnmasProvinciaPK.equals(other.cnmasProvinciaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasProvincia[ cnmasProvinciaPK=" + cnmasProvinciaPK + " ]";
    }
}
