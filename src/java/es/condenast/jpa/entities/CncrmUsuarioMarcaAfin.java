/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cncrm_usuario_marca_afin")
@NamedQueries({
    @NamedQuery(name = "CncrmUsuarioMarcaAfin.findAll", query = "SELECT c FROM CncrmUsuarioMarcaAfin c"),
    @NamedQuery(name = "CncrmUsuarioMarcaAfin.findByIdUsuario", query = "SELECT c FROM CncrmUsuarioMarcaAfin c WHERE c.cncrmUsuarioMarcaAfinPK.idUsuario = :idUsuario"),
    @NamedQuery(name = "CncrmUsuarioMarcaAfin.findByIdMarcaAfin", query = "SELECT c FROM CncrmUsuarioMarcaAfin c WHERE c.cncrmUsuarioMarcaAfinPK.idMarcaAfin = :idMarcaAfin"),
    @NamedQuery(name = "CncrmUsuarioMarcaAfin.findByIdMarca", query = "SELECT c FROM CncrmUsuarioMarcaAfin c WHERE c.cncrmUsuarioMarcaAfinPK.idMarca = :idMarca")})
public class CncrmUsuarioMarcaAfin implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CncrmUsuarioMarcaAfinPK cncrmUsuarioMarcaAfinPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    
    @JoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CncrmUsuario cncrmUsuario;
    @JoinColumn(name = "IdMarcaAfin", referencedColumnName = "IdMarcaAfin", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasMarcaAfin cnmasMarcaAfin;
    @JoinColumn(name = "IdMarca", referencedColumnName = "IdMarca", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasMarca cnmasMarca;

    public CncrmUsuarioMarcaAfin() {
    }

    public CncrmUsuarioMarcaAfin(CncrmUsuarioMarcaAfinPK cncrmUsuarioMarcaAfinPK) {
        this.cncrmUsuarioMarcaAfinPK = cncrmUsuarioMarcaAfinPK;
    }
    
    public CncrmUsuarioMarcaAfin(long idUsuario, short idMarcaAfin, short idMarca) {
        this.cncrmUsuarioMarcaAfinPK = new CncrmUsuarioMarcaAfinPK(idUsuario, idMarcaAfin, idMarca);
    }

    public CncrmUsuarioMarcaAfinPK getCncrmUsuarioMarcaAfinPK() {
        return cncrmUsuarioMarcaAfinPK;
    }

    public void setCncrmUsuarioMarcaAfinPK(CncrmUsuarioMarcaAfinPK cncrmUsuarioMarcaAfinPK) {
        this.cncrmUsuarioMarcaAfinPK = cncrmUsuarioMarcaAfinPK;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public CncrmUsuario getCncrmUsuario() {
        return cncrmUsuario;
    }

    public void setCncrmUsuario(CncrmUsuario cncrmUsuario) {
        this.cncrmUsuario = cncrmUsuario;
    }

    public CnmasMarcaAfin getCnmasMarcaAfin() {
        return cnmasMarcaAfin;
    }

    public void setCnmasMarcaAfin(CnmasMarcaAfin cnmasMarcaAfin) {
        this.cnmasMarcaAfin = cnmasMarcaAfin;
    }

    public CnmasMarca getCnmasMarca() {
        return cnmasMarca;
    }

    public void setCnmasMarca(CnmasMarca cnmasMarca) {
        this.cnmasMarca = cnmasMarca;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += this.cncrmUsuarioMarcaAfinPK.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CncrmUsuarioMarcaAfin)) {
            return false;
        }
        CncrmUsuarioMarcaAfin other = (CncrmUsuarioMarcaAfin) object;
        if ((this.cncrmUsuarioMarcaAfinPK == null && other.cncrmUsuarioMarcaAfinPK != null) ||
                (this.cncrmUsuarioMarcaAfinPK != null && !this.cncrmUsuarioMarcaAfinPK.equals(other.cncrmUsuarioMarcaAfinPK))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioMarcaAfin[ cncrmUsuarioMarcaAfinPK=" + cncrmUsuarioMarcaAfinPK + " ]";
    }
    
}
