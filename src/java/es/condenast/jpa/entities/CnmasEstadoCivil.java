/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ivan.fernandez
 */
@Entity
@Table(name="cnmas_estado_civil")
@NamedQueries({
    @NamedQuery(name = "CnmasEstadoCivil.findAll", query = "SELECT c FROM CnmasEstadoCivil C ORDER BY c.idEstadoCivil"),
    @NamedQuery(name = "CnmasEstadoCivil.findByIdEstadoCivil", query = "SELECT c FROM CnmasEstadoCivil C WHERE c.idEstadoCivil = :idEstadoCivil"),
    @NamedQuery(name = "CnmasEstadoCivil.getMaxId", query = "SELECT max(c.idEstadoCivil) + 1 FROM CnmasEstadoCivil C"),
    @NamedQuery(name = "CnmasEstadoCivil.findByNombre", query = "SELECT c FROM CnmasEstadoCivil c WHERE c.nombre = :nombre")})

public class CnmasEstadoCivil implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdEstadoCivil")
    private Short idEstadoCivil;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasEstadoCivil")
    private Collection<CncrmPersona> cncrmPersonaCollection;
    
    public CnmasEstadoCivil(){
        
    }

    public CnmasEstadoCivil(Short idEstadoCivil, String nombre){
        this.idEstadoCivil = idEstadoCivil;
        this.nombre = nombre;
    }
    /**
     * @return the idEstadoCivil
     */
    public Short getIdEstadoCivil() {
        return idEstadoCivil;
    }

    /**
     * @param idEstadoCivil the idEstadoCivil to set
     */
    public void setIdEstadoCivil(Short idEstadoCivil) {
        this.idEstadoCivil = idEstadoCivil;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public Collection<CncrmPersona> getCncrmPersonaCollection() {
        return cncrmPersonaCollection;
    }

    /**
     *
     * @param cncrmPersonaCollection
     */
    public void setCncrmPersonaCollection(Collection<CncrmPersona> cncrmPersonaCollection) {
        this.cncrmPersonaCollection = cncrmPersonaCollection;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEstadoCivil != null ? idEstadoCivil.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasEstadoCivil)) {
            return false;
        }
        CnmasEstadoCivil other = (CnmasEstadoCivil) object;
        if ((this.idEstadoCivil == null && other.idEstadoCivil != null) || (this.idEstadoCivil != null && !this.idEstadoCivil.equals(other.idEstadoCivil))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasEstadoCivil[ idEstadoCivil=" + idEstadoCivil + " ]";
    }  
}