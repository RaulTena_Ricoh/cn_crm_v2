/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnetl_dato")
@NamedQueries({
    @NamedQuery(name = "CnetlDato.findAll", query = "SELECT c FROM CnetlDato c"),
    @NamedQuery(name = "CnetlDato.findByIdDato", query = "SELECT c FROM CnetlDato c WHERE c.idDato = :idDato"),
    @NamedQuery(name = "CnetlDato.findByNombre", query = "SELECT c FROM CnetlDato c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "CnetlDato.findByDato", query = "SELECT c FROM CnetlDato c WHERE c.dato = :dato"),
    @NamedQuery(name = "CnetlDato.findByConversion", query = "SELECT c FROM CnetlDato c WHERE c.conversion = :conversion")})
public class CnetlDato implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdDato")
    private Short idDato;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "Descripcion")
    private String descripcion;    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Dato")
    private String dato;
    @Size(max = 200)
    @Column(name = "Conversion")
    private String conversion;
    @Size(max = 200)
    @Column(name = "ExpReg")
    private String expReg;    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDato")
    private Collection<CnetlOrigen> cnetlOrigenCollection;

    public CnetlDato() {
    }

    public CnetlDato(Short idDato) {
        this.idDato = idDato;
    }

    public CnetlDato(Short idDato, String nombre, String dato) {
        this.idDato = idDato;
        this.nombre = nombre;
        this.dato = dato;
    }

    public Short getIdDato() {
        return idDato;
    }

    public void setIdDato(Short idDato) {
        this.idDato = idDato;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDato() {
        return dato;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }

    public String getConversion() {
        return conversion;
    }

    public void setConversion(String conversion) {
        this.conversion = conversion;
    }

    public String getExpReg() {
        return expReg;
    }

    public void setExpReg(String expReg) {
        this.expReg = expReg;
    }
 
    public Collection<CnetlOrigen> getCnetlOrigenCollection() {
        return cnetlOrigenCollection;
    }

    public void setCnetlOrigenCollection(Collection<CnetlOrigen> cnetlOrigenCollection) {
        this.cnetlOrigenCollection = cnetlOrigenCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDato != null ? idDato.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlDato)) {
            return false;
        }
        CnetlDato other = (CnetlDato) object;
        if ((this.idDato == null && other.idDato != null) || (this.idDato != null && !this.idDato.equals(other.idDato))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnetlDato[ idDato=" + idDato + " ]";
    }
    
}
