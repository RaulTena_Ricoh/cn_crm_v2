/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnetl_origen")
@NamedQueries({
    @NamedQuery(name = "CnetlOrigen.findAll", query = "SELECT c FROM CnetlOrigen c"),
    @NamedQuery(name = "CnetlOrigen.findByIdOrigen", query = "SELECT c FROM CnetlOrigen c WHERE c.idOrigen = :idOrigen"),
    @NamedQuery(name = "CnetlOrigen.findByNombre", query = "SELECT c FROM CnetlOrigen c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "CnetlOrigen.findByDescripcion", query = "SELECT c FROM CnetlOrigen c WHERE c.descripcion = :descripcion"),
    @NamedQuery(name = "CnetlOrigen.findByIgnorar", query = "SELECT c FROM CnetlOrigen c WHERE c.ignorar = :ignorar"),
    @NamedQuery(name = "CnetlOrigen.findValoresPosibles", query = "SELECT c FROM CnetlOrigen c WHERE c.ignorar = :ignorar AND c.idOrigen NOT IN (SELECT d.cnetlFuenteTipoOrigenPK.idOrigen FROM CnetlFuenteTipoOrigen d WHERE d.cnetlFuenteTipoOrigenPK.idFuenteTipo = :idFuenteTipo) "),
    @NamedQuery(name = "CnetlOrigen.findValoresExistentes", query = "SELECT c FROM CnetlOrigen c WHERE c.ignorar = :ignorar AND c.idOrigen IN (SELECT d.cnetlFuenteTipoOrigenPK.idOrigen FROM CnetlFuenteTipoOrigen d WHERE d.cnetlFuenteTipoOrigenPK.idFuenteTipo = :idFuenteTipo) "),
    @NamedQuery(name = "CnetlOrigen.findValoresPosiblesMaster", query = "SELECT c FROM CnetlOrigen c WHERE c.ignorar = :ignorar AND c.idOrigen NOT IN (SELECT d.cnetlMasterTipoOrigenPK.idOrigen FROM CnetlMasterTipoOrigen d WHERE d.cnetlMasterTipoOrigenPK.idMasterTipo = :idMasterTipo) "),
    @NamedQuery(name = "CnetlOrigen.findValoresExistentesMaster", query = "SELECT c FROM CnetlOrigen c WHERE c.ignorar = :ignorar AND c.idOrigen IN (SELECT d.cnetlMasterTipoOrigenPK.idOrigen FROM CnetlMasterTipoOrigen d WHERE d.cnetlMasterTipoOrigenPK.idMasterTipo = :idMasterTipo) ")})
public class CnetlOrigen implements Serializable, Comparable<CnetlOrigen> {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdOrigen")
    private Integer idOrigen;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @Size(max = 300)
    @Column(name = "Descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Ignorar")
    private boolean ignorar;
    @JoinColumn(name = "IdDato", referencedColumnName = "IdDato")
    @ManyToOne(optional = false)
    private CnetlDato idDato;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnetlOrigen")
    private Collection<CnetlFuenteTipoOrigen> cnetlFuenteTipoOrigenCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnetlOrigen")
    private Collection<CnetlFuenteValor> cnetlFuenteValorCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnetlOrigen")
    private Collection<CnetlMasterTipoOrigen> cnetlMasterTipoOrigenCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnetlOrigen")
    private Collection<CnetlMasterValor> cnetlMasterValorCollection;

    public CnetlOrigen() {
    }

    public CnetlOrigen(Integer idOrigen) {
        this.idOrigen = idOrigen;
    }

    public CnetlOrigen(Integer idOrigen, String nombre, boolean ignorar) {
        this.idOrigen = idOrigen;
        this.nombre = nombre;
        this.ignorar = ignorar;
    }

    public Integer getIdOrigen() {
        return idOrigen;
    }

    public void setIdOrigen(Integer idOrigen) {
        this.idOrigen = idOrigen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean getIgnorar() {
        return ignorar;
    }

    public void setIgnorar(boolean ignorar) {
        this.ignorar = ignorar;
    }

    public CnetlDato getIdDato() {
        return idDato;
    }

    public void setIdDato(CnetlDato idDato) {
        this.idDato = idDato;
    }

    public Collection<CnetlFuenteTipoOrigen> getCnetlFuenteTipoOrigenCollection() {
        return cnetlFuenteTipoOrigenCollection;
    }

    public void setCnetlFuenteTipoOrigenCollection(Collection<CnetlFuenteTipoOrigen> cnetlFuenteTipoOrigenCollection) {
        this.cnetlFuenteTipoOrigenCollection = cnetlFuenteTipoOrigenCollection;
    }

    public Collection<CnetlFuenteValor> getCnetlFuenteValorCollection() {
        return cnetlFuenteValorCollection;
    }

    public void setCnetlFuenteValorCollection(Collection<CnetlFuenteValor> cnetlFuenteValorCollection) {
        this.cnetlFuenteValorCollection = cnetlFuenteValorCollection;
    }

    public Collection<CnetlMasterTipoOrigen> getCnetlMasterTipoOrigenCollection() {
        return cnetlMasterTipoOrigenCollection;
    }

    public void setCnetlMasterTipoOrigenCollection(Collection<CnetlMasterTipoOrigen> cnetlMasterTipoOrigenCollection) {
        this.cnetlMasterTipoOrigenCollection = cnetlMasterTipoOrigenCollection;
    }

    public Collection<CnetlMasterValor> getCnetlMasterValorCollection() {
        return cnetlMasterValorCollection;
    }

    public void setCnetlMasterValorCollection(Collection<CnetlMasterValor> cnetlMasterValorCollection) {
        this.cnetlMasterValorCollection = cnetlMasterValorCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idOrigen != null ? idOrigen.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlOrigen)) {
            return false;
        }
        CnetlOrigen other = (CnetlOrigen) object;
        if ((this.idOrigen == null && other.idOrigen != null) || (this.idOrigen != null && !this.idOrigen.equals(other.idOrigen))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre;
    }

    @Override
    public int compareTo(CnetlOrigen o) {
        //return this.idOrigen - o.idOrigen;
        return this.nombre.compareTo(o.nombre);
    }

    public static HashMap<String, CnetlOrigen> toHashMap(List<CnetlOrigen> origenes) {
        HashMap<String, CnetlOrigen> origenesHM = new HashMap<>();
        for (CnetlOrigen origen : origenes) {
            origenesHM.put(origen.getNombre(), origen);
        }
        return origenesHM;
    }

    public static HashMap<Integer, CnetlOrigen> toHashMapId(List<CnetlOrigen> origenes) {
        HashMap<Integer, CnetlOrigen> origenesHM = new HashMap<>();
        for (CnetlOrigen origen : origenes) {
            origenesHM.put(origen.getIdOrigen(), origen);
        }
        return origenesHM;
    }

}
