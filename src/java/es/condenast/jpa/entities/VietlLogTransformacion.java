/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "vietl_log_transformacion")
@NamedQueries({
    @NamedQuery(name = "VietlLogTransformacion.findAll", query = "SELECT v FROM VietlLogTransformacion v"),
    @NamedQuery(name = "VietlLogTransformacion.findByIdPool", query = "SELECT v FROM VietlLogTransformacion v WHERE v.idPool = :idPool"),
    @NamedQuery(name = "VietlLogTransformacion.findByFuente", query = "SELECT v FROM VietlLogTransformacion v WHERE v.fuente = :fuente"),
    @NamedQuery(name = "VietlLogTransformacion.findByFuenteTipo", query = "SELECT v FROM VietlLogTransformacion v WHERE v.fuenteTipo = :fuenteTipo"),
    @NamedQuery(name = "VietlLogTransformacion.findByInicioPool", query = "SELECT v FROM VietlLogTransformacion v WHERE v.inicioPool = :inicioPool"),
    @NamedQuery(name = "VietlLogTransformacion.findByLoadPool", query = "SELECT v FROM VietlLogTransformacion v WHERE v.loadPool = :loadPool"),
    @NamedQuery(name = "VietlLogTransformacion.findByFinPool", query = "SELECT v FROM VietlLogTransformacion v WHERE v.finPool = :finPool"),
    @NamedQuery(name = "VietlLogTransformacion.findByLineas", query = "SELECT v FROM VietlLogTransformacion v WHERE v.lineas = :lineas"),
    @NamedQuery(name = "VietlLogTransformacion.findByLineasOK", query = "SELECT v FROM VietlLogTransformacion v WHERE v.lineasOK = :lineasOK"),
    @NamedQuery(name = "VietlLogTransformacion.findByLineasError", query = "SELECT v FROM VietlLogTransformacion v WHERE v.lineasError = :lineasError"),
    @NamedQuery(name = "VietlLogTransformacion.findByIdTransformacion", query = "SELECT v FROM VietlLogTransformacion v WHERE v.idTransformacion = :idTransformacion"),
    @NamedQuery(name = "VietlLogTransformacion.findByTransformacion", query = "SELECT v FROM VietlLogTransformacion v WHERE v.transformacion = :transformacion"),
    @NamedQuery(name = "VietlLogTransformacion.findByInicioTransformacion", query = "SELECT v FROM VietlLogTransformacion v WHERE v.inicioTransformacion = :inicioTransformacion"),
    @NamedQuery(name = "VietlLogTransformacion.findByFinTransformacion", query = "SELECT v FROM VietlLogTransformacion v WHERE v.finTransformacion = :finTransformacion"),
    @NamedQuery(name = "VietlLogTransformacion.findByDuracionPool", query = "SELECT v FROM VietlLogTransformacion v WHERE v.duracionPool = :duracionPool"),
    @NamedQuery(name = "VietlLogTransformacion.findByDuracionLoad", query = "SELECT v FROM VietlLogTransformacion v WHERE v.duracionLoad = :duracionLoad"),
    @NamedQuery(name = "VietlLogTransformacion.findByDuracionTransformacion", query = "SELECT v FROM VietlLogTransformacion v WHERE v.duracionTransformacion = :duracionTransformacion")})
public class VietlLogTransformacion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdPool")
    @Id
    private long idPool;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Fuente")
    @Id
    private String fuente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "FuenteTipo")
    @Id
    private String fuenteTipo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "InicioPool")
    @Temporal(TemporalType.TIMESTAMP)
    private Date inicioPool;
    @Column(name = "LoadPool")
    @Temporal(TemporalType.TIMESTAMP)
    private Date loadPool;
    @Column(name = "FinPool")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finPool;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Lineas")
    private int lineas;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LineasOK")
    private int lineasOK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LineasError")
    private int lineasError;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdTransformacion")
    @Id
    private int idTransformacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Transformacion")
    @Id
    private String transformacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "InicioTransformacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date inicioTransformacion;
    @Column(name = "FinTransformacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finTransformacion;
    @Column(name = "DuracionPool")
    @Temporal(TemporalType.TIME)
    private Date duracionPool;
    @Column(name = "DuracionLoad")
    @Temporal(TemporalType.TIME)
    private Date duracionLoad;
    @Column(name = "DuracionTransformacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar duracionTransformacion;

    public VietlLogTransformacion() {
    }

    public long getIdPool() {
        return idPool;
    }

    public void setIdPool(long idPool) {
        this.idPool = idPool;
    }

    public String getFuente() {
        return fuente;
    }

    public void setFuente(String fuente) {
        this.fuente = fuente;
    }

    public String getFuenteTipo() {
        return fuenteTipo;
    }

    public void setFuenteTipo(String fuenteTipo) {
        this.fuenteTipo = fuenteTipo;
    }

    public Date getInicioPool() {
        return inicioPool;
    }

    public void setInicioPool(Date inicioPool) {
        this.inicioPool = inicioPool;
    }

    public Date getLoadPool() {
        return loadPool;
    }

    public void setLoadPool(Date loadPool) {
        this.loadPool = loadPool;
    }

    public Date getFinPool() {
        return finPool;
    }

    public void setFinPool(Date finPool) {
        this.finPool = finPool;
    }

    public int getLineas() {
        return lineas;
    }

    public void setLineas(int lineas) {
        this.lineas = lineas;
    }

    public int getLineasOK() {
        return lineasOK;
    }

    public void setLineasOK(int lineasOK) {
        this.lineasOK = lineasOK;
    }

    public int getLineasError() {
        return lineasError;
    }

    public void setLineasError(int lineasError) {
        this.lineasError = lineasError;
    }

    public int getIdTransformacion() {
        return idTransformacion;
    }

    public void setIdTransformacion(int idTransformacion) {
        this.idTransformacion = idTransformacion;
    }

    public String getTransformacion() {
        return transformacion;
    }

    public void setTransformacion(String transformacion) {
        this.transformacion = transformacion;
    }

    public Date getInicioTransformacion() {
        return inicioTransformacion;
    }

    public void setInicioTransformacion(Date inicioTransformacion) {
        this.inicioTransformacion = inicioTransformacion;
    }

    public Date getFinTransformacion() {
        return finTransformacion;
    }

    public void setFinTransformacion(Date finTransformacion) {
        this.finTransformacion = finTransformacion;
    }

    public Date getDuracionPool() {
        return duracionPool;
    }

    public void setDuracionPool(Date duracionPool) {
        this.duracionPool = duracionPool;
    }

    public Date getDuracionLoad() {
        return duracionLoad;
    }

    public void setDuracionLoad(Date duracionLoad) {
        this.duracionLoad = duracionLoad;
    }

    public Calendar getDuracionTransformacion() {
        return duracionTransformacion;
    }

    public void setDuracionTransformacion(Calendar duracionTransformacion) {
        this.duracionTransformacion = duracionTransformacion;
    }
    
}
