/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "vietl_log_validacion_linea")
@NamedQueries({
    @NamedQuery(name = "VietlLogValidacionLinea.findAll", query = "SELECT v FROM VietlLogValidacionLinea v"),
    @NamedQuery(name = "VietlLogValidacionLinea.findByIdPool", query = "SELECT v FROM VietlLogValidacionLinea v WHERE v.idPool = :idPool"),
    @NamedQuery(name = "VietlLogValidacionLinea.findByFuente", query = "SELECT v FROM VietlLogValidacionLinea v WHERE v.fuente = :fuente"),
    @NamedQuery(name = "VietlLogValidacionLinea.findByFuenteTipo", query = "SELECT v FROM VietlLogValidacionLinea v WHERE v.fuenteTipo = :fuenteTipo"),
    @NamedQuery(name = "VietlLogValidacionLinea.findByInicioPool", query = "SELECT v FROM VietlLogValidacionLinea v WHERE v.inicioPool = :inicioPool"),
    @NamedQuery(name = "VietlLogValidacionLinea.findByLoadPool", query = "SELECT v FROM VietlLogValidacionLinea v WHERE v.loadPool = :loadPool"),
    @NamedQuery(name = "VietlLogValidacionLinea.findByFinPool", query = "SELECT v FROM VietlLogValidacionLinea v WHERE v.finPool = :finPool"),
    @NamedQuery(name = "VietlLogValidacionLinea.findByLineas", query = "SELECT v FROM VietlLogValidacionLinea v WHERE v.lineas = :lineas"),
    @NamedQuery(name = "VietlLogValidacionLinea.findByLineasOK", query = "SELECT v FROM VietlLogValidacionLinea v WHERE v.lineasOK = :lineasOK"),
    @NamedQuery(name = "VietlLogValidacionLinea.findByLineasError", query = "SELECT v FROM VietlLogValidacionLinea v WHERE v.lineasError = :lineasError"),
    @NamedQuery(name = "VietlLogValidacionLinea.findByIdValidacion", query = "SELECT v FROM VietlLogValidacionLinea v WHERE v.idValidacion = :idValidacion"),
    @NamedQuery(name = "VietlLogValidacionLinea.findByValidacion", query = "SELECT v FROM VietlLogValidacionLinea v WHERE v.validacion = :validacion"),
    @NamedQuery(name = "VietlLogValidacionLinea.findByFechaValidacion", query = "SELECT v FROM VietlLogValidacionLinea v WHERE v.fechaValidacion = :fechaValidacion"),
    @NamedQuery(name = "VietlLogValidacionLinea.findByLineasOkValidacion", query = "SELECT v FROM VietlLogValidacionLinea v WHERE v.lineasOkValidacion = :lineasOkValidacion"),
    @NamedQuery(name = "VietlLogValidacionLinea.findByLineasErrorValidacion", query = "SELECT v FROM VietlLogValidacionLinea v WHERE v.lineasErrorValidacion = :lineasErrorValidacion"),
    @NamedQuery(name = "VietlLogValidacionLinea.findByLinea", query = "SELECT v FROM VietlLogValidacionLinea v WHERE v.linea = :linea"),
    @NamedQuery(name = "VietlLogValidacionLinea.findByValor", query = "SELECT v FROM VietlLogValidacionLinea v WHERE v.valor = :valor")})
public class VietlLogValidacionLinea implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdPool")
    @Id
    private long idPool;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Fuente")
    @Id
    private String fuente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "FuenteTipo")
    @Id
    private String fuenteTipo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "InicioPool")
    @Temporal(TemporalType.TIMESTAMP)
    private Date inicioPool;
    @Column(name = "LoadPool")
    @Temporal(TemporalType.TIMESTAMP)
    private Date loadPool;
    @Column(name = "FinPool")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finPool;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Lineas")
    private int lineas;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LineasOK")
    private int lineasOK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LineasError")
    private int lineasError;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdValidacion")
    @Id
    private int idValidacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Validacion")
    @Id
    private String validacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaValidacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaValidacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LineasOkValidacion")
    private int lineasOkValidacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LineasErrorValidacion")
    private int lineasErrorValidacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Linea")
    @Id
    private int linea;
    @Size(max = 200)
    @Column(name = "Valor")
    @Id
    private String valor;

    public VietlLogValidacionLinea() {
    }

    public long getIdPool() {
        return idPool;
    }

    public void setIdPool(long idPool) {
        this.idPool = idPool;
    }

    public String getFuente() {
        return fuente;
    }

    public void setFuente(String fuente) {
        this.fuente = fuente;
    }

    public String getFuenteTipo() {
        return fuenteTipo;
    }

    public void setFuenteTipo(String fuenteTipo) {
        this.fuenteTipo = fuenteTipo;
    }

    public Date getInicioPool() {
        return inicioPool;
    }

    public void setInicioPool(Date inicioPool) {
        this.inicioPool = inicioPool;
    }

    public Date getLoadPool() {
        return loadPool;
    }

    public void setLoadPool(Date loadPool) {
        this.loadPool = loadPool;
    }

    public Date getFinPool() {
        return finPool;
    }

    public void setFinPool(Date finPool) {
        this.finPool = finPool;
    }

    public int getLineas() {
        return lineas;
    }

    public void setLineas(int lineas) {
        this.lineas = lineas;
    }

    public int getLineasOK() {
        return lineasOK;
    }

    public void setLineasOK(int lineasOK) {
        this.lineasOK = lineasOK;
    }

    public int getLineasError() {
        return lineasError;
    }

    public void setLineasError(int lineasError) {
        this.lineasError = lineasError;
    }

    public int getIdValidacion() {
        return idValidacion;
    }

    public void setIdValidacion(int idValidacion) {
        this.idValidacion = idValidacion;
    }

    public String getValidacion() {
        return validacion;
    }

    public void setValidacion(String validacion) {
        this.validacion = validacion;
    }

    public Date getFechaValidacion() {
        return fechaValidacion;
    }

    public void setFechaValidacion(Date fechaValidacion) {
        this.fechaValidacion = fechaValidacion;
    }

    public int getLineasOkValidacion() {
        return lineasOkValidacion;
    }

    public void setLineasOkValidacion(int lineasOkValidacion) {
        this.lineasOkValidacion = lineasOkValidacion;
    }

    public int getLineasErrorValidacion() {
        return lineasErrorValidacion;
    }

    public void setLineasErrorValidacion(int lineasErrorValidacion) {
        this.lineasErrorValidacion = lineasErrorValidacion;
    }

    public int getLinea() {
        return linea;
    }

    public void setLinea(int linea) {
        this.linea = linea;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

}
