/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cnmas_red_social")
@NamedQueries({
    @NamedQuery(name = "CnmasRedSocial.findAll", query = "SELECT c FROM CnmasRedSocial c"),
    @NamedQuery(name = "CnmasRedSocial.findByIdRedSocial", query = "SELECT c FROM CnmasRedSocial c WHERE c.idRedSocial = :idRedSocial"),
    @NamedQuery(name = "CnmasRedSocial.findByNombre", query = "SELECT c FROM CnmasRedSocial c WHERE c.nombre = :nombre")})
public class CnmasRedSocial implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdRedSocial")
    private Short idRedSocial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasRedSocial")
    private Collection<CncrmUsuarioRedSocial> cncrmUsuarioRedSocialCollection;

    /**
     *
     */
    public CnmasRedSocial() {
    }

    /**
     *
     * @param idRedSocial
     */
    public CnmasRedSocial(Short idRedSocial) {
        this.idRedSocial = idRedSocial;
    }

    /**
     *
     * @param idRedSocial
     * @param nombre
     */
    public CnmasRedSocial(Short idRedSocial, String nombre) {
        this.idRedSocial = idRedSocial;
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public Short getIdRedSocial() {
        return idRedSocial;
    }

    /**
     *
     * @param idRedSocial
     */
    public void setIdRedSocial(Short idRedSocial) {
        this.idRedSocial = idRedSocial;
    }

    /**
     *
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public Collection<CncrmUsuarioRedSocial> getCncrmUsuarioRedSocialCollection() {
        return cncrmUsuarioRedSocialCollection;
    }

    /**
     *
     * @param cncrmUsuarioRedSocialCollection
     */
    public void setCncrmUsuarioRedSocialCollection(Collection<CncrmUsuarioRedSocial> cncrmUsuarioRedSocialCollection) {
        this.cncrmUsuarioRedSocialCollection = cncrmUsuarioRedSocialCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += this.idRedSocial;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CnmasRedSocial)) {
            return false;
        }
        CnmasRedSocial other = (CnmasRedSocial) object;
        if (!this.idRedSocial.equals(other.idRedSocial)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasRedSocial[ idRedSocial=" + idRedSocial + " ]";
    }
    
}
