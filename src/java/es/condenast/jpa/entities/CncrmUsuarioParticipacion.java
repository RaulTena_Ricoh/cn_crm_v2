/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ivan.fernandez
 */
@Entity
@Table(name = "cncrm_usuario_participacion")
@NamedQueries({
    @NamedQuery(name = "CncrmUsuarioParticipacion.findAll", query = "SELECT c FROM CncrmUsuarioParticipacion c"),
    @NamedQuery(name = "CncrmUsuarioParticipacion.findByIdUsuario", query = "SELECT c FROM CncrmUsuarioParticipacion c WHERE c.cncrmUsuarioParticipacionPK.idUsuario = :idUsuario"),
    @NamedQuery(name = "CncrmUsuarioParticipacion.findByIdParticipacion", query = "SELECT c FROM CncrmUsuarioParticipacion c WHERE c.cncrmUsuarioParticipacionPK.idParticipacion = :idParticipacion"),
    @NamedQuery(name = "CncrmUsuarioParticipacion.findByFechaInscripcion", query = "SELECT c FROM CncrmUsuarioParticipacion c WHERE c.fechaInscripcion = :fechaInscripcion")})
public class CncrmUsuarioParticipacion implements Serializable{
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    private CncrmUsuarioParticipacionPK cncrmUsuarioParticipacionPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaInscripcion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInscripcion;
    @JoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CncrmUsuario cncrmUsuario;
    @JoinColumn(name = "IdParticipacion", referencedColumnName = "IdParticipacion", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasParticipacion cnmasParticipacion;

    public CncrmUsuarioParticipacion(){
        
    }

    public CncrmUsuarioParticipacion(CncrmUsuarioParticipacionPK cncrmUsuarioParticipacionPK) {
        this.cncrmUsuarioParticipacionPK = cncrmUsuarioParticipacionPK;
    }

    public CncrmUsuarioParticipacion(CncrmUsuarioParticipacionPK cncrmUsuarioParticipacionPK, Date fechaInscripcion) {
        this.cncrmUsuarioParticipacionPK = cncrmUsuarioParticipacionPK;
        this.fechaInscripcion = fechaInscripcion;
    }

    public CncrmUsuarioParticipacion(long idUsuario, Short idParticipacion) {
        this.cncrmUsuarioParticipacionPK = new CncrmUsuarioParticipacionPK(idUsuario, idParticipacion);
    }

    public CncrmUsuarioParticipacionPK getCncrmUsuarioParticipacionPK() {
        return cncrmUsuarioParticipacionPK;
    }

    public void setCncrmUsuarioParticipacionPK(CncrmUsuarioParticipacionPK cncrmUsuarioParticipacionPK) {
        this.cncrmUsuarioParticipacionPK = cncrmUsuarioParticipacionPK;
    }

    public Date getFechaInscripcion() {
        return fechaInscripcion;
    }

    public void setFechaInscripcion(Date fechaInscripcion) {
        this.fechaInscripcion = fechaInscripcion;
    }

    public CncrmUsuario getCncrmUsuario() {
        return cncrmUsuario;
    }

    public void setCncrmUsuario(CncrmUsuario cncrmUsuario) {
        this.cncrmUsuario = cncrmUsuario;
    }

    /**
     * @return the cnmasParticipacion
     */
    public CnmasParticipacion getCnmasParticipacion() {
        return cnmasParticipacion;
    }

    /**
     * @param cnmasParticipacion the cnmasParticipacion to set
     */
    public void setCnmasParticipacion(CnmasParticipacion cnmasParticipacion) {
        this.cnmasParticipacion = cnmasParticipacion;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cncrmUsuarioParticipacionPK != null ? cncrmUsuarioParticipacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CncrmUsuarioParticipacion)) {
            return false;
        }
        CncrmUsuarioParticipacion other = (CncrmUsuarioParticipacion) object;
        if ((this.cncrmUsuarioParticipacionPK == null && other.cncrmUsuarioParticipacionPK != null) || (this.cncrmUsuarioParticipacionPK != null && !this.cncrmUsuarioParticipacionPK.equals(other.cncrmUsuarioParticipacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioParticipacion[ cncrmUsuarioParticipacionPK=" + cncrmUsuarioParticipacionPK + " ]";
    }
}
