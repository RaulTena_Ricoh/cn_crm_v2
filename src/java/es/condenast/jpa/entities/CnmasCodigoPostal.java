/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cnmas_codigo_postal")
@NamedQueries({
    @NamedQuery(name = "CnmasCodigoPostal.findAll", query = "SELECT c FROM CnmasCodigoPostal c"),
    @NamedQuery(name = "CnmasCodigoPostal.findByCodigoPostal", query = "SELECT c FROM CnmasCodigoPostal c WHERE c.cnmasCodigoPostalPK.codigoPostal = :codigoPostal"),
    @NamedQuery(name = "CnmasCodigoPostal.findByIdPoblacion", query = "SELECT c FROM CnmasCodigoPostal c WHERE c.cnmasCodigoPostalPK.idPoblacion = :idPoblacion"),
    @NamedQuery(name = "CnmasCodigoPostal.findByIdPais", query = "SELECT c FROM CnmasCodigoPostal c WHERE c.cnmasCodigoPostalPK.idPais = :idPais")})
public class CnmasCodigoPostal implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    private CnmasCodigoPostalPK cnmasCodigoPostalPK;

    @JoinColumn(name = "IdPais", referencedColumnName = "IdPais", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasPais cnmasPais;
    @JoinColumns({
        @JoinColumn(name = "IdPoblacion", referencedColumnName = "IdPoblacion", insertable = false, updatable = false),
        @JoinColumn(name = "IdProvincia", referencedColumnName = "IdProvincia", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private CnmasProvincia cnmasPoblacion;

    public CnmasCodigoPostal() {
    }

    public CnmasCodigoPostal(CnmasCodigoPostalPK cnmasCodigoPostalPK) {
        this.cnmasCodigoPostalPK = cnmasCodigoPostalPK;
    }

    public CnmasCodigoPostalPK getCnmasCodigoPostalPK() {
        return cnmasCodigoPostalPK;
    }

    public void setCnmasCodigoPostalPK(CnmasCodigoPostalPK cnmasCodigoPostalPK) {
        this.cnmasCodigoPostalPK = cnmasCodigoPostalPK;
    }

    public CnmasPais getCnmasPais() {
        return cnmasPais;
    }

    public void setCnmasPais(CnmasPais cnmasPais) {
        this.cnmasPais = cnmasPais;
    }

    public CnmasProvincia getCnmasPoblacion() {
        return cnmasPoblacion;
    }

    public void setCnmasPoblacion(CnmasProvincia cnmasPoblacion) {
        this.cnmasPoblacion = cnmasPoblacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cnmasCodigoPostalPK != null ? cnmasCodigoPostalPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasCodigoPostal)) {
            return false;
        }
        CnmasCodigoPostal other = (CnmasCodigoPostal) object;
        if ((this.cnmasCodigoPostalPK == null && other.cnmasCodigoPostalPK != null) || (this.cnmasCodigoPostalPK != null && !this.cnmasCodigoPostalPK.equals(other.cnmasCodigoPostalPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasCodigoPostal[ cnmasCodigoPostalPK=" + cnmasCodigoPostalPK + " ]";
    }
}
