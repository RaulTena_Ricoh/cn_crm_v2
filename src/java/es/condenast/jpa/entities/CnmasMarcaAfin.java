/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnmas_marca_afin")
@NamedQueries({
    @NamedQuery(name = "CnmasMarcaAfin.findAll", query = "SELECT c FROM CnmasMarcaAfin c ORDER BY c.idMarcaAfin"),
    @NamedQuery(name = "CnmasMarcaAfin.findByIdMarcaAfin", query = "SELECT c FROM CnmasMarcaAfin c WHERE c.idMarcaAfin = :idMarcaAfin"),
    @NamedQuery(name = "CnmasMarcaAfin.findByNombre", query = "SELECT c FROM CnmasMarcaAfin c WHERE c.nombre = :nombre")})
public class CnmasMarcaAfin implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdMarcaAfin")
    private Integer idMarcaAfin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;

    public CnmasMarcaAfin() {
    }

    public CnmasMarcaAfin(Integer idMarcaAfin) {
        this.idMarcaAfin = idMarcaAfin;
    }

    public CnmasMarcaAfin(Integer idMarcaAfin, String nombre) {
        this.idMarcaAfin = idMarcaAfin;
        this.nombre = nombre;
    }

    public Integer getIdMarcaAfin() {
        return idMarcaAfin;
    }

    public void setIdMarcaAfin(Integer idMarcaAfin) {
        this.idMarcaAfin = idMarcaAfin;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMarcaAfin != null ? idMarcaAfin.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasMarcaAfin)) {
            return false;
        }
        CnmasMarcaAfin other = (CnmasMarcaAfin) object;
        if ((this.idMarcaAfin == null && other.idMarcaAfin != null) || (this.idMarcaAfin != null && !this.idMarcaAfin.equals(other.idMarcaAfin))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasMarcaAfin[ idMarcaAfin=" + idMarcaAfin + " ]";
    }
    
}
