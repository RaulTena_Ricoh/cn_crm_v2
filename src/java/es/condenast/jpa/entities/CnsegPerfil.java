/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnseg_perfil")
@NamedQueries({
    @NamedQuery(name = "CnsegPerfil.findAll", query = "SELECT c FROM CnsegPerfil c"),
    @NamedQuery(name = "CnsegPerfil.findByIdPerfil", query = "SELECT c FROM CnsegPerfil c WHERE c.idPerfil = :idPerfil"),
    @NamedQuery(name = "CnsegPerfil.findByNombre", query = "SELECT c FROM CnsegPerfil c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "CnsegPerfil.findByPuedeVerDetalles", query = "SELECT c FROM CnsegPerfil c WHERE c.puedeVerDetalles = :puedeVerDetalles"),
    @NamedQuery(name = "CnsegPerfil.findByNumeroFilasDetalle", query = "SELECT c FROM CnsegPerfil c WHERE c.numeroFilasDetalle = :numeroFilasDetalle")})
public class CnsegPerfil implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdPerfil")
    private Short idPerfil;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PuedeVerDetalles")
    private boolean puedeVerDetalles;
    @Column(name = "NumeroFilasDetalle")
    private Integer numeroFilasDetalle;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPerfil")
    private Collection<CnsegUsuario> cnsegUsuarioCollection;

    public CnsegPerfil() {
    }

    public CnsegPerfil(Short idPerfil) {
        this.idPerfil = idPerfil;
    }

    public CnsegPerfil(Short idPerfil, String nombre, boolean puedeVerDetalles) {
        this.idPerfil = idPerfil;
        this.nombre = nombre;
        this.puedeVerDetalles = puedeVerDetalles;
    }

    public Short getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Short idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean getPuedeVerDetalles() {
        return puedeVerDetalles;
    }

    public void setPuedeVerDetalle(boolean puedeVerDetalles) {
        this.puedeVerDetalles = puedeVerDetalles;
    }

    public Integer getNumeroFilasDetalle() {
        return numeroFilasDetalle;
    }

    public void setNumeroFilasDetalle(Integer numeroFilasDetalle) {
        this.numeroFilasDetalle = numeroFilasDetalle;
    }

    public Collection<CnsegUsuario> getCnsegUsuarioCollection() {
        return cnsegUsuarioCollection;
    }

    public void setCnsegUsuarioCollection(Collection<CnsegUsuario> cnsegUsuarioCollection) {
        this.cnsegUsuarioCollection = cnsegUsuarioCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPerfil != null ? idPerfil.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnsegPerfil)) {
            return false;
        }
        CnsegPerfil other = (CnsegPerfil) object;
        if ((this.idPerfil == null && other.idPerfil != null) || (this.idPerfil != null && !this.idPerfil.equals(other.idPerfil))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre;
    }
    
}
