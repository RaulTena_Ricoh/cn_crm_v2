/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Raul.Tipo
 */
@Entity
@Table(name = "cnetl_master_tipo")
@NamedQueries({
    @NamedQuery(name = "CnetlMasterTipo.findAll", query = "SELECT c FROM CnetlMasterTipo c"),
    @NamedQuery(name = "CnetlMasterTipo.findByIdMasterTipo", query = "SELECT c FROM CnetlMasterTipo c WHERE c.idMasterTipo = :idMasterTipo"),
    @NamedQuery(name = "CnetlMasterTipo.findByNombre", query = "SELECT c FROM CnetlMasterTipo c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "CnetlMasterTipo.findByFieldTerminated", query = "SELECT c FROM CnetlMasterTipo c WHERE c.fieldTerminated = :fieldTerminated"),
    @NamedQuery(name = "CnetlMasterTipo.findByFieldEnclosed", query = "SELECT c FROM CnetlMasterTipo c WHERE c.fieldEnclosed = :fieldEnclosed"),
    @NamedQuery(name = "CnetlMasterTipo.findByLineTerminated", query = "SELECT c FROM CnetlMasterTipo c WHERE c.lineTerminated = :lineTerminated"),
    @NamedQuery(name = "CnetlMasterTipo.findByLineIgnore", query = "SELECT c FROM CnetlMasterTipo c WHERE c.lineIgnore = :lineIgnore")})
public class CnetlMasterTipo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdMasterTipo")
    private Short idMasterTipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "FieldTerminated")
    private String fieldTerminated;
    @Basic(optional = true)
    @Column(name = "FieldEnclosed")
    private String fieldEnclosed;
    @Basic(optional = true)
    @Column(name = "FieldScaped")
    private String fieldScaped;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "LineTerminated")
    private String lineTerminated;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LineIgnore")
    private int lineIgnore;
    @JoinColumn(name = "IdFormato", referencedColumnName = "IdFormato")
    @ManyToOne(optional = false)
    private CnetlFormato idFormato;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMasterTipo")
    private Collection<CnetlMaster> cnetlMasterCollection;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnetlMasterTipo")
    @OrderBy("orden ASC")
    private Collection<CnetlMasterTipoOrigen> cnetlMasterTipoOrigenCollection;

    public CnetlMasterTipo() {
    }

    public CnetlMasterTipo(Short idMasterTipo) {
        this.idMasterTipo = idMasterTipo;
    }

    public CnetlMasterTipo(Short idMasterTipo, String nombre, String fieldTerminated, String fieldEnclosed, String lineTerminated, int lineIgnore) {
        this.idMasterTipo = idMasterTipo;
        this.nombre = nombre;
        this.fieldTerminated = fieldTerminated;
        this.fieldEnclosed = fieldEnclosed;
        this.lineTerminated = lineTerminated;
        this.lineIgnore = lineIgnore;
    }

    public Short getIdMasterTipo() {
        return idMasterTipo;
    }

    public void setIdMasterTipo(Short idMasterTipo) {
        this.idMasterTipo = idMasterTipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFieldTerminated() {
        return fieldTerminated;
    }

    public void setFieldTerminated(String fieldTerminated) {
        this.fieldTerminated = fieldTerminated;
    }

    public String getFieldEnclosed() {
        return fieldEnclosed;
    }

    public void setFieldEnclosed(String fieldEnclosed) {
        this.fieldEnclosed = fieldEnclosed;
    }

    public String getFieldScaped() {
        return fieldScaped;
    }

    public void setFieldScaped(String fieldScaped) {
        this.fieldScaped = fieldScaped;
    }

    public String getLineTerminated() {
        return lineTerminated;
    }

    public void setLineTerminated(String lineTerminated) {
        this.lineTerminated = lineTerminated;
    }

    public int getLineIgnore() {
        return lineIgnore;
    }

    public void setLineIgnore(int lineIgnore) {
        this.lineIgnore = lineIgnore;
    }

    public CnetlFormato getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(CnetlFormato idFormato) {
        this.idFormato = idFormato;
    }

    public Collection<CnetlMaster> getCnetlMasterCollection() {
        return cnetlMasterCollection;
    }

    public void setCnetlMasterCollection(Collection<CnetlMaster> cnetlMasterCollection) {
        this.cnetlMasterCollection = cnetlMasterCollection;
    }

    public Collection<CnetlMasterTipoOrigen> getCnetlMasterTipoOrigenCollection() {
        return cnetlMasterTipoOrigenCollection;
    }

    public void setCnetlMasterTipoOrigenCollection(Collection<CnetlMasterTipoOrigen> cnetlMasterTipoOrigenCollection) {
        this.cnetlMasterTipoOrigenCollection = cnetlMasterTipoOrigenCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMasterTipo != null ? idMasterTipo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlMasterTipo)) {
            return false;
        }
        CnetlMasterTipo other = (CnetlMasterTipo) object;
        if ((this.idMasterTipo == null && other.idMasterTipo != null) || (this.idMasterTipo != null && !this.idMasterTipo.equals(other.idMasterTipo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre;
    }

}
