/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Embeddable
public class CncrmUsuarioPlataformaDigitalPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdUsuario")
    private long idUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdPlataformaDigital")
    private short idPlataformaDigital;

    /**
     *
     */
    public CncrmUsuarioPlataformaDigitalPK() {
    }

    /**
     *
     * @param idUsuario
     * @param idPlataformaDigital
     */
    public CncrmUsuarioPlataformaDigitalPK(long idUsuario, short idPlataformaDigital) {
        this.idUsuario = idUsuario;
        this.idPlataformaDigital = idPlataformaDigital;
    }

    /**
     *
     * @return
     */
    public long getIdUsuario() {
        return idUsuario;
    }

    /**
     *
     * @param idUsuario
     */
    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     *
     * @return
     */
    public short getIdPlataformaDigital() {
        return idPlataformaDigital;
    }

    /**
     *
     * @param idPlataformaDigital
     */
    public void setIdPlataformaDigital(short idPlataformaDigital) {
        this.idPlataformaDigital = idPlataformaDigital;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) this.idUsuario;
        hash += this.idPlataformaDigital;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CncrmUsuarioPlataformaDigitalPK)) {
            return false;
        }
        final CncrmUsuarioPlataformaDigitalPK other = (CncrmUsuarioPlataformaDigitalPK) object;
        if (this.idUsuario != other.idUsuario) {
            return false;
        }
        if (this.idPlataformaDigital != other.idPlataformaDigital) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioPlataformaDigitalPK[ idUsuario=" + idUsuario + ", idPlataformaDigital=" + idPlataformaDigital + " ]";
    } 
    
}
