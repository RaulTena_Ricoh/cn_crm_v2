/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnetl_pool_log_validacion_detalle")
@NamedQueries({
    @NamedQuery(name = "CnetlPoolLogValidacionDetalle.findAll", query = "SELECT c FROM CnetlPoolLogValidacionDetalle c"),
    @NamedQuery(name = "CnetlPoolLogValidacionDetalle.findByIdPool", query = "SELECT c FROM CnetlPoolLogValidacionDetalle c WHERE c.cnetlPoolLogValidacionDetallePK.idPool = :idPool"),
    @NamedQuery(name = "CnetlPoolLogValidacionDetalle.findByIdValidacion", query = "SELECT c FROM CnetlPoolLogValidacionDetalle c WHERE c.cnetlPoolLogValidacionDetallePK.idValidacion = :idValidacion"),
    @NamedQuery(name = "CnetlPoolLogValidacionDetalle.findByLinea", query = "SELECT c FROM CnetlPoolLogValidacionDetalle c WHERE c.cnetlPoolLogValidacionDetallePK.linea = :linea"),
    @NamedQuery(name = "CnetlPoolLogValidacionDetalle.findByValor", query = "SELECT c FROM CnetlPoolLogValidacionDetalle c WHERE c.valor = :valor")})
public class CnetlPoolLogValidacionDetalle implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CnetlPoolLogValidacionDetallePK cnetlPoolLogValidacionDetallePK;
    @Size(max = 200)
    @Column(name = "Valor")
    private String valor;

    @JoinColumn(name = "IdPool", referencedColumnName = "IdPool", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnetlPoolLog cnetlPoolLog;
    
    public CnetlPoolLogValidacionDetalle() {
    }

    public CnetlPoolLogValidacionDetalle(CnetlPoolLogValidacionDetallePK cnetlPoolLogValidacionDetallePK) {
        this.cnetlPoolLogValidacionDetallePK = cnetlPoolLogValidacionDetallePK;
    }

    public CnetlPoolLogValidacionDetalle(long idPool, int idValidacion, int linea) {
        this.cnetlPoolLogValidacionDetallePK = new CnetlPoolLogValidacionDetallePK(idPool, idValidacion, linea);
    }

    public CnetlPoolLogValidacionDetallePK getCnetlPoolLogValidacionDetallePK() {
        return cnetlPoolLogValidacionDetallePK;
    }

    public void setCnetlPoolLogValidacionDetallePK(CnetlPoolLogValidacionDetallePK cnetlPoolLogValidacionDetallePK) {
        this.cnetlPoolLogValidacionDetallePK = cnetlPoolLogValidacionDetallePK;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public CnetlPoolLog getCnetlPoolLog() {
        return cnetlPoolLog;
    }

    public void setCnetlPoolLog(CnetlPoolLog cnetlPoolLog) {
        this.cnetlPoolLog = cnetlPoolLog;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cnetlPoolLogValidacionDetallePK != null ? cnetlPoolLogValidacionDetallePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlPoolLogValidacionDetalle)) {
            return false;
        }
        CnetlPoolLogValidacionDetalle other = (CnetlPoolLogValidacionDetalle) object;
        if ((this.cnetlPoolLogValidacionDetallePK == null && other.cnetlPoolLogValidacionDetallePK != null) || (this.cnetlPoolLogValidacionDetallePK != null && !this.cnetlPoolLogValidacionDetallePK.equals(other.cnetlPoolLogValidacionDetallePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnetlPoolLogValidacionDetalle[ cnetlPoolLogValidacionDetallePK=" + cnetlPoolLogValidacionDetallePK + " ]";
    }
    
}
