/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Embeddable
public class CncrmUsuarioMarketingAccionPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdUsuario")
    private long idUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdMarketingAccion")
    private short idMarketingAccion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdMarca")
    private short idMarca;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdProducto")
    private short idProducto;

    public CncrmUsuarioMarketingAccionPK() {
    }

    public CncrmUsuarioMarketingAccionPK(long idUsuario, short idMarketingAccion, short idMarca, short idProducto) {
        this.idUsuario = idUsuario;
        this.idMarketingAccion = idMarketingAccion;
        this.idMarca = idMarca;
        this.idProducto = idProducto;
    }

    /**
     *
     * @return
     */
    public long getIdUsuario() {
        return idUsuario;
    }

    /**
     *
     * @param idUsuario
     */
    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     *
     * @return
     */
    public short getIdMarketingAccion() {
        return idMarketingAccion;
    }

    /**
     *
     * @param idMarketingAccion
     */
    public void setIdMarketingAccion(short idMarketingAccion) {
        this.idMarketingAccion = idMarketingAccion;
    }

    /**
     *
     * @return
     */
    public short getIdMarca() {
        return idMarca;
    }

    /**
     *
     * @param idMarca
     */
    public void setIdMarca(short idMarca) {
        this.idMarca = idMarca;
    }

    /**
     *
     * @return
     */
    public short getIdProducto() {
        return idProducto;
    }

    /**
     *
     * @param idProducto
     */
    public void setIdProducto(short idProducto) {
        this.idProducto = idProducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idUsuario;
        hash += idMarketingAccion;
        hash += idMarca;
        hash += idProducto;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CncrmUsuarioMarketingAccionPK)) {
            return false;
        }
        CncrmUsuarioMarketingAccionPK other = (CncrmUsuarioMarketingAccionPK) object;
        if (this.idUsuario != other.idUsuario) {
            return false;
        }
        if (this.idMarketingAccion != other.idMarketingAccion) {
            return false;
        }
        if (this.idMarca != other.idMarca) {
            return false;
        }
        if (this.idProducto != other.idProducto) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioMarketingAccionPK[ idUsuario=" + idUsuario + 
                ", idMarketingAccion=" + idMarketingAccion +
                ", idMarca=" + idMarca +
                ", idProducto=" + idProducto + " ]";
    }
    
}
