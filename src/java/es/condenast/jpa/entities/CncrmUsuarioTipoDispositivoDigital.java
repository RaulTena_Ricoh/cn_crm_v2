/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cncrm_usuario_tipo_dispositivo_digital")
@NamedQueries({
    @NamedQuery(name = "CncrmUsuarioTipoDispositivoDigital.findAll", query = "SELECT c FROM CncrmUsuarioTipoDispositivoDigital c"),
    @NamedQuery(name = "CncrmUsuarioTipoDispositivoDigital.findByIdUsuario", query = "SELECT c FROM CncrmUsuarioTipoDispositivoDigital c WHERE c.cncrmUsuarioTipoDispositivoDigitalPK.idUsuario = :idUsuario"),
    @NamedQuery(name = "CncrmUsuarioTipoDispositivoDigital.findByIdTerminal", query = "SELECT c FROM CncrmUsuarioTipoDispositivoDigital c WHERE c.cncrmUsuarioTipoDispositivoDigitalPK.idTerminal = :idTerminal"),
    @NamedQuery(name = "CncrmUsuarioTipoDispositivoDigital.findByIdSistemaOperativo", query = "SELECT c FROM CncrmUsuarioTipoDispositivoDigital c WHERE c.cncrmUsuarioTipoDispositivoDigitalPK.idSistemaOperativo = :idSistemaOperativo")})
public class CncrmUsuarioTipoDispositivoDigital implements Serializable {
    
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @EmbeddedId
    protected CncrmUsuarioTipoDispositivoDigitalPK cncrmUsuarioTipoDispositivoDigitalPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    
    @JoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CncrmUsuario cncrmUsuario;
    @JoinColumn(name = "IdTerminal", referencedColumnName = "IdTerminal", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasTipoTerminal cnmasTipoTerminal;
    @JoinColumn(name = "IdSistemaOperativo", referencedColumnName = "IdSistemaOperativo", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasSistemaOperativo cnmasSistemaOperativo;    
    
    /**
     *
     */
    public CncrmUsuarioTipoDispositivoDigital() {
    }

    /**
     *
     * @param cncrmUsuarioTipoDispositivoDigitalPK
     */
    public CncrmUsuarioTipoDispositivoDigital(CncrmUsuarioTipoDispositivoDigitalPK cncrmUsuarioTipoDispositivoDigitalPK) {
        this.cncrmUsuarioTipoDispositivoDigitalPK = cncrmUsuarioTipoDispositivoDigitalPK;
    }
    
    /**
     *
     * @param idUsuario
     * @param idTerminal
     * @param idSistemaOperativo
     */
    public CncrmUsuarioTipoDispositivoDigital(long idUsuario, short idTerminal, short idSistemaOperativo) {
        this.cncrmUsuarioTipoDispositivoDigitalPK = new CncrmUsuarioTipoDispositivoDigitalPK(idUsuario, idTerminal, idSistemaOperativo);
    }

    /**
     *
     * @return
     */
    public CncrmUsuarioTipoDispositivoDigitalPK getCncrmUsuarioTipoDispositivoDigitalPK() {
        return cncrmUsuarioTipoDispositivoDigitalPK;
    }

    /**
     *
     * @param cncrmUsuarioTipoDispositivoDigitalPK
     */
    public void setCncrmUsuarioTipoDispositivoDigitalPK(CncrmUsuarioTipoDispositivoDigitalPK cncrmUsuarioTipoDispositivoDigitalPK) {
        this.cncrmUsuarioTipoDispositivoDigitalPK = cncrmUsuarioTipoDispositivoDigitalPK;
    }

    /**
     *
     * @return
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     *
     * @param fecha
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     *
     * @return
     */
    public CncrmUsuario getCncrmUsuario() {
        return cncrmUsuario;
    }

    /**
     *
     * @param cncrmUsuario
     */
    public void setCncrmUsuario(CncrmUsuario cncrmUsuario) {
        this.cncrmUsuario = cncrmUsuario;
    }

    /**
     *
     * @return
     */
    public CnmasTipoTerminal getCnmasTipoTerminal() {
        return cnmasTipoTerminal;
    }

    /**
     *
     * @param cnmasTerminal
     */
    public void setCnmasTipoTerminal(CnmasTipoTerminal cnmasTipoTerminal) {
        this.cnmasTipoTerminal = cnmasTipoTerminal;
    }

    /**
     *
     * @return
     */
    public CnmasSistemaOperativo getCnmasSistemaOperativo() {
        return cnmasSistemaOperativo;
    }

    /**
     *
     * @param cnmasSistemaOperativo
     */
    public void setCnmasSistemaOperativo(CnmasSistemaOperativo cnmasSistemaOperativo) {
        this.cnmasSistemaOperativo = cnmasSistemaOperativo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += this.cncrmUsuarioTipoDispositivoDigitalPK.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CncrmUsuarioTipoDispositivoDigital)) {
            return false;
        }
        CncrmUsuarioTipoDispositivoDigital other = (CncrmUsuarioTipoDispositivoDigital) object;
        if ((this.cncrmUsuarioTipoDispositivoDigitalPK == null && other.cncrmUsuarioTipoDispositivoDigitalPK != null) 
                || (this.cncrmUsuarioTipoDispositivoDigitalPK != null && !this.cncrmUsuarioTipoDispositivoDigitalPK.equals(other.cncrmUsuarioTipoDispositivoDigitalPK))) {
            return false;
        }
        return true;
    }
    
    
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioTipoDispositivoDigital[ cncrmUsuarioTipoDispositivoDigitalPK=" + cncrmUsuarioTipoDispositivoDigitalPK + " ]";
    }

    
    
}
