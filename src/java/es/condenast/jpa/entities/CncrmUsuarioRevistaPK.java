/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Embeddable
public class CncrmUsuarioRevistaPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdUsuario")
    private long idUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdRevista")
    private Short idRevista;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdMarca")
    private Short idMarca;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdFrecuenciaAno")
    private Short idFrecuenciaAno;

    public CncrmUsuarioRevistaPK() {
    }

    public CncrmUsuarioRevistaPK(long idUsuario, Short idRevista, Short idMarca, Short idFrecuenciaAno) {
        this.idUsuario = idUsuario;
        this.idRevista = idRevista;
        this.idMarca = idMarca;
        this.idFrecuenciaAno = idFrecuenciaAno;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Short getIdRevista() {
        return idRevista;
    }

    public void setIdRevista(Short idRevista) {
        this.idRevista = idRevista;
    }

    public Short getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(Short idMarca) {
        this.idMarca = idMarca;
    }

    public Short getIdFrecuenciaAno() {
        return idFrecuenciaAno;
    }

    public void setIdFrecuenciaAno(Short idFrecuenciaAno) {
        this.idFrecuenciaAno = idFrecuenciaAno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) this.idUsuario;
        hash +=  this.idRevista;
        hash +=  this.idMarca;
        hash += this.idFrecuenciaAno;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof CncrmUsuarioRevistaPK) {
            return false;
        }
        CncrmUsuarioRevistaPK other = (CncrmUsuarioRevistaPK) object;
        if (this.idUsuario != other.idUsuario) {
            return false;
        }
        if (this.idRevista != other.idRevista) {
            return false;
        }
        if (this.idMarca != other.idMarca) {
            return false;
        }
        if (this.idFrecuenciaAno != other.idFrecuenciaAno) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioRevistaPK[ idUsuario=" + idUsuario
                + "idRevista=" + idRevista
                + "idMarca=" + idMarca
                + "idFrecuenciaAno=" + idFrecuenciaAno + " ]";
    }
    
}
