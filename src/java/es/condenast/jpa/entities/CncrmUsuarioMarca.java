/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cncrm_usuario_marca")
@NamedQueries({
    @NamedQuery(name = "CncrmUsuarioMarca.findAll", query = "SELECT c FROM CncrmUsuarioMarca c"),
    @NamedQuery(name = "CncrmUsuarioMarca.findByIdUsuario", query = "SELECT c FROM CncrmUsuarioMarca c WHERE c.cncrmUsuarioMarcaPK.idUsuario = :idUsuario"),
    @NamedQuery(name = "CncrmUsuarioMarca.findByIdMarca", query = "SELECT c FROM CncrmUsuarioMarca c WHERE c.cncrmUsuarioMarcaPK.idMarca = :idMarca"),
    @NamedQuery(name = "CncrmUsuarioMarca.findByEsOrigen", query = "SELECT c FROM CncrmUsuarioMarca c WHERE c.esOrigen = :esOrigen"),
    @NamedQuery(name = "CncrmUsuarioMarca.findByEsPrimordial", query = "SELECT c FROM CncrmUsuarioMarca c WHERE c.esPrimordial = :esPrimordial"),
    @NamedQuery(name = "CncrmUsuarioMarca.findByFechaAltaPrimera", query = "SELECT c FROM CncrmUsuarioMarca c WHERE c.fechaAltaPrimera = :fechaAltaPrimera"),
    @NamedQuery(name = "CncrmUsuarioMarca.findByFechaAlta", query = "SELECT c FROM CncrmUsuarioMarca c WHERE c.fechaAlta = :fechaAlta"),
    @NamedQuery(name = "CncrmUsuarioMarca.findByFechaBaja", query = "SELECT c FROM CncrmUsuarioMarca c WHERE c.fechaBaja = :fechaBaja"),
    @NamedQuery(name = "CncrmUsuarioMarca.findByFechaCambio", query = "SELECT c FROM CncrmUsuarioMarca c WHERE c.fechaCambio = :fechaCambio"),
    @NamedQuery(name = "CncrmUsuarioMarca.findByPuntuacion", query = "SELECT c FROM CncrmUsuarioMarca c WHERE c.puntuacion = :puntuacion")})
public class CncrmUsuarioMarca implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CncrmUsuarioMarcaPK cncrmUsuarioMarcaPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EsOrigen")
    private boolean esOrigen;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EsPrimordial")
    private boolean esPrimordial;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaAltaPrimera")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAltaPrimera;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaAlta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAlta;
    @Column(name = "FechaBaja")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaBaja;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaCambio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCambio;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "Puntuacion")
    private BigDecimal puntuacion;
    @JoinColumn(name = "IdMarca", referencedColumnName = "IdMarca", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasMarca cnmasMarca;
    @JoinColumn(name = "IdUsuarioOrigen", referencedColumnName = "IdUsuarioOrigen")
    @ManyToOne(optional = false)
    private CnmasUsuarioOrigen idUsuarioOrigen;
    @JoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CncrmUsuario cncrmUsuario;
    @JoinColumn(name = "IdUsuarioTipo", referencedColumnName = "IdUsuarioTipo")
    @ManyToOne(optional = false)
    private CnmasUsuarioTipo idUsuarioTipo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cncrmUsuarioMarca")
    private Collection<CncrmUsuarioMarcaCondicionLegal> cncrmUsuarioMarcaCondicionLegalCollection;

    public CncrmUsuarioMarca() {
    }

    public CncrmUsuarioMarca(CncrmUsuarioMarcaPK cncrmUsuarioMarcaPK) {
        this.cncrmUsuarioMarcaPK = cncrmUsuarioMarcaPK;
    }

    public CncrmUsuarioMarca(CncrmUsuarioMarcaPK cncrmUsuarioMarcaPK, boolean esOrigen, boolean esPrimordial, Date fechaAltaPrimera, Date fechaAlta, Date fechaCambio, BigDecimal puntuacion) {
        this.cncrmUsuarioMarcaPK = cncrmUsuarioMarcaPK;
        this.esOrigen = esOrigen;
        this.esPrimordial = esPrimordial;
        this.fechaAltaPrimera = fechaAltaPrimera;
        this.fechaAlta = fechaAlta;
        this.fechaCambio = fechaCambio;
        this.puntuacion = puntuacion;
    }

    public CncrmUsuarioMarca(long idUsuario, short idMarca) {
        this.cncrmUsuarioMarcaPK = new CncrmUsuarioMarcaPK(idUsuario, idMarca);
    }

    public CncrmUsuarioMarcaPK getCncrmUsuarioMarcaPK() {
        return cncrmUsuarioMarcaPK;
    }

    public void setCncrmUsuarioMarcaPK(CncrmUsuarioMarcaPK cncrmUsuarioMarcaPK) {
        this.cncrmUsuarioMarcaPK = cncrmUsuarioMarcaPK;
    }

    public boolean getEsOrigen() {
        return esOrigen;
    }

    public void setEsOrigen(boolean esOrigen) {
        this.esOrigen = esOrigen;
    }

    public boolean getEsPrimordial() {
        return esPrimordial;
    }

    public void setEsPrimordial(boolean esPrimordial) {
        this.esPrimordial = esPrimordial;
    }

    public Date getFechaAltaPrimera() {
        return fechaAltaPrimera;
    }

    public void setFechaAltaPrimera(Date fechaAltaPrimera) {
        this.fechaAltaPrimera = fechaAltaPrimera;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public Date getFechaCambio() {
        return fechaCambio;
    }

    public void setFechaCambio(Date fechaCambio) {
        this.fechaCambio = fechaCambio;
    }

    public BigDecimal getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(BigDecimal puntuacion) {
        this.puntuacion = puntuacion;
    }

    public CnmasMarca getCnmasMarca() {
        return cnmasMarca;
    }

    public void setCnmasMarca(CnmasMarca cnmasMarca) {
        this.cnmasMarca = cnmasMarca;
    }

    public CnmasUsuarioOrigen getIdUsuarioOrigen() {
        return idUsuarioOrigen;
    }

    public void setIdUsuarioOrigen(CnmasUsuarioOrigen idUsuarioOrigen) {
        this.idUsuarioOrigen = idUsuarioOrigen;
    }

    public CncrmUsuario getCncrmUsuario() {
        return cncrmUsuario;
    }

    public void setCncrmUsuario(CncrmUsuario cncrmUsuario) {
        this.cncrmUsuario = cncrmUsuario;
    }

    public CnmasUsuarioTipo getIdUsuarioTipo() {
        return idUsuarioTipo;
    }

    public void setIdUsuarioTipo(CnmasUsuarioTipo idUsuarioTipo) {
        this.idUsuarioTipo = idUsuarioTipo;
    }

    public Collection<CncrmUsuarioMarcaCondicionLegal> getCncrmUsuarioMarcaCondicionLegalCollection() {
        return cncrmUsuarioMarcaCondicionLegalCollection;
    }

    public void setCncrmUsuarioMarcaCondicionLegalCollection(Collection<CncrmUsuarioMarcaCondicionLegal> cncrmUsuarioMarcaCondicionLegalCollection) {
        this.cncrmUsuarioMarcaCondicionLegalCollection = cncrmUsuarioMarcaCondicionLegalCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cncrmUsuarioMarcaPK != null ? cncrmUsuarioMarcaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CncrmUsuarioMarca)) {
            return false;
        }
        CncrmUsuarioMarca other = (CncrmUsuarioMarca) object;
        if ((this.cncrmUsuarioMarcaPK == null && other.cncrmUsuarioMarcaPK != null) || (this.cncrmUsuarioMarcaPK != null && !this.cncrmUsuarioMarcaPK.equals(other.cncrmUsuarioMarcaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioMarca[ cncrmUsuarioMarcaPK=" + cncrmUsuarioMarcaPK + " ]";
    }
    
}
