/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cnetl_master_tipo_origen")
@NamedQueries({
    @NamedQuery(name = "CnetlMasterTipoOrigen.findAll", query = "SELECT c FROM CnetlMasterTipoOrigen c"),
    @NamedQuery(name = "CnetlMasterTipoOrigen.findByIdMasterTipo", query = "SELECT c FROM CnetlMasterTipoOrigen c WHERE c.cnetlMasterTipoOrigenPK.idMasterTipo = :idMasterTipo ORDER BY c.orden ASC"),
    @NamedQuery(name = "CnetlMasterTipoOrigen.findByIdOrigen", query = "SELECT c FROM CnetlMasterTipoOrigen c WHERE c.cnetlMasterTipoOrigenPK.idOrigen = :idOrigen"),
    @NamedQuery(name = "CnetlMasterTipoOrigen.findByOrden", query = "SELECT c FROM CnetlMasterTipoOrigen c WHERE c.orden = :orden"),
    @NamedQuery(name = "CnetlMasterTipoOrigen.removeByIdMasterTipo", query = "DELETE FROM CnetlMasterTipoOrigen c WHERE c.cnetlMasterTipoOrigenPK.idMasterTipo = :idMasterTipo")})
public class CnetlMasterTipoOrigen implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CnetlMasterTipoOrigenPK cnetlMasterTipoOrigenPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Orden")
    private short orden;
    @JoinColumn(name = "IdOrigen", referencedColumnName = "IdOrigen", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnetlOrigen cnetlOrigen;
    @JoinColumn(name = "IdMasterTipo", referencedColumnName = "IdMasterTipo", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnetlMasterTipo cnetlMasterTipo;

    public CnetlMasterTipoOrigen() {
    }

    public CnetlMasterTipoOrigen(CnetlMasterTipoOrigenPK cnetlMasterTipoOrigenPK) {
        this.cnetlMasterTipoOrigenPK = cnetlMasterTipoOrigenPK;
    }

    public CnetlMasterTipoOrigen(CnetlMasterTipoOrigenPK cnetlMasterTipoOrigenPK, short orden) {
        this.cnetlMasterTipoOrigenPK = cnetlMasterTipoOrigenPK;
        this.orden = orden;
    }

    public CnetlMasterTipoOrigen(short idMasterTipo, int idOrigen) {
        this.cnetlMasterTipoOrigenPK = new CnetlMasterTipoOrigenPK(idMasterTipo, idOrigen);
    }

    public CnetlMasterTipoOrigenPK getCnetlMasterTipoOrigenPK() {
        return cnetlMasterTipoOrigenPK;
    }

    public void setCnetlMasterTipoOrigenPK(CnetlMasterTipoOrigenPK cnetlMasterTipoOrigenPK) {
        this.cnetlMasterTipoOrigenPK = cnetlMasterTipoOrigenPK;
    }

    public short getOrden() {
        return orden;
    }

    public void setOrden(short orden) {
        this.orden = orden;
    }

    public CnetlOrigen getCnetlOrigen() {
        return cnetlOrigen;
    }

    public void setCnetlOrigen(CnetlOrigen cnetlOrigen) {
        this.cnetlOrigen = cnetlOrigen;
    }

    public CnetlMasterTipo getCnetlMasterTipo() {
        return cnetlMasterTipo;
    }

    public void setCnetlMasterTipo(CnetlMasterTipo cnetlMasterTipo) {
        this.cnetlMasterTipo = cnetlMasterTipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cnetlMasterTipoOrigenPK != null ? cnetlMasterTipoOrigenPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlMasterTipoOrigen)) {
            return false;
        }
        CnetlMasterTipoOrigen other = (CnetlMasterTipoOrigen) object;
        if ((this.cnetlMasterTipoOrigenPK == null && other.cnetlMasterTipoOrigenPK != null) || (this.cnetlMasterTipoOrigenPK != null && !this.cnetlMasterTipoOrigenPK.equals(other.cnetlMasterTipoOrigenPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnetlMasterTipoOrigen[ cnetlMasterTipoOrigenPK=" + cnetlMasterTipoOrigenPK + " ]";
    }
    
}
