/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnmas_usuario_origen")
@NamedQueries({
    @NamedQuery(name = "CnmasUsuarioOrigen.findAll", query = "SELECT c FROM CnmasUsuarioOrigen c"),
    @NamedQuery(name = "CnmasUsuarioOrigen.findByIdUsuarioOrigen", query = "SELECT c FROM CnmasUsuarioOrigen c WHERE c.idUsuarioOrigen = :idUsuarioOrigen"),
    @NamedQuery(name = "CnmasUsuarioOrigen.findByNombre", query = "SELECT c FROM CnmasUsuarioOrigen c WHERE c.nombre = :nombre")})
public class CnmasUsuarioOrigen implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUsuarioOrigen")
    private Collection<CncrmUsuarioMarca> cncrmUsuarioMarcaCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdUsuarioOrigen")
    private Short idUsuarioOrigen;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @ManyToMany(mappedBy = "cnmasUsuarioOrigenCollection")
    private Collection<CnmasProducto> cnmasProductoCollection;

    public CnmasUsuarioOrigen() {
    }

    public CnmasUsuarioOrigen(Short idUsuarioOrigen) {
        this.idUsuarioOrigen = idUsuarioOrigen;
    }

    public CnmasUsuarioOrigen(Short idUsuarioOrigen, String nombre) {
        this.idUsuarioOrigen = idUsuarioOrigen;
        this.nombre = nombre;
    }

    public Short getIdUsuarioOrigen() {
        return idUsuarioOrigen;
    }

    public void setIdUsuarioOrigen(Short idUsuarioOrigen) {
        this.idUsuarioOrigen = idUsuarioOrigen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuarioOrigen != null ? idUsuarioOrigen.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasUsuarioOrigen)) {
            return false;
        }
        CnmasUsuarioOrigen other = (CnmasUsuarioOrigen) object;
        if ((this.idUsuarioOrigen == null && other.idUsuarioOrigen != null) || (this.idUsuarioOrigen != null && !this.idUsuarioOrigen.equals(other.idUsuarioOrigen))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre;
    }

    public Collection<CnmasProducto> getCnmasProductoCollection() {
        return cnmasProductoCollection;
    }

    public void setCnmasProductoCollection(Collection<CnmasProducto> cnmasProductoCollection) {
        this.cnmasProductoCollection = cnmasProductoCollection;
    }

    public Collection<CncrmUsuarioMarca> getCncrmUsuarioMarcaCollection() {
        return cncrmUsuarioMarcaCollection;
    }

    public void setCncrmUsuarioMarcaCollection(Collection<CncrmUsuarioMarca> cncrmUsuarioMarcaCollection) {
        this.cncrmUsuarioMarcaCollection = cncrmUsuarioMarcaCollection;
    }
    
}
