/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Embeddable
public class CnetlMasterValorPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdMaster")
    private int idMaster;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdOrigen")
    private int idOrigen;

    public CnetlMasterValorPK() {
    }

    public CnetlMasterValorPK(int idMaster, int idOrigen) {
        this.idMaster = idMaster;
        this.idOrigen = idOrigen;
    }

    public int getIdMaster() {
        return idMaster;
    }

    public void setIdMaster(int idMaster) {
        this.idMaster = idMaster;
    }

    public int getIdOrigen() {
        return idOrigen;
    }

    public void setIdOrigen(int idOrigen) {
        this.idOrigen = idOrigen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idMaster;
        hash += (int) idOrigen;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlMasterValorPK)) {
            return false;
        }
        CnetlMasterValorPK other = (CnetlMasterValorPK) object;
        if (this.idMaster != other.idMaster) {
            return false;
        }
        if (this.idOrigen != other.idOrigen) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnetlMasterValorPK[ idMaster=" + idMaster + ", idOrigen=" + idOrigen + " ]";
    }
    
}
