/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cncrm_usuario_red_social")
@NamedQueries({
    @NamedQuery(name = "CncrmUsuarioRedSocial.findAll", query = "SELECT c FROM CncrmUsuarioRedSocial c"),
    @NamedQuery(name = "CncrmUsuarioRedSocial.findByIdUsuario", query = "SELECT c FROM CncrmUsuarioRedSocial c WHERE c.cncrmUsuarioRedSocialPK.idUsuario = :idUsuario"),
    @NamedQuery(name = "CncrmUsuarioRedSocial.findByIdRedSocial", query = "SELECT c FROM CncrmUsuarioRedSocial c WHERE c.cncrmUsuarioRedSocialPK.idRedSocial = :idRedSocial"),
    @NamedQuery(name = "CncrmUsuarioRedSocial.findByIdMarca", query = "SELECT c FROM CncrmUsuarioRedSocial c WHERE c.cncrmUsuarioRedSocialPK.idMarca = :idMarca")})
public class CncrmUsuarioRedSocial implements Serializable {
    private static final long serialVersionUID = 1L;
    
    /**
     *
     */
    @EmbeddedId
    protected CncrmUsuarioRedSocialPK cncrmUsuarioRedSocialPK;
    
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    
    @JoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CncrmUsuario cncrmUsuario;
    @JoinColumn(name = "IdRedSocial", referencedColumnName = "IdRedSocial", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasRedSocial cnmasRedSocial;
    @JoinColumn(name = "IdMarca", referencedColumnName = "IdMarca", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasMarca cnmasMarca;

    /**
     *
     */
    public CncrmUsuarioRedSocial() {
    }

    /**
     *
     * @param cncrmUsuarioRedSocialPK
     */
    public CncrmUsuarioRedSocial(CncrmUsuarioRedSocialPK cncrmUsuarioRedSocialPK) {
        this.cncrmUsuarioRedSocialPK = cncrmUsuarioRedSocialPK;
    }
    
    /**
     *
     * @param idUsuario
     * @param idRedSocial
     * @param idMarca
     */
    public CncrmUsuarioRedSocial(long idUsuario, short idRedSocial, short idMarca) {
        this.cncrmUsuarioRedSocialPK = new CncrmUsuarioRedSocialPK(idUsuario, idRedSocial, idMarca);
    }

    /**
     *
     * @return
     */
    public CncrmUsuarioRedSocialPK getCncrmUsuarioRedSocialPK() {
        return cncrmUsuarioRedSocialPK;
    }

    /**
     *
     * @param cncrmUsuarioRedSocialPK
     */
    public void setCncrmUsuarioRedSocialPK(CncrmUsuarioRedSocialPK cncrmUsuarioRedSocialPK) {
        this.cncrmUsuarioRedSocialPK = cncrmUsuarioRedSocialPK;
    }

    /**
     *
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     *
     * @param fecha
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     *
     * @return
     */
    public CncrmUsuario getCncrmUsuario() {
        return cncrmUsuario;
    }

    /**
     *
     * @param cncrmUsuario
     */
    public void setCncrmUsuario(CncrmUsuario cncrmUsuario) {
        this.cncrmUsuario = cncrmUsuario;
    }

    /**
     *
     * @return
     */
    public CnmasRedSocial getCnmasRedSocial() {
        return cnmasRedSocial;
    }

    /**
     *
     * @param cnmasRedSocial
     */
    public void setCnmasRedSocial(CnmasRedSocial cnmasRedSocial) {
        this.cnmasRedSocial = cnmasRedSocial;
    }

    /**
     *
     * @return
     */
    public CnmasMarca getCnmasMarca() {
        return cnmasMarca;
    }

    /**
     *
     * @param cnmasMarca
     */
    public void setCnmasMarca(CnmasMarca cnmasMarca) {
        this.cnmasMarca = cnmasMarca;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += this.cncrmUsuarioRedSocialPK.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof CncrmUsuarioRedSocial) {
            return false;
        }
        CncrmUsuarioRedSocial other = (CncrmUsuarioRedSocial) object;
        if ((this.cncrmUsuarioRedSocialPK == null && other.cncrmUsuarioRedSocialPK != null) || (this.cncrmUsuarioRedSocialPK != null && !this.cncrmUsuarioRedSocialPK.equals(other.cncrmUsuarioRedSocialPK))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioRedSocial[ cncrmUsuarioRedSocialPK=" + cncrmUsuarioRedSocialPK + " ]";
    }
}
