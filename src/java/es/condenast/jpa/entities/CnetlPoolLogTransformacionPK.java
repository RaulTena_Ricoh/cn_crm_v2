/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Javier.Castro
 */
@Embeddable
public class CnetlPoolLogTransformacionPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdPool")
    private long idPool;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdTransformacion")
    private int idTransformacion;

    public CnetlPoolLogTransformacionPK() {
    }

    public CnetlPoolLogTransformacionPK(long idPool, int idTransformacion) {
        this.idPool = idPool;
        this.idTransformacion = idTransformacion;
    }

    public long getIdPool() {
        return idPool;
    }

    public void setIdPool(long idPool) {
        this.idPool = idPool;
    }

    public int getIdTransformacion() {
        return idTransformacion;
    }

    public void setIdTransformacion(int idTransformacion) {
        this.idTransformacion = idTransformacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idPool;
        hash += (int) idTransformacion;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlPoolLogTransformacionPK)) {
            return false;
        }
        CnetlPoolLogTransformacionPK other = (CnetlPoolLogTransformacionPK) object;
        if (this.idPool != other.idPool) {
            return false;
        }
        if (this.idTransformacion != other.idTransformacion) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnetlPoolLogTransformacionPK[ idPool=" + idPool + ", idTransformacion=" + idTransformacion + " ]";
    }
    
}
