/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cnmas_marketing_accion")
@NamedQueries({
    @NamedQuery(name = "CnmasMarketingAccion.findAll", query = "SELECT c FROM CnmasMarketingAccion c"),
    @NamedQuery(name = "CnmasMarketingAccion.findByIdCnmasMarketingAccion", query = "SELECT c FROM CnmasMarketingAccion c WHERE c.idMarketingAccion = :idMarketingAccion"),
    @NamedQuery(name = "CnmasMarketingAccion.findByNombre", query = "SELECT c FROM CnmasMarketingAccion c WHERE c.nombre = :nombre")})
public class CnmasMarketingAccion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdMarketingAccion")
    private Short idMarketingAccion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasMarketingAccion")
    private Collection<CncrmUsuarioMarketingAccion> cncrmUsuarioMarketingAccionCollection;

    public CnmasMarketingAccion() {
    }

    public CnmasMarketingAccion(Short idMarketingAccion) {
        this.idMarketingAccion = idMarketingAccion;
    }

    public CnmasMarketingAccion(Short idMarketingAccion, String nombre) {
        this.idMarketingAccion = idMarketingAccion;
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public Short getIdMarketingAccion() {
        return idMarketingAccion;
    }

    /**
     *
     * @param idMarketingAccion
     */
    public void setIdMarketingAccion(Short idMarketingAccion) {
        this.idMarketingAccion = idMarketingAccion;
    }

    /**
     *
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public Collection<CncrmUsuarioMarketingAccion> getCncrmUsuarioMarketingAccionCollection() {
        return cncrmUsuarioMarketingAccionCollection;
    }

    /**
     *
     * @param cncrmUsuarioMarketingAccionCollection
     */
    public void setCncrmUsuarioMarketingAccionCollection(Collection<CncrmUsuarioMarketingAccion> cncrmUsuarioMarketingAccionCollection) {
        this.cncrmUsuarioMarketingAccionCollection = cncrmUsuarioMarketingAccionCollection;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMarketingAccion != null ? idMarketingAccion : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if(!(object instanceof CnmasMarketingAccion)) {
            return false;
        }
        CnmasMarketingAccion other = (CnmasMarketingAccion) object;
        if((this.idMarketingAccion == null && other.idMarketingAccion != null) || (this.idMarketingAccion != null && !this.idMarketingAccion.equals(other.idMarketingAccion))) {
            return false;
        }
        return true;
    }
 
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasMarketingAccion[ idMarketingAccion=" + idMarketingAccion + " ]";
    }
    
}
