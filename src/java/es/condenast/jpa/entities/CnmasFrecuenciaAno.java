/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ivan.fernandez
 */
@Entity
@Table(name="cnmas_frecuencia_ano")
@NamedQueries({
    @NamedQuery(name = "CnmasFrecuenciaAno.findAll", query = "SELECT c FROM CnmasFrecuenciaAno c ORDER BY c.idFrecuenciaAno"),
    @NamedQuery(name = "CnmasFrecuenciaAno.findByIdFrecuenciaAno", query = "SELECT c FROM CnmasFrecuenciaAno c WHERE c.idFrecuenciaAno = :idFrecuenciaAno")})
public class CnmasFrecuenciaAno implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdFrecuenciaAno")
    private Short idFrecuenciaAno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;

    /**
     * @return the idFrecuenciaAno
     */
    public Short getIdFrecuenciaAno() {
        return idFrecuenciaAno;
    }

    /**
     * @param idFrecuenciaAno the idFrecuenciaAno to set
     */
    public void setIdFrecuenciaAno(Short idFrecuenciaAno) {
        this.idFrecuenciaAno = idFrecuenciaAno;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFrecuenciaAno != null ? idFrecuenciaAno.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasFrecuenciaAno)) {
            return false;
        }
        CnmasFrecuenciaAno other = (CnmasFrecuenciaAno) object;
        if ((this.idFrecuenciaAno == null && other.idFrecuenciaAno != null) || (this.idFrecuenciaAno != null && !this.idFrecuenciaAno.equals(other.idFrecuenciaAno))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasCompraOnline[ idFrecuenciaAno=" + idFrecuenciaAno + " ]";
    }
}
