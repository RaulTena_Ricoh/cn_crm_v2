/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnetl_fuente_valor")
@NamedQueries({
    @NamedQuery(name = "CnetlFuenteValor.findAll", query = "SELECT c FROM CnetlFuenteValor c"),
    @NamedQuery(name = "CnetlFuenteValor.findByIdFuente", query = "SELECT c FROM CnetlFuenteValor c WHERE c.cnetlFuenteValorPK.idFuente = :idFuente"),
    @NamedQuery(name = "CnetlFuenteValor.findByIdOrigen", query = "SELECT c FROM CnetlFuenteValor c WHERE c.cnetlFuenteValorPK.idOrigen = :idOrigen"),
    @NamedQuery(name = "CnetlFuenteValor.findByValor", query = "SELECT c FROM CnetlFuenteValor c WHERE c.valor = :valor")})
public class CnetlFuenteValor implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CnetlFuenteValorPK cnetlFuenteValorPK;
    @Basic(optional = false)
    @NotNull
    // TODO: JDC
    @Size(min = 0, max = 200)
    @Column(name = "Valor")
    private String valor;
    @JoinColumn(name = "IdFuente", referencedColumnName = "IdFuente", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnetlFuente cnetlFuente;
    @JoinColumn(name = "IdOrigen", referencedColumnName = "IdOrigen", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnetlOrigen cnetlOrigen;

    public CnetlFuenteValor() {
    }

    public CnetlFuenteValor(CnetlFuenteValorPK cnetlFuenteValorPK) {
        this.cnetlFuenteValorPK = cnetlFuenteValorPK;
    }

    public CnetlFuenteValor(CnetlFuenteValorPK cnetlFuenteValorPK, String valor) {
        this.cnetlFuenteValorPK = cnetlFuenteValorPK;
        this.valor = valor;
    }

    public CnetlFuenteValor(int idFuente, int idOrigen) {
        this.cnetlFuenteValorPK = new CnetlFuenteValorPK(idFuente, idOrigen);
    }

    public CnetlFuenteValorPK getCnetlFuenteValorPK() {
        return cnetlFuenteValorPK;
    }

    public void setCnetlFuenteValorPK(CnetlFuenteValorPK cnetlFuenteValorPK) {
        this.cnetlFuenteValorPK = cnetlFuenteValorPK;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public CnetlFuente getCnetlFuente() {
        return cnetlFuente;
    }

    public void setCnetlFuente(CnetlFuente cnetlFuente) {
        this.cnetlFuente = cnetlFuente;
    }

    public CnetlOrigen getCnetlOrigen() {
        return cnetlOrigen;
    }

    public void setCnetlOrigen(CnetlOrigen cnetlOrigen) {
        this.cnetlOrigen = cnetlOrigen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cnetlFuenteValorPK != null ? cnetlFuenteValorPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlFuenteValor)) {
            return false;
        }
        CnetlFuenteValor other = (CnetlFuenteValor) object;
        if ((this.cnetlFuenteValorPK == null && other.cnetlFuenteValorPK != null) || (this.cnetlFuenteValorPK != null && !this.cnetlFuenteValorPK.equals(other.cnetlFuenteValorPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnetlFuenteValor[ cnetlFuenteValorPK=" + cnetlFuenteValorPK + " ]";
    }
    
}
