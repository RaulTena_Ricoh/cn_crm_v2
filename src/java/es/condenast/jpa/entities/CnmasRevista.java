/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnmas_revista")
@NamedQueries({
    @NamedQuery(name = "CnmasRevista.findAll", query = "SELECT c FROM CnmasRevista c"),
    @NamedQuery(name = "CnmasRevista.findByIdRevista", query = "SELECT c FROM CnmasRevista c WHERE c.idRevista = :idRevista"),
    @NamedQuery(name = "CnmasRevista.findByNombre", query = "SELECT c FROM CnmasRevista c WHERE c.nombre = :nombre")})
public class CnmasRevista implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdRevista")
    private Short idRevista;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;

    public CnmasRevista() {
    }

    public CnmasRevista(Short idRevista) {
        this.idRevista = idRevista;
    }

    public CnmasRevista(Short idRevista, String nombre) {
        this.idRevista = idRevista;
        this.nombre = nombre;
    }

    public Short getIdRevista() {
        return idRevista;
    }

    public void setIdRevista(Short idRevista) {
        this.idRevista = idRevista;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRevista != null ? idRevista.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasRevista)) {
            return false;
        }
        CnmasRevista other = (CnmasRevista) object;
        if ((this.idRevista == null && other.idRevista != null) || (this.idRevista != null && !this.idRevista.equals(other.idRevista))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasRevista[ idRevista=" + idRevista + " ]";
    }
    
}
