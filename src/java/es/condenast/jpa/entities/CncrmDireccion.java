/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cncrm_direccion")
@NamedQueries({
    @NamedQuery(name = "CncrmDireccion.findAll", query = "SELECT c FROM CncrmDireccion c"),
    @NamedQuery(name = "CncrmDireccion.findByIdDireccion", query = "SELECT c FROM CncrmDireccion c WHERE c.idDireccion = :idDireccion"),
    @NamedQuery(name = "CncrmDireccion.findByIdUsuario", query = "SELECT c FROM CncrmDireccion c WHERE c.cncrmUsuario.idUsuario = :idUsuario")})
public class CncrmDireccion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdDireccion")
    private Long idDireccion;
    @Size(min = 1, max = 300)
    @Column(name = "Direccion")
    private String direccion;
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @Size(min = 1, max = 200)
    @Column(name = "Apellido1")
    private String apellido1;
    @Size(min = 1, max = 200)
    @Column(name = "Apellido2")
    private String apellido2;
    @Size(min = 1, max = 200)
    @Column(name = "PaisLiteral")
    private String paisLiteral;
    @Size(min = 1, max = 200)
    @Column(name = "ProvinciaLiteral")
    private String provinciaLiteral;
    @Size(min = 1, max = 200)
    @Column(name = "PoblacionLiteral")
    private String poblacionLiteral;
    @Size(min = 1, max = 200)
    @Column(name = "CodigoPostalLiteral")
    private String codigoPostalLiteral;
    @Basic(optional = false)
    @Column(name = "FechaAlta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAlta;
    
    @JoinColumn(name = "IdDireccionTipo", referencedColumnName = "IdDireccionTipo")
    @ManyToOne
    private CnmasDireccionTipo cnmasDireccionTipo;   
    @JoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario")
    @ManyToOne
    private CncrmUsuario cncrmUsuario;  
    @JoinColumn(name = "IdMarca", referencedColumnName = "IdMarca")
    @ManyToOne
    private CnmasMarca cnmasMarca;
    @JoinColumn(name = "IdPais", referencedColumnName = "IdPais")
    @ManyToOne
    private CnmasPais cnmasPais;
    @JoinColumns({
        @JoinColumn(name = "IdProvincia", referencedColumnName = "IdProvincia", insertable = false, updatable = false),
        @JoinColumn(name = "IdPais", referencedColumnName = "IdPais", insertable = false, updatable = false)})
    @ManyToOne
    private CnmasProvincia cnmasProvincia;
    @JoinColumns({
        @JoinColumn(name = "IdPoblacion", referencedColumnName = "IdPoblacion", insertable = false, updatable = false),
        @JoinColumn(name = "IdProvincia", referencedColumnName = "IdProvincia", insertable = false, updatable = false)})
    @ManyToOne
    private CnmasPoblacion cnmasPoblacion;
    @Size(min = 1, max = 10)
    @Column(name = "CodigoPostal")
    private String codigoPostal;

    public CncrmDireccion() {
    }

    public CncrmDireccion(Long idDireccion) {
        this.idDireccion = idDireccion;
    }

    /**
     *
     * @return
     */
    public Long getIdDireccion() {
        return idDireccion;
    }

    /**
     *
     * @param idDireccion
     */
    public void setIdDireccion(Long idDireccion) {
        this.idDireccion = idDireccion;
    }

    /**
     *
     * @return
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     *
     * @param direccion
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     *
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public String getApellido1() {
        return apellido1;
    }

    /**
     *
     * @param apellido1
     */
    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    /**
     *
     * @return
     */
    public String getApellido2() {
        return apellido2;
    }

    /**
     *
     * @param apellido2
     */
    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    /**
     *
     * @return
     */
    public String getPaisLiteral() {
        return paisLiteral;
    }

    /**
     *
     * @param paisLiteral
     */
    public void setPaisLiteral(String paisLiteral) {
        this.paisLiteral = paisLiteral;
    }

    /**
     *
     * @return
     */
    public String getProvinciaLiteral() {
        return provinciaLiteral;
    }

    /**
     *
     * @param provinciaLiteral
     */
    public void setProvinciaLiteral(String provinciaLiteral) {
        this.provinciaLiteral = provinciaLiteral;
    }

    /**
     *
     * @return
     */
    public String getPoblacionLiteral() {
        return poblacionLiteral;
    }

    /**
     *
     * @param poblacionLiteral
     */
    public void setPoblacionLiteral(String poblacionLiteral) {
        this.poblacionLiteral = poblacionLiteral;
    }

    /**
     *
     * @return
     */
    public String getCodigoPostalLiteral() {
        return codigoPostalLiteral;
    }

    /**
     *
     * @param codigoPostalLiteral
     */
    public void setCodigoPostalLiteral(String codigoPostalLiteral) {
        this.codigoPostalLiteral = codigoPostalLiteral;
    }

    /**
     *
     * @return
     */
    public Date getFechaAlta() {
        return fechaAlta;
    }

    /**
     *
     * @param fechaAlta
     */
    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    /**
     *
     * @return
     */
    public CnmasDireccionTipo getCnmasDireccionTipo() {
        return cnmasDireccionTipo;
    }

    /**
     *
     * @param cnmasDireccionTipo
     */
    public void setCnmasDireccionTipo(CnmasDireccionTipo cnmasDireccionTipo) {
        this.cnmasDireccionTipo = cnmasDireccionTipo;
    }

    /**
     *
     * @return
     */
    public CncrmUsuario getCncrmUsuario() {
        return cncrmUsuario;
    }

    /**
     *
     * @param cncrmUsuario
     */
    public void setCncrmUsuario(CncrmUsuario cncrmUsuario) {
        this.cncrmUsuario = cncrmUsuario;
    }

    /**
     *
     * @return
     */
    public CnmasMarca getCnmasMarca() {
        return cnmasMarca;
    }

    /**
     *
     * @param cnmasMarca
     */
    public void setCnmasMarca(CnmasMarca cnmasMarca) {
        this.cnmasMarca = cnmasMarca;
    }

    /**
     *
     * @return
     */
    public CnmasPais getCnmasPais() {
        return cnmasPais;
    }

    /**
     *
     * @param cnmasPais
     */
    public void setCnmasPais(CnmasPais cnmasPais) {
        this.cnmasPais = cnmasPais;
    }

    /**
     *
     * @return
     */
    public CnmasProvincia getCnmasProvincia() {
        return cnmasProvincia;
    }

    /**
     *
     * @param cnmasProvincia
     */
    public void setCnmasProvincia(CnmasProvincia cnmasProvincia) {
        this.cnmasProvincia = cnmasProvincia;
    }

    /**
     *
     * @return
     */
    public CnmasPoblacion getCnmasPoblacion() {
        return cnmasPoblacion;
    }

    /**
     *
     * @param cnmasPoblacion
     */
    public void setCnmasPoblacion(CnmasPoblacion cnmasPoblacion) {
        this.cnmasPoblacion = cnmasPoblacion;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDireccion != null ? idDireccion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CncrmDireccion)) {
            return false;
        }
        CncrmDireccion other = (CncrmDireccion) object;
        if ((this.idDireccion == null && other.idDireccion != null) || (this.idDireccion != null && !this.idDireccion.equals(other.idDireccion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmDireccion[ idDireccion=" + idDireccion + " ]";
    }
}
