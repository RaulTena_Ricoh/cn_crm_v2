/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnetl_formato")
@NamedQueries({
    @NamedQuery(name = "CnetlFormato.findAll", query = "SELECT c FROM CnetlFormato c"),
    @NamedQuery(name = "CnetlFormato.findByIdFormato", query = "SELECT c FROM CnetlFormato c WHERE c.idFormato = :idFormato"),
    @NamedQuery(name = "CnetlFormato.findByNombre", query = "SELECT c FROM CnetlFormato c WHERE c.nombre = :nombre")})
public class CnetlFormato implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdFormato")
    private Short idFormato;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idFormato")
    private Collection<CnetlFuenteTipo> cnetlFuenteTipoCollection;

    public CnetlFormato() {
    }

    public CnetlFormato(Short idFormato) {
        this.idFormato = idFormato;
    }

    public CnetlFormato(Short idFormato, String nombre) {
        this.idFormato = idFormato;
        this.nombre = nombre;
    }

    public Short getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(Short idFormato) {
        this.idFormato = idFormato;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Collection<CnetlFuenteTipo> getCnetlFuenteTipoCollection() {
        return cnetlFuenteTipoCollection;
    }

    public void setCnetlFuenteTipoCollection(Collection<CnetlFuenteTipo> cnetlFuenteTipoCollection) {
        this.cnetlFuenteTipoCollection = cnetlFuenteTipoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFormato != null ? idFormato.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlFormato)) {
            return false;
        }
        CnetlFormato other = (CnetlFormato) object;
        if ((this.idFormato == null && other.idFormato != null) || (this.idFormato != null && !this.idFormato.equals(other.idFormato))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre;
    }
    
}
