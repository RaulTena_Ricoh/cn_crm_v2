/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnetl_estado")
@NamedQueries({
    @NamedQuery(name = "CnetlEstado.findAll", query = "SELECT c FROM CnetlEstado c"),
    @NamedQuery(name = "CnetlEstado.findByIdEstado", query = "SELECT c FROM CnetlEstado c WHERE c.idEstado = :idEstado"),
    @NamedQuery(name = "CnetlEstado.findByNombre", query = "SELECT c FROM CnetlEstado c WHERE c.nombre = :nombre")})
public class CnetlEstado implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdEstado")
    private Short idEstado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEstado")
//    private Collection<CnetlPool> cnetlPoolCollection;

    public CnetlEstado() {
    }

    public CnetlEstado(Short idEstado) {
        this.idEstado = idEstado;
    }

    public CnetlEstado(Short idEstado, String nombre) {
        this.idEstado = idEstado;
        this.nombre = nombre;
    }

    public Short getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Short idEstado) {
        this.idEstado = idEstado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEstado != null ? idEstado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlEstado)) {
            return false;
        }
        CnetlEstado other = (CnetlEstado) object;
        return !((this.idEstado == null && other.idEstado != null) || (this.idEstado != null && !this.idEstado.equals(other.idEstado)));
    }

    @Override
    public String toString() {
        return nombre;
    }
    
}
