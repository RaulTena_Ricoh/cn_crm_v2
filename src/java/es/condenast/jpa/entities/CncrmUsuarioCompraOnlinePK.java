/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Embeddable
public class CncrmUsuarioCompraOnlinePK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdUsuario")
    private long idUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdCompraOnline")
    private short idCompraOnline;

    public CncrmUsuarioCompraOnlinePK() {
    }

    public CncrmUsuarioCompraOnlinePK(long idUsuario, short idCompraOnline) {
        this.idUsuario = idUsuario;
        this.idCompraOnline = idCompraOnline;
    }
    
    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public short getIdCompraOnline() {
        return idCompraOnline;
    }

    public void setIdCompraOnline(short idCompraOnline) {
        this.idCompraOnline = idCompraOnline;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idUsuario;
        hash += (int) idCompraOnline;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CncrmUsuarioCompraOnlinePK)) {
            return false;
        }
        CncrmUsuarioCompraOnlinePK other = (CncrmUsuarioCompraOnlinePK) object;
        if(this.idUsuario != other.idUsuario) {
            return false;
        }
        if(this.idCompraOnline != other.idCompraOnline) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioCompraOnlinePK[ idUsuario=" + idUsuario + ", idCompraOnline=" + idCompraOnline +  " ]";
    }
    
}
