/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ivan.fernandez
 */
@Entity
@Table(name = "cnmas_poblacion")
@NamedQueries({
    @NamedQuery(name = "CnmasPoblacion.findAll", query = "SELECT c FROM CnmasPoblacion c"),
    @NamedQuery(name = "CnmasPoblacion.findByIdPoblacion", query = "SELECT c FROM CnmasPoblacion c WHERE c.cnmasPoblacionPK.idPoblacion = :idPoblacion"),
    @NamedQuery(name = "CnmasPoblacion.findByIdProvincia", query = "SELECT c FROM CnmasPoblacion c WHERE c.cnmasPoblacionPK.idProvincia = :idProvincia")})
public class CnmasPoblacion implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    private CnmasPoblacionPK cnmasPoblacionPK;
    /*@JoinColumns({
        @JoinColumn(name = "IdPais", referencedColumnName = "IdPais", insertable = false, updatable = false),
        @JoinColumn(name = "IdProvincia", referencedColumnName = "IdProvincia", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private CnmasProvincia cnmasProvincia;*/
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasPoblacion")
    private Collection<CncrmDireccion> cncrmDireccionCollection;
    
    public CnmasPoblacion(){
        
    }
    
    public CnmasPoblacion(CnmasPoblacionPK cnmasPoblacionPK, String nombre){
        this.cnmasPoblacionPK = cnmasPoblacionPK;
        this.nombre = nombre;
    }

    /**
     * @return the cnmasPoblacionPK
     */
    public CnmasPoblacionPK getCnmasPoblacionPK() {
        return cnmasPoblacionPK;
    }

    /**
     * @param cnmasPoblacionPK the cnmasPoblacionPK to set
     */
    public void setCnmasPoblacionPK(CnmasPoblacionPK cnmasPoblacionPK) {
        this.cnmasPoblacionPK = cnmasPoblacionPK;
    }

    /**
     * @return the cnmasProvincia
     */
    /*public CnmasProvincia getCnmasPoblacion() {
        return cnmasProvincia;
    }*/

    /**
     * @param cnmasProvincia the cnmasProvincia to set
     */
    /*public void CnmasProvincia(CnmasProvincia cnmasProvincia) {
        this.cnmasProvincia = cnmasProvincia;
    }*/

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public Collection<CncrmDireccion> getCncrmDireccionCollection() {
        return cncrmDireccionCollection;
    }

    /**
     *
     * @param cncrmDireccionCollection
     */
    public void setCncrmDireccionCollection(Collection<CncrmDireccion> cncrmDireccionCollection) {
        this.cncrmDireccionCollection = cncrmDireccionCollection;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cnmasPoblacionPK != null ? cnmasPoblacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasPoblacion)) {
            return false;
        }
        CnmasPoblacion other = (CnmasPoblacion) object;
        if ((this.cnmasPoblacionPK == null && other.cnmasPoblacionPK != null) || (this.cnmasPoblacionPK != null && !this.cnmasPoblacionPK.equals(other.cnmasPoblacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasPoblacion[ cnmasPoblacionPK=" + cnmasPoblacionPK + " ]";
    }
}
