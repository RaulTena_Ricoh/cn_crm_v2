/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnmas_marca")
@NamedQueries({
    @NamedQuery(name = "CnmasMarca.findAll", query = "SELECT c FROM CnmasMarca c ORDER BY c.idMarca"),
    @NamedQuery(name = "CnmasMarca.findAllMinusOtros", query = "SELECT c FROM CnmasMarca c WHERE c.nombre <> 'Otros'"),
    @NamedQuery(name = "CnmasMarca.findByIdMarca", query = "SELECT c FROM CnmasMarca c WHERE c.idMarca = :idMarca"),
    @NamedQuery(name = "CnmasMarca.findByNombre", query = "SELECT c FROM CnmasMarca c WHERE c.nombre = :nombre")})
public class CnmasMarca implements Serializable, Comparable<CnmasMarca> {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdMarca")
    private Short idMarca;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasMarca")
    private Collection<CncrmUsuarioMarca> cncrmUsuarioMarcaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasMarca")
    private Collection<CncrmUsuarioMarcaCondicionLegal> cncrmUsuarioMarcaCondicionLegalCollection;
    @ManyToMany(mappedBy = "cnmasMarcaCollection")
    private Collection<CnsegUsuario> cnsegUsuarioCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasMarca")
    private Collection<CncrmUsuarioMarketingAccion> cncrmUsuarioMarketingAccionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasMarca")
    private Collection<CncrmUsuarioRedSocial> cncrmUsuarioRedSocialCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasMarca")
    private Collection<CncrmDireccion> cncrmDireccionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasMarca")
    private Collection<CncrmUsuarioConsumo> cncrmUsuarioConsumoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasMarca")
    private Collection<CncrmEcommerce> cncrmEcommerceCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasMarca")
    private Collection<CncrmUsuarioInteres> cncrmUsuarioInteresCollection;

    public CnmasMarca() {
    }

    public CnmasMarca(Short idMarca) {
        this.idMarca = idMarca;
    }

    public CnmasMarca(Short idMarca, String nombre) {
        this.idMarca = idMarca;
        this.nombre = nombre;
    }

    public Short getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(Short idMarca) {
        this.idMarca = idMarca;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Collection<CnsegUsuario> getCnsegUsuarioCollection() {
        return cnsegUsuarioCollection;
    }

    public void setCnsegUsuarioCollection(Collection<CnsegUsuario> cnsegUsuarioCollection) {
        this.cnsegUsuarioCollection = cnsegUsuarioCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMarca != null ? idMarca.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasMarca)) {
            return false;
        }
        CnmasMarca other = (CnmasMarca) object;
        if ((this.idMarca == null && other.idMarca != null) || (this.idMarca != null && !this.idMarca.equals(other.idMarca))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre;
    }

    public Collection<CncrmUsuarioMarca> getCncrmUsuarioMarcaCollection() {
        return cncrmUsuarioMarcaCollection;
    }

    public void setCncrmUsuarioMarcaCollection(Collection<CncrmUsuarioMarca> cncrmUsuarioMarcaCollection) {
        this.cncrmUsuarioMarcaCollection = cncrmUsuarioMarcaCollection;
    }

    public Collection<CncrmUsuarioMarcaCondicionLegal> getCncrmUsuarioMarcaCondicionLegalCollection() {
        return cncrmUsuarioMarcaCondicionLegalCollection;
    }

    public void setCncrmUsuarioMarcaCondicionLegalCollection(Collection<CncrmUsuarioMarcaCondicionLegal> cncrmUsuarioMarcaCondicionLegalCollection) {
        this.cncrmUsuarioMarcaCondicionLegalCollection = cncrmUsuarioMarcaCondicionLegalCollection;
    }

    public Collection<CncrmUsuarioMarketingAccion> getCncrmUsuarioMarketingAccionCollection() {
        return cncrmUsuarioMarketingAccionCollection;
    }

    public void setCncrmUsuarioMarketingAccionCollection(Collection<CncrmUsuarioMarketingAccion> cncrmUsuarioMarketingAccionCollection) {
        this.cncrmUsuarioMarketingAccionCollection = cncrmUsuarioMarketingAccionCollection;
    }

    public Collection<CncrmUsuarioRedSocial> getCncrmUsuarioRedSocialCollection() {
        return cncrmUsuarioRedSocialCollection;
    }

    public void setCncrmUsuarioRedSocialCollection(Collection<CncrmUsuarioRedSocial> cncrmUsuarioRedSocialCollection) {
        this.cncrmUsuarioRedSocialCollection = cncrmUsuarioRedSocialCollection;
    }

    public Collection<CncrmDireccion> getCncrmDireccionCollection() {
        return cncrmDireccionCollection;
    }

    public void setCncrmDireccionCollection(Collection<CncrmDireccion> cncrmDireccionCollection) {
        this.cncrmDireccionCollection = cncrmDireccionCollection;
    }

    public Collection<CncrmUsuarioConsumo> getCncrmUsuarioConsumoCollection() {
        return cncrmUsuarioConsumoCollection;
    }

    public void setCncrmUsuarioConsumoCollection(Collection<CncrmUsuarioConsumo> cncrmUsuarioConsumoCollection) {
        this.cncrmUsuarioConsumoCollection = cncrmUsuarioConsumoCollection;
    }

    public Collection<CncrmEcommerce> getCncrmEcommerceCollection() {
        return cncrmEcommerceCollection;
    }

    public void setCncrmEcommerceCollection(Collection<CncrmEcommerce> cncrmEcommerceCollection) {
        this.cncrmEcommerceCollection = cncrmEcommerceCollection;
    }

    public Collection<CncrmUsuarioInteres> getCncrmUsuarioInteresCollection() {
        return cncrmUsuarioInteresCollection;
    }

    public void setCncrmUsuarioInteresCollection(Collection<CncrmUsuarioInteres> cncrmUsuarioInteresCollection) {
        this.cncrmUsuarioInteresCollection = cncrmUsuarioInteresCollection;
    }

    @Override
    public int compareTo(CnmasMarca o) {
        return this.nombre.compareTo(o.nombre);
    }
 }
