/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnmas_interes")
@NamedQueries({
    @NamedQuery(name = "CnmasInteres.findAll", query = "SELECT c FROM CnmasInteres c"),
    @NamedQuery(name = "CnmasInteres.findByIdInteres", query = "SELECT c FROM CnmasInteres c WHERE c.idInteres = :idInteres"),
    @NamedQuery(name = "CnmasInteres.findByNombre", query = "SELECT c FROM CnmasInteres c WHERE c.nombre = :nombre")})
public class CnmasInteres implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdInteres")
    private Integer idInteres;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @JoinColumn(name = "IdCategoria", referencedColumnName = "IdCategoria")
    @ManyToOne(optional = false)
    private CnmasCategoria idCategoria;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasInteres")
    private Collection<CncrmUsuarioInteres> cncrmUsuarioInteresCollection;

    public CnmasInteres() {
    }

    public CnmasInteres(Integer idInteres) {
        this.idInteres = idInteres;
    }

    public CnmasInteres(Integer idInteres, String nombre) {
        this.idInteres = idInteres;
        this.nombre = nombre;
    }

    public Integer getIdInteres() {
        return idInteres;
    }

    public void setIdInteres(Integer idInteres) {
        this.idInteres = idInteres;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public CnmasCategoria getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(CnmasCategoria idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Collection<CncrmUsuarioInteres> getCncrmUsuarioInteresCollection() {
        return cncrmUsuarioInteresCollection;
    }

    public void setCncrmUsuarioInteresCollection(Collection<CncrmUsuarioInteres> cncrmUsuarioInteresCollection) {
        this.cncrmUsuarioInteresCollection = cncrmUsuarioInteresCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInteres != null ? idInteres.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasInteres)) {
            return false;
        }
        CnmasInteres other = (CnmasInteres) object;
        if ((this.idInteres == null && other.idInteres != null) || (this.idInteres != null && !this.idInteres.equals(other.idInteres))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasInteres[ idInteres=" + idInteres + " ]";
    }
    
}
