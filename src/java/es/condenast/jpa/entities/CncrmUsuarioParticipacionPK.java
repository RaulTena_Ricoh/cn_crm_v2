/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ivan.fernandez
 */
@Embeddable
public class CncrmUsuarioParticipacionPK implements Serializable{
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdUsuario")
    private long idUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdParticipacion")
    private Short idParticipacion;
    
    public CncrmUsuarioParticipacionPK() {
    }

    public CncrmUsuarioParticipacionPK(long idUsuario, Short idParticipacion) {
        this.idUsuario = idUsuario;
        this.idParticipacion = idParticipacion;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Short getIdParticipacion() {
        return idParticipacion;
    }

    public void setIdParticipacion(Short idParticipacion) {
        this.idParticipacion = idParticipacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idUsuario;
        hash += (int) idParticipacion;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CncrmUsuarioParticipacionPK)) {
            return false;
        }
        CncrmUsuarioParticipacionPK other = (CncrmUsuarioParticipacionPK) object;
        if (this.idUsuario != other.idUsuario) {
            return false;
        }
        if (this.idParticipacion != other.idParticipacion) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioParticipacionPK[ idUsuario=" + idUsuario + ", idParticipacion=" + idParticipacion + " ]";
    }
}
