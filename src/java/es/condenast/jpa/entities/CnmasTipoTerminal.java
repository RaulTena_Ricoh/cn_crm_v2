/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Ivan.Fernandez
 */
@Entity
@Table(name="cnmas_terminal")
@NamedQueries({
    @NamedQuery(name = "CnmasTipoTerminal.findAll", query = "SELECT c FROM CnmasTipoTerminal C"),
    @NamedQuery(name = "CnmasTipoTerminal.findByIdTerminal", query = "SELECT c FROM CnmasTipoTerminal C WHERE c.idTerminal = :idTerminal")})

public class CnmasTipoTerminal implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdTerminal")
    private Short idTerminal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;

    /**
     * @return the idTerminal
     */
    public Short getIdTerminal() {
        return idTerminal;
    }

    /**
     * @param idTerminal the idTerminal to set
     */
    public void setIdTerminal(Short idTerminal) {
        this.idTerminal = idTerminal;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTerminal != null ? idTerminal.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasTipoTerminal)) {
            return false;
        }
        CnmasTipoTerminal other = (CnmasTipoTerminal) object;
        if ((this.idTerminal == null && other.idTerminal != null) || (this.idTerminal != null && !this.idTerminal.equals(other.idTerminal))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasTipoTerminal[ idTerminal=" + idTerminal + " ]";
    }  

}
