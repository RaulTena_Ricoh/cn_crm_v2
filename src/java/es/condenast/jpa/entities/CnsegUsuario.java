/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnseg_usuario")
@NamedQueries({
    @NamedQuery(name = "CnsegUsuario.findAll", query = "SELECT c FROM CnsegUsuario c"),
    @NamedQuery(name = "CnsegUsuario.findByIdUsuario", query = "SELECT c FROM CnsegUsuario c WHERE c.idUsuario = :idUsuario"),
    @NamedQuery(name = "CnsegUsuario.findByNombre", query = "SELECT c FROM CnsegUsuario c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "CnsegUsuario.findByApellido1", query = "SELECT c FROM CnsegUsuario c WHERE c.apellido1 = :apellido1"),
    @NamedQuery(name = "CnsegUsuario.findByApellido2", query = "SELECT c FROM CnsegUsuario c WHERE c.apellido2 = :apellido2"),
    @NamedQuery(name = "CnsegUsuario.findByNtname", query = "SELECT c FROM CnsegUsuario c WHERE c.ntname = :ntname")})
public class CnsegUsuario implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaAlta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAlta;
    @Column(name = "FechaBaja")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaBaja;
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = true)
    @Column(name = "IdUsuario")
    private Integer idUsuario;
    
    @Size(max = 200)
    @Column(name = "Nombre")
    private String nombre;
    
    @Size(max = 200)
    @Column(name = "Apellido1")
    private String apellido1;
    
    @Size(max = 200)
    @Column(name = "Apellido2")
    private String apellido2;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "NTNAME")
    private String ntname;
    
    @JoinTable(name = "cnseg_usuario_marca", joinColumns = {
        @JoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario")}, inverseJoinColumns = {
        @JoinColumn(name = "IdMarca", referencedColumnName = "IdMarca")})
    @ManyToMany
    private Collection<CnmasMarca> cnmasMarcaCollection;
    
    
    @JoinColumn(name = "IdPerfil", referencedColumnName = "IdPerfil")
    @ManyToOne(optional = false)
    private CnsegPerfil idPerfil;
    


    public CnsegUsuario() {
    }
    

    public CnsegUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public CnsegUsuario(Integer idUsuario, String ntname) {
        this.idUsuario = idUsuario;
        this.ntname = ntname;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getNtname() {
        return ntname;
    }

    public void setNtname(String ntname) {
        this.ntname = ntname;
    }

    public Collection<CnmasMarca> getCnmasMarcaCollection() {
        return cnmasMarcaCollection;
    }

    public void setCnmasMarcaCollection(Collection<CnmasMarca> cnmasMarcaCollection) {
        this.cnmasMarcaCollection = cnmasMarcaCollection;
    }
    
    public boolean addCnmasMarca(CnmasMarca marca) {
        return getCnmasMarcaCollection().add(marca);
    }
    
    public boolean deleteCnmasMarca(CnmasMarca marca) {
        return getCnmasMarcaCollection().remove(marca);
    }
    
    public boolean containsCnmasMarca(CnmasMarca marca) {
        return getCnmasMarcaCollection() != null && getCnmasMarcaCollection().contains(marca);
    }

    public CnsegPerfil getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(CnsegPerfil idPerfil) {
        this.idPerfil = idPerfil;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuario != null ? idUsuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnsegUsuario)) {
            return false;
        }
        CnsegUsuario other = (CnsegUsuario) object;
        if ((this.idUsuario == null && other.idUsuario != null) || (this.idUsuario != null && !this.idUsuario.equals(other.idUsuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnsegUsuario[ idUsuario=" + idUsuario + " ]";
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }
    
}
