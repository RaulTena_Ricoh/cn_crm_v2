/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cnmas_rango_caducidad_suscripcion")
@NamedQueries({
    @NamedQuery(name = "CnmasRangoCaducidadSuscripcion.findAll", query = "SELECT c FROM CnmasRangoCaducidadSuscripcion c"),
    @NamedQuery(name = "CnmasRangoCaducidadSuscripcion.findByIdCnmasRangoCaducidadSuscripcion", query = "SELECT c FROM CnmasRangoCaducidadSuscripcion c WHERE c.idRango = :idRango"),
    @NamedQuery(name = "CnmasRangoCaducidadSuscripcion.findByNombre", query = "SELECT c FROM CnmasRangoCaducidadSuscripcion c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "CnmasRangoCaducidadSuscripcion.findByDesde", query = "SELECT c FROM CnmasRangoCaducidadSuscripcion c WHERE c.desde >= :desde"),
    @NamedQuery(name = "CnmasRangoCaducidadSuscripcion.findByHasta", query = "SELECT c FROM CnmasRangoCaducidadSuscripcion c WHERE c.hasta <= :hasta"),
    @NamedQuery(name = "CnmasRangoCaducidadSuscripcion.findByPeriodo", query = "SELECT c FROM CnmasRangoCaducidadSuscripcion c WHERE c.desde >= :desde AND c.hasta <= :hasta")})
public class CnmasRangoCaducidadSuscripcion implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdRango")
    private Short idRango;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @Column(name = "Desde")
    private Integer desde;
    @Column(name = "Hasta")
    private Integer hasta;

    public CnmasRangoCaducidadSuscripcion() {
    }

    public CnmasRangoCaducidadSuscripcion(Short idRango) {
        this.idRango = idRango;
    }

    public CnmasRangoCaducidadSuscripcion(Short idRango, String nombre) {
        this.idRango = idRango;
        this.nombre = nombre;
    }

    public CnmasRangoCaducidadSuscripcion(Short idRango, String nombre, Integer desde, Integer hasta) {
        this.idRango = idRango;
        this.nombre = nombre;
        this.desde = desde;
        this.hasta = hasta;
    }

    /**
     *
     * @return
     */
    public Short getIdRango() {
        return idRango;
    }

    /**
     *
     * @param idRango
     */
    public void setIdRango(Short idRango) {
        this.idRango = idRango;
    }

    /**
     *
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public Integer getDesde() {
        return desde;
    }

    /**
     *
     * @param desde
     */
    public void setDesde(Integer desde) {
        this.desde = desde;
    }

    /**
     *
     * @return
     */
    public Integer getHasta() {
        return hasta;
    }

    /**
     *
     * @param hasta
     */
    public void setHasta(Integer hasta) {
        this.hasta = hasta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.idRango != null ? this.idRango : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CnmasRangoCaducidadSuscripcion)) {
            return false;
        }
        CnmasRangoCaducidadSuscripcion other = (CnmasRangoCaducidadSuscripcion) object;
        if ((this.idRango == null && other.idRango != null) || !(this.idRango != null && this.idRango.equals(other.idRango))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasRangoCaducidadSuscripcion[ idRango=" + idRango + " ]";
    }
    
}
