/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ivan.fernandez
 */
@Embeddable
public class CnmasPoblacionPK implements Serializable{
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdPoblacion")
    private long idPoblacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdProvincia")
    private long idProvincia;
    public CnmasPoblacionPK(){
        
    }
    public CnmasPoblacionPK(long idProvincia, long idPoblacion){
        this.idPoblacion = idPoblacion;
        this.idProvincia = idProvincia;
    }

    /**
     * @return the idPoblacion
     */
    public long getIdPoblacion() {
        return idPoblacion;
    }

    /**
     * @param idPoblacion the idPoblacion to set
     */
    public void setIdPoblacion(long idPoblacion) {
        this.idPoblacion = idPoblacion;
    }

    /**
     * @return the idProvincia
     */
    public long getIdProvincia() {
        return idProvincia;
    }

    /**
     * @param idProvincia the idProvincia to set
     */
    public void setIdProvincia(long idProvincia) {
        this.idProvincia = idProvincia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) (this.idPoblacion);
        hash += (int) (this.idProvincia);
        return hash;
    }
     
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasPoblacionPK)) {
            return false;
        }
        CnmasPoblacionPK other = (CnmasPoblacionPK) object;
        if (this.idPoblacion != other.idPoblacion) {
            return false;
        }
        if (this.idProvincia != other.idProvincia) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasPoblacionPK[ idPoblacion=" + idPoblacion + ", idProvincia=" + idProvincia + " ]";
    }
}
