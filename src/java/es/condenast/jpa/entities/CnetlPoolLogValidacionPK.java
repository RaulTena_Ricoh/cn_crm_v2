/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Javier.Castro
 */
@Embeddable
public class CnetlPoolLogValidacionPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdPool")
    private long idPool;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdValidacion")
    private int idValidacion;

    public CnetlPoolLogValidacionPK() {
    }

    public CnetlPoolLogValidacionPK(long idPool, int idValidacion) {
        this.idPool = idPool;
        this.idValidacion = idValidacion;
    }

    public long getIdPool() {
        return idPool;
    }

    public void setIdPool(long idPool) {
        this.idPool = idPool;
    }

    public int getIdValidacion() {
        return idValidacion;
    }

    public void setIdValidacion(int idValidacion) {
        this.idValidacion = idValidacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idPool;
        hash += (int) idValidacion;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlPoolLogValidacionPK)) {
            return false;
        }
        CnetlPoolLogValidacionPK other = (CnetlPoolLogValidacionPK) object;
        if (this.idPool != other.idPool) {
            return false;
        }
        if (this.idValidacion != other.idValidacion) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnetlPoolLogValidacionPK[ idPool=" + idPool + ", idValidacion=" + idValidacion + " ]";
    }
    
}
