/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "vietl_log")
@NamedQueries({
    @NamedQuery(name = "VietlLog.findAll", query = "SELECT v FROM VietlLog v"),
    @NamedQuery(name = "VietlLog.findByIdPool", query = "SELECT v FROM VietlLog v WHERE v.idPool = :idPool"),
    @NamedQuery(name = "VietlLog.findByFuente", query = "SELECT v FROM VietlLog v WHERE v.fuente = :fuente"),
    @NamedQuery(name = "VietlLog.findByFuenteTipo", query = "SELECT v FROM VietlLog v WHERE v.fuenteTipo = :fuenteTipo"),
    @NamedQuery(name = "VietlLog.findByArchivo", query = "SELECT v FROM VietlLog v WHERE v.archivo = :archivo"),
    @NamedQuery(name = "VietlLog.findByEstado", query = "SELECT v FROM VietlLog v WHERE v.estado = :estado"),
    @NamedQuery(name = "VietlLog.findByInicioPool", query = "SELECT v FROM VietlLog v WHERE v.inicioPool = :inicioPool"),
    @NamedQuery(name = "VietlLog.findByLoadPool", query = "SELECT v FROM VietlLog v WHERE v.loadPool = :loadPool"),
    @NamedQuery(name = "VietlLog.findByFinPool", query = "SELECT v FROM VietlLog v WHERE v.finPool = :finPool"),
    @NamedQuery(name = "VietlLog.findByLineas", query = "SELECT v FROM VietlLog v WHERE v.lineas = :lineas"),
    @NamedQuery(name = "VietlLog.findByLineasOK", query = "SELECT v FROM VietlLog v WHERE v.lineasOK = :lineasOK"),
    @NamedQuery(name = "VietlLog.findByLineasError", query = "SELECT v FROM VietlLog v WHERE v.lineasError = :lineasError"),
    @NamedQuery(name = "VietlLog.findByDuracionPool", query = "SELECT v FROM VietlLog v WHERE v.duracionPool = :duracionPool"),
    @NamedQuery(name = "VietlLog.findByDuracionLoad", query = "SELECT v FROM VietlLog v WHERE v.duracionLoad = :duracionLoad")})
public class VietlLog implements Serializable {
    @Column(name = "Lineas")
    private Integer lineas;
    @Column(name = "LineasOK")
    private Integer lineasOK;
    @Column(name = "LineasError")
    private Integer lineasError;
    @Lob
    @Size(max = 65535)
    @Column(name = "Error")
    private String error;
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdPool")
    @Id
    private long idPool;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Fuente")
    private String fuente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "FuenteTipo")
    private String fuenteTipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Archivo")
    private String archivo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Estado")
    private String estado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "InicioPool")
    @Temporal(TemporalType.TIMESTAMP)
    private Date inicioPool;
    @Column(name = "LoadPool")
    @Temporal(TemporalType.TIMESTAMP)
    private Date loadPool;
    @Column(name = "FinPool")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finPool;
    @Column(name = "DuracionPool")
    @Temporal(TemporalType.TIME)
    private Date duracionPool;
    @Column(name = "DuracionLoad")
    @Temporal(TemporalType.TIME)
    private Date duracionLoad;
    @Transient
    private String duracionPoolStr;
    public VietlLog() {
    }

    public long getIdPool() {
        return idPool;
    }

    public void setIdPool(long idPool) {
        this.idPool = idPool;
    }

    public String getFuente() {
        return fuente;
    }

    public void setFuente(String fuente) {
        this.fuente = fuente;
    }

    public String getFuenteTipo() {
        return fuenteTipo;
    }

    public void setFuenteTipo(String fuenteTipo) {
        this.fuenteTipo = fuenteTipo;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getInicioPool() {
        return inicioPool;
    }

    public void setInicioPool(Date inicioPool) {
        this.inicioPool = inicioPool;
    }

    public Date getLoadPool() {
        return loadPool;
    }

    public void setLoadPool(Date loadPool) {
        this.loadPool = loadPool;
    }

    public Date getFinPool() {
        return finPool;
    }

    public void setFinPool(Date finPool) {
        this.finPool = finPool;
    }


    public Date getDuracionPool() {
        return duracionPool;
    }

    public void setDuracionPool(Date duracionPool) {
        this.duracionPool = duracionPool;
    }

    public Date getDuracionLoad() {
        return duracionLoad;
    }

    public void setDuracionLoad(Date duracionLoad) {
        this.duracionLoad = duracionLoad;
    }

    public Integer getLineas() {
        return lineas;
    }

    public void setLineas(Integer lineas) {
        this.lineas = lineas;
    }

    public Integer getLineasOK() {
        return lineasOK;
    }

    public void setLineasOK(Integer lineasOK) {
        this.lineasOK = lineasOK;
    }

    public Integer getLineasError() {
        return lineasError;
    }

    public void setLineasError(Integer lineasError) {
        this.lineasError = lineasError;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
    
    public String getDuracionPoolStr() {
        if (finPool == null || inicioPool == null){
            return null;
        }
        long timeDiff = Math.abs(finPool.getTime() - inicioPool.getTime());
        return String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(timeDiff),
               TimeUnit.MILLISECONDS.toMinutes(timeDiff) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(timeDiff)),
               TimeUnit.MILLISECONDS.toSeconds(timeDiff) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeDiff))
        );

    }

    /**
     * @param duracionPoolStr the duracionPoolStr to set
     */
    public void setDuracionPoolStr(String duracionPoolStr) {
        this.duracionPoolStr = duracionPoolStr;
    }

}
