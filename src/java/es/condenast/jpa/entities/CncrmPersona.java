/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cncrm_persona")
@NamedQueries({
    @NamedQuery(name = "CncrmPersona.findAll", query = "SELECT c FROM CncrmPersona c"),
    @NamedQuery(name = "CncrmPersona.findByIdUsuario", query = "SELECT c FROM CncrmPersona c WHERE c.idUsuario = :idUsuario")})
public class CncrmPersona implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "IdUsuario")
    private Long idUsuario;
    @MapsId 
    @OneToOne(mappedBy = "cncrmPersona")
    @JoinColumn(name = "idUsuario") 
    private CncrmUsuario cncrmUsuario;
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @Size(min = 1, max = 200)
    @Column(name = "Apellido1")
    private String apellido1;
    @Size(min = 1, max = 200)
    @Column(name = "Apellido2")
    private String apellido2;
    @Size(min = 1, max = 200)
    @Column(name = "Documento")
    private String documento;
    
    @JoinColumn(name = "IdSexo", referencedColumnName = "IdSexo")
    @ManyToOne
    private CnmasSexo cnmasSexo;
    
    @JoinColumn(name = "IdDocumento", referencedColumnName = "IdDocumento")
    @ManyToOne
    private CnmasDocumento cnmasDocumento;
    
    @Column(name = "FechaNacimiento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaNacimiento;
    
    @JoinColumn(name = "IdEstadoCivil", referencedColumnName = "IdEstadoCivil")
    @ManyToOne
    private CnmasEstadoCivil cnmasEstadoCivil;
    
    @Column(name = "TieneHijos")
    private boolean tieneHijos;
    
    @JoinColumn(name = "IdOcupacion", referencedColumnName = "IdOcupacion")
    @ManyToOne
    private CnmasOcupacion cnmasOcupacion;
    @JoinColumn(name = "IdSituacionLaboral", referencedColumnName = "IdSituacionLaboral")
    @ManyToOne
    private CnmasSituacionLaboral cnmasSituacionLaboral;
    @JoinColumn(name = "IdNivelEstudio", referencedColumnName = "IdNivelEstudio")
    @ManyToOne
    private CnmasNivelEstudio cnmasNivelEstudio;
    @JoinColumn(name = "IdNivelIngreso", referencedColumnName = "IdNivelIngreso")
    @ManyToOne
    private CnmasNivelIngreso cnmasNivelIngreso;

    @Size(min = 1, max = 200)
    @Column(name = "Empresa")
    private String empresa;    
    
    /**
     *
     */
    public CncrmPersona() {
    }

    /**
     *
     * @param idUsuario
     */
    public CncrmPersona(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     *
     * @param cncrmUsuario
     */
    public CncrmPersona(CncrmUsuario cncrmUsuario) {
        this.cncrmUsuario = cncrmUsuario;
    }

    /**
     *
     * @return
     */
    public Long getIdUsuario() {
        return idUsuario;
    }

    /**
     *
     * @param idUsuario
     */
    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     *
     * @return
     */
    public CncrmUsuario getCncrmUsuario() {
        return cncrmUsuario;
    }

    /**
     *
     * @param cncrmUsuario
     */
    public void setCncrmUsuario(CncrmUsuario cncrmUsuario) {
        this.cncrmUsuario = cncrmUsuario;
    }

    /**
     *
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public String getApellido1() {
        return apellido1;
    }

    /**
     *
     * @param apellido1
     */
    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    /**
     *
     * @return
     */
    public String getApellido2() {
        return apellido2;
    }

    /**
     *
     * @param apellido2
     */
    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    /**
     *
     * @return
     */
    public String getDocumento() {
        return documento;
    }

    /**
     *
     * @param documento
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     *
     * @return
     */
    public CnmasSexo getCnmasSexo() {
        return cnmasSexo;
    }

    /**
     *
     * @param cnmasSexo
     */
    public void setCnmasSexo(CnmasSexo cnmasSexo) {
        this.cnmasSexo = cnmasSexo;
    }

    /**
     *
     * @return
     */
    public CnmasDocumento getCnmasDocumento() {
        return cnmasDocumento;
    }

    /**
     *
     * @param cnmasDocumento
     */
    public void setCnmasDocumento(CnmasDocumento cnmasDocumento) {
        this.cnmasDocumento = cnmasDocumento;
    }

    /**
     *
     * @return
     */
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     *
     * @param fechaNacimiento
     */
    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    /**
     *
     * @return
     */
    public CnmasEstadoCivil getCnmasEstadoCivil() {
        return cnmasEstadoCivil;
    }

    /**
     *
     * @param cnmasEstadoCivil
     */
    public void setCnmasEstadoCivil(CnmasEstadoCivil cnmasEstadoCivil) {
        this.cnmasEstadoCivil = cnmasEstadoCivil;
    }

    /**
     *
     * @return
     */
    public boolean isTieneHijos() {
        return tieneHijos;
    }

    /**
     *
     * @param tieneHijos
     */
    public void setTieneHijos(boolean tieneHijos) {
        this.tieneHijos = tieneHijos;
    }

    /**
     *
     * @return
     */
    public CnmasOcupacion getCnmasOcupacion() {
        return cnmasOcupacion;
    }

    /**
     *
     * @param cnmasOcupacion
     */
    public void setCnmasOcupacion(CnmasOcupacion cnmasOcupacion) {
        this.cnmasOcupacion = cnmasOcupacion;
    }

    /**
     *
     * @return
     */
    public CnmasSituacionLaboral getCnmasSituacionLaboral() {
        return cnmasSituacionLaboral;
    }

    /**
     *
     * @param cnmasSituacionLaboral
     */
    public void setCnmasSituacionLaboral(CnmasSituacionLaboral cnmasSituacionLaboral) {
        this.cnmasSituacionLaboral = cnmasSituacionLaboral;
    }

    /**
     *
     * @return
     */
    public CnmasNivelEstudio getCnmasNivelEstudio() {
        return cnmasNivelEstudio;
    }

    /**
     *
     * @param cnmasNivelEstudio
     */
    public void setCnmasNivelEstudio(CnmasNivelEstudio cnmasNivelEstudio) {
        this.cnmasNivelEstudio = cnmasNivelEstudio;
    }

    /**
     *
     * @return
     */
    public CnmasNivelIngreso getCnmasNivelIngreso() {
        return cnmasNivelIngreso;
    }

    /**
     *
     * @param cnmasNivelIngreso
     */
    public void setCnmasNivelIngreso(CnmasNivelIngreso cnmasNivelIngreso) {
        this.cnmasNivelIngreso = cnmasNivelIngreso;
    }

    /**
     *
     * @return
     */
    public String getEmpresa() {
        return empresa;
    }

    /**
     *
     * @param empresa
     */
    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += idUsuario.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CncrmPersona)) {
            return false;
        }
        CncrmPersona other = (CncrmPersona) object;
        if ((this.idUsuario == null && other.idUsuario != null) || (this.idUsuario != null && !this.idUsuario.equals(other.idUsuario))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuario[ CncrmPersona=" + cncrmUsuario.getIdUsuario() + " ]";
    }
    
}
