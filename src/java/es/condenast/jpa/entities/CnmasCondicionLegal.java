/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnmas_condicion_legal")
@NamedQueries({
    @NamedQuery(name = "CnmasCondicionLegal.findAll", query = "SELECT c FROM CnmasCondicionLegal c"),
    @NamedQuery(name = "CnmasCondicionLegal.findByIdCondicionLegal", query = "SELECT c FROM CnmasCondicionLegal c WHERE c.idCondicionLegal = :idCondicionLegal"),
    @NamedQuery(name = "CnmasCondicionLegal.findByNombre", query = "SELECT c FROM CnmasCondicionLegal c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "CnmasCondicionLegal.findByDescripcion", query = "SELECT c FROM CnmasCondicionLegal c WHERE c.descripcion = :descripcion")})
public class CnmasCondicionLegal implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdCondicionLegal")
    private Short idCondicionLegal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @Size(max = 300)
    @Column(name = "Descripcion")
    private String descripcion;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasCondicionLegal")
//    private Collection<CncrmUsuarioMarcaCondicionLegal> cncrmUsuarioMarcaCondicionLegalCollection;

    public CnmasCondicionLegal() {
    }

    public CnmasCondicionLegal(Short idCondicionLegal) {
        this.idCondicionLegal = idCondicionLegal;
    }

    public CnmasCondicionLegal(Short idCondicionLegal, String nombre) {
        this.idCondicionLegal = idCondicionLegal;
        this.nombre = nombre;
    }

    public Short getIdCondicionLegal() {
        return idCondicionLegal;
    }

    public void setIdCondicionLegal(Short idCondicionLegal) {
        this.idCondicionLegal = idCondicionLegal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }



    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCondicionLegal != null ? idCondicionLegal.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasCondicionLegal)) {
            return false;
        }
        CnmasCondicionLegal other = (CnmasCondicionLegal) object;
        if ((this.idCondicionLegal == null && other.idCondicionLegal != null) || (this.idCondicionLegal != null && !this.idCondicionLegal.equals(other.idCondicionLegal))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasCondicionLegal[ idCondicionLegal=" + idCondicionLegal + " ]";
    }
    
}
