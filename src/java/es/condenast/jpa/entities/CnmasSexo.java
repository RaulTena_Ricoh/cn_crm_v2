/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cnmas_sexo")
@NamedQueries({
    @NamedQuery(name = "CnmasSexo.findAll", query = "SELECT c FROM CnmasSexo c"),
    @NamedQuery(name = "CnmasSexo.findByIdSexo", query = "SELECT c FROM CnmasSexo c WHERE c.idSexo = :idSexo"),
    @NamedQuery(name = "CnmasSexo.findByNombre", query = "SELECT c FROM CnmasSexo c WHERE c.nombre = :nombre")})
public class CnmasSexo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdSexo")
    private Short idSexo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasSexo")
    private Collection<CncrmPersona> cncrmPersonaCollection;
    
    /**
     *
     */
    public CnmasSexo() {
    }

    /**
     *
     * @param idSexo
     * @param nombre
     */
    public CnmasSexo(Short idSexo, String nombre) {
        this.idSexo = idSexo;
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public Short getIdSexo() {
        return idSexo;
    }

    /**
     *
     * @param idSexo
     */
    public void setIdSexo(Short idSexo) {
        this.idSexo = idSexo;
    }

    /**
     *
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public Collection<CncrmPersona> getCncrmPersonaCollection() {
        return cncrmPersonaCollection;
    }

    /**
     *
     * @param cncrmPersonaCollection
     */
    public void setCncrmPersonaCollection(Collection<CncrmPersona> cncrmPersonaCollection) {
        this.cncrmPersonaCollection = cncrmPersonaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSexo != null ? idSexo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CnmasSexo)) {
            return false;
        }
        CnmasSexo other = (CnmasSexo) object;
        if ((this.idSexo == null && other.idSexo != null) || (this.idSexo != null && !this.idSexo.equals(other.idSexo))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasSexo[ idSexo=" + idSexo + " ]";
    }
    
}
