/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnetl_pool_log_warning")
@NamedQueries({
    @NamedQuery(name = "CnetlPoolLogWarning.findAll", query = "SELECT c FROM CnetlPoolLogWarning c"),
    @NamedQuery(name = "CnetlPoolLogWarning.findByIdWarning", query = "SELECT c FROM CnetlPoolLogWarning c WHERE c.idWarning = :idWarning"),
    @NamedQuery(name = "CnetlPoolLogWarning.findByLevel", query = "SELECT c FROM CnetlPoolLogWarning c WHERE c.level = :level"),
    @NamedQuery(name = "CnetlPoolLogWarning.findByCode", query = "SELECT c FROM CnetlPoolLogWarning c WHERE c.code = :code"),
    @NamedQuery(name = "CnetlPoolLogWarning.findByMessage", query = "SELECT c FROM CnetlPoolLogWarning c WHERE c.message = :message"),
    @NamedQuery(name = "CnetlPoolLogWarning.findByIdPool", query = "SELECT c FROM CnetlPoolLogWarning c WHERE c.idPool = :idPool")})
public class CnetlPoolLogWarning implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdWarning")
    private Long idWarning;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdPool")
    private Long idPool;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "Level")
    private String level;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "Code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "Message")
    private String message;

    @JoinColumn(name = "IdPool", referencedColumnName = "IdPool", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnetlPoolLog cnetlPoolLog;
    
    public CnetlPoolLogWarning() {
    }

    public CnetlPoolLogWarning(Long idWarning) {
        this.idWarning = idWarning;
    }

    public CnetlPoolLogWarning(Long idWarning, String level, String code, String message) {
        this.idWarning = idWarning;
        this.level = level;
        this.code = code;
        this.message = message;
    }

    public Long getIdWarning() {
        return idWarning;
    }

    public void setIdWarning(Long idWarning) {
        this.idWarning = idWarning;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getIdPool() {
        return idPool;
    }

    public void setIdPool(Long idPool) {
        this.idPool = idPool;
    }

    public CnetlPoolLog getCnetlPoolLog() {
        return cnetlPoolLog;
    }

    public void setCnetlPoolLog(CnetlPoolLog cnetlPoolLog) {
        this.cnetlPoolLog = cnetlPoolLog;
    }    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idWarning != null ? idWarning.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlPoolLogWarning)) {
            return false;
        }
        CnetlPoolLogWarning other = (CnetlPoolLogWarning) object;
        if ((this.idWarning == null && other.idWarning != null) || (this.idWarning != null && !this.idWarning.equals(other.idWarning))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnetlPoolLogWarning[ idWarning=" + idWarning + " ]";
    }

}
