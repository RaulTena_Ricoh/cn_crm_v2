/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cncrm_ecommerce")
@NamedQueries({
    @NamedQuery(name = "CncrmEcommerce.findAll", query = "SELECT c FROM CncrmEcommerce c"),
    @NamedQuery(name = "CncrmEcommerce.findByIdCommerce", query = "SELECT c FROM CncrmEcommerce c WHERE c.idCommerce = :idCommerce"),
    @NamedQuery(name = "CncrmEcommerce.findByIdUsuario", query = "SELECT c FROM CncrmEcommerce c WHERE c.cncrmUsuario.idUsuario = :idUsuario"),
    @NamedQuery(name = "CncrmEcommerce.findByIdMarca", query = "SELECT c FROM CncrmEcommerce c WHERE c.cnmasMarca.idMarca = :idMarca")})
public class CncrmEcommerce implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdCommerce")
    private Long idCommerce;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdCompra")
    private Long idCompra;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    
    
    @JoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CncrmUsuario cncrmUsuario;
    @JoinColumn(name = "IdMarca", referencedColumnName = "IdMarca", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasMarca cnmasMarca;
    @JoinColumn(name = "IdProducto", referencedColumnName = "IdProducto", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasProducto cnmasProducto;

    public CncrmEcommerce() {
    }

    public CncrmEcommerce(Long idCommerce) {
        this.idCommerce = idCommerce;
    }

    public Long getIdCommerce() {
        return idCommerce;
    }

    public void setIdCommerce(Long idCommerce) {
        this.idCommerce = idCommerce;
    }

    public Long getIdCompra() {
        return idCompra;
    }

    public void setIdCompra(Long idCompra) {
        this.idCompra = idCompra;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public CncrmUsuario getCncrmUsuario() {
        return cncrmUsuario;
    }

    public void setCncrmUsuario(CncrmUsuario cncrmUsuario) {
        this.cncrmUsuario = cncrmUsuario;
    }

    public CnmasMarca getCnmasMarca() {
        return cnmasMarca;
    }

    public void setCnmasMarca(CnmasMarca cnmasMarca) {
        this.cnmasMarca = cnmasMarca;
    }

    public CnmasProducto getCnmasProducto() {
        return cnmasProducto;
    }

    public void setCnmasProducto(CnmasProducto cnmasProducto) {
        this.cnmasProducto = cnmasProducto;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCommerce != null ? idCommerce.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CncrmEcommerce)) {
            return false;
        }
        CncrmEcommerce other = (CncrmEcommerce) object;
        if ((this.idCommerce == null && other.idCommerce != null) || (this.idCommerce != null && !this.idCommerce.equals(other.idCommerce))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmEcommerce[ idCommerce=" + idCommerce + " ]";
    }
}
