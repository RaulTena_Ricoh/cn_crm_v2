/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnetl_pool_log_transformacion")
@NamedQueries({
    @NamedQuery(name = "CnetlPoolLogTransformacion.findAll", query = "SELECT c FROM CnetlPoolLogTransformacion c"),
    @NamedQuery(name = "CnetlPoolLogTransformacion.findByIdPool", query = "SELECT c FROM CnetlPoolLogTransformacion c WHERE c.cnetlPoolLogTransformacionPK.idPool = :idPool"),
    @NamedQuery(name = "CnetlPoolLogTransformacion.findByIdTransformacion", query = "SELECT c FROM CnetlPoolLogTransformacion c WHERE c.cnetlPoolLogTransformacionPK.idTransformacion = :idTransformacion"),
    @NamedQuery(name = "CnetlPoolLogTransformacion.findByFechaInicio", query = "SELECT c FROM CnetlPoolLogTransformacion c WHERE c.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "CnetlPoolLogTransformacion.findByFechaFin", query = "SELECT c FROM CnetlPoolLogTransformacion c WHERE c.fechaFin = :fechaFin")})
public class CnetlPoolLogTransformacion implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CnetlPoolLogTransformacionPK cnetlPoolLogTransformacionPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaInicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    @Column(name = "FechaFin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;
    
    @JoinColumn(name = "IdPool", referencedColumnName = "IdPool", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnetlPoolLog cnetlPoolLog;

    public CnetlPoolLogTransformacion() {
    }

    public CnetlPoolLogTransformacion(CnetlPoolLogTransformacionPK cnetlPoolLogTransformacionPK) {
        this.cnetlPoolLogTransformacionPK = cnetlPoolLogTransformacionPK;
    }

    public CnetlPoolLogTransformacion(CnetlPoolLogTransformacionPK cnetlPoolLogTransformacionPK, Date fechaInicio) {
        this.cnetlPoolLogTransformacionPK = cnetlPoolLogTransformacionPK;
        this.fechaInicio = fechaInicio;
    }

    public CnetlPoolLogTransformacion(long idPool, int idTransformacion) {
        this.cnetlPoolLogTransformacionPK = new CnetlPoolLogTransformacionPK(idPool, idTransformacion);
    }

    public CnetlPoolLogTransformacionPK getCnetlPoolLogTransformacionPK() {
        return cnetlPoolLogTransformacionPK;
    }

    public void setCnetlPoolLogTransformacionPK(CnetlPoolLogTransformacionPK cnetlPoolLogTransformacionPK) {
        this.cnetlPoolLogTransformacionPK = cnetlPoolLogTransformacionPK;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public CnetlPoolLog getCnetlPoolLog() {
        return cnetlPoolLog;
    }

    public void setCnetlPoolLog(CnetlPoolLog cnetlPoolLog) {
        this.cnetlPoolLog = cnetlPoolLog;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cnetlPoolLogTransformacionPK != null ? cnetlPoolLogTransformacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlPoolLogTransformacion)) {
            return false;
        }
        CnetlPoolLogTransformacion other = (CnetlPoolLogTransformacion) object;
        if ((this.cnetlPoolLogTransformacionPK == null && other.cnetlPoolLogTransformacionPK != null) || (this.cnetlPoolLogTransformacionPK != null && !this.cnetlPoolLogTransformacionPK.equals(other.cnetlPoolLogTransformacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnetlPoolLogTransformacion[ cnetlPoolLogTransformacionPK=" + cnetlPoolLogTransformacionPK + " ]";
    }
    
}
