/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnmas_situacion_laboral")
@NamedQueries({
    @NamedQuery(name = "CnmasSituacionLaboral.findAll", query = "SELECT c FROM CnmasSituacionLaboral c"),
    @NamedQuery(name = "CnmasSituacionLaboral.findByIdSituacionLaboral", query = "SELECT c FROM CnmasSituacionLaboral c WHERE c.idSituacionLaboral = :idSituacionLaboral"),
    @NamedQuery(name = "CnmasSituacionLaboral.findByNombre", query = "SELECT c FROM CnmasSituacionLaboral c WHERE c.nombre = :nombre")})
public class CnmasSituacionLaboral implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdSituacionLaboral")
    private Short idSituacionLaboral;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasSituacionLaboral")
    private Collection<CncrmPersona> cncrmPersonaCollection;

    public CnmasSituacionLaboral() {
    }

    public CnmasSituacionLaboral(Short idSituacionLaboral) {
        this.idSituacionLaboral = idSituacionLaboral;
    }

    public CnmasSituacionLaboral(Short idSituacionLaboral, String nombre) {
        this.idSituacionLaboral = idSituacionLaboral;
        this.nombre = nombre;
    }

    public Short getIdSituacionLaboral() {
        return idSituacionLaboral;
    }

    public void setIdSituacionLaboral(Short idSituacionLaboral) {
        this.idSituacionLaboral = idSituacionLaboral;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Collection<CncrmPersona> getCncrmPersonaCollection() {
        return cncrmPersonaCollection;
    }

    public void setCncrmPersonaCollection(Collection<CncrmPersona> cncrmPersonaCollection) {
        this.cncrmPersonaCollection = cncrmPersonaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSituacionLaboral != null ? idSituacionLaboral.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasSituacionLaboral)) {
            return false;
        }
        CnmasSituacionLaboral other = (CnmasSituacionLaboral) object;
        if ((this.idSituacionLaboral == null && other.idSituacionLaboral != null) || (this.idSituacionLaboral != null && !this.idSituacionLaboral.equals(other.idSituacionLaboral))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasSituacionLaboral[ idSituacionLaboral=" + idSituacionLaboral + " ]";
    }
    
}
