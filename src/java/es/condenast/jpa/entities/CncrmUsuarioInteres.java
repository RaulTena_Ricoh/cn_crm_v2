/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cncrm_usuario_interes")
@NamedQueries({
    @NamedQuery(name = "CncrmUsuarioInteres.findAll", query = "SELECT c FROM CncrmUsuarioInteres c"),
    @NamedQuery(name = "CncrmUsuarioInteres.findByIdUsuario", query = "SELECT c FROM CncrmUsuarioInteres c WHERE c.cncrmUsuarioInteresPK.idUsuario = :idUsuario"),
    @NamedQuery(name = "CncrmUsuarioInteres.findByIdInteres", query = "SELECT c FROM CncrmUsuarioInteres c WHERE c.cncrmUsuarioInteresPK.idInteres = :idInteres"),
    @NamedQuery(name = "CncrmUsuarioInteres.findByIdMarca", query = "SELECT c FROM CncrmUsuarioInteres c WHERE c.cncrmUsuarioInteresPK.idMarca = :idMarca")})
public class CncrmUsuarioInteres implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    private CncrmUsuarioInteresPK cncrmUsuarioInteresPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    
    @JoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CncrmUsuario cncrmUsuario;
    @JoinColumn(name = "IdInteres", referencedColumnName = "IdInteres", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasInteres cnmasInteres;
    @JoinColumn(name = "IdInteresTipo", referencedColumnName = "IdInteresTipo", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasInteresTipo cnmasInteresTipo;
    @JoinColumn(name = "IdMarca", referencedColumnName = "IdMarca", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasMarca cnmasMarca;
    @JoinColumn(name = "IdProducto", referencedColumnName = "IdProducto", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasProducto cnmasProducto;

    public CncrmUsuarioInteres() {
    }

    public CncrmUsuarioInteres(CncrmUsuarioInteresPK cncrmUsuarioInteresPK) {
        this.cncrmUsuarioInteresPK = cncrmUsuarioInteresPK;
    }

    public CncrmUsuarioInteresPK getCncrmUsuarioInteresPK() {
        return cncrmUsuarioInteresPK;
    }

    public void setCncrmUsuarioInteresPK(CncrmUsuarioInteresPK cncrmUsuarioInteresPK) {
        this.cncrmUsuarioInteresPK = cncrmUsuarioInteresPK;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public CncrmUsuario getCncrmUsuario() {
        return cncrmUsuario;
    }

    public void setCncrmUsuario(CncrmUsuario cncrmUsuario) {
        this.cncrmUsuario = cncrmUsuario;
    }

    public CnmasInteres getCnmasInteres() {
        return cnmasInteres;
    }

    public void setCnmasInteres(CnmasInteres cnmasInteres) {
        this.cnmasInteres = cnmasInteres;
    }

    public CnmasInteresTipo getCnmasInteresTipo() {
        return cnmasInteresTipo;
    }

    public void setCnmasInteresTipo(CnmasInteresTipo cnmasInteresTipo) {
        this.cnmasInteresTipo = cnmasInteresTipo;
    }

    public CnmasMarca getCnmasMarca() {
        return cnmasMarca;
    }

    public void setCnmasMarca(CnmasMarca cnmasMarca) {
        this.cnmasMarca = cnmasMarca;
    }

    public CnmasProducto getCnmasProducto() {
        return cnmasProducto;
    }

    public void setCnmasProducto(CnmasProducto cnmasProducto) {
        this.cnmasProducto = cnmasProducto;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += this.cncrmUsuarioInteresPK.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CncrmUsuarioInteres)) {
            return false;
        }
        CncrmUsuarioInteres other = (CncrmUsuarioInteres) object;
        if ((this.cncrmUsuarioInteresPK == null && other.cncrmUsuarioInteresPK != null) ||
                (this.cncrmUsuarioInteresPK != null && !this.cncrmUsuarioInteresPK.equals(other.cncrmUsuarioInteresPK))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioInteres[ cncrmUsuarioInteresPK=" + cncrmUsuarioInteresPK + " ]";
    }
    
}
