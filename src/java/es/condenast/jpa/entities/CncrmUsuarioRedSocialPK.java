/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Embeddable
public class CncrmUsuarioRedSocialPK implements Serializable{
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdUsuario")
    private long idUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdRedSocial")
    private short idRedSocial;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdMarca")
    private short idMarca;

    public CncrmUsuarioRedSocialPK() {
    }

    public CncrmUsuarioRedSocialPK(long idUsuario, short idRedSocial, short idMarca) {
        this.idUsuario = idUsuario;
        this.idRedSocial = idRedSocial;
        this.idMarca = idMarca;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public short getIdRedSocial() {
        return idRedSocial;
    }

    public void setIdRedSocial(short idRedSocial) {
        this.idRedSocial = idRedSocial;
    }

    public short getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(short idMarca) {
        this.idMarca = idMarca;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += this.idUsuario;
        hash += this.idRedSocial;
        hash += this.idMarca;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CncrmUsuarioRedSocialPK)) {
            return false;
        }
        CncrmUsuarioRedSocialPK other = (CncrmUsuarioRedSocialPK) object;
        if (this.idUsuario != other.idUsuario) {
            return false;
        }
        if (this.idRedSocial != other.idRedSocial) {
            return false;
        }
        if (this.idMarca != other.idMarca) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioRedSocialPK[ idUsuario=" + idUsuario + 
                ", idRedSocial=" + idRedSocial +
                ", idMarca=" + idMarca +" ]";
    }
    
}
