/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cncrm_usuario_consumo")
@NamedQueries({
    @NamedQuery(name = "CncrmUsuarioConsumo.findAll", query = "SELECT c FROM CncrmUsuarioConsumo c"),
    @NamedQuery(name = "CncrmUsuarioConsumo.findByIdUsuario", query = "SELECT c FROM CncrmUsuarioConsumo c WHERE c.cncrmUsuarioConsumoPK.idUsuario = :idUsuario"),
    @NamedQuery(name = "CncrmUsuarioConsumo.findByIdMarca", query = "SELECT c FROM CncrmUsuarioConsumo c WHERE c.cncrmUsuarioConsumoPK.idMarca = :idMarca")})
public class CncrmUsuarioConsumo implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    protected CncrmUsuarioConsumoPK cncrmUsuarioConsumoPK;

    @Basic(optional = false)
    @NotNull
    @Column(name = "RevFem_Activo")
    private boolean revFemActivo;
    @Column(name = "RevFem_Fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date revFemFecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RevMas_Activo")
    private boolean revMasActivo;
    @Column(name = "RevMas_Fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date revMasFecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Online_Activo")
    private boolean onlineActivo;
    @Column(name = "Online_Fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date onlineFecha;
    
    @JoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CncrmUsuario cncrmUsuario;
    @JoinColumn(name = "IdMarca", referencedColumnName = "IdMarca", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasMarca cnmasMarca;

    public CncrmUsuarioConsumo() {
    }

    public CncrmUsuarioConsumo(CncrmUsuarioConsumoPK cncrmUsuarioConsumoPK) {
        this.cncrmUsuarioConsumoPK = cncrmUsuarioConsumoPK;
    }

    public CncrmUsuarioConsumoPK getCncrmUsuarioConsumoPK() {
        return cncrmUsuarioConsumoPK;
    }

    public void setCncrmUsuarioConsumoPK(CncrmUsuarioConsumoPK cncrmUsuarioConsumoPK) {
        this.cncrmUsuarioConsumoPK = cncrmUsuarioConsumoPK;
    }

    public boolean isRevFemActivo() {
        return revFemActivo;
    }

    public void setRevFemActivo(boolean revFemActivo) {
        this.revFemActivo = revFemActivo;
    }

    public Date getRevFemFecha() {
        return revFemFecha;
    }

    public void setRevFemFecha(Date revFemFecha) {
        this.revFemFecha = revFemFecha;
    }

    public boolean isRevMasActivo() {
        return revMasActivo;
    }

    public void setRevMasActivo(boolean revMasActivo) {
        this.revMasActivo = revMasActivo;
    }

    public Date getRevMasFecha() {
        return revMasFecha;
    }

    public void setRevMasFecha(Date revMasFecha) {
        this.revMasFecha = revMasFecha;
    }

    public boolean isOnlineActivo() {
        return onlineActivo;
    }

    public void setOnlineActivo(boolean onlineActivo) {
        this.onlineActivo = onlineActivo;
    }

    public Date getOnlineFecha() {
        return onlineFecha;
    }

    public void setOnlineFecha(Date onlineFecha) {
        this.onlineFecha = onlineFecha;
    }

    public CncrmUsuario getCncrmUsuario() {
        return cncrmUsuario;
    }

    public void setCncrmUsuario(CncrmUsuario cncrmUsuario) {
        this.cncrmUsuario = cncrmUsuario;
    }

    public CnmasMarca getCnmasMarca() {
        return cnmasMarca;
    }

    public void setCnmasMarca(CnmasMarca cnmasMarca) {
        this.cnmasMarca = cnmasMarca;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cncrmUsuarioConsumoPK != null ? cncrmUsuarioConsumoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CncrmUsuarioConsumo)) {
            return false;
        }
        CncrmUsuarioConsumo other = (CncrmUsuarioConsumo) object;
        if ((this.cncrmUsuarioConsumoPK == null && other.cncrmUsuarioConsumoPK != null) || (this.cncrmUsuarioConsumoPK != null && !this.cncrmUsuarioConsumoPK.equals(other.cncrmUsuarioConsumoPK))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioConsumo[ cncrmUsuarioConsumoPK=" + cncrmUsuarioConsumoPK + " ]";
    }
}
