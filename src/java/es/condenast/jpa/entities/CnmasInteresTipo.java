/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cnmas_interes_tipo")
@NamedQueries({
    @NamedQuery(name = "CnmasInteresTipo.findAll", query = "SELECT c FROM CnmasInteresTipo c"),
    @NamedQuery(name = "CnmasInteresTipo.findByIdInteresTipo", query = "SELECT c FROM CnmasInteresTipo c WHERE c.idInteresTipo = :idInteresTipo"),
    @NamedQuery(name = "CnmasInteresTipo.findByNombre", query = "SELECT c FROM CnmasInteresTipo c WHERE c.nombre = :nombre")})
public class CnmasInteresTipo implements Serializable {
    @Id
    @Basic(optional = false)
    @Column(name = "IdInteresTipo")
    private Short idInteresTipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasInteresTipo")
    private Collection<CncrmUsuarioInteres> cncrmUsuarioInteres;

    public CnmasInteresTipo() {
    }

    public CnmasInteresTipo(Short idInteresTipo) {
        this.idInteresTipo = idInteresTipo;
    }

    public Short getIdInteresTipo() {
        return idInteresTipo;
    }

    public void setIdInteresTipo(Short idInteresTipo) {
        this.idInteresTipo = idInteresTipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Collection<CncrmUsuarioInteres> getCncrmUsuarioInteres() {
        return cncrmUsuarioInteres;
    }

    public void setCncrmUsuarioInteres(Collection<CncrmUsuarioInteres> cncrmUsuarioInteres) {
        this.cncrmUsuarioInteres = cncrmUsuarioInteres;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInteresTipo != null ? idInteresTipo.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasInteresTipo)) {
            return false;
        }
        CnmasInteresTipo other = (CnmasInteresTipo) object;
        if ((this.idInteresTipo == null && other.idInteresTipo != null) || (this.idInteresTipo != null && !this.idInteresTipo.equals(other.idInteresTipo))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasInteresTipo[ idInteresTipo=" + idInteresTipo + " ]";
    }  
}
