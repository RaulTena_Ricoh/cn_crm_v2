/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Javier.Castro
 */
@Embeddable
public class CnetlFuenteTipoOrigenPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdFuenteTipo")
    private short idFuenteTipo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdOrigen")
    private int idOrigen;

    public CnetlFuenteTipoOrigenPK() {
    }

    public CnetlFuenteTipoOrigenPK(short idFuenteTipo, int idOrigen) {
        this.idFuenteTipo = idFuenteTipo;
        this.idOrigen = idOrigen;
    }

    public short getIdFuenteTipo() {
        return idFuenteTipo;
    }

    public void setIdFuenteTipo(short idFuenteTipo) {
        this.idFuenteTipo = idFuenteTipo;
    }

    public int getIdOrigen() {
        return idOrigen;
    }

    public void setIdOrigen(int idOrigen) {
        this.idOrigen = idOrigen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idFuenteTipo;
        hash += (int) idOrigen;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlFuenteTipoOrigenPK)) {
            return false;
        }
        CnetlFuenteTipoOrigenPK other = (CnetlFuenteTipoOrigenPK) object;
        if (this.idFuenteTipo != other.idFuenteTipo) {
            return false;
        }
        if (this.idOrigen != other.idOrigen) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnetlFuenteTipoOrigenPK[ idFuenteTipo=" + idFuenteTipo + ", idOrigen=" + idOrigen + " ]";
    }
    
}
