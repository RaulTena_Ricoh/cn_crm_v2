/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnmas_nivel_estudio")
@NamedQueries({
    @NamedQuery(name = "CnmasNivelEstudio.findAll", query = "SELECT c FROM CnmasNivelEstudio c"),
    @NamedQuery(name = "CnmasNivelEstudio.findByIdNivelEstudio", query = "SELECT c FROM CnmasNivelEstudio c WHERE c.idNivelEstudio = :idNivelEstudio"),
    @NamedQuery(name = "CnmasNivelEstudio.findByNombre", query = "SELECT c FROM CnmasNivelEstudio c WHERE c.nombre = :nombre")})
public class CnmasNivelEstudio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdNivelEstudio")
    private Short idNivelEstudio;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @Lob
    @Size(max = 65535)
    @Column(name = "Descripcion")
    private String descripcion;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasNivelEstudio")
    private Collection<CncrmPersona> cncrmPersonaCollection;
    
    public CnmasNivelEstudio() {
    }

    public CnmasNivelEstudio(Short idNivelEstudio) {
        this.idNivelEstudio = idNivelEstudio;
    }

    public CnmasNivelEstudio(Short idNivelEstudio, String nombre) {
        this.idNivelEstudio = idNivelEstudio;
        this.nombre = nombre;
    }

    public Short getIdNivelEstudio() {
        return idNivelEstudio;
    }

    public void setIdNivelEstudio(Short idNivelEstudio) {
        this.idNivelEstudio = idNivelEstudio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Collection<CncrmPersona> getCncrmPersonaCollection() {
        return cncrmPersonaCollection;
    }

    public void setCncrmPersonaCollection(Collection<CncrmPersona> cncrmPersonaCollection) {
        this.cncrmPersonaCollection = cncrmPersonaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNivelEstudio != null ? idNivelEstudio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasNivelEstudio)) {
            return false;
        }
        CnmasNivelEstudio other = (CnmasNivelEstudio) object;
        if ((this.idNivelEstudio == null && other.idNivelEstudio != null) || (this.idNivelEstudio != null && !this.idNivelEstudio.equals(other.idNivelEstudio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasNivelEstudio[ idNivelEstudio=" + idNivelEstudio + " ]";
    }
    
}
