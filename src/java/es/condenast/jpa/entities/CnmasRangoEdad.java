/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ivan.fernandez
 */
@Entity
@Table(name = "cnmas_rango_edad")
@NamedQueries({
    @NamedQuery(name = "CnmasRangoEdad.findAll", query = "SELECT c FROM CnmasRangoEdad c"),
    @NamedQuery(name = "CnmasRangoEdad.findAllOrderByDesde", query = "SELECT c FROM CnmasRangoEdad c ORDER BY c.desde"),
    @NamedQuery(name = "CnmasRangoEdad.findByIdNivelIngreso", query = "SELECT c FROM CnmasRangoEdad c WHERE c.idRango = :idRango"),
    @NamedQuery(name = "CnmasRangoEdad.findByNombre", query = "SELECT c FROM CnmasRangoEdad c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "CnmasRangoEdad.findByDesde", query = "SELECT c FROM CnmasRangoEdad c WHERE c.desde = :desde"),
    @NamedQuery(name = "CnmasRangoEdad.findByHasta", query = "SELECT c FROM CnmasRangoEdad c WHERE c.hasta = :hasta")})
public class CnmasRangoEdad implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdRango")
    private Short idRango;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @Column(name = "Desde")
    private Short desde;
    @Column(name = "Hasta")
    private Short hasta;

    /**
     * @return the idRango
     */
    public Short getIdRango() {
        return idRango;
    }

    /**
     * @param idRango the idRango to set
     */
    public void setIdRango(Short idRango) {
        this.idRango = idRango;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the desde
     */
    public Short getDesde() {
        return desde;
    }

    /**
     * @param desde the desde to set
     */
    public void setDesde(Short desde) {
        this.desde = desde;
    }

    /**
     * @return the hasta
     */
    public Short getHasta() {
        return hasta;
    }

    /**
     * @param hasta the hasta to set
     */
    public void setHasta(Short hasta) {
        this.hasta = hasta;
    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRango != null ? idRango.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasRangoEdad)) {
            return false;
        }
        CnmasRangoEdad other = (CnmasRangoEdad) object;
        if ((this.idRango == null && other.idRango != null) || (this.idRango != null && !this.idRango.equals(other.idRango))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasRangoEdad[ idRango=" + idRango + " ]";
    }
}
