/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnmas_producto_categoria")
@NamedQueries({
    @NamedQuery(name = "CnmasProductoCategoria.findAll", query = "SELECT c FROM CnmasProductoCategoria c"),
    @NamedQuery(name = "CnmasProductoCategoria.findByIdProductoCategoria", query = "SELECT c FROM CnmasProductoCategoria c WHERE c.idProductoCategoria = :idProductoCategoria"),
    @NamedQuery(name = "CnmasProductoCategoria.findByNombre", query = "SELECT c FROM CnmasProductoCategoria c WHERE c.nombre = :nombre")})
public class CnmasProductoCategoria implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdProductoCategoria")
    private Short idProductoCategoria;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProductoCategoria")
    private Collection<CnmasProducto> cnmasProductoCollection;

    public CnmasProductoCategoria() {
    }

    public CnmasProductoCategoria(Short idProductoCategoria) {
        this.idProductoCategoria = idProductoCategoria;
    }

    public CnmasProductoCategoria(Short idProductoCategoria, String nombre) {
        this.idProductoCategoria = idProductoCategoria;
        this.nombre = nombre;
    }

    public Short getIdProductoCategoria() {
        return idProductoCategoria;
    }

    public void setIdProductoCategoria(Short idProductoCategoria) {
        this.idProductoCategoria = idProductoCategoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Collection<CnmasProducto> getCnmasProductoCollection() {
        return cnmasProductoCollection;
    }

    public void setCnmasProductoCollection(Collection<CnmasProducto> cnmasProductoCollection) {
        this.cnmasProductoCollection = cnmasProductoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProductoCategoria != null ? idProductoCategoria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasProductoCategoria)) {
            return false;
        }
        CnmasProductoCategoria other = (CnmasProductoCategoria) object;
        if ((this.idProductoCategoria == null && other.idProductoCategoria != null) || (this.idProductoCategoria != null && !this.idProductoCategoria.equals(other.idProductoCategoria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre;
    }
    
}
