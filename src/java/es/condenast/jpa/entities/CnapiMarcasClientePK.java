/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author david.yunta
 */
@Embeddable
public class CnapiMarcasClientePK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "idCliente")
    private int idCliente;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idMarca")
    private Short idMarca;

    public CnapiMarcasClientePK() {
    }

    public CnapiMarcasClientePK(int idCliente, Short idMarca) {
        this.idCliente = idCliente;
        this.idMarca = idMarca;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public Short getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(Short idMarca) {
        this.idMarca = idMarca;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idCliente;
        hash += (int) idMarca;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnapiMarcasClientePK)) {
            return false;
        }
        CnapiMarcasClientePK other = (CnapiMarcasClientePK) object;
        if (this.idCliente != other.idCliente) {
            return false;
        }
        if (this.idMarca != other.idMarca) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnapiMarcasClientePK[ idCliente=" + idCliente + ", idMarca=" + idMarca + " ]";
    }
    
}
