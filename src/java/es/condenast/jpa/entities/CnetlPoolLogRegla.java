/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnetl_pool_log_regla")
@NamedQueries({
    @NamedQuery(name = "CnetlPoolLogRegla.findAll", query = "SELECT c FROM CnetlPoolLogRegla c"),
    @NamedQuery(name = "CnetlPoolLogRegla.findByIdPool", query = "SELECT c FROM CnetlPoolLogRegla c WHERE c.cnetlPoolLogReglaPK.idPool = :idPool"),
    @NamedQuery(name = "CnetlPoolLogRegla.findByIdReglaImportacion", query = "SELECT c FROM CnetlPoolLogRegla c WHERE c.cnetlPoolLogReglaPK.idReglaImportacion = :idReglaImportacion"),
    @NamedQuery(name = "CnetlPoolLogRegla.findByFecha", query = "SELECT c FROM CnetlPoolLogRegla c WHERE c.fecha = :fecha")})
public class CnetlPoolLogRegla implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CnetlPoolLogReglaPK cnetlPoolLogReglaPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @JoinColumn(name = "IdPool", referencedColumnName = "IdPool", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnetlPoolLog cnetlPoolLog;
    
    public CnetlPoolLogRegla() {
    }

    public CnetlPoolLogRegla(CnetlPoolLogReglaPK cnetlPoolLogReglaPK) {
        this.cnetlPoolLogReglaPK = cnetlPoolLogReglaPK;
    }

    public CnetlPoolLogRegla(CnetlPoolLogReglaPK cnetlPoolLogReglaPK, Date fecha) {
        this.cnetlPoolLogReglaPK = cnetlPoolLogReglaPK;
        this.fecha = fecha;
    }

    public CnetlPoolLogRegla(long idPool, short idReglaImportacion) {
        this.cnetlPoolLogReglaPK = new CnetlPoolLogReglaPK(idPool, idReglaImportacion);
    }

    public CnetlPoolLogReglaPK getCnetlPoolLogReglaPK() {
        return cnetlPoolLogReglaPK;
    }

    public void setCnetlPoolLogReglaPK(CnetlPoolLogReglaPK cnetlPoolLogReglaPK) {
        this.cnetlPoolLogReglaPK = cnetlPoolLogReglaPK;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public CnetlPoolLog getCnetlPoolLog() {
        return cnetlPoolLog;
    }

    public void setCnetlPoolLog(CnetlPoolLog cnetlPoolLog) {
        this.cnetlPoolLog = cnetlPoolLog;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cnetlPoolLogReglaPK != null ? cnetlPoolLogReglaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlPoolLogRegla)) {
            return false;
        }
        CnetlPoolLogRegla other = (CnetlPoolLogRegla) object;
        if ((this.cnetlPoolLogReglaPK == null && other.cnetlPoolLogReglaPK != null) || (this.cnetlPoolLogReglaPK != null && !this.cnetlPoolLogReglaPK.equals(other.cnetlPoolLogReglaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnetlPoolLogRegla[ cnetlPoolLogReglaPK=" + cnetlPoolLogReglaPK + " ]";
    }
    
}
