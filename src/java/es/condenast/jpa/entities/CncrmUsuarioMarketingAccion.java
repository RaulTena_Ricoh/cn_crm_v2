/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cncrm_usuario_marketing_accion")
@NamedQueries({
    @NamedQuery(name = "CncrmUsuarioMarketingAccion.findAll", query = "SELECT c FROM CncrmUsuarioMarketingAccion c"),
    @NamedQuery(name = "CncrmUsuarioMarketingAccion.findByIdUsuario", query = "SELECT c FROM CncrmUsuarioMarketingAccion c WHERE c.cncrmUsuarioMarketingAccionPK.idUsuario = :idUsuario"),
    @NamedQuery(name = "CncrmUsuarioMarketingAccion.findByIdMarketingAccion", query = "SELECT c FROM CncrmUsuarioMarketingAccion c WHERE c.cncrmUsuarioMarketingAccionPK.idMarketingAccion = :idMarketingAccion"),
    @NamedQuery(name = "CncrmUsuarioMarketingAccion.findByIdMarca", query = "SELECT c FROM CncrmUsuarioMarketingAccion c WHERE c.cncrmUsuarioMarketingAccionPK.idMarca = :idMarca"),
    @NamedQuery(name = "CncrmUsuarioMarketingAccion.findByIdProducto", query = "SELECT c FROM CncrmUsuarioMarketingAccion c WHERE c.cncrmUsuarioMarketingAccionPK.idProducto = :idProducto"),
    @NamedQuery(name = "CncrmUsuarioMarketingAccion.findByFechaUltimaAccion", query = "SELECT c FROM CncrmUsuarioMarketingAccion c WHERE c.fechaUltimaAccion = :fechaUltimaAccion")})
public class CncrmUsuarioMarketingAccion implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CncrmUsuarioMarketingAccionPK cncrmUsuarioMarketingAccionPK;
    @Column(name = "FechaUltimaAccion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaUltimaAccion;
    @JoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CncrmUsuario cncrmUsuario;
    @JoinColumn(name = "IdMarketingAccion", referencedColumnName = "IdMarketingAccion", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasMarketingAccion cnmasMarketingAccion;
    @JoinColumn(name = "IdMarca", referencedColumnName = "IdMarca", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasMarca cnmasMarca;
    @JoinColumn(name = "IdProducto", referencedColumnName = "IdProducto", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasProducto cnmasProducto;

    /**
     *
     * @return
     */
    public CncrmUsuario getCncrmUsuario() {
        return cncrmUsuario;
    }

    /**
     *
     * @param cncrmUsuario
     */
    public void setCncrmUsuario(CncrmUsuario cncrmUsuario) {
        this.cncrmUsuario = cncrmUsuario;
    }

    /**
     *
     * @return
     */
    public CnmasMarketingAccion getCnmasMarketingAccion() {
        return cnmasMarketingAccion;
    }

    /**
     *
     * @param cnmasMarketingAccion
     */
    public void setCnmasMarketingAccion(CnmasMarketingAccion cnmasMarketingAccion) {
        this.cnmasMarketingAccion = cnmasMarketingAccion;
    }

    /**
     *
     * @return
     */
    public CnmasMarca getCnmasMarca() {
        return cnmasMarca;
    }

    /**
     *
     * @param cnmasMarca
     */
    public void setCnmasMarca(CnmasMarca cnmasMarca) {
        this.cnmasMarca = cnmasMarca;
    }

    /**
     *
     * @return
     */
    public CnmasProducto getCnmasProducto() {
        return cnmasProducto;
    }

    /**
     *
     * @param cnmasProducto
     */
    public void setCnmasProducto(CnmasProducto cnmasProducto) {
        this.cnmasProducto = cnmasProducto;
    }

    public CncrmUsuarioMarketingAccion() {
    }

    public CncrmUsuarioMarketingAccion(CncrmUsuarioMarketingAccionPK cncrmUsuarioMarketingAccionPK) {
        this.cncrmUsuarioMarketingAccionPK = cncrmUsuarioMarketingAccionPK;
    }

    public CncrmUsuarioMarketingAccion(CncrmUsuarioMarketingAccionPK cncrmUsuarioMarketingAccionPK, Date fechaUltimaAccion) {
        this.cncrmUsuarioMarketingAccionPK = cncrmUsuarioMarketingAccionPK;
        this.fechaUltimaAccion = fechaUltimaAccion;
    }

    /**
     *
     * @return
     */
    public CncrmUsuarioMarketingAccionPK getCncrmUsuarioMarketingAccionPK() {
        return cncrmUsuarioMarketingAccionPK;
    }

    /**
     *
     * @param cncrmUsuarioMarketingAccionPK
     */
    public void setCncrmUsuarioMarketingAccionPK(CncrmUsuarioMarketingAccionPK cncrmUsuarioMarketingAccionPK) {
        this.cncrmUsuarioMarketingAccionPK = cncrmUsuarioMarketingAccionPK;
    }

    /**
     *
     * @return
     */
    public Date getFechaUltimaAccion() {
        return fechaUltimaAccion;
    }

    /**
     *
     * @param fechaUltimaAccion
     */
    public void setFechaUltimaAccion(Date fechaUltimaAccion) {
        this.fechaUltimaAccion = fechaUltimaAccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cncrmUsuarioMarketingAccionPK != null ? cncrmUsuarioMarketingAccionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CncrmUsuarioMarketingAccion)) {
            return false;
        }
        CncrmUsuarioMarketingAccion other = (CncrmUsuarioMarketingAccion) object;
        if ((cncrmUsuarioMarketingAccionPK == null && other.cncrmUsuarioMarketingAccionPK != null) && (cncrmUsuarioMarketingAccionPK != null && !(cncrmUsuarioMarketingAccionPK.equals(other.cncrmUsuarioMarketingAccionPK)))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioMarketingAccion[ cncrmUsuarioMarketingAccionPK=" + cncrmUsuarioMarketingAccionPK + " ]";
    }
}
