/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author ivan.fernandez
 */
@Entity
@Table(name = "vicrm_search_user")
@NamedQueries({
    @NamedQuery(name = "CncrmSearchUserResult.findAll", query = "SELECT v FROM CncrmSearchUserResult v"),
    @NamedQuery(name = "CncrmSearchUserResult.findById", query = "SELECT v FROM CncrmSearchUserResult v WHERE v.cncrmSearchUserResultFK.idUsuario = :idUsuario AND v.cncrmSearchUserResultFK.idMarca = :idMarca AND v.cncrmSearchUserResultFK.idProducto = :idProducto"),
    @NamedQuery(name = "CncrmSearchUserResult.findByIdUsuario", query = "SELECT v FROM CncrmSearchUserResult v WHERE v.cncrmSearchUserResultFK.idUsuario = :idUsuario"),
    @NamedQuery(name = "CncrmSearchUserResult.findByidMarca", query = "SELECT v FROM CncrmSearchUserResult v WHERE v.cncrmSearchUserResultFK.idMarca = :idMarca"),
    @NamedQuery(name = "CncrmSearchUserResult.findByIdProducto", query = "SELECT v FROM CncrmSearchUserResult v WHERE v.cncrmSearchUserResultFK.idProducto = :idProducto"),
    @NamedQuery(name = "CncrmSearchUserResult.findByFechaAlta", query = "SELECT v FROM CncrmSearchUserResult v WHERE v.fechaAlta = :fechaAlta")})
public class CncrmSearchUserResult implements Serializable{
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    private CncrmSearchUserResultFK cncrmSearchUserResultFK;
    @Column(name = "Email")
    private String email;
    @Column(name = "IdParticipacion")
    private Short idParticipacion;
    @Column(name = "FechaAlta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAlta;
    @Column(name = "FechaBaja")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaBaja;
    @Column(name = "NombreParticipacion")
    private String nombreParticipacion;
    @Column(name = "C1_FechaAlta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date c1_FechaAlta;
    @Column(name = "C2_FechaAlta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date c2_FechaAlta;
    @Column(name = "C3_FechaAlta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date c3_FechaAlta;
    @Column(name = "NombreMarca")
    private String nombreMarca;
    @Column(name = "NombreProducto")
    private String nombreProducto;
    @Column(name = "CL1")
    private Long cl1;
    @Column(name = "CL2")
    private Long cl2;
    @Column(name = "CL3")
    private Long cl3;
    
    public CncrmSearchUserResult(){
        
    }
    
    public CncrmSearchUserResult(CncrmSearchUserResultFK cncrmSearchUserResultFK){
        this.cncrmSearchUserResultFK = cncrmSearchUserResultFK;
    }
    
    
    public CncrmSearchUserResult(Long idUsuario, Short idMarca, Short idProducto){
        this.cncrmSearchUserResultFK = new CncrmSearchUserResultFK(idUsuario, idMarca, idProducto);
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getCncrmSearchUserResultFK() != null ? getCncrmSearchUserResultFK().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CncrmSearchUserResult)) {
            return false;
        }
        CncrmSearchUserResult other = (CncrmSearchUserResult) object;
        if ((this.getCncrmSearchUserResultFK() == null && other.getCncrmSearchUserResultFK() != null) || (this.getCncrmSearchUserResultFK() != null && !this.cncrmSearchUserResultFK.equals(other.cncrmSearchUserResultFK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioParticipacion[ cncrmSearchUserResultFK=" + getCncrmSearchUserResultFK() + " ]";
    }



    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }



    /**
     * @return the fechaAlta
     */
    public Date getFechaAlta() {
        return fechaAlta;
    }

    /**
     * @param fechaAlta the fechaAlta to set
     */
    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }


    /**
     * @return the fechaBaja
     */
    public Date getFechaBaja() {
        return fechaBaja;
    }

    /**
     * @param fechaBaja the fechaBaja to set
     */
    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    /**
     * @return the nombreParticipacion
     */
    public String getNombreParticipacion() {
        return nombreParticipacion;
    }

    /**
     * @param nombreParticipacion the nombreParticipacion to set
     */
    public void setNombreParticipacion(String nombreParticipacion) {
        this.nombreParticipacion = nombreParticipacion;
    }

    /**
     * @return the c1_FechaAlta
     */
    public Date getC1_FechaAlta() {
        return c1_FechaAlta;
    }

    /**
     * @param c1_FechaAlta the c1_FechaAlta to set
     */
    public void setC1_FechaAlta(Date c1_FechaAlta) {
        this.c1_FechaAlta = c1_FechaAlta;
    }

    /**
     * @return the c2_FechaAlta
     */
    public Date getC2_FechaAlta() {
        return c2_FechaAlta;
    }

    /**
     * @param c2_FechaAlta the c2_FechaAlta to set
     */
    public void setC2_FechaAlta(Date c2_FechaAlta) {
        this.c2_FechaAlta = c2_FechaAlta;
    }

    /**
     * @return the c3_FechaAlta
     */
    public Date getC3_FechaAlta() {
        return c3_FechaAlta;
    }

    /**
     * @param c3_FechaAlta the c3_FechaAlta to set
     */
    public void setC3_FechaAlta(Date c3_FechaAlta) {
        this.c3_FechaAlta = c3_FechaAlta;
    }

    /**
     * @return the cl1
     */
    public Long getCl1() {
        return cl1;
    }

    /**
     * @param cl1 the cl1 to set
     */
    public void setCl1(Long cl1) {
        this.cl1 = cl1;
    }

    /**
     * @return the cl2
     */
    public Long getCl2() {
        return cl2;
    }

    /**
     * @param cl2 the cl2 to set
     */
    public void setCl2(Long cl2) {
        this.cl2 = cl2;
    }

    /**
     * @return the cl3
     */
    public Long getCl3() {
        return cl3;
    }

    /**
     * @param cl3 the cl3 to set
     */
    public void setCl3(Long cl3) {
        this.cl3 = cl3;
    }

    /**
     * @return the nombreMarca
     */
    public String getNombreMarca() {
        return nombreMarca;
    }

    /**
     * @param nombreMarca the nombreMarca to set
     */
    public void setNombreMarca(String nombreMarca) {
        this.nombreMarca = nombreMarca;
    }

    /**
     * @return the nombreProducto
     */
    public String getNombreProducto() {
        return nombreProducto;
    }

    /**
     * @param nombreProducto the nombreProducto to set
     */
    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    /**
     * @return the cncrmSearchUserResultFK
     */
    public CncrmSearchUserResultFK getCncrmSearchUserResultFK() {
        return cncrmSearchUserResultFK;
    }

    /**
     * @param cncrmSearchUserResultFK the cncrmSearchUserResultFK to set
     */
    public void setCncrmSearchUserResultFK(CncrmSearchUserResultFK cncrmSearchUserResultFK) {
        this.cncrmSearchUserResultFK = cncrmSearchUserResultFK;
    }


    /**
     * @return the idParticipacion
     */
    public Short getIdParticipacion() {
        return idParticipacion;
    }

    /**
     * @param idParticipacion the idParticipacion to set
     */
    public void setIdParticipacion(Short idParticipacion) {
        this.idParticipacion = idParticipacion;
    }

}
