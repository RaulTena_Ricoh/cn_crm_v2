/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnetl_fuente")
@NamedQueries({
    @NamedQuery(name = "CnetlFuente.findAll", query = "SELECT c FROM CnetlFuente c"),
    @NamedQuery(name = "CnetlFuente.findByIdFuente", query = "SELECT c FROM CnetlFuente c WHERE c.idFuente = :idFuente"),
    @NamedQuery(name = "CnetlFuente.findByNombre", query = "SELECT c FROM CnetlFuente c WHERE UPPER(c.nombre) = UPPER(:nombre)"),
    @NamedQuery(name = "CnetlFuente.findByOrigen", query = "SELECT c FROM CnetlFuente c WHERE c.origen = :origen"),
    @NamedQuery(name = "CnetlFuente.findByEsFichero", query = "SELECT c FROM CnetlFuente c WHERE c.esFichero = :esFichero"),
    @NamedQuery(name = "CnetlFuente.findByActivo", query = "SELECT c FROM CnetlFuente c WHERE c.activo = :activo")})
public class CnetlFuente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = true)
    @Column(name = "IdFuente")
    private Integer idFuente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 0, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 0, max = 300)
    @Column(name = "Origen")
    private String origen;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EsFichero")
    private boolean esFichero;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Activo")
    private boolean activo;
    @JoinColumn(name = "IdFuenteTipo", referencedColumnName = "IdFuenteTipo")
    @ManyToOne(optional = false)
    private CnetlFuenteTipo idFuenteTipo;

    public CnetlFuente() {
    }

    public CnetlFuente(Integer idFuente) {
        this.idFuente = idFuente;
    }

    public CnetlFuente(Integer idFuente, String nombre, String origen, boolean esFichero, boolean activo) {
        this.idFuente = idFuente;
        this.nombre = nombre;
        this.origen = origen;
        this.esFichero = esFichero;
        this.activo = activo;
    }

    public Integer getIdFuente() {
        return idFuente;
    }

    public void setIdFuente(Integer idFuente) {
        this.idFuente = idFuente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public boolean getEsFichero() {
        return esFichero;
    }

    public void setEsFichero(boolean esFichero) {
        this.esFichero = esFichero;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public CnetlFuenteTipo getIdFuenteTipo() {
        return idFuenteTipo;
    }

    public void setIdFuenteTipo(CnetlFuenteTipo idFuenteTipo) {
        this.idFuenteTipo = idFuenteTipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFuente != null ? idFuente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlFuente)) {
            return false;
        }
        CnetlFuente other = (CnetlFuente) object;
        if ((this.idFuente == null && other.idFuente != null) || (this.idFuente != null && !this.idFuente.equals(other.idFuente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre;
    }
    
}
