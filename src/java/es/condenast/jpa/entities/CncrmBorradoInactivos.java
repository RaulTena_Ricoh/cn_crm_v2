/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cncrm_borrado_inactivos")
@NamedQueries({
    @NamedQuery(name = "CncrmBorradoInactivos.findAll", query = "SELECT c FROM CncrmBorradoInactivos c"),
    @NamedQuery(name = "CncrmBorradoInactivos.findByIdProceso", query = "SELECT c FROM CncrmBorradoInactivos c WHERE c.idProceso = :idProceso"),
    @NamedQuery(name = "CncrmBorradoInactivos.findByFechaCambio", query = "SELECT c FROM CncrmBorradoInactivos c ORDER BY c.fechaCambio DESC")})

@NamedStoredProcedureQueries({
    @NamedStoredProcedureQuery(name = "CncrmBorradoInactivos.SPCRM_INACTIVE_USERS_WARNING", procedureName = "SPCRM_INACTIVE_USERS_WARNING"),
    @NamedStoredProcedureQuery(name = "CncrmBorradoInactivos.SPCRM_INACTIVE_USERS_DELETE", procedureName = "SPCRM_INACTIVE_USERS_DELETE")})
public class CncrmBorradoInactivos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdProceso")
    private Short idProceso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Activo")
    private boolean activo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MesesInactividad")
    private int mesesInactividad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SemanasAviso")
    private int semanasAviso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "Destinatarios")
    private String destinatarios;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FechaCambio")
    private Date fechaCambio;

    /**
     *
     */
    public CncrmBorradoInactivos() {
    }

    /**
     *
     * @param idProceso
     * @param activo
     */
    public CncrmBorradoInactivos(Short idProceso, boolean activo) {
        this.idProceso = idProceso;
        this.activo = activo;
    }

    /**
     *
     * @param activo
     * @param mesesInactividad
     * @param semanasAviso
     * @param destinatarios
     */
    public CncrmBorradoInactivos(boolean activo, int mesesInactividad, int semanasAviso, String destinatarios) {
        this.activo = activo;
        this.mesesInactividad = mesesInactividad;
        this.semanasAviso = semanasAviso;
        this.destinatarios = destinatarios;
    }

    /**
     *
     * @return
     */
    public Short getIdProceso() {
        return idProceso;
    }

    /**
     *
     * @param idProceso
     */
    public void setIdProceso(Short idProceso) {
        this.idProceso = idProceso;
    }

    /**
     *
     * @return
     */
    public boolean isActivo() {
        return activo;
    }

    /**
     *
     * @param activo
     */
    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    /**
     *
     * @return
     */
    public int getMesesInactividad() {
        return mesesInactividad;
    }

    /**
     *
     * @param mesesInactividad
     */
    public void setMesesInactividad(int mesesInactividad) {
        this.mesesInactividad = mesesInactividad;
    }

    /**
     *
     * @return
     */
    public int getSemanasAviso() {
        return semanasAviso;
    }

    /**
     *
     * @param semanasAviso
     */
    public void setSemanasAviso(int semanasAviso) {
        this.semanasAviso = semanasAviso;
    }

    /**
     *
     * @return
     */
    public String getDestinatarios() {
        return destinatarios;
    }

    /**
     *
     * @param destinatarios
     */
    public void setDestinatarios(String destinatarios) {
        this.destinatarios = destinatarios;
    }
    
    /**
     *
     * @return
     */
    public Date getFechaCambio() {
        return fechaCambio;
    }

    /**
     *
     * @param fechaCambio
     */
    public void setFechaCambio(Date fechaCambio) {
        this.fechaCambio = fechaCambio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProceso != null ? idProceso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CncrmBorradoInactivos)) {
            return false;
        }
        CncrmBorradoInactivos other = (CncrmBorradoInactivos) object;
        if ((this.idProceso == null && other.idProceso != null) || (this.idProceso != null && !this.idProceso.equals(other.idProceso))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmBorradoInactivos[ idProceso=" + idProceso + " ]";
    }
    
}
