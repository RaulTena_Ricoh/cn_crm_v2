/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Javier.Castro
 */
@Embeddable
public class CnetlPoolLogValidacionDetallePK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdPool")
    private long idPool;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdValidacion")
    private int idValidacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Linea")
    private int linea;

    public CnetlPoolLogValidacionDetallePK() {
    }

    public CnetlPoolLogValidacionDetallePK(long idPool, int idValidacion, int linea) {
        this.idPool = idPool;
        this.idValidacion = idValidacion;
        this.linea = linea;
    }

    public long getIdPool() {
        return idPool;
    }

    public void setIdPool(long idPool) {
        this.idPool = idPool;
    }

    public int getIdValidacion() {
        return idValidacion;
    }

    public void setIdValidacion(int idValidacion) {
        this.idValidacion = idValidacion;
    }

    public int getLinea() {
        return linea;
    }

    public void setLinea(int linea) {
        this.linea = linea;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idPool;
        hash += (int) idValidacion;
        hash += (int) linea;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlPoolLogValidacionDetallePK)) {
            return false;
        }
        CnetlPoolLogValidacionDetallePK other = (CnetlPoolLogValidacionDetallePK) object;
        if (this.idPool != other.idPool) {
            return false;
        }
        if (this.idValidacion != other.idValidacion) {
            return false;
        }
        if (this.linea != other.linea) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnetlPoolLogValidacionDetallePK[ idPool=" + idPool + ", idValidacion=" + idValidacion + ", linea=" + linea + " ]";
    }
    
}
