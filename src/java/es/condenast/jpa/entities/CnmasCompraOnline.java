/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
/**
 *
 * @author ivan.fernandez
 */
@Entity
@Table(name="cnmas_compra_online")
@NamedQueries({
    @NamedQuery(name = "CnmasCompraOnline.findAll", query = "SELECT c FROM CnmasCompraOnline c ORDER BY c.idCompraOnline"),
    @NamedQuery(name = "CnmasCompraOnline.findByIdCompraOnline", query = "SELECT c FROM CnmasCompraOnline c WHERE c.idCompraOnline = :idCompraOnline")})

public class CnmasCompraOnline implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdCompraOnline")
    private Short idCompraOnline;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;

    /**
     * @return the idCompraOnline
     */
    public Short getIdCompraOnline() {
        return idCompraOnline;
    }

    /**
     * @param idCompraOnline the idCompraOnline to set
     */
    public void setIdCompraOnline(Short idCompraOnline) {
        this.idCompraOnline = idCompraOnline;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCompraOnline != null ? idCompraOnline.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasCompraOnline)) {
            return false;
        }
        CnmasCompraOnline other = (CnmasCompraOnline) object;
        if ((this.idCompraOnline == null && other.idCompraOnline != null) || (this.idCompraOnline != null && !this.idCompraOnline.equals(other.idCompraOnline))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasCompraOnline[ idCompraOnline=" + idCompraOnline + " ]";
    }
}
