/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Embeddable
public class CnetlMasterTipoOrigenPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdMasterTipo")
    private short idMasterTipo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdOrigen")
    private int idOrigen;

    public CnetlMasterTipoOrigenPK() {
    }

    public CnetlMasterTipoOrigenPK(short idMasterTipo, int idOrigen) {
        this.idMasterTipo = idMasterTipo;
        this.idOrigen = idOrigen;
    }

    public short getIdMasterTipo() {
        return idMasterTipo;
    }

    public void setIdMasterTipo(short idMasterTipo) {
        this.idMasterTipo = idMasterTipo;
    }

    public int getIdOrigen() {
        return idOrigen;
    }

    public void setIdOrigen(int idOrigen) {
        this.idOrigen = idOrigen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idMasterTipo;
        hash += (int) idOrigen;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlMasterTipoOrigenPK)) {
            return false;
        }
        CnetlMasterTipoOrigenPK other = (CnetlMasterTipoOrigenPK) object;
        if (this.idMasterTipo != other.idMasterTipo) {
            return false;
        }
        if (this.idOrigen != other.idOrigen) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnetlMasterTipoOrigenPK[ idMasterTipo=" + idMasterTipo + ", idOrigen=" + idOrigen + " ]";
    }
    
}
