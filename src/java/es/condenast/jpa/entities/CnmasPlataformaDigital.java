/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ivan.fernandez
 */
@Entity
@Table(name="cnmas_plataforma_digital")
@NamedQueries({
    @NamedQuery(name = "CnmasPlataformaDigital.findAll", query = "SELECT c FROM CnmasPlataformaDigital c ORDER BY c.idPlataformaDigital"),
    @NamedQuery(name = "CnmasPlataformaDigital.findByIdPlataformaDigitals", query = "SELECT c FROM CnmasPlataformaDigital C WHERE c.idPlataformaDigital = :idPlataformaDigital")})
public class CnmasPlataformaDigital implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdPlataformaDigital")
    private Short idPlataformaDigital;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;

    /**
     * @return the idPlataformaDigital
     */
    public Short getIdPlataformaDigital() {
        return idPlataformaDigital;
    }

    /**
     * @param idPlataformaDigital the idPlataformaDigital to set
     */
    public void setIdPlataformaDigital(Short idPlataformaDigital) {
        this.idPlataformaDigital = idPlataformaDigital;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPlataformaDigital != null ? idPlataformaDigital.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasPlataformaDigital)) {
            return false;
        }
        CnmasPlataformaDigital other = (CnmasPlataformaDigital) object;
        if ((this.idPlataformaDigital == null && other.idPlataformaDigital != null) || (this.idPlataformaDigital != null && !this.idPlataformaDigital.equals(other.idPlataformaDigital))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasPlataformaDigital[ idPlataformaDigital=" + idPlataformaDigital + " ]";
    }  
}
