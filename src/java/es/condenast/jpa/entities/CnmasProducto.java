/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnmas_producto")
@NamedQueries({
    @NamedQuery(name = "CnmasProducto.findAll", query = "SELECT c FROM CnmasProducto c"),
    @NamedQuery(name = "CnmasProducto.findByIdProducto", query = "SELECT c FROM CnmasProducto c WHERE c.idProducto = :idProducto"),
    @NamedQuery(name = "CnmasProducto.findByNombre", query = "SELECT c FROM CnmasProducto c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "CnmasProducto.findByCaducidad", query = "SELECT c FROM CnmasProducto c WHERE c.caducidad = :caducidad"),
    @NamedQuery(name = "CnmasProducto.findByPeso", query = "SELECT c FROM CnmasProducto c WHERE c.peso = :peso"),
    @NamedQuery(name = "CnmasProducto.findByEsSuscripcion", query = "SELECT c FROM CnmasProducto c WHERE c.esSuscripcion = :esSuscripcion")})
public class CnmasProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdProducto")
    private Short idProducto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @Column(name = "Caducidad")
    private Integer caducidad;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Peso")
    private BigDecimal peso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EsSuscripcion")
    private boolean esSuscripcion;
    @OneToMany(mappedBy = "idProductoPadre")
    private Collection<CnmasProducto> cnmasProductoCollection;
    @JoinColumn(name = "IdProductoPadre", referencedColumnName = "IdProducto")
    @ManyToOne
    private CnmasProducto idProductoPadre;
    @JoinColumn(name = "IdProductoCategoria", referencedColumnName = "IdProductoCategoria")
    @ManyToOne(optional = false)
    private CnmasProductoCategoria idProductoCategoria;
        @JoinTable(name = "cnmas_producto_usuario_origen", joinColumns = {
        @JoinColumn(name = "IdProducto", referencedColumnName = "IdProducto")}, inverseJoinColumns = {
        @JoinColumn(name = "IdUsuarioOrigen", referencedColumnName = "IdUsuarioOrigen")})
    @ManyToMany
    private Collection<CnmasUsuarioOrigen> cnmasUsuarioOrigenCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasProducto")
    private Collection<CncrmUsuarioMarketingAccion> cncrmUsuarioMarketingAccionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasProducto")
    private Collection<CncrmEcommerce> cncrmEcommerceCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasProducto")
    private Collection<CncrmUsuarioInteres> cncrmUsuarioInteresCollection;
    
    public CnmasProducto() {
    }

    public CnmasProducto(Short idProducto) {
        this.idProducto = idProducto;
    }

    public CnmasProducto(Short idProducto, String nombre, boolean esSuscripcion) {
        this.idProducto = idProducto;
        this.nombre = nombre;
        this.esSuscripcion = esSuscripcion;
    }

    public Short getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Short idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCaducidad() {
        return caducidad;
    }

    public void setCaducidad(Integer caducidad) {
        this.caducidad = caducidad;
    }

    public BigDecimal getPeso() {
        return peso;
    }

    public void setPeso(BigDecimal peso) {
        this.peso = peso;
    }

    public boolean getEsSuscripcion() {
        return esSuscripcion;
    }

    public void setEsSuscripcion(boolean esSuscripcion) {
        this.esSuscripcion = esSuscripcion;
    }

    public Collection<CnmasProducto> getCnmasProductoCollection() {
        return cnmasProductoCollection;
    }

    public void setCnmasProductoCollection(Collection<CnmasProducto> cnmasProductoCollection) {
        this.cnmasProductoCollection = cnmasProductoCollection;
    }

    public CnmasProducto getIdProductoPadre() {
        return idProductoPadre;
    }

    public void setIdProductoPadre(CnmasProducto idProductoPadre) {
        this.idProductoPadre = idProductoPadre;
    }

    public CnmasProductoCategoria getIdProductoCategoria() {
        return idProductoCategoria;
    }

    public void setIdProductoCategoria(CnmasProductoCategoria idProductoCategoria) {
        this.idProductoCategoria = idProductoCategoria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProducto != null ? idProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasProducto)) {
            return false;
        }
        CnmasProducto other = (CnmasProducto) object;
        if ((this.idProducto == null && other.idProducto != null) || (this.idProducto != null && !this.idProducto.equals(other.idProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre;
    }

    public Collection<CnmasUsuarioOrigen> getCnmasUsuarioOrigenCollection() {
        return cnmasUsuarioOrigenCollection;
    }

    public void setCnmasUsuarioOrigenCollection(Collection<CnmasUsuarioOrigen> cnmasUsuarioOrigenCollection) {
        this.cnmasUsuarioOrigenCollection = cnmasUsuarioOrigenCollection;
    }

    public Collection<CncrmUsuarioMarketingAccion> getCncrmUsuarioMarketingAccionCollection() {
        return cncrmUsuarioMarketingAccionCollection;
    }

    public void setCncrmUsuarioMarketingAccionCollection(Collection<CncrmUsuarioMarketingAccion> cncrmUsuarioMarketingAccionCollection) {
        this.cncrmUsuarioMarketingAccionCollection = cncrmUsuarioMarketingAccionCollection;
    }

    public Collection<CncrmEcommerce> getCncrmEcommerceCollection() {
        return cncrmEcommerceCollection;
    }

    public void setCncrmEcommerceCollection(Collection<CncrmEcommerce> cncrmEcommerceCollection) {
        this.cncrmEcommerceCollection = cncrmEcommerceCollection;
    }

    public Collection<CncrmUsuarioInteres> getCncrmUsuarioInteresCollection() {
        return cncrmUsuarioInteresCollection;
    }

    public void setCncrmUsuarioInteresCollection(Collection<CncrmUsuarioInteres> cncrmUsuarioInteresCollection) {
        this.cncrmUsuarioInteresCollection = cncrmUsuarioInteresCollection;
    }
    
}
