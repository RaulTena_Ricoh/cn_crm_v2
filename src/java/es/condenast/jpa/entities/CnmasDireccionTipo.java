/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ivan.fernandez
 */
@Entity
@Table(name="cnmas_direccion_tipo")
@NamedQueries({
    @NamedQuery(name = "CnmasDireccionTipo.findAll", query = "SELECT c FROM CnmasDireccionTipo C"),
    @NamedQuery(name = "CnmasDireccionTipo.findByIdDireccionTipo", query = "SELECT c FROM CnmasDireccionTipo C WHERE c.idDireccionTipo = :idDireccionTipo")})
public class CnmasDireccionTipo implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdDireccionTipo")
    private Short idDireccionTipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasDireccionTipo")
    private Collection<CncrmDireccion> cncrmDireccionCollection;
    
    /**
     * @return the idDireccionTipo
     */
    public Short getIdDireccionTipo() {
        return idDireccionTipo;
    }

    /**
     * @param idDireccionTipo the idDireccionTipo to set
     */
    public void setIdDireccionTipo(Short idDireccionTipo) {
        this.idDireccionTipo = idDireccionTipo;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public Collection<CncrmDireccion> getCncrmDireccionCollection() {
        return cncrmDireccionCollection;
    }

    /**
     *
     * @param cncrmDireccionCollection
     */
    public void setCncrmDireccionCollection(Collection<CncrmDireccion> cncrmDireccionCollection) {
        this.cncrmDireccionCollection = cncrmDireccionCollection;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDireccionTipo != null ? idDireccionTipo.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasDireccionTipo)) {
            return false;
        }
        CnmasDireccionTipo other = (CnmasDireccionTipo) object;
        if ((this.idDireccionTipo == null && other.idDireccionTipo != null) || (this.idDireccionTipo != null && !this.idDireccionTipo.equals(other.idDireccionTipo))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasDireccionTipo[ idDireccionTipo=" + idDireccionTipo + " ]";
    }  
}
