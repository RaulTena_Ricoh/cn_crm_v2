/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Embeddable
public class CnmasCodigoPostalPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "CodigoPostal")
    private String codigoPostal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdPais")
    private long idPais;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdPoblacion")
    private long idPoblacion;

    /**
     *
     */
    public CnmasCodigoPostalPK() {
    }

    /**
     *
     * @param codigoPostal
     * @param idPais
     * @param idPoblacion
     */
    public CnmasCodigoPostalPK(String codigoPostal, long idPais, long idPoblacion) {
        this.codigoPostal = codigoPostal;
        this.idPais = idPais;
        this.idPoblacion = idPoblacion;
    }

    /**
     *
     * @return
     */
    public String getCodigoPostal() {
        return codigoPostal;
    }

    /**
     *
     * @param codigoPostal
     */
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    /**
     *
     * @return
     */
    public long getIdPais() {
        return idPais;
    }

    /**
     *
     * @param idPais
     */
    public void setIdPais(long idPais) {
        this.idPais = idPais;
    }

    /**
     *
     * @return
     */
    public long getIdPoblacion() {
        return idPoblacion;
    }

    /**
     *
     * @param idPoblacion
     */
    public void setIdPoblacion(long idPoblacion) {
        this.idPoblacion = idPoblacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += Objects.hashCode(this.codigoPostal);
        hash += (int) (this.idPais);
        hash += (int) (this.idPoblacion);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasCodigoPostalPK)) {
            return false;
        }
        CnmasCodigoPostalPK other = (CnmasCodigoPostalPK) object;
        if ((this.codigoPostal == null && other.codigoPostal != null)
                || (this.codigoPostal != null && !this.codigoPostal.equals(other.codigoPostal))) {
            return false;
        }
        if (this.idPoblacion != other.idPoblacion) {
            return false;
        }
        if (this.idPais != other.idPais) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasCodigoPostalPK[ codigoPostal=" + codigoPostal
                + ", idPoblacion=" + idPoblacion + 
                ", idPais=" + idPais + " ]";
    }
}
