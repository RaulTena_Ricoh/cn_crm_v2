/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Embeddable
public class CncrmUsuarioTipoDispositivoDigitalPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdUsuario")
    private long idUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdTerminal")
    private short idTerminal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdSistemaOperativo")
    private short idSistemaOperativo;

    public CncrmUsuarioTipoDispositivoDigitalPK() {
    }

    public CncrmUsuarioTipoDispositivoDigitalPK(long idUsuario, short idTipoTerminal, short idSistemaOperativo) {
        this.idUsuario = idUsuario;
        this.idTerminal = idTipoTerminal;
        this.idSistemaOperativo = idSistemaOperativo;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public short getIdTerminal() {
        return idTerminal;
    }

    public void setIdTerminal(short idTerminal) {
        this.idTerminal = idTerminal;
    }

    public short getIdSistemaOperativo() {
        return idSistemaOperativo;
    }

    public void setIdSistemaOperativo(short idSistemaOperativo) {
        this.idSistemaOperativo = idSistemaOperativo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) this.idUsuario;
        hash += this.idTerminal;
        hash += this.idSistemaOperativo;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof CncrmUsuarioTipoDispositivoDigitalPK) {
            return false;
        }
        CncrmUsuarioTipoDispositivoDigitalPK other = (CncrmUsuarioTipoDispositivoDigitalPK) object;
        if (this.idUsuario != other.idUsuario) {
            return false;
        }
        if (this.idTerminal != other.idTerminal) {
            return false;
        }
        if (this.idSistemaOperativo != other.idSistemaOperativo) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioTipoDispositivoDigitalPK[ idUsuario=" + idUsuario + 
                ", idTipoTerminal=" + idTerminal + ", idSistemaOperativo=" + idSistemaOperativo + " ]";
    }
}
