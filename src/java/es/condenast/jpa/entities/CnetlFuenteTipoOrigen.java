/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnetl_fuente_tipo_origen")
@NamedQueries({
    @NamedQuery(name = "CnetlFuenteTipoOrigen.findAll", query = "SELECT c FROM CnetlFuenteTipoOrigen c"),
    @NamedQuery(name = "CnetlFuenteTipoOrigen.findByIdFuenteTipo", query = "SELECT c FROM CnetlFuenteTipoOrigen c WHERE c.cnetlFuenteTipoOrigenPK.idFuenteTipo = :idFuenteTipo ORDER BY c.orden ASC"),
    @NamedQuery(name = "CnetlFuenteTipoOrigen.findByIdOrigen", query = "SELECT c FROM CnetlFuenteTipoOrigen c WHERE c.cnetlFuenteTipoOrigenPK.idOrigen = :idOrigen"),
    @NamedQuery(name = "CnetlFuenteTipoOrigen.findByOrden", query = "SELECT c FROM CnetlFuenteTipoOrigen c WHERE c.orden = :orden"),
    @NamedQuery(name = "CnetlFuenteTipoOrigen.removeByIdFuenteTipo", query = "DELETE FROM CnetlFuenteTipoOrigen c WHERE c.cnetlFuenteTipoOrigenPK.idFuenteTipo = :idFuenteTipo")})
public class CnetlFuenteTipoOrigen implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CnetlFuenteTipoOrigenPK cnetlFuenteTipoOrigenPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Orden")
    private short orden;
    @JoinColumn(name = "IdOrigen", referencedColumnName = "IdOrigen", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnetlOrigen cnetlOrigen;
    @JoinColumn(name = "IdFuenteTipo", referencedColumnName = "IdFuenteTipo", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnetlFuenteTipo cnetlFuenteTipo;

    public CnetlFuenteTipoOrigen() {
    }

    public CnetlFuenteTipoOrigen(CnetlFuenteTipoOrigenPK cnetlFuenteTipoOrigenPK) {
        this.cnetlFuenteTipoOrigenPK = cnetlFuenteTipoOrigenPK;
    }

    public CnetlFuenteTipoOrigen(CnetlFuenteTipoOrigenPK cnetlFuenteTipoOrigenPK, short orden) {
        this.cnetlFuenteTipoOrigenPK = cnetlFuenteTipoOrigenPK;
        this.orden = orden;
    }

    public CnetlFuenteTipoOrigen(short idFuenteTipo, int idOrigen) {
        this.cnetlFuenteTipoOrigenPK = new CnetlFuenteTipoOrigenPK(idFuenteTipo, idOrigen);
    }

    public CnetlFuenteTipoOrigenPK getCnetlFuenteTipoOrigenPK() {
        return cnetlFuenteTipoOrigenPK;
    }

    public void setCnetlFuenteTipoOrigenPK(CnetlFuenteTipoOrigenPK cnetlFuenteTipoOrigenPK) {
        this.cnetlFuenteTipoOrigenPK = cnetlFuenteTipoOrigenPK;
    }

    public short getOrden() {
        return orden;
    }

    public void setOrden(short orden) {
        this.orden = orden;
    }

    public CnetlOrigen getCnetlOrigen() {
        return cnetlOrigen;
    }

    public void setCnetlOrigen(CnetlOrigen cnetlOrigen) {
        this.cnetlOrigen = cnetlOrigen;
    }

    public CnetlFuenteTipo getCnetlFuenteTipo() {
        return cnetlFuenteTipo;
    }

    public void setCnetlFuenteTipo(CnetlFuenteTipo cnetlFuenteTipo) {
        this.cnetlFuenteTipo = cnetlFuenteTipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cnetlFuenteTipoOrigenPK != null ? cnetlFuenteTipoOrigenPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlFuenteTipoOrigen)) {
            return false;
        }
        CnetlFuenteTipoOrigen other = (CnetlFuenteTipoOrigen) object;
        if ((this.cnetlFuenteTipoOrigenPK == null && other.cnetlFuenteTipoOrigenPK != null) || (this.cnetlFuenteTipoOrigenPK != null && !this.cnetlFuenteTipoOrigenPK.equals(other.cnetlFuenteTipoOrigenPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnetlFuenteTipoOrigen[ cnetlFuenteTipoOrigenPK=" + cnetlFuenteTipoOrigenPK + " ]";
    }
    
}
