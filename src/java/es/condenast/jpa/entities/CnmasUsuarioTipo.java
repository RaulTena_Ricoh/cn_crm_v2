/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnmas_usuario_tipo")
@NamedQueries({
    @NamedQuery(name = "CnmasUsuarioTipo.findAll", query = "SELECT c FROM CnmasUsuarioTipo c"),
    @NamedQuery(name = "CnmasUsuarioTipo.findAllOrderByMarcaDesde", query = "SELECT c FROM CnmasUsuarioTipo c ORDER BY c.marcaDesde"),
    @NamedQuery(name = "CnmasUsuarioTipo.findAllOrderByCNDesde", query = "SELECT c FROM CnmasUsuarioTipo c ORDER BY c.cnDesde"),    
    @NamedQuery(name = "CnmasUsuarioTipo.findByIdUsuarioTipo", query = "SELECT c FROM CnmasUsuarioTipo c WHERE c.idUsuarioTipo = :idUsuarioTipo"),
    @NamedQuery(name = "CnmasUsuarioTipo.findByActivo", query = "SELECT c FROM CnmasUsuarioTipo c WHERE c.activo = :activo"),
    @NamedQuery(name = "CnmasUsuarioTipo.findByNombre", query = "SELECT c FROM CnmasUsuarioTipo c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "CnmasUsuarioTipo.findByMarcaDesde", query = "SELECT c FROM CnmasUsuarioTipo c WHERE c.marcaDesde = :marcaDesde"),
    @NamedQuery(name = "CnmasUsuarioTipo.findByMarcaHasta", query = "SELECT c FROM CnmasUsuarioTipo c WHERE c.marcaHasta = :marcaHasta"),
    @NamedQuery(name = "CnmasUsuarioTipo.findByPeso", query = "SELECT c FROM CnmasUsuarioTipo c WHERE c.peso = :peso"),
    @NamedQuery(name = "CnmasUsuarioTipo.findByCnDesde", query = "SELECT c FROM CnmasUsuarioTipo c WHERE c.cnDesde = :cnDesde"),
    @NamedQuery(name = "CnmasUsuarioTipo.findByCnHasta", query = "SELECT c FROM CnmasUsuarioTipo c WHERE c.cnHasta = :cnHasta")})
public class CnmasUsuarioTipo implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUsuarioTipo")
    private Collection<CncrmUsuarioMarca> cncrmUsuarioMarcaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUsuarioTipo")
    private Collection<CncrmUsuario> cncrmUsuarioCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdUsuarioTipo")
    private Short idUsuarioTipo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Activo")
    private boolean activo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "MarcaDesde")
    private BigDecimal marcaDesde;
    @Column(name = "MarcaHasta")
    private BigDecimal marcaHasta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Peso")
    private int peso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CnDesde")
    private BigDecimal cnDesde;
    @Column(name = "CnHasta")
    private BigDecimal cnHasta;

    public CnmasUsuarioTipo() {
    }

    public CnmasUsuarioTipo(Short idUsuarioTipo) {
        this.idUsuarioTipo = idUsuarioTipo;
    }

    public CnmasUsuarioTipo(Short idUsuarioTipo, boolean activo, String nombre, BigDecimal marcaDesde, int peso, BigDecimal cnDesde) {
        this.idUsuarioTipo = idUsuarioTipo;
        this.activo = activo;
        this.nombre = nombre;
        this.marcaDesde = marcaDesde;
        this.peso = peso;
        this.cnDesde = cnDesde;
    }

    public Short getIdUsuarioTipo() {
        return idUsuarioTipo;
    }

    public void setIdUsuarioTipo(Short idUsuarioTipo) {
        this.idUsuarioTipo = idUsuarioTipo;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigDecimal getMarcaDesde() {
        return marcaDesde;
    }

    public void setMarcaDesde(BigDecimal marcaDesde) {
        this.marcaDesde = marcaDesde;
    }

    public BigDecimal getMarcaHasta() {
        return marcaHasta;
    }

    public void setMarcaHasta(BigDecimal marcaHasta) {
        this.marcaHasta = marcaHasta;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public BigDecimal getCnDesde() {
        return cnDesde;
    }

    public void setCnDesde(BigDecimal cnDesde) {
        this.cnDesde = cnDesde;
    }

    public BigDecimal getCnHasta() {
        return cnHasta;
    }

    public void setCnHasta(BigDecimal cnHasta) {
        this.cnHasta = cnHasta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuarioTipo != null ? idUsuarioTipo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasUsuarioTipo)) {
            return false;
        }
        CnmasUsuarioTipo other = (CnmasUsuarioTipo) object;
        if ((this.idUsuarioTipo == null && other.idUsuarioTipo != null) || (this.idUsuarioTipo != null && !this.idUsuarioTipo.equals(other.idUsuarioTipo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasUsuarioTipo[ idUsuarioTipo=" + idUsuarioTipo + " ]";
    }

    public Collection<CncrmUsuarioMarca> getCncrmUsuarioMarcaCollection() {
        return cncrmUsuarioMarcaCollection;
    }

    public void setCncrmUsuarioMarcaCollection(Collection<CncrmUsuarioMarca> cncrmUsuarioMarcaCollection) {
        this.cncrmUsuarioMarcaCollection = cncrmUsuarioMarcaCollection;
    }

    public Collection<CncrmUsuario> getCncrmUsuarioCollection() {
        return cncrmUsuarioCollection;
    }

    public void setCncrmUsuarioCollection(Collection<CncrmUsuario> cncrmUsuarioCollection) {
        this.cncrmUsuarioCollection = cncrmUsuarioCollection;
    }
    
}
