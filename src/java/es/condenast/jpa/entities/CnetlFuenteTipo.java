/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnetl_fuente_tipo")
@NamedQueries({
    @NamedQuery(name = "CnetlFuenteTipo.findAll", query = "SELECT c FROM CnetlFuenteTipo c"),
    @NamedQuery(name = "CnetlFuenteTipo.findByIdFuenteTipo", query = "SELECT c FROM CnetlFuenteTipo c WHERE c.idFuenteTipo = :idFuenteTipo"),
    @NamedQuery(name = "CnetlFuenteTipo.findByNombre", query = "SELECT c FROM CnetlFuenteTipo c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "CnetlFuenteTipo.findByFieldTerminated", query = "SELECT c FROM CnetlFuenteTipo c WHERE c.fieldTerminated = :fieldTerminated"),
    @NamedQuery(name = "CnetlFuenteTipo.findByFieldEnclosed", query = "SELECT c FROM CnetlFuenteTipo c WHERE c.fieldEnclosed = :fieldEnclosed"),
    @NamedQuery(name = "CnetlFuenteTipo.findByLineTerminated", query = "SELECT c FROM CnetlFuenteTipo c WHERE c.lineTerminated = :lineTerminated"),
    @NamedQuery(name = "CnetlFuenteTipo.findByLineIgnore", query = "SELECT c FROM CnetlFuenteTipo c WHERE c.lineIgnore = :lineIgnore")})
public class CnetlFuenteTipo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdFuenteTipo")
    private Short idFuenteTipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "FieldTerminated")
    private String fieldTerminated;
    @Basic(optional = true)
    @Column(name = "FieldEnclosed")
    private String fieldEnclosed;
    @Basic(optional = true)
    @Column(name = "FieldScaped")
    private String fieldScaped;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "LineTerminated")
    private String lineTerminated;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LineIgnore")
    private int lineIgnore;
    @JoinColumn(name = "IdFormato", referencedColumnName = "IdFormato")
    @ManyToOne(optional = false)
    private CnetlFormato idFormato;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idFuenteTipo")
    private Collection<CnetlFuente> cnetlFuenteCollection;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnetlFuenteTipo")
    @OrderBy("orden ASC")
    private Collection<CnetlFuenteTipoOrigen> cnetlFuenteTipoOrigenCollection;

    public CnetlFuenteTipo() {
    }

    public CnetlFuenteTipo(Short idFuenteTipo) {
        this.idFuenteTipo = idFuenteTipo;
    }

    public CnetlFuenteTipo(Short idFuenteTipo, String nombre, String fieldTerminated, String fieldEnclosed, String lineTerminated, int lineIgnore) {
        this.idFuenteTipo = idFuenteTipo;
        this.nombre = nombre;
        this.fieldTerminated = fieldTerminated;
        this.fieldEnclosed = fieldEnclosed;
        this.lineTerminated = lineTerminated;
        this.lineIgnore = lineIgnore;
    }

    public Short getIdFuenteTipo() {
        return idFuenteTipo;
    }

    public void setIdFuenteTipo(Short idFuenteTipo) {
        this.idFuenteTipo = idFuenteTipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFieldTerminated() {
        return fieldTerminated;
    }

    public void setFieldTerminated(String fieldTerminated) {
        this.fieldTerminated = fieldTerminated;
    }

    public String getFieldEnclosed() {
        return fieldEnclosed;
    }

    public void setFieldEnclosed(String fieldEnclosed) {
        this.fieldEnclosed = fieldEnclosed;
    }

    public String getFieldScaped() {
        return fieldScaped;
    }

    public void setFieldScaped(String fieldScaped) {
        this.fieldScaped = fieldScaped;
    }

    public String getLineTerminated() {
        return lineTerminated;
    }

    public void setLineTerminated(String lineTerminated) {
        this.lineTerminated = lineTerminated;
    }

    public int getLineIgnore() {
        return lineIgnore;
    }

    public void setLineIgnore(int lineIgnore) {
        this.lineIgnore = lineIgnore;
    }

    public CnetlFormato getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(CnetlFormato idFormato) {
        this.idFormato = idFormato;
    }

    public Collection<CnetlFuente> getCnetlFuenteCollection() {
        return cnetlFuenteCollection;
    }

    public void setCnetlFuenteCollection(Collection<CnetlFuente> cnetlFuenteCollection) {
        this.cnetlFuenteCollection = cnetlFuenteCollection;
    }

    public Collection<CnetlFuenteTipoOrigen> getCnetlFuenteTipoOrigenCollection() {
        return cnetlFuenteTipoOrigenCollection;
    }

    public void setCnetlFuenteTipoOrigenCollection(Collection<CnetlFuenteTipoOrigen> cnetlFuenteTipoOrigenCollection) {
        this.cnetlFuenteTipoOrigenCollection = cnetlFuenteTipoOrigenCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFuenteTipo != null ? idFuenteTipo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlFuenteTipo)) {
            return false;
        }
        CnetlFuenteTipo other = (CnetlFuenteTipo) object;
        if ((this.idFuenteTipo == null && other.idFuenteTipo != null) || (this.idFuenteTipo != null && !this.idFuenteTipo.equals(other.idFuenteTipo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre;
    }

}
