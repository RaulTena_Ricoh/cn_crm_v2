/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ivan.fernandez
 */
@Entity
@Table(name = "cnmas_ocupacion_tipo")
@NamedQueries({
    @NamedQuery(name = "CnmasOcupacionTipo.findAll", query = "SELECT c FROM CnmasOcupacionTipo c"),
    @NamedQuery(name = "CnmasOcupacionTipo.findByIdProductoCategoria", query = "SELECT c FROM CnmasOcupacionTipo c WHERE c.idOcupacionTipo = :idOcupacionTipo"),
    @NamedQuery(name = "CnmasOcupacionTipo.findByNombre", query = "SELECT c FROM CnmasOcupacionTipo c WHERE c.nombre = :nombre")})
public class CnmasOcupacionTipo implements Serializable{
     private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdOcupacionTipo")
    private Short idOcupacionTipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idOcupacionTipo")
    private Collection<CnmasOcupacion> cnmasOcupacionCllection;

    /**
     * @return the idOcupacionTipo
     */
    public Short getIdOcupacionTipo() {
        return idOcupacionTipo;
    }

    /**
     * @param idOcupacionTipo the idOcupacionTipo to set
     */
    public void setIdOcupacionTipo(Short idOcupacionTipo) {
        this.idOcupacionTipo = idOcupacionTipo;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the cnmasOcupacionCllection
     */
    public Collection<CnmasOcupacion> getCnmasOcupacionCllection() {
        return cnmasOcupacionCllection;
    }

    /**
     * @param cnmasOcupacionCllection the cnmasOcupacionCllection to set
     */
    public void setCnmasOcupacionCllection(Collection<CnmasOcupacion> cnmasOcupacionCllection) {
        this.cnmasOcupacionCllection = cnmasOcupacionCllection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idOcupacionTipo != null ? idOcupacionTipo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasOcupacionTipo)) {
            return false;
        }
        CnmasOcupacionTipo other = (CnmasOcupacionTipo) object;
        if ((this.idOcupacionTipo == null && other.idOcupacionTipo != null) || (this.idOcupacionTipo != null && !this.idOcupacionTipo.equals(other.idOcupacionTipo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
