/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cnmas_consumo")
@NamedQueries({
    @NamedQuery(name = "CnmasConsumo.findAll", query = "SELECT c FROM CnmasConsumo c ORDER BY c.idConsumo"),
    @NamedQuery(name = "CnmasConsumo.findByIdConsumo", query = "SELECT c FROM CnmasConsumo c WHERE c.idConsumo = :idConsumo"),
    @NamedQuery(name = "CnmasConsumo.findByNombre", query = "SELECT c FROM CnmasConsumo c WHERE c.nombre = :nombre")})
public class CnmasConsumo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdConsumo")
    private Short idConsumo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;

    public CnmasConsumo() {
    }

    public CnmasConsumo(Short idConsumo) {
        this.idConsumo = idConsumo;
    }

    public CnmasConsumo(Short idConsumo, String nombre) {
        this.idConsumo = idConsumo;
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public Short getIdConsumo() {
        return idConsumo;
    }

    /**
     *
     * @param idConsumo
     */
    public void setIdConsumo(Short idConsumo) {
        this.idConsumo = idConsumo;
    }

    /**
     *
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idConsumo != null ? idConsumo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if(!(object instanceof CnmasConsumo)) {
            return false;
        }
        CnmasConsumo other = (CnmasConsumo) object;
        if((this.idConsumo == null && other.idConsumo != null) || (this.idConsumo != null && !this.idConsumo.equals(other.idConsumo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasConsumo[ idConsumo=" + idConsumo + " ]";
    }
    
}
