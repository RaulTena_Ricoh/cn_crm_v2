/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnmas_nivel_ingreso")
@NamedQueries({
    @NamedQuery(name = "CnmasNivelIngreso.findAll", query = "SELECT c FROM CnmasNivelIngreso c"),
    @NamedQuery(name = "CnmasNivelIngreso.findAllOrderByDesde", query = "SELECT c FROM CnmasNivelIngreso c ORDER BY c.desde"),
    @NamedQuery(name = "CnmasNivelIngreso.findByIdNivelIngreso", query = "SELECT c FROM CnmasNivelIngreso c WHERE c.idNivelIngreso = :idNivelIngreso"),
    @NamedQuery(name = "CnmasNivelIngreso.findByNombre", query = "SELECT c FROM CnmasNivelIngreso c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "CnmasNivelIngreso.findByDesde", query = "SELECT c FROM CnmasNivelIngreso c WHERE c.desde = :desde"),
    @NamedQuery(name = "CnmasNivelIngreso.findByHasta", query = "SELECT c FROM CnmasNivelIngreso c WHERE c.hasta = :hasta")})
public class CnmasNivelIngreso implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdNivelIngreso")
    private Short idNivelIngreso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Nombre")
    private String nombre;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Desde")
    private BigDecimal desde;
    @Column(name = "Hasta")
    private BigDecimal hasta;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnmasNivelIngreso")
    private Collection<CncrmPersona> cncrmPersonaCollection;
    
    public CnmasNivelIngreso() {
    }

    public CnmasNivelIngreso(Short idNivelIngreso) {
        this.idNivelIngreso = idNivelIngreso;
    }

    public CnmasNivelIngreso(Short idNivelIngreso, String nombre) {
        this.idNivelIngreso = idNivelIngreso;
        this.nombre = nombre;
    }

    public Short getIdNivelIngreso() {
        return idNivelIngreso;
    }

    public void setIdNivelIngreso(Short idNivelIngreso) {
        this.idNivelIngreso = idNivelIngreso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigDecimal getDesde() {
        return desde;
    }

    public void setDesde(BigDecimal desde) {
        this.desde = desde;
    }

    public BigDecimal getHasta() {
        return hasta;
    }

    public void setHasta(BigDecimal hasta) {
        this.hasta = hasta;
    }

    public Collection<CncrmPersona> getCncrmPersonaCollection() {
        return cncrmPersonaCollection;
    }

    public void setCncrmPersonaCollection(Collection<CncrmPersona> cncrmPersonaCollection) {
        this.cncrmPersonaCollection = cncrmPersonaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNivelIngreso != null ? idNivelIngreso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnmasNivelIngreso)) {
            return false;
        }
        CnmasNivelIngreso other = (CnmasNivelIngreso) object;
        if ((this.idNivelIngreso == null && other.idNivelIngreso != null) || (this.idNivelIngreso != null && !this.idNivelIngreso.equals(other.idNivelIngreso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnmasNivelIngreso[ idNivelIngreso=" + idNivelIngreso + " ]";
    }
    
}
