/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cnetl_pool_log_validacion")
@NamedQueries({
    @NamedQuery(name = "CnetlPoolLogValidacion.findAll", query = "SELECT c FROM CnetlPoolLogValidacion c"),
    @NamedQuery(name = "CnetlPoolLogValidacion.findByIdPool", query = "SELECT c FROM CnetlPoolLogValidacion c WHERE c.cnetlPoolLogValidacionPK.idPool = :idPool"),
    @NamedQuery(name = "CnetlPoolLogValidacion.findByIdValidacion", query = "SELECT c FROM CnetlPoolLogValidacion c WHERE c.cnetlPoolLogValidacionPK.idValidacion = :idValidacion"),
    @NamedQuery(name = "CnetlPoolLogValidacion.findByLineasOk", query = "SELECT c FROM CnetlPoolLogValidacion c WHERE c.lineasOk = :lineasOk"),
    @NamedQuery(name = "CnetlPoolLogValidacion.findByLineasError", query = "SELECT c FROM CnetlPoolLogValidacion c WHERE c.lineasError = :lineasError"),
    @NamedQuery(name = "CnetlPoolLogValidacion.findByFecha", query = "SELECT c FROM CnetlPoolLogValidacion c WHERE c.fecha = :fecha")})
public class CnetlPoolLogValidacion implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CnetlPoolLogValidacionPK cnetlPoolLogValidacionPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LineasOk")
    private int lineasOk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LineasError")
    private int lineasError;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @JoinColumn(name = "IdPool", referencedColumnName = "IdPool", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnetlPoolLog cnetlPoolLog;
    
    public CnetlPoolLogValidacion() {
    }

    public CnetlPoolLogValidacion(CnetlPoolLogValidacionPK cnetlPoolLogValidacionPK) {
        this.cnetlPoolLogValidacionPK = cnetlPoolLogValidacionPK;
    }

    public CnetlPoolLogValidacion(CnetlPoolLogValidacionPK cnetlPoolLogValidacionPK, int lineasOk, int lineasError, Date fecha) {
        this.cnetlPoolLogValidacionPK = cnetlPoolLogValidacionPK;
        this.lineasOk = lineasOk;
        this.lineasError = lineasError;
        this.fecha = fecha;
    }

    public CnetlPoolLogValidacion(long idPool, int idValidacion) {
        this.cnetlPoolLogValidacionPK = new CnetlPoolLogValidacionPK(idPool, idValidacion);
    }

    public CnetlPoolLogValidacionPK getCnetlPoolLogValidacionPK() {
        return cnetlPoolLogValidacionPK;
    }

    public void setCnetlPoolLogValidacionPK(CnetlPoolLogValidacionPK cnetlPoolLogValidacionPK) {
        this.cnetlPoolLogValidacionPK = cnetlPoolLogValidacionPK;
    }

    public int getLineasOk() {
        return lineasOk;
    }

    public void setLineasOk(int lineasOk) {
        this.lineasOk = lineasOk;
    }

    public int getLineasError() {
        return lineasError;
    }

    public void setLineasError(int lineasError) {
        this.lineasError = lineasError;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public CnetlPoolLog getCnetlPoolLog() {
        return cnetlPoolLog;
    }

    public void setCnetlPoolLog(CnetlPoolLog cnetlPoolLog) {
        this.cnetlPoolLog = cnetlPoolLog;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cnetlPoolLogValidacionPK != null ? cnetlPoolLogValidacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlPoolLogValidacion)) {
            return false;
        }
        CnetlPoolLogValidacion other = (CnetlPoolLogValidacion) object;
        if ((this.cnetlPoolLogValidacionPK == null && other.cnetlPoolLogValidacionPK != null) || (this.cnetlPoolLogValidacionPK != null && !this.cnetlPoolLogValidacionPK.equals(other.cnetlPoolLogValidacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnetlPoolLogValidacion[ cnetlPoolLogValidacionPK=" + cnetlPoolLogValidacionPK + " ]";
    }
    
}
