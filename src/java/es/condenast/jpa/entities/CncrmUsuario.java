/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Javier.Castro
 */
@Entity
@Table(name = "cncrm_usuario")
@NamedQueries({
    @NamedQuery(name = "CncrmUsuario.findAll", query = "SELECT c FROM CncrmUsuario c"),
    @NamedQuery(name = "CncrmUsuario.findByIdUsuario", query = "SELECT c FROM CncrmUsuario c WHERE c.idUsuario = :idUsuario"),
    @NamedQuery(name = "CncrmUsuario.findByEmail", query = "SELECT c FROM CncrmUsuario c WHERE c.email = :email"),
    @NamedQuery(name = "CncrmUsuario.findByEsEmailAuto", query = "SELECT c FROM CncrmUsuario c WHERE c.esEmailAuto = :esEmailAuto"),
    @NamedQuery(name = "CncrmUsuario.findByFechaAltaPrimera", query = "SELECT c FROM CncrmUsuario c WHERE c.fechaAltaPrimera = :fechaAltaPrimera"),
    @NamedQuery(name = "CncrmUsuario.findByFechaAlta", query = "SELECT c FROM CncrmUsuario c WHERE c.fechaAlta = :fechaAlta"),
    @NamedQuery(name = "CncrmUsuario.findByFechaBaja", query = "SELECT c FROM CncrmUsuario c WHERE c.fechaBaja = :fechaBaja"),
    @NamedQuery(name = "CncrmUsuario.findByFechaCambio", query = "SELECT c FROM CncrmUsuario c WHERE c.fechaCambio = :fechaCambio"),
    @NamedQuery(name = "CncrmUsuario.deleteByEmail", query = "DELETE FROM CncrmUsuario c WHERE c.email = :email"),
    @NamedQuery(name = "CncrmUsuario.searchUser", query = "SELECT c FROM CncrmUsuario c WHERE c.idUsuario IN :Ids"),
    @NamedQuery(name = "CncrmUsuario.countSearchUser", query = "SELECT count(c.idUsuario) FROM CncrmUsuario c WHERE c.idUsuario IN :Ids"),
    @NamedQuery(name = "CncrmUsuario.deleteByIdUsuario", query = "DELETE FROM CncrmUsuario c WHERE c.idUsuario = :idUsuario")})
public class CncrmUsuario implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdUsuario")
    private Long idUsuario;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EsEmailAuto")
    private boolean esEmailAuto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaAltaPrimera")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAltaPrimera;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaAlta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAlta;
    @Column(name = "FechaBaja")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaBaja;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaCambio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCambio;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cncrmUsuario")
    private Collection<CncrmUsuarioMarca> cncrmUsuarioMarcaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cncrmUsuario")
    private Collection<CncrmUsuarioMarcaAfin> cncrmUsuarioMarcaAfinCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cncrmUsuario")
    private Collection<CncrmUsuarioMarcaProducto> cncrmUsuarioMarcaProductoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cncrmUsuario")
    private Collection<CncrmUsuarioParticipacion> cncrmUsuarioParticipacionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cncrmUsuario")
    private Collection<CncrmUsuarioMarcaCondicionLegal> cncrmUsuarioMarcaCondicionLegalCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cncrmUsuario")
    private Collection<CncrmUsuarioMarketingAccion> cncrmUsuarioMarketingAccionCollection;
    @JoinColumn(name = "IdUsuarioTipo", referencedColumnName = "IdUsuarioTipo")
    @ManyToOne(optional = false)
    private CnmasUsuarioTipo idUsuarioTipo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cncrmUsuario")
    private Collection<CncrmUsuarioRedSocial> cncrmUsuarioRedSocialCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cncrmUsuario")
    private Collection<CncrmUsuarioTipoDispositivoDigital> cncrmUsuarioTipoDispositivoDigitalCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cncrmUsuario")
    private Collection<CncrmTelefono> cncrmTelefonoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cncrmUsuario")
    private Collection<CncrmDireccion> cncrmDireccionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cncrmUsuario")
    private Collection<CncrmUsuarioCompraOnline> cncrmUsuarioCompraOnlineCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cncrmUsuario")
    private Collection<CncrmUsuarioConsumo> cncrmUsuarioConsumoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cncrmUsuario")
    private Collection<CncrmEcommerce> cncrmEcommerceCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cncrmUsuario")
    private Collection<CncrmUsuarioPlataformaDigital> cncrmUsuarioPlataformaDigitalCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cncrmUsuario")
    private Collection<CncrmUsuarioRevista> cncrmUsuarioRevistaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cncrmUsuario")
    private Collection<CncrmUsuarioInteres> cncrmUsuarioInteresCollection;
    
    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private CncrmPersona cncrmPersona;

    public CncrmUsuario() {
    }

    public CncrmUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public CncrmUsuario(Long idUsuario, String email, boolean esEmailAuto, Date fechaAltaPrimera, Date fechaAlta, Date fechaCambio) {
        this.idUsuario = idUsuario;
        this.email = email;
        this.esEmailAuto = esEmailAuto;
        this.fechaAltaPrimera = fechaAltaPrimera;
        this.fechaAlta = fechaAlta;
        this.fechaCambio = fechaCambio;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getEsEmailAuto() {
        return esEmailAuto;
    }

    public void setEsEmailAuto(boolean esEmailAuto) {
        this.esEmailAuto = esEmailAuto;
    }

    public Date getFechaAltaPrimera() {
        return fechaAltaPrimera;
    }

    public void setFechaAltaPrimera(Date fechaAltaPrimera) {
        this.fechaAltaPrimera = fechaAltaPrimera;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public Date getFechaCambio() {
        return fechaCambio;
    }

    public void setFechaCambio(Date fechaCambio) {
        this.fechaCambio = fechaCambio;
    }

    public Collection<CncrmUsuarioMarca> getCncrmUsuarioMarcaCollection() {
        return cncrmUsuarioMarcaCollection;
    }

    public void setCncrmUsuarioMarcaCollection(Collection<CncrmUsuarioMarca> cncrmUsuarioMarcaCollection) {
        this.cncrmUsuarioMarcaCollection = cncrmUsuarioMarcaCollection;
    }

    public Collection<CncrmUsuarioMarcaAfin> getCncrmUsuarioMarcaAfinCollection() {
        return cncrmUsuarioMarcaAfinCollection;
    }

    public void setCncrmUsuarioMarcaAfinCollection(Collection<CncrmUsuarioMarcaAfin> cncrmUsuarioMarcaAfinCollection) {
        this.cncrmUsuarioMarcaAfinCollection = cncrmUsuarioMarcaAfinCollection;
    }

    public Collection<CncrmUsuarioMarcaProducto> getCncrmUsuarioMarcaProductoCollection() {
        return cncrmUsuarioMarcaProductoCollection;
    }

    public void setCncrmUsuarioMarcaProductoCollection(Collection<CncrmUsuarioMarcaProducto> cncrmUsuarioMarcaProductoCollection) {
        this.cncrmUsuarioMarcaProductoCollection = cncrmUsuarioMarcaProductoCollection;
    }

    /**
     * @return the cncrmUsuarioParticipacionCollection
     */
    public Collection<CncrmUsuarioParticipacion> getCncrmUsuarioParticipacionCollection() {
        return cncrmUsuarioParticipacionCollection;
    }

    /**
     * @param cncrmUsuarioParticipacionCollection the cncrmUsuarioParticipacionCollection to set
     */
    public void setCncrmUsuarioParticipacionCollection(Collection<CncrmUsuarioParticipacion> cncrmUsuarioParticipacionCollection) {
        this.cncrmUsuarioParticipacionCollection = cncrmUsuarioParticipacionCollection;
    }

    public Collection<CncrmUsuarioMarcaCondicionLegal> getCncrmUsuarioMarcaCondicionLegalCollection() {
        return cncrmUsuarioMarcaCondicionLegalCollection;
    }

    public void setCncrmUsuarioMarcaCondicionLegalCollection(Collection<CncrmUsuarioMarcaCondicionLegal> cncrmUsuarioMarcaCondicionLegalCollection) {
        this.cncrmUsuarioMarcaCondicionLegalCollection = cncrmUsuarioMarcaCondicionLegalCollection;
    }

    public Collection<CncrmUsuarioMarketingAccion> getCncrmUsuarioMarketingAccionCollection() {
        return cncrmUsuarioMarketingAccionCollection;
    }

    public void setCncrmUsuarioMarketingAccionCollection(Collection<CncrmUsuarioMarketingAccion> cncrmUsuarioMarketingAccionCollection) {
        this.cncrmUsuarioMarketingAccionCollection = cncrmUsuarioMarketingAccionCollection;
    }    
    
    public CnmasUsuarioTipo getIdUsuarioTipo() {
        return idUsuarioTipo;
    }

    public void setIdUsuarioTipo(CnmasUsuarioTipo idUsuarioTipo) {
        this.idUsuarioTipo = idUsuarioTipo;
    }

    public Collection<CncrmUsuarioRedSocial> getCncrmUsuarioRedSocialCollection() {
        return cncrmUsuarioRedSocialCollection;
    }

    public void setCncrmUsuarioRedSocialCollection(Collection<CncrmUsuarioRedSocial> cncrmUsuarioRedSocialCollection) {
        this.cncrmUsuarioRedSocialCollection = cncrmUsuarioRedSocialCollection;
    }

    public Collection<CncrmUsuarioTipoDispositivoDigital> getCncrmUsuarioTipoDispositivoDigitalCollection() {
        return cncrmUsuarioTipoDispositivoDigitalCollection;
    }

    public void setCncrmUsuarioTipoDispositivoDigitalCollection(Collection<CncrmUsuarioTipoDispositivoDigital> cncrmUsuarioTipoDispositivoDigitalCollection) {
        this.cncrmUsuarioTipoDispositivoDigitalCollection = cncrmUsuarioTipoDispositivoDigitalCollection;
    }
    
    public CncrmPersona getCncrmPersona() {
        return cncrmPersona;
    }

    public void setCncrmPersona(CncrmPersona cncrmPersona) {
        this.cncrmPersona = cncrmPersona;
    }

    public Collection<CncrmTelefono> getCncrmTelefonoCollection() {
        return cncrmTelefonoCollection;
    }

    public void setCncrmTelefonoCollection(Collection<CncrmTelefono> cncrmTelefonoCollection) {
        this.cncrmTelefonoCollection = cncrmTelefonoCollection;
    }

    public Collection<CncrmDireccion> getCncrmDireccionCollection() {
        return cncrmDireccionCollection;
    }

    public void setCncrmDireccionCollection(Collection<CncrmDireccion> cncrmDireccionCollection) {
        this.cncrmDireccionCollection = cncrmDireccionCollection;
    }

    public Collection<CncrmUsuarioCompraOnline> getCncrmUsuarioCompraOnlineCollection() {
        return cncrmUsuarioCompraOnlineCollection;
    }

    public void setCncrmUsuarioCompraOnlineCollection(Collection<CncrmUsuarioCompraOnline> cncrmUsuarioCompraOnlineCollection) {
        this.cncrmUsuarioCompraOnlineCollection = cncrmUsuarioCompraOnlineCollection;
    }

    public Collection<CncrmUsuarioConsumo> getCncrmUsuarioConsumoCollection() {
        return cncrmUsuarioConsumoCollection;
    }

    public void setCncrmUsuarioConsumoCollection(Collection<CncrmUsuarioConsumo> cncrmUsuarioConsumoCollection) {
        this.cncrmUsuarioConsumoCollection = cncrmUsuarioConsumoCollection;
    }

    public Collection<CncrmEcommerce> getCncrmEcommerceCollection() {
        return cncrmEcommerceCollection;
    }

    public void setCncrmEcommerceCollection(Collection<CncrmEcommerce> cncrmEcommerceCollection) {
        this.cncrmEcommerceCollection = cncrmEcommerceCollection;
    }

    public Collection<CncrmUsuarioPlataformaDigital> getCncrmUsuarioPlataformaDigitalCollection() {
        return cncrmUsuarioPlataformaDigitalCollection;
    }

    public void setCncrmUsuarioPlataformaDigitalCollection(Collection<CncrmUsuarioPlataformaDigital> cncrmUsuarioPlataformaDigitalCollection) {
        this.cncrmUsuarioPlataformaDigitalCollection = cncrmUsuarioPlataformaDigitalCollection;
    }

    public Collection<CncrmUsuarioRevista> getCncrmUsuarioRevistaCollection() {
        return cncrmUsuarioRevistaCollection;
    }

    public void setCncrmUsuarioRevistaCollection(Collection<CncrmUsuarioRevista> cncrmUsuarioRevistaCollection) {
        this.cncrmUsuarioRevistaCollection = cncrmUsuarioRevistaCollection;
    }

    public Collection<CncrmUsuarioInteres> getCncrmUsuarioInteresCollection() {
        return cncrmUsuarioInteresCollection;
    }

    public void setCncrmUsuarioInteresCollection(Collection<CncrmUsuarioInteres> cncrmUsuarioInteresCollection) {
        this.cncrmUsuarioInteresCollection = cncrmUsuarioInteresCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuario != null ? idUsuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CncrmUsuario)) {
            return false;
        }
        CncrmUsuario other = (CncrmUsuario) object;
        if ((this.idUsuario == null && other.idUsuario != null) || (this.idUsuario != null && !this.idUsuario.equals(other.idUsuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuario[ idUsuario=" + idUsuario + " ]";
    }
    
}
