/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cncrm_usuario_plataforma_digital")
@NamedQueries({
    @NamedQuery(name = "CncrmUsuarioPlataformaDigital.findAll", query = "SELECT c FROM CncrmUsuarioPlataformaDigital c"),
    @NamedQuery(name = "CncrmUsuarioPlataformaDigital.findByIdUsuario", query = "SELECT c FROM CncrmUsuarioPlataformaDigital c WHERE c.cncrmUsuarioPlataformaDigitalPK.idUsuario = :idUsuario"),
    @NamedQuery(name = "CncrmUsuarioPlataformaDigital.findByIdPlataformaDigital", query = "SELECT c FROM CncrmUsuarioPlataformaDigital c WHERE c.cncrmUsuarioPlataformaDigitalPK.idPlataformaDigital = :idPlataformaDigital")})
public class CncrmUsuarioPlataformaDigital implements Serializable {
    
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @EmbeddedId
    protected CncrmUsuarioPlataformaDigitalPK cncrmUsuarioPlataformaDigitalPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    
    @JoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CncrmUsuario cncrmUsuario;
    @JoinColumn(name = "IdPlataformaDigital", referencedColumnName = "IdPlataformaDigital", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnmasPlataformaDigital cnmasPlataformaDigital;

    /**
     *
     */
    public CncrmUsuarioPlataformaDigital() {
    }

    /**
     *
     * @param cncrmUsuarioPlataformaDigitalPK
     */
    public CncrmUsuarioPlataformaDigital(CncrmUsuarioPlataformaDigitalPK cncrmUsuarioPlataformaDigitalPK) {
        this.cncrmUsuarioPlataformaDigitalPK = cncrmUsuarioPlataformaDigitalPK;
    }

    /**
     *
     * @param cncrmUsuarioPlataformaDigitalPK
     * @param fecha
     */
    public CncrmUsuarioPlataformaDigital(CncrmUsuarioPlataformaDigitalPK cncrmUsuarioPlataformaDigitalPK, Date fecha) {
        this.cncrmUsuarioPlataformaDigitalPK = cncrmUsuarioPlataformaDigitalPK;
        this.fecha = fecha;
    }

    /**
     *
     * @param idUsuario
     * @param idPlataformaDigital
     */
    public CncrmUsuarioPlataformaDigital(Long idUsuario, short idPlataformaDigital) {
        this.cncrmUsuarioPlataformaDigitalPK = new CncrmUsuarioPlataformaDigitalPK(idUsuario, idPlataformaDigital);
    }

    /**
     *
     * @return
     */
    public CncrmUsuarioPlataformaDigitalPK getCncrmUsuarioPlataformaDigitalPK() {
        return cncrmUsuarioPlataformaDigitalPK;
    }

    /**
     *
     * @param cncrmUsuarioPlataformaDigitalPK
     */
    public void setCncrmUsuarioPlataformaDigitalPK(CncrmUsuarioPlataformaDigitalPK cncrmUsuarioPlataformaDigitalPK) {
        this.cncrmUsuarioPlataformaDigitalPK = cncrmUsuarioPlataformaDigitalPK;
    }

    /**
     *
     * @return
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     *
     * @param fecha
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     *
     * @return
     */
    public CncrmUsuario getCncrmUsuario() {
        return cncrmUsuario;
    }

    /**
     *
     * @param cncrmUsuario
     */
    public void setCncrmUsuario(CncrmUsuario cncrmUsuario) {
        this.cncrmUsuario = cncrmUsuario;
    }

    /**
     *
     * @return
     */
    public CnmasPlataformaDigital getCnmasPlataformaDigital() {
        return cnmasPlataformaDigital;
    }

    /**
     *
     * @param cnmasPlataformaDigital
     */
    public void setCnmasPlataformaDigital(CnmasPlataformaDigital cnmasPlataformaDigital) {
        this.cnmasPlataformaDigital = cnmasPlataformaDigital;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += this.cncrmUsuarioPlataformaDigitalPK != null ? this.cncrmUsuarioPlataformaDigitalPK.hashCode() : 0;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CncrmUsuarioPlataformaDigital)) {
            return false;
        }
        CncrmUsuarioPlataformaDigital other = (CncrmUsuarioPlataformaDigital) object;
        if ((this.cncrmUsuarioPlataformaDigitalPK == null && other.cncrmUsuarioPlataformaDigitalPK != null) || (this.cncrmUsuarioPlataformaDigitalPK != null && !this.cncrmUsuarioPlataformaDigitalPK.equals(other.cncrmUsuarioPlataformaDigitalPK))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CncrmUsuarioPlataformaDigital[ cncrmUsuarioPlataformaDigitalPK=" + cncrmUsuarioPlataformaDigitalPK + " ]";
    }
    
}
