/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import static javax.persistence.ParameterMode.IN;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Raul.Tena
 */
@Entity
@Table(name = "cnetl_pool_master")
@NamedQueries({
    @NamedQuery(name = "CnetlPoolMaster.findAll", query = "SELECT c FROM CnetlPoolMaster c"),
    @NamedQuery(name = "CnetlPoolMaster.findByIdPool", query = "SELECT c FROM CnetlPoolMaster c WHERE c.idPoolMaster = :idPoolMaster"),
    @NamedQuery(name = "CnetlPoolMaster.findByArchivo", query = "SELECT c FROM CnetlPoolMaster c WHERE c.archivo = :archivo"),
    @NamedQuery(name = "CnetlPoolMaster.findByError", query = "SELECT c FROM CnetlPoolMaster c WHERE c.error = :error"),
    @NamedQuery(name = "CnetlPoolMaster.findByEsAutomatico", query = "SELECT c FROM CnetlPoolMaster c WHERE c.esAutomatico = :esAutomatico"),
    @NamedQuery(name = "CnetlPoolMaster.getEstado", query = "SELECT c.idEstado FROM CnetlPoolMaster c WHERE c.idPoolMaster = :idPoolMaster"),
    @NamedQuery(name = "CnetlPoolMaster.findByIdEstado", query = "SELECT c FROM CnetlPoolMaster c WHERE c.idEstado = :idEstado")})

@NamedStoredProcedureQueries({
    @NamedStoredProcedureQuery(name = "CnetlPoolMaster.SPETL_MASTER_BUILD", procedureName = "SPETL_MASTER_BUILD"),
    @NamedStoredProcedureQuery(name = "CnetlPoolMaster.SPETL_MASTER_QUERY", procedureName = "SPETL_MASTER_QUERY",
            parameters = {
                @StoredProcedureParameter(mode = IN, name = "P_IdMaster", type = Integer.class),
                @StoredProcedureParameter(mode = IN, name = "P_File", type = String.class),
                @StoredProcedureParameter(mode = IN, name = "P_File_Ok", type = String.class),
                @StoredProcedureParameter(mode = IN, name = "P_File_Error", type = String.class),
                @StoredProcedureParameter(mode = IN, name = "P_EsAutomatico", type = Boolean.class)
            }),
    @NamedStoredProcedureQuery(name = "CnetlPoolMaster.SPETL_MASTER_EXECUTE", procedureName = "SPETL_MASTER_EXECUTE",
            parameters = {
                @StoredProcedureParameter(mode = IN, name = "P_IdPoolMaster", type = Long.class)
            })
})
public class CnetlPoolMaster implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdPoolMaster")
    private Long idPoolMaster;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "Query")
    private String query;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Archivo")
    private String archivo;
    @Size(max = 200)
    @Column(name = "Error")
    private String error;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EsAutomatico")
    private boolean esAutomatico;
//    @JoinColumn(name = "IdEstado", referencedColumnName = "IdEstado")
////    @ManyToOne(optional = false)
//    private CnetlEstado idEstado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdEstado")
    private Short idEstado;

    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "ArchivoOk")
    private String archivoOk;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "ArchivoError")
    private String archivoError;

    public CnetlPoolMaster() {
    }

    public CnetlPoolMaster(Long idPoolMaster) {
        this.idPoolMaster = idPoolMaster;
    }

    public CnetlPoolMaster(Long idPoolMaster, String query, String archivo, boolean esAutomatico) {
        this.idPoolMaster = idPoolMaster;
        this.query = query;
        this.archivo = archivo;
        this.esAutomatico = esAutomatico;
    }

    public Long getIdPoolMaster() {
        return idPoolMaster;
    }

    public void setIdPoolMaster(Long idPoolMaster) {
        this.idPoolMaster = idPoolMaster;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public boolean getEsAutomatico() {
        return esAutomatico;
    }

    public void setEsAutomatico(boolean esAutomatico) {
        this.esAutomatico = esAutomatico;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPoolMaster != null ? idPoolMaster.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnetlPoolMaster)) {
            return false;
        }
        CnetlPoolMaster other = (CnetlPoolMaster) object;
        if ((this.idPoolMaster == null && other.idPoolMaster != null) || (this.idPoolMaster != null && !this.idPoolMaster.equals(other.idPoolMaster))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.condenast.jpa.entities.CnetlPoolMaster[ idPoolMaster=" + idPoolMaster + " ]";
    }

//    public CnetlEstado getIdEstado() {
//        return idEstado;
//    }
//
//    public void setIdEstado(CnetlEstado idEstado) {
//        this.idEstado = idEstado;
//    }
    public Short getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Short idEstado) {
        this.idEstado = idEstado;
    }

    
    
    
    public String getArchivoOk() {
        return archivoOk;
    }

    public void setArchivoOk(String archivoOk) {
        this.archivoOk = archivoOk;
    }

    public String getArchivoError() {
        return archivoError;
    }

    public void setArchivoError(String archivoError) {
        this.archivoError = archivoError;
    }

}
