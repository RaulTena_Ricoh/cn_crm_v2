/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.ingesta.entities;

import java.util.Date;

/**
 *
 * @author Javier.Castro
 */
public class LogValidacion {

    public long idPool;
    public int idValidacion;
    public String validacion;
    public String fecha;
    public int lineasOk;
    public int lineasError;

    public LogValidacion() {
    }

    public long getIdPool() {
        return idPool;
    }

    public void setIdPool(long idPool) {
        this.idPool = idPool;
    }

    public int getIdValidacion() {
        return idValidacion;
    }

    public void setIdValidacion(int idValidacion) {
        this.idValidacion = idValidacion;
    }

    public String getValidacion() {
        return validacion;
    }

    public void setValidacion(String validacion) {
        this.validacion = validacion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getLineasOk() {
        return lineasOk;
    }

    public void setLineasOk(int lineasOk) {
        this.lineasOk = lineasOk;
    }

    public int getLineasError() {
        return lineasError;
    }

    public void setLineasError(int lineasError) {
        this.lineasError = lineasError;
    }


    
    

  

}
