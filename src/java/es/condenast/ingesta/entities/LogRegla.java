/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.ingesta.entities;

import java.util.Date;

/**
 *
 * @author Javier.Castro
 */
public class LogRegla {

    public long idPool;
    public int idRegla;
    public String regla;
    public String inicio;
    public String fin;
    public String duracion;

    public LogRegla() {
    }
    
    

    public LogRegla(long idPool, int idRegla, String regla, String inicio, String fin, String duracion) {
        this.idPool = idPool;
        this.idRegla = idRegla;
        this.regla = regla;
        this.inicio = inicio;
        this.fin = fin;
        this.duracion = duracion;
    }
    
    
    

    public long getIdPool() {
        return idPool;
    }

    public void setIdPool(long idPool) {
        this.idPool = idPool;
    }

    public int getIdRegla() {
        return idRegla;
    }

    public void setIdRegla(int idRegla) {
        this.idRegla = idRegla;
    }

    public String getRegla() {
        return regla;
    }

    public void setRegla(String regla) {
        this.regla = regla;
    }

    public String getInicio() {
        return inicio;
    }

    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

    public String getFin() {
        return fin;
    }

    public void setFin(String fin) {
        this.fin = fin;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }
    
    

}
