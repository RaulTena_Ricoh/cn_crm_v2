/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.ingesta.entities;

import java.util.List;

public class Log {

    public long IdPool;
    public String Fuente;
    public String FuenteTipo;
    public String fichero;
    public String Estado;
    public String InicioPool;
    public String LoadPool;
    public String FinPool;
    public Integer Lineas;
    public Integer LineasOk;
    public Integer LineasError;
    public String DuracionPool;
    public String DuracionLoad;
    public String Error;

    List<LogRegla> reglas;
    List<LogTransformacion> tranformaciones;
    List<LogValidacion> validaciones;
    List<LogValidacionDetalle> validacionesDetalle;
    List<LogWarning> warnings;

    public Log() {
    }

    public Log(long IdPool, String Fuente, String FuenteTipo, String Estado, String InicioPool, String LoadPool, String FinPool, int Lineas, int LineasOk, int LineasError, String DuracionPool, String DuracionLoad) {
        this.IdPool = IdPool;
        this.Fuente = Fuente;
        this.FuenteTipo = FuenteTipo;
        this.Estado = Estado;
        this.InicioPool = InicioPool;
        this.LoadPool = LoadPool;
        this.FinPool = FinPool;
        this.Lineas = Lineas;
        this.LineasOk = LineasOk;
        this.LineasError = LineasError;
        this.DuracionPool = DuracionPool;
        this.DuracionLoad = DuracionLoad;
    }

    public long getIdPool() {
        return IdPool;
    }

    public void setIdPool(long IdPool) {
        this.IdPool = IdPool;
    }

    public String getFuente() {
        return Fuente;
    }

    public void setFuente(String Fuente) {
        this.Fuente = Fuente;
    }

    public String getFuenteTipo() {
        return FuenteTipo;
    }

    public void setFuenteTipo(String FuenteTipo) {
        this.FuenteTipo = FuenteTipo;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public String getInicioPool() {
        return InicioPool;
    }

    public void setInicioPool(String InicioPool) {
        this.InicioPool = InicioPool;
    }

    public String getLoadPool() {
        return LoadPool;
    }

    public void setLoadPool(String LoadPool) {
        this.LoadPool = LoadPool;
    }

    public String getFinPool() {
        return FinPool;
    }

    public void setFinPool(String FinPool) {
        this.FinPool = FinPool;
    }

    public int getLineas() {
        return Lineas;
    }

    public void setLineas(Integer Lineas) {
        this.Lineas = Lineas;
    }

    public Integer getLineasOk() {
        return LineasOk;
    }

    public void setLineasOk(Integer LineasOk) {
        this.LineasOk = LineasOk;
    }

    public Integer getLineasError() {
        return LineasError;
    }

    public void setLineasError(Integer LineasError) {
        this.LineasError = LineasError;
    }

    public String getDuracionPool() {
        return DuracionPool;
    }

    public void setDuracionPool(String DuracionPool) {
        this.DuracionPool = DuracionPool;
    }

    public String getDuracionLoad() {
        return DuracionLoad;
    }

    public void setDuracionLoad(String DuracionLoad) {
        this.DuracionLoad = DuracionLoad;
    }

    public List<LogRegla> getReglas() {
        return reglas;
    }

    public void setReglas(List<LogRegla> reglas) {
        this.reglas = reglas;
    }

    public List<LogTransformacion> getTranformaciones() {
        return tranformaciones;
    }

    public void setTranformaciones(List<LogTransformacion> tranformaciones) {
        this.tranformaciones = tranformaciones;
    }

    public List<LogValidacion> getValidaciones() {
        return validaciones;
    }

    public void setValidaciones(List<LogValidacion> validaciones) {
        this.validaciones = validaciones;
    }

    public List<LogValidacionDetalle> getValidacionesDetalle() {
        return validacionesDetalle;
    }

    public void setValidacionesDetalle(List<LogValidacionDetalle> validacionesDetalle) {
        this.validacionesDetalle = validacionesDetalle;
    }

    public String getError() {
        return Error;
    }

    public void setError(String Error) {
        this.Error = Error;
    }

    public List<LogWarning> getWarnings() {
        return warnings;
    }

    public void setWarnings(List<LogWarning> warnings) {
        this.warnings = warnings;
    }

    public String getFichero() {
        return fichero;
    }

    public void setFichero(String fichero) {
        this.fichero = fichero;
    }
    
    

}
