/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.ingesta.entities;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Javier.Castro
 */
public class LogTransformacion {

    public long idPool;
    public long idTransformacion;
    public String transformacion;
    public String inicio;
    public String fin;
    public String duracion;

    public LogTransformacion() {
    }

    public long getIdPool() {
        return idPool;
    }

    public void setIdPool(long idPool) {
        this.idPool = idPool;
    }

    public long getIdTransformacion() {
        return idTransformacion;
    }

    public void setIdTransformacion(long idTransformacion) {
        this.idTransformacion = idTransformacion;
    }

    public String getTransformacion() {
        return transformacion;
    }

    public void setTransformacion(String transformacion) {
        this.transformacion = transformacion;
    }

    public String getInicio() {
        return inicio;
    }

    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

    public String getFin() {
        return fin;
    }

    public void setFin(String fin) {
        this.fin = fin;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    

}
