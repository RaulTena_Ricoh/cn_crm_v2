/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.ingesta.entities;

/**
 *
 * @author Javier.Castro
 */
public class LogPlano {

    private static final String SEPARADOR = ";";
    public Log log;
    public LogRegla regla;
    public LogTransformacion transformacion;
    public LogValidacion validacion;
    public LogValidacionDetalle detalle;
    public LogWarning warning;

    public String toCSV() {

        StringBuilder fila = new StringBuilder();

        // Log
        fila.append(log.IdPool).append(SEPARADOR)
                .append(log.Fuente).append(SEPARADOR)
                .append(log.FuenteTipo).append(SEPARADOR)
                .append(log.fichero).append(SEPARADOR)
                .append(log.Estado).append(SEPARADOR)
                .append(log.InicioPool).append(SEPARADOR)
                .append(log.LoadPool).append(SEPARADOR)
                .append(log.FinPool).append(SEPARADOR)
                .append(log.Lineas).append(SEPARADOR)
                .append(log.LineasOk).append(SEPARADOR)
                .append(log.LineasError).append(SEPARADOR)
                .append(log.DuracionPool).append(SEPARADOR)
                .append(log.DuracionLoad);

        // LogRegla
        if (regla != null) {
            fila.append(SEPARADOR)
                    .append((regla.idPool == 0) ? "" : regla.idPool).append(SEPARADOR)
                    .append((regla.idRegla == 0) ? "" : regla.idRegla).append(SEPARADOR)
                    .append(regla.regla).append(SEPARADOR)
                    .append(regla.inicio).append(SEPARADOR)
                    .append(regla.fin).append(SEPARADOR)
                    .append(regla.duracion);
        } else {
            fila.append(SEPARADOR).append(SEPARADOR).append(SEPARADOR).append(SEPARADOR).append(SEPARADOR).append(SEPARADOR);
        }

        // LogTransformacion
        if (transformacion != null) {
            fila.append(SEPARADOR)
                    .append((transformacion.idPool == 0) ? "" : transformacion.idPool).append(SEPARADOR)
                    .append((transformacion.idTransformacion == 0) ? "" : transformacion.idTransformacion).append(SEPARADOR)
                    .append(transformacion.transformacion).append(SEPARADOR)
                    .append(transformacion.inicio).append(SEPARADOR)
                    .append(transformacion.fin).append(SEPARADOR)
                    .append(transformacion.duracion);
        } else {
            fila.append(SEPARADOR).append(SEPARADOR).append(SEPARADOR).append(SEPARADOR).append(SEPARADOR).append(SEPARADOR);
        }

        // LogValidacion
        if (validacion != null) {
            fila.append(SEPARADOR)
                    .append((validacion.idPool == 0) ? "" : validacion.idPool).append(SEPARADOR)
                    .append((validacion.idValidacion == 0) ? "" : validacion.idValidacion).append(SEPARADOR)
                    .append(validacion.validacion).append(SEPARADOR)
                    .append(validacion.fecha).append(SEPARADOR)
                    .append(validacion.lineasOk).append(SEPARADOR)
                    .append(validacion.lineasError);
        } else {
            fila.append(SEPARADOR).append(SEPARADOR).append(SEPARADOR).append(SEPARADOR).append(SEPARADOR).append(SEPARADOR);
        }

        // LogValidacionDetalle
        if (detalle != null) {
            fila.append(SEPARADOR)
                    .append((detalle.idPool == 0) ? "" : detalle.idPool).append(SEPARADOR)
                    .append((detalle.idValidacion == 0) ? "" : detalle.idValidacion).append(SEPARADOR)
                    .append(detalle.validacion).append(SEPARADOR)
                    .append(detalle.numLinea).append(SEPARADOR)
                    .append(detalle.valor);
        } else {
            fila.append(SEPARADOR).append(SEPARADOR).append(SEPARADOR).append(SEPARADOR).append(SEPARADOR);
        }

        //LogWarning
        if (warning != null) {
            fila.append(SEPARADOR)
                    .append(warning.level).append(SEPARADOR)
                    .append(warning.code).append(SEPARADOR)
                    .append(warning.message);
        } else {
            fila.append(SEPARADOR).append(SEPARADOR).append(SEPARADOR);
        }

        return fila.toString();
    }


}
