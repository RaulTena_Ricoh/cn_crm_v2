/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.ingesta;

import es.condenast.jpa.entities.CnetlFuente;
import es.condenast.jpa.entities.CnetlMaster;
import es.condenast.jpa.entities.CnetlPool;
import es.condenast.jpa.entities.CnetlPoolMaster;
import es.condenast.jpa.session.CnetlFuenteFacade;
import es.condenast.jpa.session.CnetlMasterFacade;
import es.condenast.jpa.session.CnetlPoolFacade;
import es.condenast.jpa.session.CnetlPoolLogWarningFacade;
import es.condenast.jpa.session.CnetlPoolMasterFacade;
import es.condenast.jpa.session.CnetlPoolMasterLogWarningFacade;
import es.condenast.jpa.session.VietlLogFacade;
import es.condenast.jpa.session.VietlLogReglaFacade;
import es.condenast.jpa.session.VietlLogTransformacionFacade;
import es.condenast.jpa.session.VietlLogValidacionFacade;
import es.condenast.jpa.session.VietlLogValidacionLineaFacade;

public class Proceso {

    private static CnetlPoolFacade ejbPoolFacade;
    private static CnetlFuenteFacade ejbFuenteFacade;
    private static CnetlPoolLogWarningFacade ejbLogWarningFacade;
    private static VietlLogFacade ejbLogFacade;
    private static VietlLogReglaFacade ejbLogReglaFacade;
    private static VietlLogTransformacionFacade ejbLogTransformacionFacade;
    private static VietlLogValidacionFacade ejbLogValidacionFacade;
    private static VietlLogValidacionLineaFacade ejbLogValidacionLineaFacade;

    private static CnetlPoolMasterFacade ejbPoolMasterFacade;
    private static CnetlMasterFacade ejbMasterFacade;
    private static CnetlPoolMasterLogWarningFacade ejbMasterLogWarningFacade;
    
    public static final int MANUAL = 0;
    public static final int AUTOMATICA = 1;
    public static final int MASTER_MANUAL = 2;
    public static final int MASTER_AUTO = 3;
    
    public static final int PROCESO_OK = 0;
    public static final int PROCESO_ERROR = 1;
    public static final int PROCESO_ERROR_CORREO = 2;

    private static int tipoIngesta;
    private static int resultado;
    

    public static void ingestaAutomatica() {
        setTipoIngesta(AUTOMATICA);
        Handler handler = new Handler(getPoolFacade(), getLogWarningFacade(), getFuenteFacade(), getLogFacade(),
                getLogReglaFacade(), getLogTransformacionFacade(), getLogValidacionFacade(), getLogValidacionLineaFacade());
        handler.procesarFuenteActivas();
        handler.generarTablasTemporales();
        handler.procesarPool();
        handler.reglasImportacion();
        handler.procesarMail();
    }

    public static void ingestaManual(CnetlFuente fuente, String file) {

        setTipoIngesta(MANUAL);
        Handler handler = new Handler(getPoolFacade(), getLogWarningFacade(), getFuenteFacade(), getLogFacade(),
                getLogReglaFacade(), getLogTransformacionFacade(), getLogValidacionFacade(), getLogValidacionLineaFacade());
        int idPool = handler.procesaFuenteFichero(fuente, file);
        handler.generarTablasTemporales();
        CnetlPool poolActivo = getPoolFacade().findByIdPool(new Long(idPool));
        handler.procesarPoolManual(poolActivo);
        handler.procesarMailManual(poolActivo);

    }

    public static void cargaMaster() {

        setTipoIngesta(MASTER_AUTO);
        HandlerMaster handler = new HandlerMaster(getPoolMasterFacade(), getMasterFacade(), getMasterLogWarningFacade());
        handler.procesarMasterActivos();
        handler.generarTablasTemporales();
        handler.procesarPool();
        handler.procesarMail();

    }
    
    public static void cargaMaster(CnetlMaster fuente, String file) {

        setTipoIngesta(MASTER_MANUAL);
        HandlerMaster handler = new HandlerMaster(getPoolMasterFacade(), getMasterFacade(), getMasterLogWarningFacade());
        int idPoolMaster = handler.procesaMasterFichero(fuente, file);
        handler.generarTablasTemporales();
        CnetlPoolMaster poolActivo = getPoolMasterFacade().findByIdPool(new Long(idPoolMaster));
        handler.procesarMasterManual(poolActivo);
        handler.procesarMailManual(poolActivo);

    }
    
    public static int getTipoIngesta() {
        return tipoIngesta;
    }

    public static void setTipoIngesta(int tipoIngesta) {
        Proceso.tipoIngesta = tipoIngesta;
    }

    public static int getResultado() {
        return resultado;
    }

    public static void setResultado(int resultado) {
        Proceso.resultado = resultado;
    }



    public static CnetlPoolFacade getPoolFacade() {
        return ejbPoolFacade;
    }

    public static void setPoolFacade(CnetlPoolFacade ejbPoolFacade) {
        Proceso.ejbPoolFacade = ejbPoolFacade;
    }

    public static CnetlFuenteFacade getFuenteFacade() {
        return ejbFuenteFacade;
    }

    public static void setFuenteFacade(CnetlFuenteFacade ejbFuenteFacade) {
        Proceso.ejbFuenteFacade = ejbFuenteFacade;
    }

    public static VietlLogFacade getLogFacade() {
        return ejbLogFacade;
    }

    public static void setLogFacade(VietlLogFacade ejbLogFacade) {
        Proceso.ejbLogFacade = ejbLogFacade;
    }

    public static VietlLogReglaFacade getLogReglaFacade() {
        return ejbLogReglaFacade;
    }

    public static void setLogReglaFacade(VietlLogReglaFacade ejbLogReglaFacade) {
        Proceso.ejbLogReglaFacade = ejbLogReglaFacade;
    }

    public static VietlLogTransformacionFacade getLogTransformacionFacade() {
        return ejbLogTransformacionFacade;
    }

    public static void setLogTransformacionFacade(VietlLogTransformacionFacade ejbLogTransformacionFacade) {
        Proceso.ejbLogTransformacionFacade = ejbLogTransformacionFacade;
    }

    public static VietlLogValidacionFacade getLogValidacionFacade() {
        return ejbLogValidacionFacade;
    }

    public static void setLogValidacionFacade(VietlLogValidacionFacade ejbLogValidacionFacade) {
        Proceso.ejbLogValidacionFacade = ejbLogValidacionFacade;
    }

    public static VietlLogValidacionLineaFacade getLogValidacionLineaFacade() {
        return ejbLogValidacionLineaFacade;
    }

    public static void setLogValidacionLineaFacade(VietlLogValidacionLineaFacade ejbLogValidacionLineaFacade) {
        Proceso.ejbLogValidacionLineaFacade = ejbLogValidacionLineaFacade;
    }

    public static CnetlPoolLogWarningFacade getLogWarningFacade() {
        return ejbLogWarningFacade;
    }

    public static void setLogWarningFacade(CnetlPoolLogWarningFacade ejbLogWarningFacade) {
        Proceso.ejbLogWarningFacade = ejbLogWarningFacade;
    }

    public static CnetlPoolMasterFacade getPoolMasterFacade() {
        return ejbPoolMasterFacade;
    }

    public static void setPoolMasterFacade(CnetlPoolMasterFacade ejbPoolMasterFacade) {
        Proceso.ejbPoolMasterFacade = ejbPoolMasterFacade;
    }

    public static CnetlMasterFacade getMasterFacade() {
        return ejbMasterFacade;
    }

    public static void setMasterFacade(CnetlMasterFacade ejbMasterFacade) {
        Proceso.ejbMasterFacade = ejbMasterFacade;
    }
    
    public static CnetlPoolMasterLogWarningFacade getMasterLogWarningFacade() {
        return ejbMasterLogWarningFacade;
    }

    public static void setMasterLogWarningFacade(CnetlPoolMasterLogWarningFacade ejbMasterLogWarningFacade) {
        Proceso.ejbMasterLogWarningFacade = ejbMasterLogWarningFacade;
    }
    
}
