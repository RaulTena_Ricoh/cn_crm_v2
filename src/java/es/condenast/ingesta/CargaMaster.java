/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.ingesta;

import es.condenast.jpa.session.CnetlMasterFacade;
import es.condenast.jpa.session.CnetlPoolMasterFacade;
import es.condenast.jpa.session.CnetlPoolMasterLogWarningFacade;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Raul.Tena
 */
public class CargaMaster {

    @EJB
    private static final es.condenast.jpa.session.CnetlMasterFacade ejbMasterFacade = new CnetlMasterFacade();

    @EJB
    private static final es.condenast.jpa.session.CnetlPoolMasterFacade ejbPoolMasterFacade = new CnetlPoolMasterFacade();

    @EJB
    private static final es.condenast.jpa.session.CnetlPoolMasterLogWarningFacade ejbMasterLogWarningFacade = new CnetlPoolMasterLogWarningFacade();
    
    public static void main(String args[]) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("CondeNastPU");
        EntityManager em = emf.createEntityManager();
        ejbMasterFacade.setEntityManager(em);
        ejbPoolMasterFacade.setEntityManager(em);
        ejbMasterLogWarningFacade.setEntityManager(em);

        Proceso.setMasterFacade(ejbMasterFacade);
        Proceso.setPoolMasterFacade(ejbPoolMasterFacade);
        Proceso.setMasterLogWarningFacade(ejbMasterLogWarningFacade);

        Proceso.cargaMaster();

    }

}
