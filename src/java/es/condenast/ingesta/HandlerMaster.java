/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.ingesta;

import es.condenast.ingesta.entities.Log;
import es.condenast.ingesta.entities.LogWarning;
import es.condenast.ingesta.helper.CSVUtils;
import es.condenast.ingesta.helper.FileUtils;
import es.condenast.ingesta.helper.Mail;
import es.condenast.ingesta.helper.Zip;
import es.condenast.jpa.entities.CnetlMaster;
import es.condenast.jpa.entities.CnetlPoolMaster;
import es.condenast.jpa.entities.CnetlPoolMasterLogWarning;
import es.condenast.jpa.session.CnetlMasterFacade;
import es.condenast.jpa.session.CnetlPoolMasterFacade;
import es.condenast.jpa.session.CnetlPoolMasterLogWarningFacade;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;

/**
 *
 * @author Raul.Tena
 */
public class HandlerMaster {
    
    private List<CnetlPoolMaster> poolesActivo;
    private static final String MAIL_SEND = "mail.send";
    
    private CnetlPoolMasterFacade ejbPoolFacade;
    private CnetlMasterFacade ejbMasterFacade;
    private CnetlPoolMasterLogWarningFacade ejbLogWarningFacade;

    public HandlerMaster(CnetlPoolMasterFacade ejbPoolFacade, CnetlMasterFacade ejbMasterFacade, CnetlPoolMasterLogWarningFacade ejbLogWarningFacade) {
        this.ejbPoolFacade = ejbPoolFacade;
        this.ejbMasterFacade = ejbMasterFacade;
        this.ejbLogWarningFacade = ejbLogWarningFacade;
    }
    
    private List<CnetlPoolMaster> getPoolesActivo() {
        if (poolesActivo == null) {
            this.poolesActivo = getPoolMasterFacade().findAllActivos();
        }
        return poolesActivo;
    }
    
    public void procesarMasterActivos() {
        System.out.println("INICIO - procesarMasterActivos");
        for (CnetlMaster master : getMasterFacade().findAllActivas()) {
            if (master.getEsFichero()) {
                procesaMasterFichero(master, master.getOrigen());
            } else {
                procesarMasterDirectorio(master);
            }
        }
        System.out.println("FIN - procesarMasterActivos");
    }
    
    public int procesaMasterFichero(CnetlMaster master, String file) {
        try {
            System.out.println("INICIO - procesaMasterFichero " + file);
            int idPool = getPoolMasterFacade().registrar(master.getIdMaster(), file, file + "_OK", file + "_Error", false);
            System.out.println("FIN - procesaMasterFichero " + file);
            return idPool;
        } catch (Exception ex) {
            System.out.println("FIN - procesaMasterFichero ERRONEO " + file);
            Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }
    
    private void procesarMasterDirectorio(CnetlMaster master) {
        System.out.println("INICIO - procesarMasterDirectorio " + master.getOrigen());
        File folder = new File(master.getOrigen());
        if (folder.exists() && folder.isDirectory()) {
            File[] ficheros = folder.listFiles();
            Arrays.sort(ficheros);
            for (File file : ficheros) {
                if (file.isFile()) {
                    procesarMasterFichero(master, file.getName());
                }
            }
        }
        System.out.println("FIN - procesarMasterDirectorio " + master.getOrigen());
    }
 
    private void procesarMasterFichero(CnetlMaster master, String file) {
        try {
            System.out.println("INICIO - procesarMasterFichero " + file);
            //String separador = FileSystems.getDefault().getSeparator();
            String separador = "//";

            File folderOK = new File(master.getOrigen() + separador + "OK");
            if (!folderOK.exists()) {
                folderOK.mkdir();
            }

            File folderERROR = new File(master.getOrigen() + separador + "ERROR");
            if (!folderERROR.exists()) {
                folderERROR.mkdir();
            }

            getPoolMasterFacade().registrar(master.getIdMaster(),
                    master.getOrigen() + separador + file,
                    master.getOrigen() + separador + "OK" + separador + file,
                    master.getOrigen() + separador + "ERROR" + separador + file, true);

            System.out.println("FIN - procesarMasterFichero " + file);
        } catch (Exception ex) {
            System.out.println("FIN - procesarMasterFichero ERRONEO" + file);
            Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    public void generarTablasTemporales() {
        try {
            System.out.println("INICIO - GenerarTablasTemporales");
            getPoolMasterFacade().generarTablasTemporales();
            System.out.println("FIN - GenerarTablasTemporales");
        } catch (Exception ex) {
            System.out.println("FIN - GenerarTablasTemporales ERRONEO ");
            Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void procesarPool() {
        System.out.println("INICIO - ProcesarPool - Pooles activos -> " + getPoolesActivo().size());

        for (CnetlPoolMaster pool : getPoolesActivo()) {
            try {

                if (FileUtils.exists(pool.getArchivo())) {

                    try {
                        System.out.println("INICIO - Pool " + pool.getIdPoolMaster());

                        System.out.println("         Pool -- executeQuery");
                        List<LogWarning> warnings = getPoolMasterFacade().executeQuery(pool.getQuery());
                        getLogWarningFacade().saveWarnings(pool.getIdPoolMaster(), warnings);

                        System.out.println("         Pool -- procesar");
                        getPoolMasterFacade().procesar(pool.getIdPoolMaster());

                    } catch (SQLException ex) {
                        Proceso.setResultado(Proceso.PROCESO_ERROR);
                        System.out.println("FIN - Pool " + pool.getIdPoolMaster() + " ERRONEO ");
                        Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
                        pool.setIdEstado(CnetlPoolMasterFacade.POOL_EXCEPCION);
                        pool.setError(ex.toString());
                        getPoolMasterFacade().edit(pool);
                    }

                    System.out.println("         Pool -- moverFichero");
                    getPoolMasterFacade().moverFichero(pool);
                    System.out.println("FIN - Pool " + pool.getIdPoolMaster());

                } else {
                    pool.setIdEstado(CnetlPoolMasterFacade.POOL_FILE_NO_EXISTE);
                    getPoolMasterFacade().edit(pool);
                }

            } catch (Exception ex) {
                Proceso.setResultado(Proceso.PROCESO_ERROR);
                pool.setError(ex.toString());
                getPoolMasterFacade().edit(pool);
                System.out.println("FIN - Pool " + pool.getIdPoolMaster() + " ERRONEO ");
                Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
            }

            System.out.println("FIN - ProcesarPool");
        }
    }

    public void procesarMail() {
        try {
            System.out.println("INICIO - procesarMail");
            ArrayList<Log> logs = new ArrayList<>();

            for (CnetlPoolMaster pool : getPoolesActivo()) {

                if (!pool.getIdEstado().equals(CnetlPoolMasterFacade.POOL_FILE_NO_EXISTE) && !pool.getIdEstado().equals(CnetlPoolMasterFacade.POOL_EXCEPCION)) {
                    System.out.println(" ----- PROCESAR MAIL POOL --> " + pool.getIdPoolMaster());

                    List<LogWarning> warnings = convertLogWarning(getLogWarningFacade().findByIdPoolMaster(pool.getIdPoolMaster()));
                    Log log = new Log();
                    log.setIdPool(pool.getIdPoolMaster());
                    log.setFuente("MASTER");
                    log.setEstado(Short.toString(pool.getIdEstado()));
                    log.setFichero(pool.getArchivo());
                    log.setWarnings(warnings);
                    
                    logs.add(log);
                }
            }

//             GENERACION DE XML
//            Serialize<Log> serializar = new Serialize<>();
//            String xml = serializar.SerializarList(logs);
//            File zip = FileUtils.createZipFile(xml,Zip.XML);
            
            // GENERACION CSV
            String csv = CSVUtils.convertCSV(logs);
            File zip = FileUtils.createZipFile(csv, Zip.CSV);
            
            boolean enviarMail = Boolean.valueOf(ResourceBundle.getBundle("resources/Configuration").getString(MAIL_SEND));
            if (enviarMail) {
                Mail.sendEmailWithAttachments(new String[]{zip.getAbsolutePath()}, logs);
                //FileUtils.delete(zip.getAbsolutePath());
            }
            System.out.println("FIN - procesarMail");

        } catch (IOException | MessagingException ex) {
            if (Proceso.getResultado() != Proceso.PROCESO_ERROR) {
                Proceso.setResultado(Proceso.PROCESO_ERROR_CORREO);
            }
            System.out.println("FIN - procesarMail ERRONEO ");
            Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public void procesarMasterManual(CnetlPoolMaster pool) {

        try {
            System.out.println("INICIO - ProcesarMasterManual ");

            System.out.println("INICIO - Pool " + pool.getIdPoolMaster());
            System.out.println("         Pool -- executeQuery");
            List<LogWarning> warnings = getPoolMasterFacade().executeQuery(pool.getQuery());
            getLogWarningFacade().saveWarnings(pool.getIdPoolMaster(), warnings);
            System.out.println("         Pool -- procesar");
            getPoolMasterFacade().procesar(pool.getIdPoolMaster());
            System.out.println("         Pool -- eliminarFichero");
            getPoolMasterFacade().eliminarFichero(pool);
            System.out.println("FIN - Pool " + pool.getIdPoolMaster());

            System.out.println("FIN - ProcesarMasterManual");
        } catch (SQLException ex) {
            Proceso.setResultado(Proceso.PROCESO_ERROR);
            System.out.println("FIN - ProcesarPoolManual ERRONEO");
            Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Proceso.setResultado(Proceso.PROCESO_ERROR);
            System.out.println("FIN - ProcesarPoolManual ERRONEO");
            Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);

        }
    }
    
    public void procesarMailManual(CnetlPoolMaster pool) {
        try {
            System.out.println("INICIO - procesarMail");

            boolean enviarMail = Boolean.valueOf(ResourceBundle.getBundle("resources/Configuration").getString(MAIL_SEND));
            if (enviarMail) {
                Mail.sendEmailMaster();
            }
            System.out.println("FIN - procesarMail");

        } catch (MessagingException ex) {
            if (Proceso.getResultado() != Proceso.PROCESO_ERROR) {
                Proceso.setResultado(Proceso.PROCESO_ERROR_CORREO);
            }
            System.out.println("FIN - procesarMail ERRONEO ");
            Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private List<LogWarning> convertLogWarning(List<CnetlPoolMasterLogWarning> poolWarning) {
        ArrayList<LogWarning> lista = new ArrayList<>();
        for (CnetlPoolMasterLogWarning item : poolWarning) {
            LogWarning log = new LogWarning();
            log.setCode(item.getCode());
            log.setLevel(item.getLevel());
            log.setMessage(item.getMessage());
            lista.add(log);
        }
        return lista;
    }
    
    public CnetlPoolMasterFacade getPoolMasterFacade() {
        return ejbPoolFacade;
    }

    public void setPoolMasterFacade(CnetlPoolMasterFacade ejbPoolFacade) {
        this.ejbPoolFacade = ejbPoolFacade;
    }

    public CnetlMasterFacade getMasterFacade() {
        return ejbMasterFacade;
    }

    public void setMasterFacade(CnetlMasterFacade ejbMasterFacade) {
        this.ejbMasterFacade = ejbMasterFacade;
    }

    public CnetlPoolMasterLogWarningFacade getLogWarningFacade() {
        return ejbLogWarningFacade;
    }

    public void setLogWarningFacade(CnetlPoolMasterLogWarningFacade ejbLogWarningFacade) {
        this.ejbLogWarningFacade = ejbLogWarningFacade;
    }
    
}
