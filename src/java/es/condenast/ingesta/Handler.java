/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.ingesta;

import es.condenast.ingesta.entities.Log;
import es.condenast.ingesta.entities.LogRegla;
import es.condenast.ingesta.entities.LogTransformacion;
import es.condenast.ingesta.entities.LogValidacion;
import es.condenast.ingesta.entities.LogValidacionDetalle;
import es.condenast.ingesta.entities.LogWarning;
import es.condenast.ingesta.helper.CSVUtils;
import es.condenast.ingesta.helper.FileUtils;
import es.condenast.ingesta.helper.Mail;
import es.condenast.ingesta.helper.TimeUtils;
import es.condenast.ingesta.helper.Zip;
import es.condenast.jpa.entities.CnetlFuente;
import es.condenast.jpa.entities.CnetlPool;
import es.condenast.jpa.entities.CnetlPoolLogWarning;
import es.condenast.jpa.entities.VietlLog;
import es.condenast.jpa.entities.VietlLogRegla;
import es.condenast.jpa.entities.VietlLogTransformacion;
import es.condenast.jpa.entities.VietlLogValidacion;
import es.condenast.jpa.entities.VietlLogValidacionLinea;
import es.condenast.jpa.session.CnetlFuenteFacade;
import es.condenast.jpa.session.CnetlPoolFacade;
import es.condenast.jpa.session.CnetlPoolLogWarningFacade;
import es.condenast.jpa.session.VietlLogFacade;
import es.condenast.jpa.session.VietlLogReglaFacade;
import es.condenast.jpa.session.VietlLogTransformacionFacade;
import es.condenast.jpa.session.VietlLogValidacionFacade;
import es.condenast.jpa.session.VietlLogValidacionLineaFacade;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;

public class Handler {

    private List<CnetlPool> poolesActivo;
    private static final String MAIL_SEND = "mail.send";

    private CnetlPoolFacade ejbPoolFacade;
    private CnetlFuenteFacade ejbFuenteFacade;
    private CnetlPoolLogWarningFacade ejbLogWarningFacade;
    private VietlLogFacade ejbLogFacade;
    private VietlLogReglaFacade ejbLogReglaFacade;
    private VietlLogTransformacionFacade ejbLogTransformacionFacade;
    private VietlLogValidacionFacade ejbLogValidacionFacadde;
    private VietlLogValidacionLineaFacade ejbLogValidacionLineaFacade;

    public Handler(CnetlPoolFacade ejbPoolFacade, CnetlPoolLogWarningFacade ejbLogWarningFacade, CnetlFuenteFacade ejbFuenteFacade, VietlLogFacade ejbLogFacade,
            VietlLogReglaFacade ejbLogReglaFacade, VietlLogTransformacionFacade ejbLogTransformacionFacade,
            VietlLogValidacionFacade ejbLogValidacionFacadde, VietlLogValidacionLineaFacade ejbLogValidacionLineaFacade) {
        this.ejbPoolFacade = ejbPoolFacade;
        this.ejbLogWarningFacade = ejbLogWarningFacade;
        this.ejbFuenteFacade = ejbFuenteFacade;
        this.ejbLogFacade = ejbLogFacade;
        this.ejbLogReglaFacade = ejbLogReglaFacade;
        this.ejbLogTransformacionFacade = ejbLogTransformacionFacade;
        this.ejbLogValidacionFacadde = ejbLogValidacionFacadde;
        this.ejbLogValidacionLineaFacade = ejbLogValidacionLineaFacade;
    }

    private List<CnetlPool> getPoolesActivo() {
        if (poolesActivo == null) {
            this.poolesActivo = getPoolFacade().findAllActivos();
        }
        return poolesActivo;
    }

    public void procesarFuenteActivas() {
        System.out.println("INICIO - ProcesarFuenteActivas");
        for (CnetlFuente fuente : getFuenteFacade().findAllActivas()) {
            if (fuente.getEsFichero()) {
                procesaFuenteFichero(fuente, fuente.getOrigen());
            } else {
                procesarFuenteDirectorio(fuente);
            }
        }
        System.out.println("FIN - ProcesarFuenteActivas");
    }

    public void generarTablasTemporales() {
        try {
            System.out.println("INICIO - GenerarTablasTemporales");
            getPoolFacade().generarTablasTemporales();
            System.out.println("FIN - GenerarTablasTemporales");
        } catch (Exception ex) {
            System.out.println("FIN - GenerarTablasTemporales ERRONEO ");
            Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void optimizar() {
        try {
            System.out.println("INICIO - Optimizar");
            getPoolFacade().optimize();
            System.out.println("FIN - Optimizar");
        } catch (Exception ex) {
            System.out.println("FIN - Optimizar ERRONEO");
            Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void procesarPoolManual(CnetlPool pool) {

        try {
            System.out.println("INICIO - ProcesarPoolManual ");

            System.out.println("INICIO - Pool " + pool.getIdPool());
            System.out.println("         Pool -- executeQuery");
            List<LogWarning> warnings = getPoolFacade().executeQuery(pool.getQuery());
            getLogWarningFacade().saveWarnings(pool.getIdPool(), warnings);
            System.out.println("         Pool -- procesar");
            getPoolFacade().procesar(pool.getIdPool());
            System.out.println("         Pool -- eliminarFichero");
            getPoolFacade().eliminarFichero(pool);
            System.out.println("FIN - Pool " + pool.getIdPool());

            System.out.println("FIN - ProcesarPoolManual");
        } catch (SQLException ex) {
            Proceso.setResultado(Proceso.PROCESO_ERROR);
            System.out.println("FIN - ProcesarPoolManual ERRONEO");
            Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Proceso.setResultado(Proceso.PROCESO_ERROR);
            System.out.println("FIN - ProcesarPoolManual ERRONEO");
            Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    public void procesarPool() {
        System.out.println("INICIO - ProcesarPool - Pooles activos -> " + getPoolesActivo().size());

        for (CnetlPool pool : getPoolesActivo()) {
            try {

                if (FileUtils.exists(pool.getArchivo())) {

                    try {
                        System.out.println("INICIO - Pool " + pool.getIdPool());

                        System.out.println("         Pool -- executeQuery");
                        List<LogWarning> warnings = getPoolFacade().executeQuery(pool.getQuery());
                        getLogWarningFacade().saveWarnings(pool.getIdPool(), warnings);

                        System.out.println("         Pool -- procesar");
                        getPoolFacade().procesar(pool.getIdPool());

                    } catch (SQLException ex) {
                        Proceso.setResultado(Proceso.PROCESO_ERROR);
                        System.out.println("FIN - Pool " + pool.getIdPool() + " ERRONEO ");
                        Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
                        pool.setIdEstado(CnetlPoolFacade.POOL_EXCEPCION);
                        pool.setError(ex.toString());
                        getPoolFacade().edit(pool);
                    }

                    System.out.println("         Pool -- moverFichero");
                    getPoolFacade().moverFichero(pool);
                    System.out.println("FIN - Pool " + pool.getIdPool());

                } else {
                    pool.setIdEstado(CnetlPoolFacade.POOL_FILE_NO_EXISTE);
                    getPoolFacade().edit(pool);
                }

            } catch (Exception ex) {
                Proceso.setResultado(Proceso.PROCESO_ERROR);
                pool.setError(ex.toString());
                getPoolFacade().edit(pool);
                System.out.println("FIN - Pool " + pool.getIdPool() + " ERRONEO ");
                Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
            }

            System.out.println("FIN - ProcesarPool");
        }
    }

    public void procesarMailManual(CnetlPool pool) {
        try {
            System.out.println("INICIO - procesarMail");
            ArrayList<Log> logs = new ArrayList<>();

            if (!pool.getIdEstado().equals(CnetlPoolFacade.POOL_FILE_NO_EXISTE) && !pool.getIdEstado().equals(CnetlPoolFacade.POOL_EXCEPCION)) {
                System.out.println(" ----- PROCESAR MAIL POOL --> " + pool.getIdPool());

                ArrayList<VietlLog> poolLogs = (ArrayList<VietlLog>) getLogFacade().findByIdPool(pool.getIdPool());
                for (VietlLog poolLog : poolLogs) {
                    Log log = convertVietlLog(poolLog, pool);
                    log.setReglas(convertVietlLogRegla(getLogReglaFacade().findByIdPool(pool.getIdPool())));
                    log.setTranformaciones(convertVietlLogTransformacion(getLogTransformacionFacade().findByIdPool(pool.getIdPool())));
                    log.setValidaciones(convertVietlValidacion(getLogValidacionFacadde().findByIdPool(pool.getIdPool())));
                    log.setValidacionesDetalle(convertVietlValidacionLinea(getLogValidacionLineaFacade().findByIdPool(pool.getIdPool())));
                    log.setWarnings(convertLogWarning(getLogWarningFacade().findByIdPool(pool.getIdPool())));
                    logs.add(log);
                }
            }

            // GENERACION DE XML
//            Serialize<Log> serializar = new Serialize<>();
//            String xml = serializar.SerializarList(logs);
//            File zip = FileUtils.createZipFile(xml,Zip.XML);
            
            // GENERACION CSV
            String csv = CSVUtils.convertCSV(logs);
            File zip = FileUtils.createZipFile(csv, Zip.CSV);

            boolean enviarMail = Boolean.valueOf(ResourceBundle.getBundle("resources/Configuration").getString(MAIL_SEND));
            if (enviarMail) {
                Mail.sendEmailWithAttachments(new String[]{zip.getAbsolutePath()}, logs);
                //FileUtils.delete(zip.getAbsolutePath());
            }
            System.out.println("FIN - procesarMail");

        } catch (IOException | MessagingException ex) {
            if (Proceso.getResultado() != Proceso.PROCESO_ERROR) {
                Proceso.setResultado(Proceso.PROCESO_ERROR_CORREO);
            }
            System.out.println("FIN - procesarMail ERRONEO ");
            Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void procesarMail() {
        try {
            System.out.println("INICIO - procesarMail");
            ArrayList<Log> logs = new ArrayList<>();

            for (CnetlPool pool : getPoolesActivo()) {

                if (!pool.getIdEstado().equals(CnetlPoolFacade.POOL_FILE_NO_EXISTE) && !pool.getIdEstado().equals(CnetlPoolFacade.POOL_EXCEPCION)) {
                    System.out.println(" ----- PROCESAR MAIL POOL --> " + pool.getIdPool());

                    ArrayList<VietlLog> poolLogs = (ArrayList<VietlLog>) getLogFacade().findByIdPool(pool.getIdPool());
                    for (VietlLog poolLog : poolLogs) {
                        Log log = convertVietlLog(poolLog, pool);
                        log.setReglas(convertVietlLogRegla(getLogReglaFacade().findByIdPool(pool.getIdPool())));
                        log.setTranformaciones(convertVietlLogTransformacion(getLogTransformacionFacade().findByIdPool(pool.getIdPool())));
                        log.setValidaciones(convertVietlValidacion(getLogValidacionFacadde().findByIdPool(pool.getIdPool())));
                        log.setValidacionesDetalle(convertVietlValidacionLinea(getLogValidacionLineaFacade().findByIdPool(pool.getIdPool())));
                        log.setWarnings(convertLogWarning(getLogWarningFacade().findByIdPool(pool.getIdPool())));
                        logs.add(log);
                    }
                }
            }

//             GENERACION DE XML
//            Serialize<Log> serializar = new Serialize<>();
//            String xml = serializar.SerializarList(logs);
//            File zip = FileUtils.createZipFile(xml,Zip.XML);
            
            // GENERACION CSV
            String csv = CSVUtils.convertCSV(logs);
            File zip = FileUtils.createZipFile(csv, Zip.CSV);
            
            boolean enviarMail = Boolean.valueOf(ResourceBundle.getBundle("resources/Configuration").getString(MAIL_SEND));
            if (enviarMail) {
                Mail.sendEmailWithAttachments(new String[]{zip.getAbsolutePath()}, logs);
                //FileUtils.delete(zip.getAbsolutePath());
            }
            System.out.println("FIN - procesarMail");

        } catch (IOException | MessagingException ex) {
            if (Proceso.getResultado() != Proceso.PROCESO_ERROR) {
                Proceso.setResultado(Proceso.PROCESO_ERROR_CORREO);
            }
            System.out.println("FIN - procesarMail ERRONEO ");
            Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void procesarFuenteDirectorio(CnetlFuente fuente) {
        System.out.println("INICIO - ProcesarFuenteDirectorio " + fuente.getOrigen());
        File folder = new File(fuente.getOrigen());
        if (folder.exists() && folder.isDirectory()) {
            File[] ficheros = folder.listFiles();
            Arrays.sort(ficheros);
            for (File file : ficheros) {
                if (file.isFile()) {
                    procesarFuenteFichero(fuente, file.getName());
                }
            }
        }
        System.out.println("FIN - ProcesarFuenteDirectorio " + fuente.getOrigen());
    }

    public int procesaFuenteFichero(CnetlFuente fuente, String file) {
        try {
            System.out.println("INICIO - procesaFuenteFichero " + file);
            int idPool = getPoolFacade().registrar(fuente.getIdFuente(), file, file + "_OK", file + "_Error", false);
            System.out.println("FIN - procesaFuenteFichero " + file);
            return idPool;
        } catch (Exception ex) {
            System.out.println("FIN - procesaFuenteFichero ERRONEO " + file);
            Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }

    private void procesarFuenteFichero(CnetlFuente fuente, String file) {
        try {
            System.out.println("INICIO - procesaFuenteFichero " + file);
            //String separador = FileSystems.getDefault().getSeparator();
            String separador = "//";

            File folderOK = new File(fuente.getOrigen() + separador + "OK");
            if (!folderOK.exists()) {
                folderOK.mkdir();
            }

            File folderERROR = new File(fuente.getOrigen() + separador + "ERROR");
            if (!folderERROR.exists()) {
                folderERROR.mkdir();
            }

            getPoolFacade().registrar(fuente.getIdFuente(),
                    fuente.getOrigen() + separador + file,
                    fuente.getOrigen() + separador + "OK" + separador + file,
                    fuente.getOrigen() + separador + "ERROR" + separador + file, true);

            System.out.println("FIN - procesaFuenteFichero " + file);
        } catch (Exception ex) {
            System.out.println("FIN - procesaFuenteFichero ERRONEO" + file);
            Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void reglasImportacion() {
        System.out.println("INICIO - reglasImportacion ");
        if (!getPoolesActivo().isEmpty()) {
            try {
                getPoolFacade().reglasImportacion(getPoolesActivo());
                System.out.println("FIN - reglasImportacion ");
            } catch (Exception ex) {
                Proceso.setResultado(Proceso.PROCESO_ERROR);
                System.out.println("FIN - reglasImportacion ERRONEO ");
                Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.println("FIN - reglasImportacion. No hay pooles activos");
        }
    }

    public CnetlPoolFacade getPoolFacade() {
        return ejbPoolFacade;
    }

    public void setPoolFacade(CnetlPoolFacade ejbPoolFacade) {
        this.ejbPoolFacade = ejbPoolFacade;
    }

    public CnetlFuenteFacade getFuenteFacade() {
        return ejbFuenteFacade;
    }

    public void setFuenteFacade(CnetlFuenteFacade ejbFuenteFacade) {
        this.ejbFuenteFacade = ejbFuenteFacade;
    }

    public VietlLogFacade getLogFacade() {
        return ejbLogFacade;
    }

    public void setLogFacade(VietlLogFacade ejbLogFacade) {
        this.ejbLogFacade = ejbLogFacade;
    }

    public VietlLogReglaFacade getLogReglaFacade() {
        return ejbLogReglaFacade;
    }

    public void setLogReglaFacade(VietlLogReglaFacade ejbLogReglaFacade) {
        this.ejbLogReglaFacade = ejbLogReglaFacade;
    }

    public VietlLogTransformacionFacade getLogTransformacionFacade() {
        return ejbLogTransformacionFacade;
    }

    public void setLogTransformacionFacade(VietlLogTransformacionFacade ejbLogTransformacionFacade) {
        this.ejbLogTransformacionFacade = ejbLogTransformacionFacade;
    }

    public VietlLogValidacionFacade getLogValidacionFacadde() {
        return ejbLogValidacionFacadde;
    }

    public void setLogValidacionFacadde(VietlLogValidacionFacade ejbLogValidacionFacadde) {
        this.ejbLogValidacionFacadde = ejbLogValidacionFacadde;
    }

    public VietlLogValidacionLineaFacade getLogValidacionLineaFacade() {
        return ejbLogValidacionLineaFacade;
    }

    public void setLogValidacionLineaFacade(VietlLogValidacionLineaFacade ejbLogValidacionLineaFacade) {
        this.ejbLogValidacionLineaFacade = ejbLogValidacionLineaFacade;
    }

    public CnetlPoolLogWarningFacade getLogWarningFacade() {
        return ejbLogWarningFacade;
    }

    public void setLogWarningFacade(CnetlPoolLogWarningFacade ejbLogWarningFacade) {
        this.ejbLogWarningFacade = ejbLogWarningFacade;
    }

    private Log convertVietlLog(VietlLog vietlLog, CnetlPool pool) {

        Log log = new Log();
        log.setIdPool(vietlLog.getIdPool());
        log.setFuente(vietlLog.getFuente());
        log.setFuenteTipo(vietlLog.getFuenteTipo());
        log.setFichero(pool.getArchivo());
        log.setEstado(vietlLog.getEstado());
        log.setInicioPool(TimeUtils.convertUTC2(vietlLog.getInicioPool()));
        log.setLoadPool(TimeUtils.convertUTC2(vietlLog.getLoadPool()));
        log.setFinPool(TimeUtils.convertUTC2(vietlLog.getFinPool()));
        log.setLineas(vietlLog.getLineas());
        log.setLineasOk(vietlLog.getLineasOK());
        log.setLineasError(vietlLog.getLineasError());
        log.setDuracionPool(TimeUtils.convertDateDuration(vietlLog.getInicioPool(), vietlLog.getFinPool()));
        log.setDuracionLoad(TimeUtils.convertDateDuration(vietlLog.getInicioPool(), vietlLog.getLoadPool()));
        log.setError(vietlLog.getError());

        return log;
    }

    private List<LogRegla> convertVietlLogRegla(List<VietlLogRegla> logsRegla) {
        ArrayList<LogRegla> lista = new ArrayList<>();
        for (VietlLogRegla item : logsRegla) {
            LogRegla regla = new LogRegla();
            regla.setIdPool(item.getIdPool());
            regla.setIdRegla(item.getIdReglaImportacion());
            regla.setRegla(item.getRegla());
            regla.setInicio(TimeUtils.convertUTC2(item.getFechaInicio()));
            regla.setFin(TimeUtils.convertUTC2(item.getFechaFin()));
            regla.setDuracion(TimeUtils.convertDateDuration(item.getFechaInicio(), item.getFechaFin()));
            lista.add(regla);
        }
        return lista;
    }

    private List<LogTransformacion> convertVietlLogTransformacion(List<VietlLogTransformacion> transformaciones) {
        ArrayList<LogTransformacion> lista = new ArrayList<>();
        for (VietlLogTransformacion item : transformaciones) {
            LogTransformacion transformacion = new LogTransformacion();
            transformacion.setIdPool(item.getIdPool());
            transformacion.setIdTransformacion(item.getIdTransformacion());
            transformacion.setTransformacion(item.getTransformacion());
            transformacion.setInicio(TimeUtils.convertUTC2(item.getInicioTransformacion()));
            transformacion.setFin(TimeUtils.convertUTC2(item.getFinTransformacion()));
            transformacion.setDuracion(TimeUtils.convertDateDuration(item.getInicioTransformacion(), item.getFinTransformacion()));
            lista.add(transformacion);
        }
        return lista;
    }

    private List<LogValidacion> convertVietlValidacion(List<VietlLogValidacion> validaciones) {
        ArrayList<LogValidacion> lista = new ArrayList<>();
        for (VietlLogValidacion item : validaciones) {
            LogValidacion validacion = new LogValidacion();
            validacion.setIdPool(item.getIdPool());
            validacion.setIdValidacion(item.getIdValidacion());
            validacion.setValidacion(item.getValidacion());
            validacion.setFecha(TimeUtils.convertUTC2(item.getFechaValidacion()));
            validacion.setLineasOk(item.getLineasOkValidacion());
            validacion.setLineasError(item.getLineasErrorValidacion());
            lista.add(validacion);
        }
        return lista;
    }

    private List<LogValidacionDetalle> convertVietlValidacionLinea(List<VietlLogValidacionLinea> validacionesLinea) {
        ArrayList<LogValidacionDetalle> lista = new ArrayList<>();
        for (VietlLogValidacionLinea item : validacionesLinea) {
            LogValidacionDetalle detalle = new LogValidacionDetalle();
            detalle.setIdPool(item.getIdPool());
            detalle.setIdValidacion(item.getIdValidacion());
            detalle.setValidacion(item.getValidacion());
            detalle.setNumLinea(item.getLinea());
            detalle.setValor(item.getValor());
            lista.add(detalle);
        }
        return lista;
    }

    private List<LogWarning> convertLogWarning(List<CnetlPoolLogWarning> poolWarning) {
        ArrayList<LogWarning> lista = new ArrayList<>();
        for (CnetlPoolLogWarning item : poolWarning) {
            LogWarning log = new LogWarning();
            log.setCode(item.getCode());
            log.setLevel(item.getLevel());
            log.setMessage(item.getMessage());
            lista.add(log);
        }
        return lista;
    }

}
