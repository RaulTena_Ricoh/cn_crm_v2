/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.ingesta;

import es.condenast.jpa.session.CnetlFuenteFacade;
import es.condenast.jpa.session.CnetlPoolFacade;
import es.condenast.jpa.session.CnetlPoolLogWarningFacade;
import es.condenast.jpa.session.VietlLogFacade;
import es.condenast.jpa.session.VietlLogReglaFacade;
import es.condenast.jpa.session.VietlLogTransformacionFacade;
import es.condenast.jpa.session.VietlLogValidacionFacade;
import es.condenast.jpa.session.VietlLogValidacionLineaFacade;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class IngestaAutomatica {

    @EJB
    private static final es.condenast.jpa.session.CnetlFuenteFacade ejbFuenteFacade = new CnetlFuenteFacade();

    @EJB
    private static final es.condenast.jpa.session.CnetlPoolFacade ejbPoolFacade = new CnetlPoolFacade();

    @EJB
    private static final es.condenast.jpa.session.CnetlPoolLogWarningFacade ejbLogWarningFacade = new CnetlPoolLogWarningFacade();

    @EJB
    private static final es.condenast.jpa.session.VietlLogFacade ejbLogFacade = new VietlLogFacade();

    @EJB
    private static final es.condenast.jpa.session.VietlLogReglaFacade ejbLogReglaFacade = new VietlLogReglaFacade();

    @EJB
    private static final es.condenast.jpa.session.VietlLogTransformacionFacade ejbLogTransformacionFacade = new VietlLogTransformacionFacade();

    @EJB
    private static final es.condenast.jpa.session.VietlLogValidacionFacade ejbLogValidacionFacade = new VietlLogValidacionFacade();

    @EJB
    private static final es.condenast.jpa.session.VietlLogValidacionLineaFacade ejbLogValidacionLineaFacade = new VietlLogValidacionLineaFacade();

    public static void main(String args[]) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("CondeNastPU");
        EntityManager em = emf.createEntityManager();
        ejbFuenteFacade.setEntityManager(em);
        ejbPoolFacade.setEntityManager(em);
        ejbLogFacade.setEntityManager(em);
        ejbLogReglaFacade.setEntityManager(em);
        ejbLogTransformacionFacade.setEntityManager(em);
        ejbLogValidacionLineaFacade.setEntityManager(em);
        ejbLogValidacionFacade.setEntityManager(em);
        ejbLogWarningFacade.setEntityManager(em);

        Proceso.setFuenteFacade(ejbFuenteFacade);
        Proceso.setPoolFacade(ejbPoolFacade);
        Proceso.setLogFacade(ejbLogFacade);
        Proceso.setLogReglaFacade(ejbLogReglaFacade);
        Proceso.setLogTransformacionFacade(ejbLogTransformacionFacade);
        Proceso.setLogValidacionLineaFacade(ejbLogValidacionLineaFacade);
        Proceso.setLogValidacionFacade(ejbLogValidacionFacade);
        Proceso.setLogWarningFacade(ejbLogWarningFacade);

        Proceso.ingestaAutomatica();

    }

}
