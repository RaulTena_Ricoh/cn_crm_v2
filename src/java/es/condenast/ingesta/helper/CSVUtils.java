/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.ingesta.helper;

import es.condenast.ingesta.entities.Log;
import es.condenast.ingesta.entities.LogPlano;
import es.condenast.ingesta.entities.LogRegla;
import es.condenast.ingesta.entities.LogTransformacion;
import es.condenast.ingesta.entities.LogValidacion;
import es.condenast.ingesta.entities.LogValidacionDetalle;
import es.condenast.ingesta.entities.LogWarning;
import java.util.ArrayList;

/**
 *
 * @author Javier.Castro
 */
public class CSVUtils {

    private static final String SEPARADOR = ";";
    
    public static String convertCSV(ArrayList<Log> logs) {
        StringBuilder csv = new StringBuilder();
        
        // Cabecera
         csv.append(cabeceraCSV()).append("\n");

        for (Log log : logs) {

            LogPlano plano;
            // Reglas
            if (log.getReglas() != null) {
                for (LogRegla regla : log.getReglas()) {
                    plano = new LogPlano();
                    plano.log = log;
                    plano.regla = regla;
                    csv.append(plano.toCSV()).append("\n");
                }
            }

            // Transformacion
            if (log.getTranformaciones() != null) {
                for (LogTransformacion transformacion : log.getTranformaciones()) {
                    plano = new LogPlano();
                    plano.log = log;
                    plano.transformacion = transformacion;
                    csv.append(plano.toCSV()).append("\n");
                }
            }

            // Validacion
            if (log.getValidaciones() != null) {
                for (LogValidacion validacion : log.getValidaciones()) {
                    plano = new LogPlano();
                    plano.log = log;
                    plano.validacion = validacion;
                    csv.append(plano.toCSV()).append("\n");
                }
            }

            // Validacion Detalle
            if (log.getValidacionesDetalle() != null) {
                for (LogValidacionDetalle detalle : log.getValidacionesDetalle()) {
                    plano = new LogPlano();
                    plano.log = log;
                    plano.detalle = detalle;
                    csv.append(plano.toCSV()).append("\n");
                }
            }

            // Warning
            if (log.getWarnings() != null) {
                for (LogWarning warning : log.getWarnings()) {
                    plano = new LogPlano();
                    plano.log = log;
                    plano.warning = warning;
                    csv.append(plano.toCSV()).append("\n");
                }
            }

        }

        return csv.toString();
    }

    public static String cabeceraCSV() {
        StringBuilder cabecera = new StringBuilder();
        cabecera.append("IdPool").append(SEPARADOR)
                .append("Fuente").append(SEPARADOR)
                .append("FuenteTipo").append(SEPARADOR)
                .append("Fichero").append(SEPARADOR)
                .append("Estado").append(SEPARADOR)
                .append("InicioPool").append(SEPARADOR)
                .append("LoadPool").append(SEPARADOR)
                .append("FinPool").append(SEPARADOR)
                .append("Lineas").append(SEPARADOR)
                .append("LineasOk").append(SEPARADOR)
                .append("LineasError").append(SEPARADOR)
                .append("DuracionPool").append(SEPARADOR)
                .append("DuracionLoad").append(SEPARADOR)
                .append("IdPool2").append(SEPARADOR)
                .append("idRegla").append(SEPARADOR)
                .append("regla").append(SEPARADOR)
                .append("inicio").append(SEPARADOR)
                .append("fin").append(SEPARADOR)
                .append("duracion").append(SEPARADOR)
                .append("idPool3").append(SEPARADOR)
                .append("idTransformacion").append(SEPARADOR)
                .append("transformacion").append(SEPARADOR)
                .append("inicio4").append(SEPARADOR)
                .append("fin5").append(SEPARADOR)
                .append("duracion6").append(SEPARADOR)
                .append("idPool7").append(SEPARADOR)
                .append("idValidacion").append(SEPARADOR)
                .append("validacion").append(SEPARADOR)
                .append("fecha").append(SEPARADOR)
                .append("lineasOk8").append(SEPARADOR)
                .append("lineasError9").append(SEPARADOR)
                .append("idPool10").append(SEPARADOR)
                .append("idValidacion11").append(SEPARADOR)
                .append("validacion12").append(SEPARADOR)
                .append("numLinea").append(SEPARADOR)
                .append("valor").append(SEPARADOR)
                .append("level").append(SEPARADOR)
                .append("code").append(SEPARADOR)
                .append("message");

        return cabecera.toString();
    }

}
