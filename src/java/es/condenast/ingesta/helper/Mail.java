/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.ingesta.helper;

import es.condenast.borrado.ProcesoBorrado;
import es.condenast.ingesta.Proceso;
import es.condenast.ingesta.entities.Log;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Mail {

//    @Resource(name = "mailSession") 
//    private Session mailSession;
    private static final String MAIL_SMTP_HOST = "mail.smtp.host";
    private static final String MAIL_SMTP_PORT = "mail.smtp.port";
    private static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
    private static final String MAIL_SMTP_STARTLS_ENABLE = "mail.smtp.starttls.enable";
    private static final String MAIL_SMTP_USER = "mail.user";
    private static final String MAIL_SMTP_PASSWORD = "mail.password";
    private static final String MAIL_TO_ADDRESS = "mail.toAddress";
    private static final String MAIL_SUBJECT_MANUAL = "mail.subject.manual";
    private static final String MAIL_SUBJECT_AUTOMATICO = "mail.subject.automatico";
    private static final String MAIL_SUBJECT_MASTER = "mail.subject.master";
    private static final String MAIL_SUBJECT_AVISO = "mail.subject.borrado.aviso";
    private static final String MAIL_SUBJECT_BORRADO = "mail.subject.borrado.ejecucion";
    private static final String MAIL_MESSAGE_MANUAL = "mail.message.manual";
    private static final String MAIL_MESSAGE_AUTOMATICO = "mail.message.automatico";
    private static final String MAIL_MESSAGE_MASTER = "mail.message.master";
    private static final String MAIL_MESSAGE_AVISO = "mail.message.borrado.aviso";
    private static final String MAIL_MESSAGE_BORRADO = "mail.message.borrado.ejecucion";

    private static final DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

    static final Properties properties = new Properties();
    static final String userName;
    static final String password;
    static final String toAddress;
    static final String subject;
    static final String message;

    static {

        userName = ResourceBundle.getBundle("resources/Configuration").getString(MAIL_SMTP_USER);
        password = ResourceBundle.getBundle("resources/Configuration").getString(MAIL_SMTP_PASSWORD);

        toAddress = ResourceBundle.getBundle("resources/Configuration").getString(MAIL_TO_ADDRESS);

        if (Proceso.getTipoIngesta() == Proceso.AUTOMATICA) {
            subject = ResourceBundle.getBundle("resources/Configuration").getString(MAIL_SUBJECT_AUTOMATICO);
            message = ResourceBundle.getBundle("resources/Configuration").getString(MAIL_MESSAGE_AUTOMATICO);
        } else if (Proceso.getTipoIngesta() == Proceso.MANUAL) {
            subject = ResourceBundle.getBundle("resources/Configuration").getString(MAIL_SUBJECT_MANUAL);
            message = ResourceBundle.getBundle("resources/Configuration").getString(MAIL_MESSAGE_MANUAL);
        } else {
            subject = ResourceBundle.getBundle("resources/Configuration").getString(MAIL_SUBJECT_MASTER);
            message = ResourceBundle.getBundle("resources/Configuration").getString(MAIL_MESSAGE_MASTER);
        }

        properties.put(MAIL_SMTP_HOST, ResourceBundle.getBundle("resources/Configuration").getString(MAIL_SMTP_HOST));
        properties.put(MAIL_SMTP_PORT, ResourceBundle.getBundle("resources/Configuration").getString(MAIL_SMTP_PORT));
        properties.put(MAIL_SMTP_AUTH, ResourceBundle.getBundle("resources/Configuration").getString(MAIL_SMTP_AUTH));
        properties.put(MAIL_SMTP_STARTLS_ENABLE, ResourceBundle.getBundle("resources/Configuration").getString(MAIL_SMTP_STARTLS_ENABLE));
        properties.put(MAIL_SMTP_USER, userName);
        properties.put(MAIL_SMTP_PASSWORD, password);
    }

    public static void sendEmailMaster() throws MessagingException {
        
        Session session = Session.getInstance(properties);

        Message msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject(subject.replace("%1%", df.format(new Date())));
        msg.setSentDate(new Date());
        
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(message.replace("%1%", df.format(new Date())), "text/plain");
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        
        msg.setContent(multipart);

        Transport.send(msg);
    }
    
    public static void sendEmailWithAttachments(String[] attachFiles, ArrayList<Log> logs) throws MessagingException {

        Session session = Session.getInstance(properties);

        Message msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject(subject.replace("%1%", df.format(new Date())));
        msg.setSentDate(new Date());

        MimeBodyPart messageBodyPart = new MimeBodyPart();

        String mensaje = messageBody(logs);

        messageBodyPart.setContent(mensaje, "text/plain");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);

        if (attachFiles != null && attachFiles.length > 0) {
            for (String filePath : attachFiles) {
                MimeBodyPart attachPart = new MimeBodyPart();

                try {
                    attachPart.attachFile(filePath);
                } catch (IOException ex) {
                    System.out.println("Error al enviar correos: " + ex.toString());
                }

                multipart.addBodyPart(attachPart);
            }
        }
        msg.setContent(multipart);

        System.out.println(mensaje);

        Transport.send(msg);
    }

    public static void sendWarningDeleteEmail(List<String> inactiveUsers, String fechaBorrado, List<String> sToAddresses) throws MessagingException {
    
        Session session = Session.getInstance(properties);

        Message msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = new InternetAddress[sToAddresses.size()];
        for(int i = 0; i < sToAddresses.size(); i++) {
            toAddresses[i] = new InternetAddress(sToAddresses.get(i));
        }
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject(subject.replace("%1%", String.valueOf(inactiveUsers.size()))
                              .replace("%2%", fechaBorrado));
        msg.setSentDate(new Date());
        
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        StringBuilder mensaje = new StringBuilder();
        mensaje.append(message);
        mensaje.append("\n\n");
        for(String registro : inactiveUsers) {
            mensaje.append(registro);
            mensaje.append("\n");
        }
        messageBodyPart.setContent(mensaje.toString(), "text/plain");
        
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        msg.setContent(multipart);
        
        System.out.println(mensaje);

        Transport.send(msg);
    }
    
    private static String messageBody(ArrayList<Log> logs) {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append(message.replace("%1%", df.format(new Date())));

        mensaje.append("\n\n");

        for (Log log : logs) {
            mensaje.append(log.getFuente());
            mensaje.append("\t");
            mensaje.append(log.getFichero().replace(FileUtils.TMP_DIR, ""));
            mensaje.append("\t");
            mensaje.append(log.getEstado());
            mensaje.append("\n");
        }

        return mensaje.toString();
    }

}
