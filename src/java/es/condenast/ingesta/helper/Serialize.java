/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.ingesta.helper;

import com.thoughtworks.xstream.XStream;
import java.util.ArrayList;

/**
 *
 * @author Javier.Castro
 * @param <T>
 */
public class Serialize<T> {

    public T deserializar(String xml) {
        XStream stream = new XStream();
        return (T) stream.fromXML(xml);
    }

    public String serializar(T object) throws Exception {
        XStream stream = new XStream();
        String xml = stream.toXML(object);
        return xml;
    }

    public String SerializarList(ArrayList<T> objeto) throws Exception {
        if (objeto == null) {
            return null;
        } else {
            String serial = "<Log>";
            for (T item : objeto) {
                serial += serializar(item);
            }
            return serial+"</Log>";
        }
    }
}
