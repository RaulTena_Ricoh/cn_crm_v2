/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.ingesta.helper;

import java.io.File;

import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ResourceBundle;

/**
 *
 * @author Javier.Castro
 */
public class FileUtils {

   
    public static final String TMP_DIR = "C:\\\\cn\\\\";
    //public static final String TMP_DIR = System.getProperty("java.io.tmpdir");
    public static final String LOG_DIR = ResourceBundle.getBundle("resources/Configuration").getString("log.dir");
    public static final String FILE_SEPARATOR = System.getProperty("file.separator");

    public static void move(String source, String target) throws IOException {
        Path FROM = Paths.get(source);
        Path TO = Paths.get(target);
        CopyOption[] options = new CopyOption[]{
            StandardCopyOption.REPLACE_EXISTING
        };
        Files.move(FROM, TO, options);
    }

    public static void delete(String file) throws IOException {
        Path FILE = Paths.get(file);
        Files.delete(FILE);
    }

    public static File createDataFile(int tipo, String datosFichero) throws IOException {
        Path path = Paths.get(LOG_DIR + FILE_SEPARATOR + ((tipo == Zip.CSV) ? Zip.FILE_NAME_ITEM_CSV : Zip.FILE_NAME_ITEM_XML));
        deleteIfExists(path);
        if (tipo == Zip.CSV) {
            return Files.write(path, datosFichero.getBytes("ISO-8859-1")).toFile();
        } else {
            return Files.write(path, datosFichero.getBytes()).toFile();
        }
    }

    public static void deleteIfExists(Path path) throws IOException {
        if (Files.exists(path)) {
            Files.delete(path);
        }
    }

    public static boolean exists(String file) {
        Path FILE = Paths.get(file);
        return Files.exists(FILE);
    }

    public static File createTmpFile(String name) throws IOException {
        Path target = Paths.get(LOG_DIR + FILE_SEPARATOR + name);
        deleteIfExists(target);
        return Files.createFile(target).toFile();

    }

    public static File createZipFile(String datosFichero, int tipo) throws IOException {
        File file = createDataFile(tipo, datosFichero);
        return Zip.createZipFile(file, tipo);
    }
}
