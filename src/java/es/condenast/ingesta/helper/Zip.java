package es.condenast.ingesta.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
public class Zip {
    
    private static final String FILE_NAME_ZIP = "log";
    public static final String FILE_NAME_ITEM_CSV = "log.csv";
    public static final String FILE_NAME_ITEM_XML = "log.xml";
    public static final int CSV = 0;
    public static final int XML = 1;
    
    public static File createZipFile(File fichero, int tipo) throws FileNotFoundException, IOException {
        
        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        File ficheroZip = FileUtils.createTmpFile(FILE_NAME_ZIP + "-" + df.format(new Date()) + ".zip");
        
        byte[] buffer = new byte[1024];
        
        FileOutputStream fos = new FileOutputStream(ficheroZip);
        ZipOutputStream zos = new ZipOutputStream(fos);
        ZipEntry ze = new ZipEntry((tipo == CSV) ? FILE_NAME_ITEM_CSV : FILE_NAME_ITEM_XML);
        zos.putNextEntry(ze);
        FileInputStream in = new FileInputStream(fichero);
        
        int len;
        while ((len = in.read(buffer)) > 0) {
            zos.write(buffer, 0, len);
        }
        
        in.close();
        zos.closeEntry();
        zos.close();
        
        return ficheroZip;
    }
    
}
