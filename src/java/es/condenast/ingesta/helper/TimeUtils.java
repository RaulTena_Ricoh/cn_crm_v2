/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.condenast.ingesta.helper;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Javier.Castro
 */
public class TimeUtils {

    public static String convertDateDuration(Date inicio, Date fin) {
        if (inicio == null || fin == null){
            return null;
        }
        long diff = fin.getTime() - inicio.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        Date resultado = new Date();
        resultado.setSeconds((int) diffSeconds);
        resultado.setMinutes((int) diffMinutes);
        resultado.setHours((int) diffHours);

        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");

        return df.format(resultado);
    }

    public static String convertUTC2(Date fecha) {
        String resultado = "";
        SimpleDateFormat dateFormatLocal = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if (fecha != null){
            resultado = dateFormatLocal.format(fecha);
        }
        return resultado;
    }

}
