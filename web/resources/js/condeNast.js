// Actualizar ruta automaticamente 
// en Fuentes de Datos
function actualizarRutas(ruta, suf_ok, suf_error) {
    var procesado = document.getElementById('form:procesado');
    var error = document.getElementById('form:error');
    if (ruta.length > 0) {
        procesado.value = ruta + suf_ok;
        error.value = ruta + suf_error;
    } else {
        procesado.value = "";
        error.value = "";
    }
}

//////////////////////////////
// Tipo de Fuentes de Datos //
//////////////////////////////
//
// Descripcion de origenes en picklist
function rebindClicks() {
    $('#formId').on('click', '.ui-picklist-target li', function() {
        $('[id="formId:messagesSource"]').hide();
        var nombre = $(this).attr('data-item-value');
        descriptionDestino([{name: 'param', value: nombre}]);
    });

    $('#formId').on('click', '.ui-picklist-source li', function() {
        $('[id="formId:messagesTarget"]').hide();
        var nombre = $(this).attr('data-item-value');
        descriptionOrigen([{name: 'param', value: nombre}]);
    });
}

//////////////////////
// Fuentes de Datos //
//////////////////////

function showDescription() {
    var idOrigen = $("select[name='formId:valorBBDD'] option:selected").val();
    descriptionOrigen([{name: 'param', value: idOrigen}]);
}

// Actualizar origenes al cambiar Fuente tipo
function changeFuenteTipo() {
    var idFuenteTipo = $("select[name='formId:idFuenteTipo'] option:selected").val();
    changeFuenteTipoJSF([{name: 'param', value: idFuenteTipo}]);
}


/////////////////
// UsuariosCRM //
/////////////////

// Mostrar filas detalle al cambiar perfil
function changePerfil(perfil) {
    $('[id="formId:divNumFilasDetalle2"]').hide();
    var idPerfil = $("select[name='formId:idPerfil'] option:selected").val();
    changePerfilJSF([{name: 'param', value: idPerfil}]);
    if (idPerfil == perfil) {
        $('[id="formId:divNumFilasDetalle"]').show();
    } else {
        $('[id="formId:divNumFilasDetalle"]').hide();
    }
}

// Actualizar textbox filas detalle al cambiar perfil
function changePerfilComplete() {
    $('[id="formId:numeroFilasDetalle"]').val($('[id="formId:hiddenNumeroFilasDetalle"]').text());
}

